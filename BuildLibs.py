"""
Simple alternative to make for building python librariries from swig or other sources
"""


import sys, os
from subprocess import run, Popen, PIPE
PyVer   =  str(sys.version_info.major) + "." + str(sys.version_info.minor) 
PyConf = "pythonVER-config".replace("VER", PyVer)
# python-dev utility to obtain correct flags for compile/link

CC  = ["gcc","-c", "-fPIC"]
LD  = ["gcc", "-shared"]


StartDir = os.getcwd()

Debug = 2 # 0:nothing, 1:stderr, 2:stderr+stdout 3: as 2 but with "Wall" flag set on compiler
DebugFP = sys.stderr


def Report(cmd, out, errs, retcode):

    if retcode >0:
        print("FAILED:")
        print("\tcmd:", cmd, sep="\n\t",end="\n\n",file=DebugFP)
        print("\treturn:", retcode, file=DebugFP)
        print("\terrors:", errs, sep="\n\t",end="\n\n",file=DebugFP)
        print("\toutput:", out, sep="\n\t",end="\n\n\n",file=DebugFP)

        raise OSError(retcode)

    if Debug>0:
        if type(cmd)==list:
            cmd = " ".join(cmd)

        print("cmd:", cmd, sep="\n",end="\n\n",file=DebugFP)
        if len(errs)>0:
            print("errors:", errs, sep="\n",end="\n\n",file=DebugFP)
        if Debug>1:
            print("output:", out, sep="\n",end="\n\n",file=DebugFP)




def Exec(argv):
    """ execute argv in a separate process, report the outcome and return errocode and output"""
    #p = Popen(argv, stdout=PIPE, stderr=PIPE)
    p = run(argv, stdout=PIPE, stderr=PIPE)
    errs = p.stderr.decode().strip()
    out = p.stdout.decode().strip()
    retcode = p.returncode
    Report(argv, out, errs, retcode)
    return out,errs


CFlags  = Exec([PyConf, "--cflags"])[0].split()
# Execute PyConf to obtain compiler flags
if Debug <3:
    CFlags.remove("-Wall")
    # reduce compiler warnings as indicated by Debug

LFlags  = Exec([PyConf,"--ldflags"])[0].split()
# Execute PyConf to get linker flags

SWFlags = ["-python", "-py3"]
# Flags to pass to swig


def Swig(swflags, swfile):

    swig = ["swig"]
    for args in SWFlags, swflags, [swfile]:
        swig.extend(args)
    Exec(swig)


def Compile(cflags, cfiles, Compiler):


    for args in CFlags, cflags, cfiles:
        Compiler.extend(args)

    Exec(Compiler)



def Link(lflags, obfiles, sofile):

    targ = ["-o", sofile]
    ld = LD[:]
    for args in obfiles, LFlags, lflags,  targ:
        ld.extend(args)
    Exec(ld)



def BuildLibs(Path=None): #TODO add optional args for cc and ld flags

    LastDir = os.getcwd()

    if not Path is None:
        os.chdir(Path)
        BuildLibs()
        os.chdir(LastDir)
        return

    print("Building", Path)

    locs = {} # dictionary of lcal variables defined in BuildInfo
    exec(open("BuildInfo").read(), globals(), locs)

    if "Dirs" in locs: # recurse into any directories defined in BuildInfo
        for d in locs["Dirs"]:
            try:
                print ("Building", d, file=sys.stderr)
                BuildLibs(d)
            except:
                print ("Build failed for", d, file=sys.stderr)


    if "SwigFile" in locs:  # execute swig if swigfile in BuildInfo
        Swig(locs["SwigFlags"], locs["SwigFile"])

    if "CFiles" in locs:   # complile c files as defined in BuildInfo
        cfiles = locs["CFiles"]
        if "Compiler" in locs:
            Compiler = [locs["Compiler"]]
        else:
            Compiler = CC[:]

        Compile(locs["CFlags"], cfiles, Compiler)

    if "SOFile" in locs: # link object files to generate teh so library
        if "OFiles" in locs:
            obfiles = locs["OFiles"]
        else:
            obfiles = [f.replace(".c", ".o") for f in cfiles]
        Link(locs["LFlags"], obfiles, locs["SOFile"])





if __name__ == "__main__":
    if len(sys.argv)  == 2:
       path = sys.argv[1]
    else:
       path = None
    BuildLibs(path)
