  

from ScrumPy.Util import DynMatrix, Sci, Tree

def Test():

    delta = DynMatrix.matrix(rnames="A B C D".split(), cnames="A B C D".split(), Conv=float)

    delta[0] = [0,1,1,10]
    delta[1] = [1,0,1,10]
    delta[2] = [1,1,0,10]
    delta[3] = [10,10,10,0]
    #mreturn FindNearestRows(delta)
    return Tree.FromDeltaMtx(delta)

def Test1():

    delta = DynMatrix.matrix(rnames="A B C D".split(), cnames="A B C D".split(), Conv=float)

    delta[0] = [0,1,1,10]
    delta[1] = [1,0,1,10]
    delta[2] = [1,1,0,10]
    delta[3] = [10,10,10,0]
    return delta.ToNJTree()


def Test2(m):

    k = m.sm.OrthNullSpace()
    d = k.RowDiffMtx(Sci.Q1Theta)
    return Tree.FromDeltaMtx(d)


def Test3(m):

    k = m.sm.OrthNullSpace()
    d = k.RowDiffMtx(Sci.Q1Theta)
    return d.ToNJTree()
        
    
        
                     

