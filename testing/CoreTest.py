import sys
sys.path.append('/usr/local/lib/python3/dist-packages')

from importlib import reload

import ScrumPy

from ScrumPy.Structural import Decompose, LPEMs
from ScrumPy.Data import DataSets


reload(Decompose)
reload(LPEMs)

m = ScrumPy.Model("../ScrumPy/models/calvin1.spy")

def Test():
    ds = DataSets.DataSet()
    m.Simulate(NSteps=250)
    m.FindSS()
    fd = m.GetFluxDic()
    ds.UpdateFromDic(fd)
    mo = m.ElModes()
    dc = Decompose.Decompose(mo,ds)
    dc.Transpose()

    lpe = LPEMs.LPEMs(m,fd)
    return mo, dc, lpe

