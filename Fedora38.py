import sys


PyVer   =  sys.version[:4]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary
PyDevPkg = "python3-devel" # + PyVer +"-dev"      # python development package .

GccVer = ""  # gcc version, leave empty for the distribution default


Packages=[
    "gcc"+GccVer,
    "gcc-c++"+GccVer,
    PyDevPkg,
    "gmp-c++",   # gnu multiple precision library - development package
    "gmp-devel",
    
    "mpfr-devel", # check if we need these two
    "libmpc-devel",
    
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot",
    "python3-sympy",
    "python3-ply",
    "sundials-devel",
    "cminpack-devel",
    "glpk-devel", # NOTE: new for Py3 version
    "python3-idle", 
    "python3-numpy",
    "python3-scipy",
    # "python3-swiglpk",   ## MISSING -install via pip  ##
    "python3-gmpy2",
    
    "python3-pip",
    "python3-QtPy",  
    #"python3-lxml",
    "python3-libsbml" # read/write sbml 
]

PipPkgs = ["swiglpk"]

InstallCommand="dnf -y install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
