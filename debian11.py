import sys


PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary
PyDevPkg = "python" + PyVer +"-dev"                # python development package .

GccVer = ""  # gcc version, leave empty for the distribution default


Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package
    # TODO: verify correct package name
    
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-qt",
    "python3-sympy",
    "python3-ply",
    "libsundials-cvode4", 
    "libsundials-dev",
    "libcminpack-dev",
    "libglpk-dev", # NOTE: new for Py3 version
    "idle-python"+PyVer, 
    "python3-numpy",
    "python3-scipy",
    "python3-swiglpk", 
    "python3-gmpy2",
    "python3-tk",     # ditto
    
    "python3-pip",
    "python3-qtpy",
    "python3-lxml",
    #"python-six",
    #"python3-pyqt4.qtopengl", # pre-req for ete3
    "python3-sbml5" # read/write sbml 
]

PipPkgs = []

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
