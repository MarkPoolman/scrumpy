import sys

PyVer   =  str(sys.version_info.major) + "." + str(sys.version_info.minor)  
PyCom = sys.executable  # binary
PyDevPkg = "python" + PyVer +"-dev"                # python development package .


GccVer = ""  # gcc version, leave empty for the distribution default


Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package  
    "swig3.0",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-qt",
    "python3-sympy",
    "python3-ply",
    "libsundials-cvode4", 
    "libsundials-dev",
    "libcminpack-dev",
    "libmpfr-dev", # should be pulled in by gmpy, but it seems not
    "libmpc-dev",  # ditto
    "libglpk-dev", 
    "idle-python"+PyVer, 
    "python3-numpy",
    "python3-scipy", 
    "python3-gmpy2",
    "python3-qtpy",
    "python3-pyqtgraph",
    
    "python3-pyqt5",
    "python3-pyqt5.qtsvg",
     "python3-pyqt5.qtopengl",
    "python3-swiglpk",
    "python3-sbml5", # read/write sbml 
   
]

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
