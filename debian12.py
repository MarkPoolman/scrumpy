import sys


PyVer   =  sys.version[:3]     # python version
PyPfx   =  sys.exec_prefix     # path to binary
PyCom = "python"+PyVer   # binary
PyDevPkg = "python" + PyVer +"-dev"                # python development package .

GccVer = ""  # gcc version, leave empty for the distribution default


#
## add python3-qtpy python3-pyqtgraph
#

Packages=[
    "gcc"+GccVer,
    "g++"+GccVer,
    PyDevPkg,
    "libgmp3-dev",   # gnu multiple precision library - development package
    "libmpfr-dev",
    "libmpc-dev",
    
    "swig",          # this can vary  between distros, maybe need (e.g.) swig-python as well
    "gnuplot-qt",
    "python3-sympy",
    "python3-ply",
    #"libsundials-cvode4", 
    "libsundials-dev",
    "libcminpack-dev",
    "libglpk-dev",
    "idle", 
    "python3-numpy",
    "python3-scipy",
    "python3-swiglpk", 
    "python3-gmpy2",
    "python3-tk",     # ditto
    
    "python3-pip",  # why need this ?
    "python3-qtpy",
    "python3-pyqtgraph",
    "python3-lxml",
    "python3-sbml5" # read/write sbml 
]

#PipPkgs = ["ete3"]

InstallCommand="apt-get --yes install "

LinkFlags = "-shared -Wl,--copy-dt-needed-entries,--no-as-needed"
# for gcc <= 4.4 LinkFlags = "-shared"
