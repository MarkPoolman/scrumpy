#!/usr/bin/python3


#
## TODO: check for existing installation
#

# where to put the executable and ScrumPy package files
ExecPath = "/usr/local/bin/ScrumPy3"
InstallPath = "/usr/local/lib/python3/dist-packages/ScrumPy"


import os, sys, shutil, compileall

import install_conf # os/distro dependent info and tools

if sys.version[0] != "3":
    print("This version of ScrumPy needs python 3.x, giving up!")
    sys.exit(1)


# uncomment one or the other of these two lines
#ScrumPyExec = os.sep.join((InstallPath, "UI", "ScrumPyIdle"))  # old idle subclassed editor
ScrumPyExec = os.sep.join((InstallPath, "UI", "ScrumPy32"))     # new tabbed tk editor

""" trivially modified shutil.copytree() """
def Mirror(src, dst, symlinks=False, mode=0o755):

    names = [name for name in os.listdir(src) if name != "__pycache__"]
    os.makedirs(dst, mode=mode, exist_ok=True)
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                Mirror(srcname, dstname, symlinks,mode)
            else:
                shutil.copy2(srcname, dstname)
                os.chmod(dstname, mode)
         
        except OSError as why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
  

    return errors


def InstallPrereqs():
    
    fails = install_conf.InstallPackages()
    fails += install_conf.InstallPip()
    nf = len(fails)
    if nf != 0:
        print("The following package(s) failed to install\n" + "\n".join(fails))
        
    return nf==0



def MakeExec():

    if os.path.exists(ExecPath):
        os.unlink(ExecPath)

    os.symlink( ScrumPyExec, ExecPath)


def Main():

    OK = True

    if "NOPKG" in sys.argv:
        print ("Skipping installation of prerequisite packages")
        
    else:
        if not InstallPrereqs():
            print("\nFailed to install prerequisite packages\nProceed recklessly anyway ?\n")
            if input("Proceed ? ").upper() != "Y":
                print("\nGiving up\n")
                OK = False # might still partially work if you know what you're doing
    

    if OK:
        import BuildLibs  # module to build the swig interfaces and shared objects
        #requires prereqs first
        Mirror("./ScrumPy",InstallPath)
        BuildLibs.BuildLibs(InstallPath)
        compileall.compile_dir(InstallPath)
        
        try:      
            MakeExec()
        except:
            print("couldn't write to ", ExecPath, "check permissions etc.")




if __name__=="__main__":

    Main()

