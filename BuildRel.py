#!/usr/bin/python



import os
import datetime
import git

def TimeStamp():
    
    today = datetime.datetime.today()
    todaystr  = today.isoformat("-")              # e.g. '2023-04-06-13:08:18.086989'
    todaylist = todaystr.split(":")[0].split("-") # ===> [2023, 04, 06, 13]
    todaylist.reverse()
    todaylist[-1] = todaylist[-1][2:]             # ===> [13, 06, 04, 23]
    stamp = "".join(todaylist)                    # ===> '13060423'
    human = "/".join(todaylist[1:])               # ===> '06/04/23'
    
    return stamp, human
    

def BuildRel():

    repo = git.repo.Repo()
    
    gstamp = str(list(repo.iter_commits())[0])
    tstamp, hdate = TimeStamp()

    repo.clone(tstamp)
    CleanUp(tstamp,hdate,gstamp)
    BuildTGZ(tstamp)
   


def CleanUp(tstamp,hdate,gstamp):

    cwd = os.getcwd()
    os.chdir(tstamp)

    # may want to do more here later, so UpdateBasicInfo
    # is a func of its own
    UpdateBasicInfo(hdate, gstamp)

    os.chdir(cwd)


def UpdateBasicInfo(hdate, gstamp):
    basicinfo = open("ScrumPy/UI/BasicInfo.py").read()
    newbasic = basicinfo.replace("DATE", hdate).replace("COMMIT",gstamp)

    out = open("ScrumPy/UI/BasicInfo.py", "w")
    out.write(newbasic)
    out.close()

    

def BuildTGZ(src):

    # src is both the name of directory and the timestamp

    tarcmd  = "/bin/tar"
    tarflgs = "--exclude-vcs"

    outfile = "-".join(("ScrumPy",src))+".tgz"
    
    cmd = " ".join((tarcmd, "zcf", outfile, tarflgs, src))
    os.system(cmd)


if __name__ == "__main__":
    BuildRel()
