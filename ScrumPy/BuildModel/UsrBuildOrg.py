



from ScrumPy.Bioinf import PyoCyc
from . import BuildOrg, CompartmentDic
# CompartmentDic contains the class that will be populated by CompDic

    
orgdb = None
CompDic = CompartmentDic.CompartmentDic()

def ShowCorrec():
    BuildOrg.CorrecMod.Show()

def HideCorrec():
    BuildOrg.CorrecMod.Hide()

    
def Init(OrgName, DBUpdateName=None, CompartmentInfo={}, NoCompID=None):
        """ Pre: OrgName - name of a PyoCyc organism
                    DBUpdateName - name of a biocyc .dat file containig additional or modified compoind information
                    CompartmentDic - dictionary mapping reactions to compartments
                    NoCompID - Name of the compartment whose reacs and mets will NOT be given compartment suffices
            post: other functions in theis module are usable"""
        

        global orgdb    
        if orgdb == None:
                orgdb = PyoCyc.Organism(data=OrgName)

        global CompDic  # TODO: eliminate globals (?)
        CompDic.update(CompartmentInfo)

        BuildOrg.NoCompID=NoCompID

        if DBUpdateName !=None:
                updatedb = PyoCyc.Compound.DB(".", DBUpdateName)
                orgdb.updates = updatedb
                compdb = orgdb.dbs["Compound"]
                for k in updatedb:
                        rec= updatedb[k]
                        if k in compdb:
                                compdb[k].update(rec)
                        else:
                                orgdb[k] = compdb[k] = rec
    


  
def BuildModel(Reacs=None):
    BuildOrg.BuildModel(orgdb, CompDic,Reacs)
                                

def RebuildModel(m, Reacs=None,Hide=True):

        m = BuildOrg.RebuildModel(m,orgdb,CompDic,Reacs)
        if Hide:
            m.Hide()
        return m


def UnBalReacs(m,Reacs=None):

    rv = {}

    if Reacs is None:
        Reacs = m.sm.cnames


    for reac in Reacs:
        imbal = orgdb.dbs["Compound"].AtomImbal(m.sm.InvolvedWith(reac))
        if len(imbal) >0:
            rv[reac] = imbal

    return rv
