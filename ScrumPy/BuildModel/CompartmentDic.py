

EnQuote = lambda s: '"'+s+'"'
IsQuote = lambda s: s[0] == s[-1] == '"'
DeQuote = lambda s: s[1:-1] if IsQuote(s) else s

MakeSfx = lambda s: "_"+s[:4].title()
## not a class member so that user can redefine on the fly


class CompartmentDic(dict):

    def __init__(self, CompDic={}, DefaultCompartment="Cytosol", DefaultTx="Transporters"):

        self[DefaultCompartment] = ""

        self.update(CompDic)
        self.DefComp = DefaultCompartment
        self.DefTx = DefaultTx
        self.SfxMap = dict(list(zip(list(self.keys()), list(map(MakeSfx, list(self.keys()))))))


    def Reac2Comps(self, Reac):
        
        rv = []
        for comp in self:
            if Reac in self[comp]:
                rv.append(comp)
        if rv == []:
            rv.append(self.DefComp)

        return rv


    def MapNames(self, names,comp):

        sfx =  self.SfxMap[comp] if comp in self.SfxMap else MakeSfx(comp)
        return [name+sfx for name in names]


    def AllReacs(self):
        """ list of all reacs in all compartments """

        rvd = {}
        for comp in list(self.values()):
            for reac in comp:
                rvd[reac] = 1
                
        return list(rvd.keys())
                
    

    

    

    
