import os, sys
import ScrumPy

from ScrumPy.Util import Set
from ScrumPy.Structural import StoMat

from ScrumPy.Bioinf import PyoCyc

import Unwanted, Substitutes # These must be in the cwd of the importing module
import importlib


##Corrections = "Corrections.spy"
##CorrecMod = ScrumPy.Model(Corrections)
##CorrecMod.Hide()

DirectionMap= {
    "IRREVERSIBLE-LEFT-TO-RIGHT" : StoMat.t_Irrev,
    "IRREVERSIBLE-RIGHT-TO-LEFT" : StoMat.t_BackIrrev,
    "PHYSIOL-LEFT-TO-RIGHT"      : StoMat.t_Irrev,
    "PHYSIOL-RIGHT-TO-LEFT"      : StoMat.t_BackIrrev,
    "LEFT-TO-RIGHT"              : StoMat.t_Rever,
    "RIGHT-TO-LEFT"              : StoMat.t_BackIrrev,
    "REVERSIBLE"                 : StoMat.t_Rever
}
DefaultDirec = "IRREVERSIBLE-LEFT-TO-RIGHT"
#NoCompID = None

ReacDirKey = PyoCyc.Tags.ReacDir
       

def RemovePipes(sm):
    sm.rnames  = [x.replace("|","") for x in sm.rnames]
    sm.Externs = [x.replace("|","") for x in sm.Externs]

##
##def RemoveBadMets(sm, Unwanted):
##
##    BadMatches = [met for met in sm.rnames if Unwanted.BadMetMatches(met)]
##
##    for met in Set.Intersect(Unwanted.Mets, sm.rnames)+BadMatches:
##                
##        for reac in sm.InvolvedWith(met):
##            sm.DelReac(reac)
##        try:
##            sm.DelRow(met)
##        except:
##            print(met, " not found", file=sys.stderr)
##            # this is a bug, Set.Intersect should ensure this doesn't happen
##
##
##
##def RemoveBadReacs(sm, Unwanted):
##    
##    BadMatches = [met for met in sm.cnames if Unwanted.BadReacMatches(met)]
##    for r in Set.Intersect(Unwanted.Reacs,sm.cnames)+BadMatches:
##        try:        sm.DelReac(r)
##        except: pass # lazy 


def AddCorrections(sm, CorrecMod):


    csm = CorrecMod.smx
    for c in Set.Intersect(csm.cnames, sm.cnames):
        
        revprop = csm.RevProps[c]
    
        sm.Delete(c)
        sm.NewReaction(c, csm.InvolvedWith(c), revprop)

  
def ReverseIsomerases(sm, CorrecMod):
    """ make isomerases in sm reversible """

    for c in sm.cnames:
        invw = sm.InvolvedWith(c)
        IsIso = list(map(abs, list(invw.values()))) == [1,1]                     
        if IsIso and c not in  CorrecMod.sm.cnames:
            sm.RevProps[c] = StoMat.t_Rever


def SubMetabolites(sm,Substitutes):
 
  
    subdic = Substitutes.MetNames

    for met in list(subdic.keys()):
       if met in sm.rnames[:]:
            row = sm[met]
            newmet = subdic[met]
            print("subst met", met, newmet)
            if newmet in sm.rnames:
                sm.AddRow(newmet, row)
            else:
                sm.NewRow(row,newmet)
          
            sm.DelRow(met)
     

def FixNads(sm): # TODO - re-write using proper sm.Add/DelReac()
 
    def AsNAD(metname):
        return metname.replace("(P)","")

    def AsNADP(metname):
        return metname.replace("(P)","P")

    def NADPRName(rname):
        return rname+'-(NADP)'

    def NADRName(rname):
        return rname+'-(NAD)'

    reacs = list(sm.InvolvedWith("NAD(P)H").keys())
    if len(reacs)>0:
        reacsd = {}
        revprops = {}

        for reac in reacs:
            reacsd[reac] = sm.InvolvedWith(reac)
            revprops[reac] = sm.RevProps[reac]
            sm.DelCol(reac)        
        
        sm.DelRow("NAD(P)H")
        sm.DelRow("NAD(P)")

        for reac in list(reacsd.keys()):
            reacnad  = NADRName(reac)
            sm.NewCol(name=reacnad)
            reacnadp = NADPRName(reac)
            sm.NewCol(name=reacnadp)
            
            stod = reacsd[reac]
            for met in list(stod.keys()):
                sm[AsNADP(met),reacnadp] = stod[met]
                sm[AsNAD(met),reacnad] = stod[met]
            
            sm.RevProps[reacnad] = revprops[reac]
            sm.RevProps[reacnadp] = revprops[reac]
            del sm.RevProps[reac]
     

def DelIsoforms(sm):

    for iso in sm.FindIsoforms():
        for reac in iso[1:]:
           sm.DelReac(reac)
    return

def GetQuoteDic(sm):

    rv = {}
    for name in sm.cnames+sm.rnames:
        if name[0] == '"':
            qname = name
            name = name[1:-1]
        else:
            qname = '"' + name + '"'
        rv[name] = qname
        rv[qname] = name
    return rv
    

 
def MakeCompNames(sm,comp,CompDic):

    cnames = CompDic.MapNames(sm.cnames,comp)
    NewOld = list(zip(cnames, sm.cnames))
    
    for new,old in NewOld:
        sm.RevProps[new] = sm.RevProps[old]
        del sm.RevProps[old]
        
    sm.cnames = cnames
    sm.rnames = CompDic.MapNames(sm.rnames,comp)

  




def MakeCore(m):

    Externs = m.sm.Externs

    orphans  = m.sm.OrphanMets()    
    while len(orphans) > 0:
        print("len(o)", len(orphans))
        reacd = {}
        for met in orphans:
            for reac in list(m.sm.InvolvedWith(met).keys()):
                reacd[reac] =1

        m.DelReactions(list(reacd.keys()))
        orphans  = m.sm.OrphanMets()



def BuildModules(ReacDB, CorrecMod, CompDic, Unwanted, Substitutes, Label1Comp=False):

    smdic = {}
    Transport = CompDic.DefTx
    smdic[Transport] = StoMat.StoMat(Conv=float)
    
    for comp in CompDic:
        smdic[comp] = StoMat.StoMat(Conv=float)

    
    for reac,reacrec in ReacDB.items(): 
        
        stod = reacrec.GetStod() #dict(reacrec.StoDict)
        if ReacDirKey in reacrec:
            direc = reacrec[ReacDirKey][0]  
        else:
            direc = DefaultDirec
        direc = DirectionMap[direc]

        if reacrec.IsTransporter():
            try:
                smdic[Transport].NewReaction(reac, stod,direc)
            except:
                print("Bad transporter", reac,": removing it") #TODO: don't let these get so far. Most likely non-numeric sto coeff
                smdic[Transport].DelCol(reac)
        else:
                        
            for comp in CompDic.Reac2Comps(reac):
                try:
                    smdic[comp].NewReaction(reac, stod,direc)
                except:
                    print("Bad reaction", reac,": removing it") #TODO: don't let these get so far. Most likely non-numeric sto coeff
                    smdic[comp].DelCol(reac)

    for comp in smdic:
        
        out = open(comp+".spy","w")
        print("\n\n#\n##\n### ", comp, "\n##\n#\n\n", file=out)
        sm = smdic[comp]
       
        RemovePipes(sm)
        #RemoveBadMets(sm, Unwanted)
        
        #RemoveBadReacs(sm,Unwanted)
        SubMetabolites(sm,Substitutes)
        
        AddCorrections(sm, CorrecMod)  
        ReverseIsomerases(sm, CorrecMod) 
        FixNads(sm)  
        DelIsoforms(sm)

        if len (CompDic) >1 or Label1Comp:
            MakeCompNames(sm,comp,CompDic)
            
        qdic = GetQuoteDic(sm)
        sm.ToScrumPy(out)# TODO: Include an informative comment in the header
        


    
