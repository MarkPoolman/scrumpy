import os

import ScrumPy
from ScrumPy.Bioinf import PyoCyc
from importlib import reload

from ScrumPy.Bioinf import PyoCyc

import ScrumPy

#from . import BuildOrg2 as BuildOrg
# CompartmentDic contains the class that will be populated by CompDic
from . import BuildOrg, CompartmentDic


class Builder:

    def __init__(self,
                 DBName,
                 DBPath=PyoCyc.DBBase.DefaultPath,
                 DBUpdateName=None,
                 Corrections="Corrections.spy",
                 CompartmentInfo={},
                 DefaultCompName="Cytosol",
                 DefaultTxName="Transporters",
                 UnwantedMod="Unwanted",
                 SubstMod="Substitutes",
                 TopLevelName="TopLevel.spy"):
        

        self.OrgDB = PyoCyc.Organism(DBName)
        self.DBUpdateName = DBUpdateName
        
        #self.UpdateOrgDB() #### see UpdateDB comment

        self.CorrecMod = ScrumPy.Model(Corrections)
        md = self.CorrecMod.md
        if len(md.Errors) == len(md.Warnings) == 0:
            self.CorrecMod.Hide()

        self.CompDic = CompDic = CompartmentDic.CompartmentDic(CompartmentInfo,DefaultCompName)
    
        exec("import %s as Unwanted" % UnwantedMod, {}, self.__dict__) # make the Unwanted and Substitutes modules attributes of self
        exec("import %s as Substitutes" % SubstMod, {}, self.__dict__)

        self.TopLevel=ScrumPy.Model(TopLevelName)


    def __del__(self):
        self.TopLevel.Close()
        self.CorrecMod.Close()


    def UpdateOrgDB(self):

        """ Assume the data file, DBUpdateName, is on a relative path to cwd,
        Update org db with the user specified extra records"""

        #
        ## Probably better not to, keep original pristine and use updated *copies* PRN
        ## Because the user can make arbitrary edits to the contents of self.DBUpdateName
        ## Use self.FreshMetDB instead.
        #

        if not self.DBUpdateName is None:
            
            self.UpdateDB  = udb = PyoCyc.Compound.DB(".", self.DBUpdateName)
            metDB = self.OrgDB.dbs[PyoCyc.Tags.Compound]
            reacDB = self.OrgDB.dbs[PyoCyc.Tags.Reac]
            
            for k in udb:
                rec= udb[k]
                if k in self.OrgDB:
                    metDB.update(rec)
                else:
                    self.OrgDB[k] = metDB[k] = rec

            for reaction in reacDB.values():
                for reactant in reaction.GetReactants():
                    if reactant in udb:
                        child = self.OrgDB[reactant]
                        if not hasattr(child, "Parents"):
                            child.Parents = []
                        child.RegisterParent(reaction)


    def FreshMetDB(self):
        """ Fresh copy of the metabolite DB including any user edits via self.UpDateDB """

        metdb = self.OrgDB.dbs[PyoCyc.Tags.Compound]
        rv = metdb.Copy()
        udb = PyoCyc.Compound.DB(".", self.DBUpdateName)
        for k,v in udb.items():
            if k in rv:
                rv[k].update(v)
            else:
                rv[k] = v

        return rv

    
    def GetReacDB(self):

        """ get a copy of the orginal reaction DB with reactions identified directly
            as unwanted (or indirectly via unwanted mets) aremoved
        """    
        
        rdb = self.OrgDB.dbs[PyoCyc.Tags.Reac].Copy(OrgDB=self.OrgDB)
        #rdb.__class__(rdb.Path, None,rdb.OrgDB) # ie a COPY of the orignal
        for reac in list(rdb.keys()):
            Delete = False
            if reac in self.Unwanted.Reacs or self.Unwanted.BadReacMatches(reac):
                Delete = True
            else:
                rec = rdb[reac]
                mets = rec[PyoCyc.Tags.Left] + rec[PyoCyc.Tags.Right]
                for met in mets:
                    if met in self.Unwanted.Mets or self.Unwanted.BadMetMatches(met):
                        Delete = True
            if Delete:
                del rdb[reac]
                
        return rdb
                    
                
       
      

    def _BuildModules(self):
        """ Generate the ScrumPy model modules """

        ReacDB = self.GetReacDB()

        BuildOrg.BuildModules(
            ReacDB,
            self.CorrecMod,
            self.CompDic,
            self.Unwanted,
            self.Substitutes
        )


    def ReloadPyMods(self):
        
        for mod in self.Substitutes,self.Unwanted:
            if os.path.exists(mod.__cached__):
                os.unlink(mod.__cached__)
                # cache doesn't always seem to update correctly
            reload(mod)
                          

    def BuildModel(self):
                            
        self.ReloadPyMods()
        self.CorrecMod.Reload()
        self.UpdateOrgDB()
        self._BuildModules()

        if self.TopLevel is not None: # user might have closed EdWin etc
            for mod in self.CompDic:
                FName  = mod+".spy"
                self.TopLevel.ui.ReRead(FName)
        self.TopLevel.Reload()


    def ShowModel(self):
        self.TopLevel.Edit()

    def HideModel(self):
        self.TopLevel.Hide()

    def ShowCorrecs(self):
        self.CorrecMod.Edit()

    def HideCorrecs(self):
        self.CorrecMod.Hide()


    def MetsNotInDB(self,db = None):
        """ set of internal metabolites not in DB
            (assume no compartment suffixes for now)  """

        if db is None:
            db = self.FreshMetDB()

        return set(self.TopLevel.sm.rnames) - db.keys()


    def MetsNoForm(self):
        """ set of internal metabolites in the model without empirical formulae
            Excluding MetsNotInDB """

        rv = set()

        sm = self.TopLevel.sm
        db = self.FreshMetDB()
        
        for met in set(sm.rnames) - self.MetsNotInDB(db):
            if PyoCyc.Tags.ChemForm not in db[met]:
                rv.add(met)
                
        return rv
                
        
    def UnbalReacs(self, Atoms={"C","H", "N", "O", "P", "S"}):
        '''Pre:True
        Post: return dictionary of imbalanced reactions'''

        #TODO: Check why not use self.OrgDB.AtomImbal() ?

        rv = {}
        db = self.FreshMetDB()
        sm = self.TopLevel.smx
        
        for reac in sm.cnames:
            stod  = sm.InvolvedWith(reac)
            imbal = db.AtomImbal(stod)
            if not Atoms.isdisjoint(imbal.keys()):
                rv[reac] = imbal

        return rv


    def SingleFluxCheck(self, Reac, ExcludeReacs=[], Flux=1, lp=None):
        """ pre: Reac not in ExcludeReacs """

        m = self.TopLevel
        if lp is None:
            lp = m.GetLP()
            lp.SetObjective(m.sm.cnames)
            
        FluxDic = dict(zip(ExcludeReacs, [0]*len(ExcludeReacs)))
        FluxDic[Reac] = Flux
        lp.SetFixedFlux(FluxDic)

        lp.Solve()
        return lp.GetPrimSol()


    def ClosedSingleFluxCheck(self, Reac):
        return self.SingleFluxCheck(Reac, [r for r in self.TopLevel.sm.cnames if r.lower().endswith("_tx")])

    def ATPConsistency(self, Reac="ATPase"):
        return self.ClosedSingleFluxCheck(Reac)

    def NADHConsistency(self, Reac="NADHOxid"):
        return self.ClosedSingleFluxCheck(Reac)

    def NADPHConsistency(self, Reac="NADPHOxid"):
        return self.ClosedSingleFluxCheck(Reac)


    def IndividualFluxChecks(self, Reacs, ExcludeReacs=[], Flux=1):
        
        rv = {}

        m = self.TopLevel
        lp = m.GetLP()
        lp.SetObjective(m.sm.cnames)

        for r in Reacs:
            rv[r] = self.SingleFluxCheck(r, ExcludeReacs,Flux,lp)
            lp.ClearFluxConstraint(r)
            
        return rv


    def CheckBiomass(self):
        """ pre: biomass exporters have the non-case senditive suffix "_bm_tx"
                 export fluxes are negative
        """

        txs = [reac for reac in self.TopLevel.sm.cnames if reac.lower().endswith("_bm_tx")]
        return self.IndividualFluxChecks(txs, Flux=-1)
        

        

        

        
    
    
        

    
   
##
##        
        


        
        
