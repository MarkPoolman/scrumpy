import re, os, sys

#
## TODO - rewrite using m.md only
#

from ..Util import  String, Kali

#TODO Get rid of the String module, this is the only module using it and only uses one function

libsbml = None

def ErrRep(msg):
    
    print(msg) # TODO put ErrRep in UI ? 




def FindLibSBML():
    for direc in "/usr/lib", "/usr/local/lib": 
        for r,d,f in os.walk(direc):
            if  "python3" in r and "libsbml" in d:
                return r


def ImportSBML():
    # locations of and access to sbml seem to vary with version and distro, so do our best to find it

    global libsbml
    OK = False

    try:
        import libsbml
        OK = True
    except:
        print ("Searching for libsbml...")
        path = FindLibSBML()
        if path is not None:
            sys.path.append(path)
            print ("... found it at", path)
            try:
                import libsbml
                OK = True
            except:
                pass

    if OK:
        if hasattr(libsbml,"libsbml"):
            libsbml = libsbml.libsbml
        elif hasattr(libsbml, "readSBML"):
            pass
        else:
            from libsbml import libsbml
                     
        print("Loaded libsbml")

try:
    ImportSBML()
except:
    print("SBML not available", file = sys.stderr)


def IsSBML(FName):

    if os.path.exists(FName):
        f = open(FName)
        f.readline()
        l2 = f.readline()
        f.close()
        if l2.startswith("<sbml"):
            return True
        
    return False

IdentRE  = re.compile(r'[a-zA-Z][\w]*')     # regex for spy and sbml ids
#TODO: get this from lexer.py

# The two intended public functions are SBML2Spy and Spy2SBML
# at the end of this file
#



#
## a set of low level helpers
## converting various sbml entities in to ScrumPy
#

def SBMLErr2Str(err):

    return " ".join([err.getMessage(), "(at line)", str(err.getLine())])


def GetIdents(string):
    """ list of unique ScrumPy identifiers in string """

    rvd = {}

    for id in IdentRE.findall(string):
        rvd[id] = 1

    return list(rvd.keys())


def GetEffectors(m, reac):
    """ list of metabolites that are effectors of reac in model m """

    rv = []

    if m.md.Directives["Structural"].IsDefault():
        ratelaw = m.md.Reactions[reac].Kin
        params = m.md.GetParamNames()
        reactants = list(m.sm.InvolvedWith(reac).keys())
        ids = GetIdents(ratelaw)
        rv = []
        for i in ids:
            if not (i in reactants or  i in params):
                rv.append(i)
    return rv


def SKinToSpy(r,klps={}):
    """ extract the ScrumPy format kinetic law from reaction r,
         saving any bound parameters in klps """

    rv = "~"

    if r.isSetKineticLaw():
        law = r.getKineticLaw()
        if law != None:
            formula = law.getFormula()
            if not(formula == "" or formula.isspace()):
                rv = formula
                for p in law.getListOfParameters():
                    klps[p.getId()] = p.getValue()

    return rv


def SidToSpy(sid):

    #rv = sid.getName() or
    rv = sid.getId()  # TODO: need an option to decide which to use

    #name = sid.getName()

    #if rv != name and name != "":
    #     rv += "_" + name
#TODO replace String.IsMatch with appropriate function from lexer.py
    # if not String.IsMatch(IdentRE, rv):
    rv =  String.EnQuote(rv) # always quote (Dipali's request)

    #print "SidToSpy",  rv

    return rv


def SpMapNames(slist):

    rv = {}
    for s in slist:
        rv[s.getId()] = SidToSpy(s)


    return rv



def SStoToSpy(mets,namedic,rname):
    """ convert list of Reactants or Products to a ScrumPy stoichiometry """

    defaultname ="_UnknownMetabolite"
    if rname[-1]=='"':
        rname = rname[:-1]
        defaultname += '"'
    rv = rname+defaultname

    if len(mets) >0:
        rv =""
        for met in mets[:-1]:
            # rv += str(met.getStoichiometry()) + " " + met.getSpecies() + " + "
            rv += str(met.getStoichiometry()) + " " + namedic[met.getSpecies()] + " + "


        met =  mets[-1]
        #rv += str(met.getStoichiometry()) + " " + met.getSpecies()
        rv += str(met.getStoichiometry()) + " " + namedic[met.getSpecies()]
    return rv


def SParamsToSpy(ps):
    """ ps is listOfParameters, return the ScrumPy initialisations for ps """

    rv = "\n# Parameters follow\n"
    for p in ps:
        rv += p.getId() + " = " + str(p.getValue()) + "\n"
    return rv


def SMetsToSpy(mets):
    """ list of species as a ScrumPy initialisation """

    rv = "\n#Initial metabolite values (including externals) follow\n"
    for met in mets:
        value = met.getInitialConcentration()
        if value != 0.0:  # avoid initialising metabolite values in structural models,
                          #(0.0 is default for kinetics anyway)
            rv += met.getId() + " = " + str(value) + "\n"
    return rv


def SBoundrsToSpy(sps, SpNameDic):
    """ generate a ScrumPy "External()" directive from a list of species,
        ignoring those prefixed "x_" or "X_" """

    rv = ""
    exts = []

    for s in sps:
        if s.getBoundaryCondition():
            exts.append(SpNameDic[s.getId()])

    if len(exts) > 0:
        for ex in exts[:-1]:
            if not ex[:].upper() == "X_":  # ScrumPy will treat as external anyway, looks tidier
                rv += ex+", "
        return "External(" + rv + exts[-1] + ")\n"

    return rv




def SReacsToSpy(rs,klps,SpNameDic):
    """ convert a list of reactions to ScrumPy format,
        parameters bound to kinetic laws saved in klps """

    rv = ""
    for r in rs:

        rname = SidToSpy(r)
        rv += rname + ":\n    "

        rv += SStoToSpy(r.getListOfReactants(),SpNameDic, rname)
        if r.getReversible():
            rv += " <> "
        else:
            rv += " -> "

        rv += SStoToSpy(r.getListOfProducts(), SpNameDic, rname) + "\n    "
        rv += SKinToSpy(r,klps) + "\n\n"
    return rv





#
## a set of low level helpers
## converting various Scrumpy entities in to sbml equivalents
#


def SpyFNameToSBML(fname):

    extn = fname.split(".")[-1]
    extnu = extn.upper()
    if extnu == "SPY":
        fname = fname.replace(extn,"sbml")
    else:
        fname += ".spy"

    return fname


def SBMLFNameToSpy(fname):

    extn = fname.split(".")[-1]
    if extn.upper() in ("SBML", "XML"):
        fname = fname.replace(extn, "spy")
    else:
        fname += ".spy"

    return fname



def InitSBMLModel():
    """ return an empty sbml model with a single compartment """
##############
    rv = libsbml.Model(SBML_ns)
    c = libsbml.Compartment(SBML_ns)
    c.setName("DefaultCompartment")
    c.setVolume(1.0)
    c.setId("DefaultCompartment")
    rv.addCompartment(c)

    return rv

def InitSBMLModel2(m_sbml):
    """ return an empty sbml model with a single compartment """
##############

    c = m_sbml.createCompartment()
    c.setName("DefaultCompartment")
    c.setId("DefaultCompartment")
    c.setVolume(1.0)
    c.setConstant(True)
    #m_sbml.addCompartment(c)

    #return rv

def SetMets(m, m_sbml, met2spc):
    """ pre: m=ScrumPy.Model(*),m_sbml=InitSBMLModel(), met2spc={}, met2spcr={}
       post: metabolites in m created in m_sbml,
             met2spc and met2spcr map ScrumPy met names to SBML species and species refs """

    vsmall = 1e-7

    mets =  m.smx.rnames[:]
    mets.sort()   # not essential but keeps repeated save/load more consistent

    if m.md.Directives["Structural"].IsDefault():
        Initial = lambda met: m[met]
    else:
        Initial = lambda met: vsmall


    for met in mets:
        if String.IsMatch(IdentRE,met):
            id = met
        else:
            id = "met_" + str(mets.index(met))

        sp = m_sbml.createSpecies()
        sp.setId(id)
        sp.setName(met)
        sp.setCompartment("DefaultCompartment")
        sp.setHasOnlySubstanceUnits(False)

        extern = not met in m.sm.rnames  # ie external metabolite
        sp.setBoundaryCondition(extern)
        sp.setConstant(extern)

        sp.setHasOnlySubstanceUnits(False)
        ini = Initial(met)
        if ini == 0.0:
            ini = vsmall   # libSBML will ignore initial concs of zero

        sp.setInitialConcentration(ini)
        met2spc[met] = id







def SetReacs(m, m_sbml, reac2sb, met2spc):
    """ pre: SetMets(m, m_sbml, met2spc), reac2sb={}
       post: reactions from m created in m_sbml and stoichometries initialised
             reac2sb maps model reactiopn names to SBML reactions """

    reacs = m.smx.cnames[:]
    reacs.sort() # as mets.sort()
    for reac in reacs:

        if String.IsMatch(IdentRE,reac):
            id = reac
        else:
            id = "reac_" + str(reacs.index(reac))

        sreac = m_sbml.createReaction()
        sreac.setId(id)
        sreac.setName(reac)
        sreac.setFast(False)
        
        RevProps = m.sm.RevProps[reac] 
        sreac.setReversible(RevProps=="<>")
        sto = m.smx.InvolvedWith(reac)
        if RevProps == "<-":
            for met in sto:
                sto[met] *= -1
            

        
        for met in list(sto.keys()):
            smet = met2spc[met]
            smetid = smet+"_"+id
            #smetr = libsbml.SpeciesReference(SBML_ns)
            #smetr.setId(id+smet)
            #smetr.setSpecies(smet)

            if sto[met] >0:
                p =  sreac.createProduct()
                p.setId(smetid)
                p.setSpecies(smet)
                p.setConstant(False)
                p.setStoichiometry(float(sto[met]))
                #sreac.addProduct(smetr)
            else:
                r = sreac.createReactant()
                r.setId(smetid)
                r.setSpecies(smet)
                r.setConstant(False)
                r.setStoichiometry(-float(sto[met]))


        for effec in GetEffectors(m, reac):
            mod = sreac.createModifier()
            #mod.setName(effec)
            mod.setId("_".join((reac,"mod",effec)))
            mod.setSpecies(effec)

        reac2sb[reac] = sreac
        #m_sbml.addReaction(sreac)


def SetKinetics(m,m_sbml,reac2sb,met2spc):
    """ pre: m.WithKin, SetReacs, SetMets.
       post: reaction rate laws, initial metabolite and param values in m_sbml set from m """


    for reac in m.sm.cnames:
        kin = libsbml.KineticLaw(SBML_ns)
        kin.setFormula(m.md.Reactions[reac].Kin)
        reac2sb[reac].setKineticLaw(kin)

    paramfilt = lambda x: not (x.startswith("CSUM_"))
    # CSUM paarmeters are created on the fly when model is loaded, so we don't save them.
    # ScrumPy treats external mets as params, but we will init them as mets below

    params = list(filter(paramfilt,m.GetParamNames()))
    params.sort() # not essential to sort but makes for more consistent output
    for param in params:
        p = m_sbml.createParameter()
        p.setValue(m[param])
        p.setName(param)
        p.setId(param)
        p.setConstant(True)





#
##
###   Public functions below  ###################################
##
#

#
## TODO: add options to decide (e.g.) whether to use ids or names, or both, as idetifiers etc

def SBML2Spy(SBMLFile, SpyFile=None,AcceptErrors=True, MaxReportErrors=10):
    """ Convert sbml in file name SBMLFile to a ScrumPy in SPYFile,
        If SPYFile is None, munge the SBMLFile in the obvious manner
        return a (hopefully empty) list of sbml errors """

    sbml  = libsbml.readSBML(SBMLFile)
    errors = []
    klps = {} # kinetic law parameters, bound to a law (not global)
    rv = "\n".join(["# Converted from " + SBMLFile,
                    "\nElType(Float)",
                    "nan = 0 \n"])

    nerr = sbml.getNumErrors()

    Proceed = (nerr == 0) or AcceptErrors
    for e in range(nerr):
        errors.append(sbml.getError(e))

    if nerr >0:
        if nerr > MaxReportErrors:
            nmsg = MaxReportErrors
        else:
            nmsg = nerr

        ErrRep([
                "Warning ",
                str(nerr) + " errors reported from libsbml.readSBML",
                "will return complete list of libsbml.SBMLError instances",
                "reporting the first " + str(nmsg),
                "\n".join(map(SBMLErr2Str, errors[:nmsg]))
            ])
        if AcceptErrors:
            ErrRep("Will attempt to carry on regardless")
        else:
            ErrRep("Giving up")

    if Proceed:

        model = sbml.getModel()
        if model == None:
            ErrRep("Couldn't get model from sbml, check error messages,\n\nGiving up")
            rv += "# "+"# ".join(map(SBMLErr2Str, errors))
        else:
            plist = model.getListOfParameters()
            if len(plist)==0:
                rv += "Structural()\n" # ie no parameters, so it's a structural model

            SpList = model.getListOfSpecies()
            SpNameDic = SpMapNames(SpList)
            rv += SBoundrsToSpy(SpList,SpNameDic) + "\n"

            rv += SReacsToSpy(model.getListOfReactions(),klps,SpNameDic)
            rv += "\n" + SMetsToSpy(model.getListOfSpecies())
            rv += "\n" + SParamsToSpy(plist) + "\n\n\n"
            for p in list(klps.keys()):
                rv += p + " = " + str(klps[p]) + "\n"

    if SpyFile == None:
        SpyFile = SBMLFNameToSpy(SBMLFile)
    open(SpyFile,"w").write(rv)

    return errors



def Spy2SBML(m, SBMLFile=None,PiIsMet=True):
    """ Save the ScrumPy.Model, m, in SBMLFile.
         If SBMLFile==None, generate a file name from m.FileName """


    met2spc = {}  # map met names to sbml species
    reac2sb = {}  # ditto reactions

    doc=libsbml.SBMLDocument()
    m_sbml = doc.createModel()
    InitSBMLModel2(m_sbml)

    SetMets( m, m_sbml, met2spc)
    SetReacs(m, m_sbml, reac2sb, met2spc)

    if  m.md.Directives["Structural"].IsDefault():
        SetKinetics(m,m_sbml,reac2sb,met2spc)


    doc.setModel(m_sbml)


    if SBMLFile==None:
        SBMLFile=SpyFNameToSBML(m.md.GetRootFile())

    Str = libsbml.writeSBMLToString(doc)

    if PiIsMet:
        if "Pi" in m.smx.rnames:  #  distinguish phosphate from the mathematical constant
            p = "<ci> Pi </ci>"
        elif "pi" in  m.smx.rnames:
            p = "<ci> pi </ci>"

        Str = Str.replace("<pi/>", p)

    
    print(Str, file=open(SBMLFile,"w"))



