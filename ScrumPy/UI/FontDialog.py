from tkinter import Frame, Toplevel, Tk, ttk

from idlelib.configdialog import ExtPage, HighPage, FontPage
from idlelib.config import idleConf


class FontDialog(FontPage):

    

    def __init__(self, root=None):

        if root is None:
            root = Tk()
            
        self.DefaultFont = idleConf.GetFont(None, "main", "EditorWindow")
        self.CurrentFont = idleConf.GetFont(None, "main", "EditorWindow")
        self.LastFont    = idleConf.GetFont(None, "main", "EditorWindow")
        # keep as instance, not class, attributes - weird occaisional bug

        self.root = root
        root.protocol("WM_DELETE_WINDOW", self.OnClose)

        self.ExtPage =  ExtPage(root) 
        self.HighPage = HighPage(root, self.ExtPage)
        FontPage.__init__(self, root, self.HighPage)

        self.ButtonFrame   = buf = Frame(root)
        self.OKButton      = okb = ttk.Button(self.ButtonFrame, text="OK", command=self.OnOK)
        self.CancelButton  = cab = ttk.Button(self.ButtonFrame, text="Cancel", command=self.OnCancel)
        self.DefaultButton = deb = ttk.Button(self.ButtonFrame, text="Default", command=self.OnDefault)
        
        [w.pack(side="left") for w in (cab, okb,deb)]
        buf.pack(side="bottom")
        
        self.IsOpen = True

        self.pack()
       

    def on_fontlist_select(self, event):
        
        FontPage.on_fontlist_select(self, event)
        self.var_changed_font()


    def OnClose(self):
        self._root().destroy()
        self.IsOpen = False


    def OnOK(self,*args):
        """ overload in sub-class"""

        selec =  self.font_name.get(), self.font_size.get(), self.font_bold.get()
        if selec[-1] is False:
            selec = selec[0], selec[1], "normal"
        elif selec[-1] is True:
            selec = selec[0], selec[1], "bold"
        
        self.CurrentFont = selec


    def OnDefault(self, *args):

        self.CurrentFont = self.LastFont = self.DefaultFont

    
    def OnCancel(self, *args):
        self.CurrentFont = self.LastFont
        self.Close()
    
    
  



"""
f = Frame()
s = FontSel.FontDialog(f)
s.pack()
f.pack()
"""
