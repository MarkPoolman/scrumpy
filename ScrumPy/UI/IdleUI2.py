

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2019

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import os,  types

from ScrumPy.ModelDescription import SBML

from  . import tkUI2 as tkUI
tkinter = tkUI.tkinter

import TkTabEd

class EdWin(TkTabEd.TabbedEditor):

    def __init__(self,FNames, ui=None):
        TkTabEd.TabbedEditor.__init__(self, FNames)
        self.root.protocol("WM_DELETE_WINDOW", self.OnClose)
        self.ui = ui
        
        self.MakeMenu()
        self.MakeSearchBar()
        

    def MakeSearchBar(self):

        root = self.root

        frame = tkinter.Frame(root)
        frame.pack(expand=True, fill="x")
        
        sl = self.SearchLab = tkinter.Label(frame, text = 'Find', font=('calibre',10, 'bold'))
        se = self.SearchEnt = tkinter.Entry(frame,textvariable=tkinter.StringVar(), font=('calibre',10,'normal'))
        se.bind("<Return>", self.Search)
        sb = self.SearchBut = tkinter.Button(frame,text = 'Search', command = self.Search)

        rl = self.ReplLab = tkinter.Label(frame, text = 'Replace', font = ('calibre',10,'bold'))
        re = self.ReplEnt = tkinter.Entry(frame, textvariable = tkinter.StringVar(), font = ('calibre',10,'normal'))
        rb = self.ReplBut = tkinter.Button(frame,text = "Replace", command = self.Replace)

        [widg.pack(side="left", expand=True, fill="x") for widg in (sl,se,sb,rl,re,rb)]
        
       

    def Search(self,*args):
    
        tab = self.select()
        frame =  self.children[tab.split(".")[-1]]
        text = frame.children["!scrolledtext"]

        targ = self.SearchEnt.get()
        lentarg = str(len(targ))+"c"
        line = text.search(targ, text.LastSearchHit)

        if line != "":
            
            l,c = line.split(".")
            c = str(int(c)+1)
            text.LastSearchHit = ".".join((l,c))
            
            text.mark_set("insert", line)
            text.see(line)
            text.tag_remove("sel", "1.0", "end")
            text.tag_add("sel", "insert", "insert +%s"%lentarg)


    def Replace(self):
        pass

    def Revert(self, FName):

        self.Files[FName].delete("0.0", "end")
        fin = open(FName)
        self.Files[FName].insert("0.0",fin.read())
        fin.close()
        

        
        
        
    


    def OnClose(self):
        self.ui.EditorClosed()
        self.root.destroy()

    def MakeMenu(self):

        Menu = tkinter.Menu(self.root)
        Menu.add("command",{"label":"Compile", "command":self.MenuCompile})
        Menu.add("command",{"label":"Save SBML", "command": self.MenuSaveSBML})
       
        self.menubar.add("cascade",{"label":"ScrumPy","menu":Menu}) 

           
    def MenuCompile(self):
        self.ui.Reload()

    def MenuSaveSBML(self):
        #fname = FileDialogs.SaveFileName()
        fname = self.ui.ChooseFile(Read=False)
        self.ui.SaveSBML(str(fname)) # str because libSBML barfs on unicode


    def MakeMenu(self):

        Menu = tkinter.Menu(self.root)
        Menu.add("command",{"label":"Compile", "command":self.MenuCompile})
        Menu.add("command",{"label":"Save SBML", "command": self.MenuSaveSBML})
       
        self.menubar.add("cascade",{"label":"ScrumPy","menu":Menu})


class  IdleUI(tkUI.tkUI):
    #TODO: Ensure EdWins close properly when model is deleted

    Editor = None
    

    def BindModel(self,m):

        tkUI.tkUI.BindModel(self,m)
        self.NewEditor()          
        self.ReportWarnings()
        self.ReportErrors()


    def NewEditor(self):

        m = self.Model
        
        path = m.md.GetRootFilePath().rsplit(os.sep,1)[0]
        if path == "":
            path = "."
        self.EditPath = path
        
        files = [os.sep.join((path, file)) for file in m.md.FileNames]

        if self.Editor is None:                
            self.Editor =  EdWin(files, ui=self)

        else:
            for f in list(self.Editor.Files.keys()):
                if not f in files:
                    self.Editor.CloseFile(f)

            for f in files:
                if not f in self.Editor.Files:
                    self.Editor.ReadFile(f)


    def EditorClosed(self):
        self.Editor = None
        

    def Close(self):
        return

            
        
    def ReportErrors(self, MaxRep=10):
        
        LastMsg =""
        errors = self.Model.md.Errors
        n_err = len(errors)
        
        if n_err !=0:
        
            if n_err > MaxRep:
                LastMsg = ["\nA further " + str(n_err - MaxRep) + "errors not reported"]
                errors = errors[:MaxRep]+LastMsg
                
            msg = "\n".join([str(e) for e in errors])
            self.ErrorMsg(msg)

            for err in errors[:MaxRep]:
                fname = err.FName
                line = err.LineNo
                if line.isdigit(): 
                    self.Editor.GotoFileLine(fname,int(line))                    
                
            
            
        
    def ReportWarnings(self, MaxRep=10):
        
        n_err = len(self.Model.md.Errors) 
        warnings = self.Model.md.Warnings
        n_warn = len(warnings)
        LastMsg =""
        
        if  n_err == 0 and n_warn !=0:     
        
            if n_warn > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "warnings not reported"
                warnings = warnings[:MaxRep]

            msg = "\n".join([str(w) for w in warnings])
            self.WarnMsg(msg)
            
            for warn in warnings:
                fname = warn.FName
                line = warn.LineNo
                if line.isdigit():
                    self.Editor.GotoFileLine(fname,int(line))
            
            msg = "\n".join(map(str, warnings))+LastMsg
            self.WarnMsg(msg)

          
    def SaveSBML(self,  FileName):
        SBML.Spy2SBML(self.Model,  FileName)
        
        
    def NotifyReload(self):

        if self.Editor is not None:
            self.Editor.SaveFiles() 
            
        return True


    def ReRead(self, FileName):
        """ Pre: FileName in self.model.md.FileNames
           Post: Reread  FileName from disc, loosing any interactive edits. """

        FileName= os.sep.join((self.EditPath, FileName))

        self.Editor.Revert(FileName)

        
    def Hide(self):
        if not self.Editor is None:
            self.Editor.iconify()


    def Show(self):
        if self.Editor is None:
            self.NewEditor()
        else:
            self.Editor.deiconify()
    
            
        
    
        
        
            
        

