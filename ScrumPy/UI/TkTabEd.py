import os

import tkinter
from tkinter import ttk, scrolledtext, Menu, Tk, Toplevel, StringVar

import FontDialog


class Closed:
    IsOpen = False

class FontSelect(FontDialog.FontDialog):

    def __init__(self, Editor):
        
        self.Base = FontDialog.FontDialog
        self.Base.__init__(self)
        self.Editor = Editor
        self.master.title("ScrumPy: Model Editor Font Selection")
        
    
    def Update(self):
        self.Editor.UpdateFont(self.CurrentFont)


    def OnOK(self, *args):

        self.Base.OnOK(self, *args)
        self.Update()
        
        
    def OnCancel(self, *args):

        self.Base.OnCancel(self, *args)
        self.Update()
        
        
    def OnDefault(self, *args):

        self.Base.OnDefault(self, *args)
        self.Update()
        
        
        
    def Close(self):
    
        self.Editor.FontSelOpen = False
        self.Base.Close(self)
        




class TabbedEditor(Toplevel):

    WinTitle = "ScrumPy Model Editor"


    def __init__(self, FNames=[], root=None):

        if root is None:
            root = Tk()
            root.withdraw()
        self.root = root
        Toplevel.__init__(self, root)
          
        self.TextOpts={
            "undo" : True,
            "bg"   : "white",
            "fg"   : "black",
            "tabs" : (4),
            "font" : FontDialog.idleConf.GetFont(None, "main", "EditorWindow")
        }
        
        self.title(TabbedEditor.WinTitle)


        self.NoteBook = nb = ttk.Notebook(self)
        nb.enable_traversal()
         
        self.SetupMenu()
        self.MakeSearchBar()
        self.MakeGrid()
       
        self.FontSel = Closed
        self.Files = {}
        self.ReadFiles(FNames)
    
    

    def MakeGrid(self):

        ncol = len(self.SBWidgets)
        self.grid()
        self.NoteBook.grid(row=1,columnspan=ncol,sticky="nsew")
        [w.grid(row=10, column=c,sticky="nsew") for c,w in enumerate(self.SBWidgets)]
        [self.columnconfigure(n, weight=1) for n in range(ncol)]
        self.rowconfigure(1,weight=1)

        
    def MakeSearchBar(self):
        
        sb = self.SearchBut = ttk.Button(self, text='Search', command=self.Search)
        se = self.SearchEnt = ttk.Entry(self,textvariable=StringVar())
        se.bind("<Return>", self.Search)

        sp = self.SearchSep = ttk.Separator(self)

        rb = self.ReplBut = ttk.Button(self, text="Replace", command=self.Replace)
        re = self.ReplEnt = ttk.Entry(self, textvariable=StringVar())
        re.bind("<Return>", self.Replace)

        self.SBWidgets= sb,se,sp,rb,re 
        

    def CurText(self):
        """ return the text widget associated with the currently selected tab """
      
        sel = self.NoteBook.select()
        widg = self.nametowidget(sel)
        return widg.children["!scrolledtext"]
       
        
     
    def CurSel(self):
        """ => ((start, finish), text)
            of the current selection in the current tab,((), "") if no selection """
            
        t = self.CurText()
        txt = ""
        idxs = t.tag_ranges("sel")
        if idxs != ():
            idxs = [str(i) for i in idxs]
            txt = t.get(*idxs)
        
        return idxs, txt
        
        
    def CurLineCol(self):
        """ => "l.c" 
            pos of text cursor in currently selected tab """
        return self.CurText().index("insert")
        
        


    def Search(self,*args):
        """ search for the next instance of the current string in 
        self.SearchEnt and highlight it """
        
        text = self.CurText()
        targ = self.SearchEnt.get()
        lentarg = str(len(targ))+"c"
        line = text.search(targ, text.LastSearchHit)

        if line != "":
            
            l,c = line.split(".")
            c = str(int(c)+1)
            text.LastSearchHit = ".".join((l,c))
            
            text.mark_set("insert", line)
            text.see(line)
            text.tag_remove("sel", "1.0", "end")
            text.tag_add("sel", "insert", "insert +%s"%lentarg)
            
        return line


    def Replace(self,*args):
    
        targ = self.SearchEnt.get()
        repl  = self.ReplEnt.get()
        if targ != "" and repl != "":
            start = self.Search()
            if start != "":
                l,c = start.split(".")
                c = str(int(c) + len(targ))
                end = ".".join((l,c))
                
                self.CurText().replace(start, end, repl)
            
            
        
        

    def Revert(self, FName):

        self.Files[FName].delete("0.0", "end")
        fin = open(FName)
        self.Files[FName].insert("0.0",fin.read())
        fin.close()
        

    def __getattr__(self, a):

        try:
            return getattr(self.root, a)
        except:
            raise AttributeError(str(a))
        
    
    def SetupMenu(self):

        self.menubar = menu = Menu(self)
        self.config(menu=menu)
        
        editMenu = Menu(menu)
        editMenu.add_command(label="Undo", command = self.OnMenuUndo)
        editMenu.add_command(label="Redo", command = self.OnMenuRedo)
        editMenu.add_command(label="Font", command = self.OnMenuFont)
        menu.add_cascade(label="Edit", menu=editMenu)


    def OnMenuUndo(self, *args):
        try:
            self.CurText().edit_undo()
        except:
            pass # ignore the exception raised if there's nothing to undo

    def OnMenuRedo(self, *args):
        try:
            self.CurText().edit_redo()
        except:
            pass # ignore the exception raised if there's nothing to redo
        
        
    def OnMenuFont(self, *args):
        if not self.FontSel.IsOpen:
            self.FontSel = FontSelect(self)
            
        
    def ReadFiles(self, FNames):

        if type(FNames) is str:
            FNames = [FNames]

        for f in FNames:
            self.ReadFile(f)

            
        fabspath = os.path.abspath(FNames[0])
        WinTit = " : ".join((self.WinTitle, fabspath))
        self.title(WinTit)


    def ReadFile(self, FName):

        if FName in self.Files:
            self.Reload(FName)
        else:
            t = self.Files[FName] = scrolledtext.ScrolledText(master=self, **self.TextOpts)
            TabName = FName.rsplit(os.sep, 1)[-1]
            self.NoteBook.add(t, text=TabName)
            t.LastSearchHit="0.0"
            try:
                if os.path.isfile(FName):
                    content = open(FName).read()
                    t.insert(tkinter.END,content)
                    t.edit_reset() # ensure "undo" doesn't undo the load file
                else:
                    open(FName,"w").close()
                
            except Exception  as ex:
                print("failed to open", FName, ex)


    def SaveFiles(self):

        for f, widg in self.Files.items():
            text = widg.get("0.0", tkinter.END)[:-1]
            with  open(f,"w") as out:
                out.write(text)


    def CloseFile(self, FName):
        tab = self.Files[FName].winfo_parent()
        self.NoteBook.forget(tab)
        del self.Files[FName]


    def GotoFileLine(self, FName, LineNo):

        text = self.Files[FName]
        self.NoteBook.select(text)
        strline = "%d.0" % LineNo
        text.mark_set("insert", strline)
        text.see(strline)
        text.tag_remove("sel", "1.0", "end")
        text.tag_add("sel", "insert", "insert +1l")


    def UpdateFont(self, font):
        self.TextOpts["font"] = font
        self.UpdateTextOpts()


    def UpdateTextOpts(self):
        for f in self.Files:
            self.Files[f].configure(**self.TextOpts)
        


    def SetWinTitle(self, Title):

        self.root.title(" : ".join((self.WinTitle, Title)))
        

        
       
