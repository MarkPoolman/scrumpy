import sys

try:
    from . import BasicInfo
except:
    import BasicInfo # depends how and where we're being imported
    
#print (BasicInfo.Greeting)

class BaseUI:

    Title = BasicInfo.Title
    Greeting = BasicInfo.Greeting

    ErrStream = sys.stderr
    WarnStream = sys.stdout

    
    def _Exec(self, *args, **kwargs):
       pass       

    
    def __getattr__(self, a):
        self.TermError ("UI  - %s not implemented"%a)
        return self._Exec
   
    def TermError(self,msg):
        """ 'last-ditch' error messages to terminal """
        print (msg, file=self.ErrStream)
     
        
    def BindModel(self,m):
        self.Model = m
        self.ReportErrors()
        self.ReportWarnings()

    def ChooseFile(self):

        print("ChooseFile must be implemented by a sub-class")


    def ReportErrors(self):

        errs = self.Model.md.Errors
        for e in [str(e) for e in errs]:
            print (e, file = self.ErrStream)


    def ReportWarnings(self):

        warns = self.Model.md.Warnings
        for w in [str(w) for w in warns]:
            print (w, file = self.WarnStream)

    
    def ViewMatrix(self, mtx):
        print(mtx) # overload as needed


    def GetPlotter(self):
        print("GetPlotter must be implemented by a sub-class")




    def NotifyReload(self):
        """ Subclasses should overload this to close and save files etc.
            before the model Re-reads them.
            It should NOT invoke self.Model.Reload() or self.Reload()
            The boolean return value indicates whether the model should go
            ahead with the reload (let users  change their mind) """
        print ("ui got NotifyReload")
        return True


    def ReRead(self, FileName):
        """ Pre: FileName in self.model.md.FileNames
           Post: Reread  FileName from disc, loosing any interactive edits. """

        raise NotImplemented()


    def Reload(self):
        """ Reload the model, this should not be overloaded, ui housekeeping
            should be done in NotifyReload() """
        self.Model.Reload()


    def Close(self):
        """ Subclasses implement this to perform releant tear-down ops etc. """
        pass
    
