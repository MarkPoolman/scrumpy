
import sys, os
import multiprocessing

from qtpy import QtCore, QtWidgets

try:
    from . import PlotWin
except:
    import PlotWin


class PlotServer(PlotWin.PlotWin):

    def __init__(self):
    
    #
    ##  First establish connection with the client
    #                   #print("__child got", sys.argv)
        fds = eval(sys.argv[1])  
        # tuple of file descriptors passed by the client #print("__child", fds)

        p2ch  = multiprocessing.Pipe()
        os.dup2(fds[0], p2ch[0].fileno())
        self.FromCli =  p2ch[0] # from client
        p2ch[1].close() # close unused end of pipe

        ch2p  = multiprocessing.Pipe()
        os.dup2(fds[1], ch2p[1].fileno())
        self.ToCli = ch2p[1]  # to client
        ch2p[0].close()

        msg = self.FromCli.recv()
        self.ToCli.send(("server recv ",msg)) # coms check w/ client #print ("__child sent to parent")
         
    #
    ##  Now set up the Qt stuff
    #
        self.app = QtWidgets.QApplication([])
        PlotWin.PlotWin.__init__(self, sys.argv[2])
        self.show()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.app.processEvents)
        self.timer.timeout.connect(self.GetRequests)
        self.timer.start(2)
        
        self.app.exec_()
        #print("exec done", file=sys.__stderr__)
        sys.exit()


    def GetRequests(self):

        while self.FromCli.poll():
            try:
                cmd, args, kwargs = self.FromCli.recv()
            except:
                #print("lost cli 1", file=sys.__stderr__)
                self.close()
                self.app.exit(1)
                return
                # lost client connection
            #print ("server gets", cmd, args, kwargs)
            
            try:
                rv = getattr(self, cmd)(*args, **kwargs)
            except Exception as  ex:
                rv = ex
                
            #print("Server sends", rv)
            try:
                self.ToCli.send(rv)
            except:
                #print("lost cli 2", file=sys.__stderr__)
                self.close()
                self.app.exit(1)
                return
                # lost client connection
        

if __name__ == "__main__":

    PlotServer()
    
