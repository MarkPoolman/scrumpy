#! /usr/bin/env python3
# -*- coding: utf-8 -*-


import sys, os


from qtpy.QtWidgets import QMainWindow, QWidgetAction
from qtpy.QtGui import QCursor, QColor
from qtpy import QtCore

import qtpy
os.environ["PYQTGRAPH_QT_LIB"] = qtpy.API_NAME # make sure we all agree which version of Qt we're using
import pyqtgraph
import pyqtgraph.exporters

BGColour   = "white"
FGColour = "k"
AxisColour = FGColour
AxisWidth  = 2
TextColour = "k"


pyqtgraph.setConfigOptions(foreground=FGColour, background=BGColour)
   
class PlotItemInfo:

    def __init__(self, PlotItem, Name, row,col):

        self.PlotItem = PlotItem
        self.Name = Name
        self.row = row
        self.col = col
        
        act = PlotItem.ctrlMenu.addAction("Replicate")
        act.triggered.connect(self.Replicate)

        self.StyleInfo = {}
        self.PlotData = {}

    def Plot(self, xdat, ydat, Label, **kwargs):

        self.StyleInfo[Label] = dict(kwargs)
        self.PlotData[Label] = self.PlotItem.plot(xdat,ydat,name=Label,**kwargs)


    def SetTextLabel(self, txt, pos, *args, **kwargs):
        item = pyqtgraph.TextItem(txt, *args, **kwargs)
        self.PlotItem.addItem(item)
        item.setPos(*pos)


    def Update(self, xdat, ydat, Label, **kwargs):
        self.StyleInfo[Label].update(dict(kwargs))
        self.PlotData[Label].setData(xdat,ydat,**self.StyleInfo[Label])
        
    
    def  Export(self, FName ):
        print("Export", FName)
        exp = pyqtgraph.exporters.SVGExporter(self.PlotItem)

        # remove the overlying black rectangle
        # totally grody solution, but all others fail
        bg = '<rect width="100%" height="100%" style="fill:rgba(255, 255, 255, 1.000000)" />\n'
        content = open(FName).read()
        out = open(FName,"w")
        print("Clean!")
        out.write(content.replace(bg, ""))
        out.close()

        #https://pyqtgraph.readthedocs.io/en/latest/user_guide/exporting.html
    


    def Remove(self,Label):

       
        try:
            self.PlotItem.removeItem(self.PlotData[Label])
            del self.PlotData[Label]
            del self.StyleInfo[Label]
        except:
            pass # client tried to access a closed window

    def __getattr__(self, a):
        return getattr(self.PlotItem, a)

    def GetRC(self):
        return self.row, self.col


    def Replicate(self): # 
        """ Demo - ignore """

        w = self.w = PlotWin(id(self), "Replicate"+self.Name,None)
        w.NewPlotter(0,0,self.Name)
        w.SetActivePlotter(self.Name)
        for d in self.PlotItem.listDataItems():
            x,y = d.getData()
            label = d.name()
            style = self.StyleInfo[label]
            w.NewPlot(label, x,y, **style)
    
        w.show()


        
        
class PlotWin(QMainWindow):

    
    def __init__(self, Title):

        self.Title = Title
        Width  = 900  #ISO 216 (A4 etc) landscape
        Height = int(Width/2**.5)
        pos = QCursor().pos()

        QMainWindow.__init__(self)
        
        self.layout = pyqtgraph.GraphicsLayoutWidget()
        self.setCentralWidget(self.layout)
        self.setGeometry(pos.x(), pos.y(), Width, Height)
        self.setWindowTitle(Title)
        #self.show()
        
       
        self.PlotInfo = {}
        self.ActivePlotter = None # name of the plotter currently in use
     

    def closeEvent(self, e):

        #print(self.Title, "closing!")
        
        e.accept()


    def Resize(self, w, h, x=None, y=None):

        if x is None:
            x = QCursor().pos().x()
        if y is None:
            y = QCursor().pos().y()

        self.setGeometry(x, y, w, h)

    
    def SetActivePlotter(self, plid):
       
        self.ActivePlotter = self.PlotInfo[plid]
        #print("Act", self.ActivePlotter,file=sys.stderr)
        
    def __getattr__(self, a):
        return getattr(self.ActivePlotter, a)

        
    def NewPlotter(self, Row=0, Col=0, Name="NoName", WithTitle=True, WithLegend=True):

        if WithTitle:
            title=Name
        else:
            title=None
            
        plotter = self.layout.addPlot(Row,Col,title=title)
        if WithLegend:
            plotter.addLegend(labelTextColor="k")

        plid = id(plotter)
        self.ActivePlotter = self.PlotInfo[plid] = PlotItemInfo(plotter, Name, Row, Col)

        return plid
    
    


    def NewPlot(self, Label, xdat, ydat, **kwargs):
        #print("NewPlot", Label, xdat, ydat, kwargs, file=sys.stderr     )    
        self.ActivePlotter.Plot(xdat,ydat,Label,**kwargs)

    
        
        
    def Remove(self, Labels):

        for label in Labels:
            self.ActivePlotter.Remove(label)
            


    def SetAxisPen(self, PenSpec):
        
        Pen = pyqtgraph.mkPen(PenSpec)
        for axis in "left" ,"bottom", "top", "right":
            ax = self.ActivePlotter.getAxis(axis)
            ax.setPen(Pen)
            ax.setTextPen(color=PenSpec["color"])
         
            
    def SetXY0(self):
        
        x = pyqtgraph.InfiniteLine(angle=0)
        print("got x", x)
        y = pyqtgraph.InfiniteLine(angle=90)
        print("got y", y)
        self.ActivePlotter.addItem(x)
        self.ActivePlotter.addItem(y)
        print("added x,y")
        return id(x),id(y)
       
        
            
        
    
    def  ExportSVG(self, FName ):
        exp = pyqtgraph.exporters.SVGExporter(self.Plotter)
        exp.export(FName)

        # remove the overlying black rectangle
        # totally grody solution, but all others fail
        bg = '<rect width="100%" height="100%" style="fill:rgba(255, 255, 255, 1.000000)" />\n'
        content = open(FName).read()
        out = open("A"+FName,"w")
        print("Clean!")
        out.write(content.replace(bg, ""))
        out.close()

    




























"""
self.Plotter.SetMissing()
self.Plotter.SetLog(Axis, On)
self.Plotter.SetStyle(d, Style)
self.Plotter.AddData(y, [self.GetCol(self.PlotX), self.GetCol(y)],style)
self.Plotter.AddData(z, [self.GetCol(self.PlotX), self.GetCol(self.PlotY),  self.GetCol(z)],style)
self.Plotter.RemoveData(y)
self.Plotter.keys()
self.Plotter.SavePlot(filename)
self.Plotter.SaveA4PDF(filename)
self.Plotter[k].UpdateData([x[:], y])
self.Plotter.Plot()
self.Plotter.SurfPlot()
"""


"""
       

    def OpenFile(self):
        print("OpenFile")
        
    
    def SaveFile(self):
        print ("SaveFile")

    
    def SaveFileAs(self):
        print ("SaveFileAs")

    
    def CloseFile(self):
        print ("CloseFile")
                
            
    def MakeMenus(self):

        menubar = self.menuBar()
        
        FileMenu = menubar.addMenu("&File")               
        EditMenu = menubar.addMenu("&Edit")
        ViewMenu = menubar.addMenu("&View")
        HelpMenu = menubar.addMenu("&Help")
        
        return

        NewFileAct   = FileMenu.addAction("&New")
        NewFileAct.triggered.connect(self.NewFile)
        
        OpenFileAct  = FileMenu.addAction("&Open")
        OpenFileAct.triggered.connect(self.OpenFile)
        
        SaveFileAct  = FileMenu.addAction("Save")
        SaveFileAct.triggered.connect(self.SaveFile)

        SaveFileAsAct  = FileMenu.addAction("SaveAs")
        SaveFileAsAct.triggered.connect(self.SaveFileAs)

        GotoFileLineAct = FileMenu.addAction("Goto File/Line")
        GotoFileLineAct.triggered.connect(self.GotoFileLine)
        
        
        CloseFileAct = FileMenu.addAction("Close")
        CloseFileAct.triggered.connect(self.CloseFile)


    def keys(self):

        return self.PlotDataItems.keys()

    
    def MakeStatusBar(self):
        self.statusBar()      
        
        
    def AddData(self, name, xy_data, style = "lines"):
        x,y = xy_data
        self.PlotDataItems[name] =  self.Plotter.plot(x,y)

   # def Update
        
"""     
        
        

