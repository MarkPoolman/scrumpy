
from . import PlotStyles

def SmallestNotIn(L):

    L = sorted(L)
    lenL = len(L)
    if lenL == 0 or L[0] >0:
        rv = 0

    else:
        idx = 0
        maxidx = lenL-1

        while idx < maxidx and L[idx+1]-L[idx] == 1:
            idx += 1
        rv = L[idx]+1
    return rv


def CalcRC(n):
    """caluculate nearest number of rows and cols to form a grid
       holding n items """
    
    r =  c =1
    nextr = True
    while r * c < n:
        if nextr:
            r+= 1
        else:
            c+= 1
        nextr = not nextr
    return r,c


class PlotData(dict):

    def __init__(self, Label, plotter, xdat, ydat, StyleDic):

        self.Label = Label
        self.plotter = plotter
        self.update(StyleDic)
        self.xdat = xdat
        self.ydat = ydat

    def UpdateData(self, xydat):
        self.plotter.Update(self.Label, *xydat)


DefaultAxisStyle ={"width":2, "color":"b"}
class Plotter:
    
    def __init__(self, win, plid):
        self.win = win      # the window in which we're embedded
        self.plid = plid    # a unique id to identify us to win
        
        self.PlotData  = {} # plotted data ref by key
        self.TxtLabels = {} # Text labels ref by key
        self.Decorations = {} # Additional non-data items x=0, y=0 etc
        self.AutosUsed   = [] # Automatic style indices currently in use
        
        self.MakeActive()        
        self.Decorations["xy0"] = win.SetXY0()


    def MakeActive(self):
        """ tell our window we want action """
        self.win.SetActivePlotter(self.plid)


    def GetAuto(self):
        """ get an unused automatic style index """
        
        rv = SmallestNotIn(self.AutosUsed)
        self.AutosUsed.append(rv)
        return rv

              
    def ClearAuto(self, a):
        """ relinquish a used automatic style index """
        if a in self.AutosUsed:
            self.AutosUsed.remove(a)

    
    def NewPlot(self, Label, xdat, ydat, StyleDic={}):
        """ Pre:  len(xdat) == len(ydat),
                  Label not in self.PlotData,
            Post: Plot the x and y data as specified by StyleDic """

        if Label in self.PlotData:
            raise LookupError("\n!! " + Label + " already exists - Update() or remove() it first !!")

        self.PlotData[Label] = PlotData(Label, self, xdat, ydat, StyleDic)
      
        self.MakeActive()
        return self.win._NewPlot(Label, xdat, ydat, **StyleDic)
        

    def LinePlot(self, Label, xdat, ydat, UsrStyle={}):

        auto = self.GetAuto()
        Style = PlotStyles.AutoLine(auto)
        Style["AUTO"] = auto
        Style["pen"].update(UsrStyle)
        
        return self.NewPlot(Label, xdat, ydat, Style)

        
    def ScatterPlot(self, Label, xdat, ydat, UsrStyle={}):

        auto = self.GetAuto()
        Style = PlotStyles.AutoSymbol(auto)
        Style["AUTO"] = auto
        Style.update(UsrStyle)
        
        return self.NewPlot(Label, xdat, ydat, Style)


    def LinePointPlot(self, Label, xdat, ydat, UsrStyle={}):

        auto = self.GetAuto()
        Style = PlotStyles.AutoLinePoint(auto)
        Style["AUTO"] = auto
        Style["pen"].update(UsrStyle)

        return self.NewPlot(Label, xdat, ydat, Style)


    def Histogram(self,  Label, xdat, ydat, **kwargs):
        
        auto = self.GetAuto()
        Style = PlotStyles.AutoHisto(auto)
        Style["AUTO"] = auto
        
        return self.NewPlot(Label, xdat, ydat, Style)


    def SetTextLabel(self, txt, pos=(0,0), anchor=(0.5,0), colour="k", *args, **kwargs):
        """ pre: Plotter has some plotted data or xy ranges set
            post: text label with content txt at printed at position pos,
            (other *args/**kwargs forwarded to pyqtgrapth.TextItem())
        """

        self.MakeActive()
        self.win._SetTextLabel(txt, pos, anchor=anchor, color=colour, *args, **kwargs)
        
    def Export(self, FName, *args, **kwargs):
    
        self.MakeActive()
        self.win._Export(FName, *args, **kwargs)
        
        
        



    def Update(self, Label, xdat, ydat):
        self.MakeActive()
        return self.win._Update(Label, xdat, ydat, **self.PlotData[Label])

    
    def Remove(self, Labels):
        
        if type(Labels) is str:
            Labels = [Labels]
        
        for label in Labels[:]:
            if label not in self.PlotData:
                print ("Ignoring non-existent plot", label)
                Labels.remove(label)
            else:
                if "AUTO" in self.PlotData[label]:
                    self.ClearAuto(self.PlotData[label]["AUTO"])
                   
                del self.PlotData[label]
                
        self.MakeActive()      
        return self.win.Remove(Labels)


    def RemoveAll(self):

        return self.Remove(list(self.PlotData.keys()))
