import sys
python = sys._base_executable

import subprocess, multiprocessing

from ScrumPy.Util import Procs

from . import PlotServer, Plotter
serv = PlotServer.__file__



def CalcRC(n):
    """caluculate nearest number of rows and cols to form a grid
       holding n items """
    
    r =  c =1
    nextr = True
    while r * c < n:
        if nextr:
            r+= 1
        else:
            c+= 1
        nextr = not nextr
    return r,c


class ClosedWin:
    def __getattr__(self, a):
        print ("\n!! Window has closed, this plotter can't be used !!\n",
                file=sys.stderr)
        return self.Ignore

    def Ignore(self, *args, **kwargs):
        pass

        

class PlotWinCli:

    def __init__(self, WinTitle="ScrumPy Plotter"):

        p2ch = multiprocessing.Pipe() # parent to child
        p2chfd = p2ch[1].fileno()     # parent to child file descriptor

        ch2p = multiprocessing.Pipe() # child to parent
        ch2pfd = ch2p[1].fileno()     # child to parent file descriptor

        fds = (p2chfd, ch2pfd)        # child uses these two to listen and send
        #print("__parent starts child")
        
        self._Server = subprocess.Popen([python, serv, str(fds), "WinTitle"], pass_fds=fds)
        p2ch[1].close() # close unused ends of the pipes
        ch2p[1].close()

        self._ToServ   = p2ch[0]
        self._FromServ = ch2p[0]

        msg = self.SndRcv("Client starts server") # coms check

        #print("client receives ", msg)

        self.Plotters = {}


    def SndRcv(self, method, *args, **kwargs):

        try:
            self._ToServ.send((method, args, kwargs))
            rv =  self._FromServ.recv()
        except:
            print("\n!! Window has closed and can't be used !!",file = sys.stderr)
            rv = None
            
        if hasattr(rv,"__traceback__"):
            raise(rv)
        return rv

    def IsOpen(self):
        return self._Server.poll() is None


    def Close(self):
    
        for p in self.Plotters:
            self.Plotters[p].win = ClosedWin() # ensure usr knows plotter can't be used
        self.Plotters = {}  # disconnect - reduce GC delay

        if self.IsOpen():
            self.SndRcv("close")

        if self.IsOpen():
            self._Server.terminate()
            # Belt and Braces - "close" should kill the server
            
        self._Server.wait()
        Procs.GreedyWait() # Harvest any other terminated child procs
        

    def __del__(self):
        self.Close()

    def Resize(self, w, h, x=None, y=None):
        self.SndRcv("Resize", w, h, x, y)
        

    def NewPlotter(self, Row=0, Col=0, Name="NoName", WithTitle=True, WithLegend=True):
        plid = self.SndRcv("NewPlotter", Row, Col, Name, WithTitle, WithLegend)
        rv = self.Plotters[plid] = Plotter.Plotter(self, plid)
        return rv

    def SetActivePlotter(self, plid):
        """ pre: plid in self.Plotters
           post: subsequent plots will be performed on this plotter """
        return self.SndRcv("SetActivePlotter", plid)


    def _NewPlot(self, Label, xdat, ydat, **kwargs):
        """ pre: self.setActivePlotter(plid)
                 xdat, ydat, **kwargs are valid for 
                 pyqtgraph.PlotDataItem(xdat, ydat, **kwargs)
                 
           post: data plotted according to args """
           
        return self.SndRcv("NewPlot", Label, xdat, ydat, **kwargs)


    def _Update(self, Label, xdat, ydat, **kwargs):
        return self.SndRcv("Update",  xdat, ydat, Label, **kwargs)

    def _SetTextLabel(self, txt, pos, *args, **kwargs):
        return self.SndRcv("SetTextLabel", txt, pos, *args, **kwargs)
        
    def _Export(self, FileName, *args, **kwargs):
        return self.SndRcv("Export", FileName, *args, **kwargs)
        
    def SetXY0(self):
        return self.SndRcv("SetXY0")


    def Remove(self, Labels):
        if type(Labels) is str:
            Labels = [Labels]
        for l in Labels:
            del self.PlotData[l]
            
        return self.SndRcv("Remove", Labels)
        

        
class SimplePlotWinCli(PlotWinCli):
    
    def __init__(self,  WinTitle="Title", Caption="Caption", Client=None):

        PlotWinCli.__init__(self, WinTitle)
        self.Plotter = p = PlotWinCli.NewPlotter(self, Name=Caption)
        p.MakeActive()


    def NewPlotter(self, *args, **kwargs):
        print ("Not implemented in SimplePlotWinCli - use PlotWinCli instead")

    def __getattr__(self, a):
        return getattr(self.Plotter,a)
        
        

        
        

        
