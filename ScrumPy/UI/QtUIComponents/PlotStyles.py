
import pyqtgraph

from qtpy.QtGui import QColor

"""
‘o’ circle (default)
‘s’ square
‘t’ triangle
‘d’ diamond
‘+’ plus
‘t1’ triangle pointing upwards
‘t2’ triangle pointing right side
‘t3’ triangle pointing left side
‘p’ pentagon
‘h’ hexagon
‘star’
‘x’ cross
‘arrow_up’
‘arrow_right’
‘arrow_down’
‘arrow_left’
‘crosshair’
"""




Symbols="o", "s", "t", "d", "+", "star", "x", "h", "crosshair"
Colours = "violet indigo blue green yellow orange red".split()
#MaxHue=len(Symbols)+1
HistoFillAlpha=128

def GetPen(width=2, Colour="k",**kwargs):

    if width >0:
        return  {'width':width, 'color':Colour}


def GetSymbol(shape="o", pen=GetPen(), FillColour=None, Size=6):

    if FillColour is None:
        FillColour = pen["color"]

    return {
        "symbol"      : shape,
        "symbolPen"   : pen,
        "symbolBrush" : FillColour,
        "symbolSize"  : Size
    }



#def AutoColour(n):
#    return (n%MaxHue, MaxHue) # see pyqtgraph intColor()

def AutoColour(n,alpha=255):
    
    n = n % len(Colours)
    rv = QColor(Colours[n])
    rv.setAlpha(alpha)

    return rv


def AutoSymbol(n,**kwargs):

    ps = n % len(Symbols)
    
    rv =  GetSymbol(Symbols[ps], pen=GetPen(Colour=AutoColour(n),**kwargs),**kwargs)
    rv["pen"] = None
    return rv


def AutoLine(n, **kwargs):
    return {"pen" : GetPen(Colour=AutoColour(n), **kwargs)}


def AutoLinePoint(n,**kwargs):
    
    Colour = AutoColour(n,**kwargs)
    Pen = GetPen(Colour=Colour,**kwargs)
    Symbol = AutoSymbol(n,**kwargs)
    Symbol["pen"]=Pen
    
    return Symbol


def AutoHisto(n,LineColour="Red", FillColour="Red"):

    LineColour = GetPen(Colour=AutoColour(n))
    FillColour = AutoColour(n,alpha=HistoFillAlpha)

    return {
        "pen": LineColour,
        "fillLevel" : 0,
        "brush" : FillColour,
        "stepMode" : "center"
        }
        
    
    

