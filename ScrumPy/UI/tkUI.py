

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2019

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import os,  types


from  . import BaseUI
import tkinter
from tkinter import messagebox, filedialog


class  tkUI(BaseUI.BaseUI):
    
    def __init__(self):

        self.parent = tkinter.Tk()
        self.parent.wm_withdraw()


    def BindModel(self,m):
    
        self.Model = m
        self.RootFile = m.md.GetRootFile()
        self.Path =  m.md.Path
        
        
    def __del__(self): 
        self.Close()     # ensure editor windows are closed when deleted

        
    def Close(self):
        self.parent.destroy()
        
    def InfoMsg(self, Message = ""):
        messagebox.showinfo(message=Message,title=self.Title,parent=self.parent)

    def WarnMsg(self, Message = ""):
        messagebox.showwarning(message=Message,title=self.Title,parent=self.parent)

    def ErrorMsg(self, Message = ""):
        messagebox.showerror(message=Message,title=self.Title,parent=self.parent)

    def ChooseFile(self):
        return filedialog.askopenfilename(title=self.Title,parent=self.parent)

    def NotifyReload(self):
        self.InfoMsg("Reloading "+self.RootFile + "\nSave files and press OK")
        return True
        for e in self.Editors:
            self.Editors[e].SaveFile(e)
            self.Editors[e].close(Ask=False)
     

    def ReportErrors(self, MaxRep=10):
        
        LastMsg =""
        errors = self.Model.md.Errors
        n_err = len(errors)
        
        if n_err !=0:
        
            if n_err > MaxRep:
                LastMsg = ["\nA further " + str(n_err - MaxRep) + "errors not reported"]
                errors = errors[:MaxRep]+LastMsg
                
            msg = "\n".join([str(e) for e in errors])
            self.ErrorMsg(msg)

            return
      
            for err in errors:
                fname = os.sep.join((self.Path, err.FName))
                line = err.LineNo
                if self.Editors.has_key(fname) and  type(line) == types.IntType:
                    ed = self.Editors[fname]
                    ed.gotoline(line)
                # TODO: how to make EdWins with errors stay on top ??
            
            
            
        
    def ReportWarnings(self, MaxRep=10):
        
        n_err = len(self.Model.md.Errors) 
        warnings = self.Model.md.Warnings
        n_warn = len(warnings)
        LastMsg =""
        
        if  n_err == 0 and n_warn !=0:     
        
            if n_warn > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "warnings not reported"
                warnings = warnings[:MaxRep]
      
            for warn in warnings:
                fname = os.sep.join((self.Path, warn.FName))
                line = int(warn.LineNo)
                ed = self.Editors[fname]
                ed.gotoline(line)
                
            
            msg = "\n".join(map(str, warnings))+LastMsg
            WarnMsg(msg)

          
    #def SaveSBML(self,  FileName):
    #    SBML.Spy2SBML(self.Model,  FileName)
        
        
        
    def HideChildren(self):
        return
        
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.iconify()
                except:
                    pass
                
                
    def ShowChildren(self):
        return
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.deiconify()
                except:
                    self.Editors[f] = EdWin(f, self)

        
            
        

