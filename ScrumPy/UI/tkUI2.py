

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2019

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

test git
"""

import sys, os,  types
import tkinter
from tkinter import messagebox, filedialog


import  BasicInfo # general info
import BaseUI, TkTabEd, MtxView # ui specific modules

class EdWin(TkTabEd.TabbedEditor):

    def __init__(self,FNames, ui=None):
    
        TkTabEd.TabbedEditor.__init__(self, FNames)
        self.protocol("WM_DELETE_WINDOW", self.OnClose)
        self.ui = ui
        
        self.MakeMenu()

        
    def OnClose(self):
        
        self.root.destroy()
        if  self.ui is not None: # could happen in dev/debug environment
            self.ui.EditorClosed()
        


    def MakeMenu(self):

        Menu = tkinter.Menu(self.root)
        Menu.add("command",{"label":"Compile", "command":self.OnMenuCompile})
        Menu.add("command",{"label":"Save SBML", "command": self.OnMenuSaveSBML})
       
        self.menubar.add("cascade",{"label":"ScrumPy","menu":Menu}) 

           
    def OnMenuCompile(self,*args):
        self.ui.Reload()
        md = self.ui.Model.md
      
        Path = md.GetRootFilePath().rsplit(os.sep,1)[0]+os.sep
        ModelFiles = [Path+f for f in md.FileNames]
        
        for f in list(self.Files.keys()): # because we might change the items in self.Files
            if not f in ModelFiles:
                self.CloseFile(f)
        

    def OnMenuSaveSBML(self,*args):
        fname = self.ui.ChooseFile(Read=False)
        self.ui.SaveSBML(str(fname)) # str because libSBML barfs on unicode


class DMtxView(MtxView.MtxView):
    """
        How to cope with the matrix changing while the viewer is open?
        We *can't* change the matrix source code (use expose/focus/visibility events?)
        
        Also tell the UI when we've been closed.
    """
   
    def __init__(self, mtx):
        MtxView.MtxView.__init__(self, mtx.rnames, mtx.cnames, mtx.rows)


class  tkUI(BaseUI.BaseUI):
    
    def __init__(self):
    
        #BaseUI.__init__(self)

        self.root = tkinter.Tk()
        self.root.wm_withdraw()
        self.Editor = None
     
        
    def BindModel(self,m):
    
        self.Model = m
        self.NewEditor() 
        self.ReportWarnings()
        self.ReportErrors()


    def NewEditor(self):

        md = self.Model.md
        
        if md.Directives["NoEdit"].IsDefault():
            # defualt is to open an editor
            
            files = [os.sep.join((md.Path, file)) for file in md.FileNames]

            if self.Editor is None:                
                self.Editor =  EdWin(files, ui=self)

            else:
                for f in list(self.Editor.Files.keys()):
                    if not f in files:
                        self.Editor.CloseFile(f)

                for f in files:
                    if not f in self.Editor.Files:
                        self.Editor.ReadFile(f)

            self.Editor.SetWinTitle(md.Path+os.sep)


    def EditorClosed(self):
        self.Editor = None
        

    def __del__(self): 
        self.Close()     # ensure  windows are closed when deleted

        
    def Close(self):
        if self.Editor is not None:
            self.Editor.OnClose() # any other clearing up here as needed
        else:
            pass
            #print("!! Already closed !!")
        
    def InfoMsg(self, Message = ""):
        messagebox.showinfo(message=Message,title=self.Title,parent=self.root)

    def WarnMsg(self, Message = ""):
        messagebox.showwarning(message=Message,title=self.Title,parent=self.root)

    def ErrorMsg(self, Message = ""):
        messagebox.showerror(message=Message,title=self.Title,parent=self.root)

    def ChooseFile(self, Read=True):
        
        if Read:
            rv = filedialog.askopenfilename(title=self.Title,parent=self.root)
        else:
            rv = filedialog.asksaveasfile(title=self.Title,parent=self.root).name
            
        return rv


     

    def ReportErrors(self, MaxRep=10):
     
        LastMsg =""
        md = self.Model.md
        errors = md.Errors
        n_err = len(errors)
        
        if n_err !=0:
        
            if n_err > MaxRep:
                LastMsg = ["\nA further " + str(n_err - MaxRep) + "errors not reported"]
                errors = errors[:MaxRep]+LastMsg
                
            msg = "\n".join([str(e) for e in errors])
            self.ErrorMsg(msg)

            for err in errors[:MaxRep]:
                fname = os.sep.join((md.Path,err.FName))
                line = err.LineNo
                if line.isdigit(): 
                    self.Editor.GotoFileLine(fname,int(line))                    


    def ReportWarnings(self, MaxRep=10):
        
        md = self.Model.md
        n_err = len(md.Errors) 
        warnings = md.Warnings
        n_warn = len(warnings)
        LastMsg =""
        
        if  n_err == 0 and n_warn !=0:     
        
            if n_warn > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "warnings not reported"
                warnings = warnings[:MaxRep]

            msg = "\n".join([str(w) for w in warnings])
            self.WarnMsg(msg)
            
            for warn in warnings:
                fname = os.sep.join((md.Path,warn.FName))
                line = warn.LineNo
                if line.isdigit():
                    self.Editor.GotoFileLine(fname,int(line))
            
            msg = "\n".join(map(str, warnings))+LastMsg
            self.WarnMsg(msg)
            
          
    def ViewMatrix(self, mtx):
        DMtxView(mtx)
        ## obvs a bit more to do here!
    
    def SaveSBML(self,  FileName):
        SBML.Spy2SBML(self.Model,  FileName)
        
        
    def NotifyReload(self):

        if self.Editor is not None:
            self.Editor.SaveFiles() 
            
        return True


    def ReRead(self, FileName):
        """ Pre: FileName in self.model.md.FileNames
           Post: Reread  FileName from disc, loosing any interactive edits. """

        if self.Editor is not None: # usr might have closed EdWin
            FileName= os.sep.join((self.Model.md.Path, FileName))
            self.Editor.Revert(FileName)

        
    def Hide(self):
        if not self.Editor is None:
            self.Editor.iconify()


    def Edit(self):
        if self.Editor is None:
            self.NewEditor()
        else:
            self.Editor.deiconify()
    
            
if __name__ == "__main__":
 
    
    if not BasicInfo.PPath in sys.path:
        sys.path.append(BasicInfo.PPath)
    import ScrumPy

    ScrumPy.SetUI(tkUI)
    from ScrumPy.ModelDescription import SBML

    print(BasicInfo.Greeting)
            
        
    
            
        

