import tkinter
from tkinter import ttk

""" Basic table viewer .
Quite a lot needs doing 

e.g. sensible scrolling scaling and col widths.

"""

class MtxView:
    
    def __init__(self, rnames, cnames, body, master=None):
        """ pre: rnames - list of row names
                 cnames - list of col names
                 body   - contents of table (list of lists)
                 len(body) == len(rnames)
                 len(body[n]) == len(cnames) for n in range(len(rnames))
           
           post: Display the table in a window
       """
           
           
        if master is None:
            master = tkinter.Tk()
        self.master = master
            
        self.Frame = f = tkinter.Frame(self.master)
        f.pack()
        self.Tree  = t = ttk.Treeview(f)
        
        nRows = len(body)

        cnames = ["Row"]+cnames # so we can display the row names as well as contents

        t["columns"] = cnames
        t["height"] = len(rnames)
        for c in cnames:
            t.heading(c, text=c)
            
        for r in range(nRows):
            t.insert("","end", values=[rnames[r]]+body[r])

     
        # horizontal scrollbar
        self.xscrollbar = sbx = tkinter.Scrollbar(f,
                                                 orient=tkinter.HORIZONTAL,
                                                 command=t.xview)
        t['xscrollcommand'] = sbx.set
        sbx.pack(side="bottom",
                fill = 'x',
                expand=tkinter.TRUE)

        # vertical scrollbar
        self.yscrollbar = sby = tkinter.Scrollbar(f,
                                                  orient=tkinter.VERTICAL,
                                                  command=t.yview)
        t['yscrollcommand'] = sby.set
        sby.pack(side='right',
                 fill='y',
                 expand=tkinter.TRUE)
        
        t.pack()
        
        t.pack()
            
      
