

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2019

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import os,  types

try:
    from idlelib import  macosxSupport
except:
    from idlelib import macosx as macosxSupport

##try:
##    from idlelib
##except:
##    from idlelib import editor as EditorWindow


from ScrumPy.ModelDescription import SBML

from idlelib import pyshell

macosxSupport._tk_type="other"

from  . import tkUI
tkinter = tkUI.tkinter


class EdWin(pyshell.EditorWindow):
    def __init__(self, ui, *args, **kwargs):
        pyshell.EditorWindow.__init__(self, *args, **kwargs)
        self.MakeMenu()
        self.ui = ui
        
        self.io.eol_convention = "\n"
        # needed internally by idlelib but EditorWindow.__init__ leaves as None
        # fixes fixed bug in self.WriteFile which otherwise fails for new files.

    
    def close(self, Ask=True):
        
        if self.text != None:# text == None => window already closed
            if Ask:
                pyshell.EditorWindow.close(self)
            else:
                pyshell.EditorWindow._close(self)


    def SaveFile(self, fname):
        if self.text != None:   # text == None => window already closed
            self.io.save(None)
            
           
    def MenuCompile(self):
        self.ui.Reload()

    def MenuSaveSBML(self):
        #fname = FileDialogs.SaveFileName()
        fname = self.io.asksavefile()
        self.ui.SaveSBML(str(fname)) # str because libSBML barfs on unicode

    def MenuHideChildren(self):
        self.ui.HideChildren()

    def MenuShowChildren(self):
        self.ui.ShowChildren()
        

    def MakeMenu(self):

        Menu = tkinter.Menu(self.root)
        Menu.add("command",{"label":"Compile", "command":self.MenuCompile})
        Menu.add("command",{"label":"Save SBML", "command": self.MenuSaveSBML})
        Menu.add("command", {"label":"Hide Children", "command": self.MenuHideChildren})
        Menu.add("command", {"label":"Show Children", "command": self.MenuShowChildren})
       
        self.menubar.add("cascade",{"label":"ScrumPy","menu":Menu})


        self.menubar.delete(4,5) # remove format, run and options menu
        for m in   'run', 'options':
            del(self.menudict[m])
            
        self.menudict["file"].delete(0,6) # remove unwanted items from the file menu
        self.menudict["file"].delete(1)



class  IdleUI(tkUI.tkUI):
    #TODO: Ensure EdWins close properly when model is deleted
    
    def __init__(self):
        tkUI.tkUI.__init__(self)
        self.Editors={}

    def BindModel(self,m):
        tkUI.tkUI.BindModel(self,m)
        
        for e in list(self.Editors.keys()):
            if not e in m.md.FileNames:
                self.Editors[e].close(Ask=False)
                del self.Editors[e]


        if m.md.IsEdit():                    
            for f in m.md.FileNames:
                if not f in self.Editors:
                    filepath = os.sep.join((m.md.Path, f))
                    self.Editors[f] = EdWin(ui=self,filename=filepath, root=self.parent, )
                    
            if (len(m.md.Errors) == len(m.md.Warnings) == 0) and m.md.IsAutoHide():
                self.HideChildren()

            
        self.ReportWarnings()
        self.ReportErrors()
        

    def Close(self):

        for e in self.Editors:
            self.Editors[e].close()
            
            
        
    def ReportErrors(self, MaxRep=10):
        
        LastMsg =""
        errors = self.Model.md.Errors
        n_err = len(errors)
        
        if n_err !=0:
        
            if n_err > MaxRep:
                LastMsg = ["\nA further " + str(n_err - MaxRep) + "errors not reported"]
                errors = errors[:MaxRep]+LastMsg
                
            msg = "\n".join([str(e) for e in errors])
            self.ErrorMsg(msg)

            for err in errors[:MaxRep]:
                fname = err.FName
                line = err.LineNo
                if line.isdigit(): # and  fname in self.Editors ??
                    self.Editors[fname].gotoline(int(line))                    
                # TODO: how to make EdWins with errors stay on top ??
            
            
        
    def ReportWarnings(self, MaxRep=10):
        
        n_err = len(self.Model.md.Errors) 
        warnings = self.Model.md.Warnings
        n_warn = len(warnings)
        LastMsg =""
        
        if  n_err == 0 and n_warn !=0:     
        
            if n_warn > MaxRep:
                LastMsg = "\nA further " + str(n_err - MaxRep) + "warnings not reported"
                warnings = warnings[:MaxRep]

            msg = "\n".join([str(w) for w in warnings])
            self.WarnMsg(msg)
            
            for warn in warnings:
                fname = warn.FName
                line = warn.LineNo
                if line.isdigit():
                    self.Editors[fname].gotoline(int(line))
                
            
            msg = "\n".join(map(str, warnings))+LastMsg
            self.WarnMsg(msg)

          
    def SaveSBML(self,  FileName):
        SBML.Spy2SBML(self.Model,  FileName)
        
        
    def NotifyReload(self):
        
        for e in self.Editors:
            self.Editors[e].SaveFile(e) 
            
        return True


    def ReRead(self, FileName):
        """ Pre: FileName in self.model.md.FileNames
           Post: Reread  FileName from disc, loosing any interactive edits. """

        self.Editors[FileName].io.loadfile(FileName)

        
    def Hide(self):
        for f in self.Editors:
            self.Editors[f].top.iconify()


    def Show(self):
        for f in self.Editors:
            try:
                self.Editors[f].top.deiconify()
            except:
                self.Editors[f] = EdWin(ui=self,filename=filepath, root=self.parent)


        
        
        
    def HideChildren(self):
        #return
        
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.iconify()
                except:
                    print("Exception in HideChildren ignored (Bug?)") # follow up if this is ever seen
                    #FIXME: triggered if user has closed the window - Same for ShowChildren() too?
                
                
    def ShowChildren(self):
        #return
        for f in self.Editors:
            if f != self.RootFile:
                try:
                    self.Editors[f].top.deiconify()
                except:
                    #self.Editors[f] = EdWin(f, self)
                    #CHECKME: see HideChildren()
                    self.Editors[f] = EdWin(ui=self,filename=filepath, root=self.parent)

        
            
        

