

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


#TODO deal with no parameters if all reactions are ~



import random, sys

from ..Data import DataSets
from ..Util import  Sci,  HopcroftKarp

from ..Structural.Model import Model as StructuralModel
from ..Data import FluxDesc

from . import CEval


def LeastConMets(sm):
    """ least connected metabolite(s) in a sto mtx """
#TODO make this a method of sm

    rvd ={}

    for r in sm.rnames:
        rvd[r] = len(sm.InvolvedWith(r))
    least = min(rvd.values())

    for k in list(rvd.keys()):
        if rvd[k] > least:
            del rvd[k]
    return list(rvd.keys())


# a string representing time, avoids use of literals in code
TimeString = "Time"



def SimRange(Start, Stop,  Steps):
    """ scipy linspace without the start value, including the end value
         Because we don't generally want to simulate to the current time"""
         
    rv,s = Sci.linspace(Start,  Stop, Steps,  endpoint=False,retstep=True)
    rv += s
    return rv


class Monitor(DataSets.DataSet):

    def __init__(self, model, Params=[],  InclDerivs=False, UpdateIfNotSS="Ignore",WithPlotter=True):

        ValidUDnotSS =  "Yes","Zero",  "Ignore"
        if UpdateIfNotSS not in ValidUDnotSS:
            raise ValueError ("UpdateIfNotSS must be one of " +  " ".join(ValidUDNSS))
        self.UpdateIfNotSS=UpdateIfNotSS        
            
        self.model=model

    
        Reacs = model.sm.cnames
        Mets  = model.sm.rnames
        if InclDerivs:
            dMets = ["."+s for s in Mets]
        else:
            dMets = []
          
        if Params==[]:
            Params = model.md.GetParamNames()


        DiffNames = model.md.GetExplDiffNames()
        Mets  += DiffNames
        dMets += ["."+ d for d in DiffNames]
            
                
        ItemNames = Reacs + Mets + dMets + Params

        DataSets.DataSet.__init__(self, ItemNames=ItemNames, SrcDic=model)#, WithPlotter=WithPlotter)


    def Update(self):

        m = self.model

        if self.UpdateIfNotSS=="Yes" or m.IsSS():
            self.UpdateFromDic(m)
        elif self.UpdateIfNotSS=="Zero":        
            self.UpdateFromDic({p:m[p] for p in m.ParamNames})
            # ie all variables 0 but params filled in
        self.UpdatePlot()
            

class EmptyMatrix:

    rnames=[]
    cnames=[]
    Externs=[]

    def __init__(self, *a, **k):
        pass

    def __len__(self):
        return 0

    def __nullmeth__(self, *a, **k):
        pass

    def __getattr__(self, a):
        return self.__nullmeth__

class Model (StructuralModel):
    """ The Kinetic model class    """
    def __init__(self, md, ui=None):

        StructuralModel.__init__(self, md, ui)
        
        Valid = len(md.Reactions) >0 or md.ExplicitOnly()

        if Valid and len(md.Errors) ==0 :
            try:
                self.InitKin()
                self.OK = False # lsat sim or sol call was successful
            except Exception as Ex:
                md.InternalError(Msg="Kinetic.Model.__init__()", Excpn=Ex)


    def Reload(self):
               
        StructuralModel.Reload(self)
        if len(self.md.Errors) ==0:

            if self.md.IsKinetic():
                self.InitKin()
            elif hasattr(self,  "Evaluator"):
                del self.Evaluator
                # we have changed from kinetic to structural, deleting the evaluator means that kinetic functions
                # will raise an exception as would a model that was structural from the start
            
    def InitKin(self, SSTol=1e-6):

        md = self.md
        self.ParamNames = md.GetParamNames()
        self.SSTol = SSTol
        
        if not md.ExplicitOnly() : # no stoichiometry if explicit only or no reactions
    
            self.cmtx = cm = self.ConsMoieties().Copy(Conv=float)
            cm.cnames = ["CSUM_" + str(s) for s in range(len(cm.cnames))]
    
            for name in cm.cnames:  # calculate the conserved totals
                c = 0
                for r in cm.rnames:
                    c += float(md.Metabolites[r]) * cm[r, name]
                md.NewParam(name, c)
        else:
            self.sm = self.smx = self.cmtx = EmptyMatrix()

        md.NewParam("Time")
        self.Evaluator = CEval.Evaluator(self)
        
        if self.Evaluator.LLKM == None:
            print("Error: couldn't compile/load model (this is a bug!!)", file=sys.stderr)
        else:
    
            for attr in [
            #'GetMets',
            'GetIndepMets',
            'GetDerivs',
            #'GetRates',
            'GetParams',
            'Eval',
            'CalcVels', 
            'GetSSErr'
            ]:
                setattr(self, attr, getattr(self.Evaluator, attr))
    
            self.GetInitState()
    
            self["Time"]=0.0
    
            self.DynMons = []
            self.StatMons = []



    def __repr__(self):
        return "ScrumPy kinetic model"


    def __del__(self):
        self.Destroy()
        #StructuralModel.__del__(self)

    def Destroy(self):
        if hasattr(self, "LowLevel"):
            self.Evaluator.Destroy()

    def __getitem__(self, name):
        return self.Evaluator.GetVal(name)

    def __setitem__(self, name, val):
        self.Evaluator.SetVal(name, val)

    def IsSS(self):
        return self.GetSSErr() < self.SSTol

    def ShowDepInfo(self):
        print(self.Evaluator.DepReport)



    def GetInitState(self):
        # TODO: ensure GetIndepMets() returns correct order
        self._InitState = self.GetIndepMets()


    def SetInitState(self):
        self.SetIndepMets(self._InitState)
        self.Eval()


    def GetEquilDeps(self):
        """ sets of mets involved in equilibrium relationships """

        reacs = {}
        rv = []
        
        EQReacs = self.md.GetEquilReacs()

        for reac in EQReacs:
            reacs[reac] = list(self.sm.InvolvedWith(reac).keys())
        
        h = HopcroftKarp.HopcroftKarp(reacs) # TODO modify hk so as not to change its input!
        # See refs in the HopcroftKarp module - is this the right/best way to achieve this ??
        mm = h.maximum_matching()
        
        for reac in EQReacs:
            rv.append(mm[reac])
            
        return rv


    def GetConsMets(self):
        """ sets of mets inovlved in conservation relationships """

        rv = []
        for c in self.cmtx.cnames:
            rv.append(list(self.cmtx.InvolvedWith(c).keys()))
        return rv
    

    def GetEigValsJ(self):
        print("GetEigVals not implemented")
        
        
    def GetJac(self):
        print("GetJac not implemented")


    def GetRates(self):
        return [self[reac] for reac in self.sm.cnames]

    def GetFluxDic(self):
        return {reac:self[reac] for reac in self.sm.cnames}

    def GetMets(self):
        return [self[met] for met in self.sm.rnames]

    def GetMetDic(self):
        return {met:self[met] for met in self.sm.rnames}

    def GetDerivs(self):
        return [self["."+met] for met in self.sm.rnames]

    def GetDerivDic(self):
        return {met:self["."+met] for met in self.sm.rnames}
        
        

    def Simulate(self, StepSize=0.01, NSteps=1):
        
        self.Evaluator.ResetInteg(self["Time"]) # because model might have ben changed since last call

        for n in range(NSteps):
            self.Evaluator.Integrate(StepSize)
            self.UpdateDynMons()

            
    def SimTo(self,  Time,  nSteps):
        
        Now = self["Time"]
        
        Times = SimRange(Now,  Now+Time,  nSteps)
        nVals = self.Evaluator.GetNVals()
        tidx = self.Evaluator.GetTimeIdx()
        res = Sci.zeros((nSteps, nVals))
        res[:, tidx] = Times
        
        self.Evaluator.SimTo(res)
        
        rv = DataSets.DataSet(ItemNames=self.GetAllNames(Diffs=True))
        rv.rows= res.tolist()
        
        return rv


    def SetSSTol(self, tol=1e-6):

        self.SSTol= tol



    def FindSS(self, SimTime=0.1, SimSteps=15):
        
        t = self["Time"]

        while self.GetSSErr()> self.SSTol*10 and SimSteps >0:
            self.Evaluator.Integrate(SimTime)
            SimSteps -=1
        
        
        self.Evaluator.Solve()
        self.Evaluator.ResetInteg(t)
        self["Time"] = t # reset ISP as well ?
        self.UpdateStatMons()
      

    def GetVals(self, names):
        """ pre: names is a list of strings
           post: returns a list of the values of names """

        rv = []
        for n in names:
            rv.append(self[n])

        return rv

    def SetVals(self, names, vals):
        """ pre: names is a list of strings, vals is a list of float,
                 len(names) == len (vals)
           post: GetVals(names) == vals"""

        for i in range(len(names)):
            self[names[i]] = vals[i]


    def Update(self, dic):
        for k in dic:
            self[k] = dic[k]




    def GetAllNames(self):#,  Diffs=False):
        """ return a list (of strings) of all identifiers in the model
             Diffs: include names of the met differential as ".MetName" 
        """
        pNames = self.md.GetParamNames()
        rNames = self.sm.cnames
        mNames = self.sm.rnames + self.md.GetExplDiffNames()
        dNames = ["."+s for s in mNames]
            
        return(
            pNames +
            rNames +
            mNames +
            dNames
        )

    def keys(self):
        return self.GetAllNames()
    """
    because models anyway have dictionary-like characteristics we supply
    a keys function for completeness. This is useful for the monitor
    classes which can then be updated from any dicionary-like instance
    """

    def GetParamNames(self, ExclExts=True):
        """ ExclExts => do not include external mets"""

        names = [n for n in self.Evaluator.NamesToSymDic if self.Evaluator.NamesToSymDic[n].startswith("_P_")]
        if ExclExts:
            return [name for name in names if not name in self.smx.rnames]
        return names

    def PingConc(self, pert=1e-6):
        """ perturb all concentrations by multiplying by a random number [1-pert..1+pert]"""

        for met in self.md.GetIntMetNames():
            self[met] *=  random.uniform(1-pert,1+pert)



    def ScaledSensits(self, pname, Vars, Scaled=True,  SS=True, Pert=1e-3):
#TODO PingConc(pert)
        if SS:
            def Eval():
                self.PingConc()
                self.FindSS()
        else:
            Eval = self.CalcVels

        pval = self[pname]
        delta_p = pval*Pert
        TwodP = 2 * delta_p

        if Scaled:
            Eval()
            midvars = self.GetVals(Vars)   # mid of 3 point differential

        self[pname] += delta_p
        Eval()
        hivars = self.GetVals(Vars)         # high point of 3 point diff

        self[pname] -= TwodP
        Eval()
        lovars = self.GetVals(Vars)         # low point of 3 point diff

        self[pname] = pval

        dydx = list(map(lambda yh, yl:(yh-yl)/(TwodP),  hivars, lovars))
        if Scaled:
            dydx = list(map(lambda d, y:d*pval/y, dydx, midvars))

        return dydx




    def AddDynMonitor(self, mon=None, *args, **kwargs):
        if mon != None:
            self.DynMons.append(mon)
        else:
            mon = Monitor(self, UpdateIfNotSS="Yes", *args, **kwargs)
            mon.SetPlotX("Time")
            self.DynMons.append(mon)
            return mon


    def AddStatMonitor(self,mon=None,  *args,  **kwargs):
        if mon != None:
            self.StatMons.append(mon)
        else:
            mon = Monitor(self, *args,  **kwargs)
            self.StatMons.append(mon)
            return mon


    def UpdateDynMons(self):
        self.Eval() # the integrate leaves dMet in an undefined state so recalculate it
        for mon in self.DynMons:
            mon.Update()


    def UpdateStatMons(self):
        for mon in self.StatMons:
            mon.Update()

    def RemMon(self, mon):
        """ remove mon from static and dynamic monitor lists """
        if mon in self.StatMons:
            del self.StatMons[self.StatMons.index(mon)]

        if mon in self.DynMons:
            del self.DynMons[self.DynMons.index(mon)]

    def GetDepSols(self):
        '''
                pre     :   True
                post    :   Dependency solutions returned as list of strings
        '''
        sols = self.Evaluator.DepReport.sol
        rv = []
        for sol in sols:
            rv.append(str(sol)+' = '+str(sols[sol]))
        return rv











