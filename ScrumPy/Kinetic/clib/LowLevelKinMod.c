

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/********

M.G. Poolman 11/5/2001

LowLevelKinMod.c -- C++ class interface to Scampi model API,
to act as a "shadow class" for python via swig

********/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<dlfcn.h>

#include<cvode/cvode.h>

#include"LowLevelKinMod.h"


/***

build this module
g++ -I/usr/include/python2.7 -fPIC -shared -o _LowLevelKinMod.so LowLevelKinMod.c LowLevelKinMod_wrap.cxx -lsundials_cvode -lsundials_nvecserial

build the test module
gcc -shared -fPIC test.c -o test.so

***/

void PrintDVec(double *vec, int len, FILE *fp){

    int n ;

    if (fp == 0)
        fp = stderr ;

    for (n=0 ; n<len ; n++)
        fprintf(fp, "%g\t",vec[n]) ;

    fprintf(fp, "\n") ;
}


LowLevelKinMod::LowLevelKinMod(){
    fprintf(stderr, "\nLLKM constructor\n") ;
}


LowLevelKinMod::~LowLevelKinMod(){
    if (nIndeps>0){
        Del_ISP(ISP) ;
        Del_SSP(SSP) ;
    }
    dlclose(dlib) ;


    fprintf(stderr, "\nLLKM destructor\n") ;
}



void LowLevelKinMod::PrintMsg(char *msg){
    fprintf(stderr,"%s", msg) ;
}

void LowLevelKinMod::Load(char *libname, double rtol, double atol){

    dlib = dlopen (libname, RTLD_NOW) ;
    if(!dlib){
        fprintf(stderr,"\n!!\n!! Load_so - dlopen(%s) failed because %s !!\n!!\n", libname, dlerror()) ;
    }
    else{

        nMets     = *(int *) dlsym(dlib, "nMets")   ;
        nIndeps  = *(int *) dlsym(dlib, "nIndeps") ;
        nRates    = *(int *) dlsym(dlib, "nRates")  ;
        nParams  = *(int *) dlsym(dlib, "nParams") ;
        t_Idx       =*(int *) dlsym(dlib, "t_Idx") ;

        Mets     = (double *) dlsym(dlib, "Mets")   ;
        dMets    = (double *) dlsym(dlib, "dMets") ;
        Rates    = (double *) dlsym(dlib, "Rates")  ;
        Params = (double *) dlsym(dlib, "Params") ;


        DepEqns  = (vvFunc_t)  dlsym(dlib, "DepEqns")  ;
        RateEqns = (vvFunc_t)  dlsym(dlib, "RateEqns") ;
        DiffEqns = (vdpFunc_t) dlsym(dlib, "DiffEqns") ;

        Eval = (ModelFunc_t) dlsym(dlib, "Eval") ;
        
        if (nIndeps >0){
            ISP = New_ISP(nIndeps, Mets, (ModelFunc_t) dlsym(dlib, "Eval"), rtol, atol)  ;
            SSP = New_SSP(nIndeps, Mets, (hybrd1_func_t ) dlsym(dlib, "hybrd1_func"))  ;
        }
    }
}




int  LowLevelKinMod::Integrate(double t){
    int rv ;
    if (nIndeps >0){
        rv  =  Integrate_ISP((Integ_ScratchPad_t) ISP, t) ;
        Params[t_Idx] = ISP->t ;
    }
    else 
        rv = -99 ;
    return rv ;
}

void LowLevelKinMod::UpdateBuffer(double *buf){

    double *src ;
    int  n, dst_idx =0;
    size_t nBytes, lensrc, src_idx ;
    char names[] = "PRMD" ; // parameters, rates, netabolites, differentials
    
    UpdateVals() ;
    
    for (n = 0 ; n<4 ; n++){
        GetBufInfo(names+n, &src, &lensrc) ; 
        for (src_idx = 0 ; src_idx < lensrc ; src_idx++){
            buf[dst_idx] = src[src_idx] ; // TODO: why not use memcpy??
            dst_idx++ ;
        }
    }
}

int LowLevelKinMod::SimTo(PyObject *o){

	PyArrayObject *arr =  (PyArrayObject *) o ;
	npy_intp *dims, nRows, nCols, CurRow ;
    int err = 0 ;
	double  *CurBuf, CurTime, NextTime ;

	dims = PyArray_DIMS(arr) ;
    nRows = dims[0] ;
    nCols = dims[1] ;
    
	for (CurRow = 0 ; CurRow < nRows && err==0 ; CurRow++){
        CurBuf = (double *) PyArray_GETPTR1(arr, CurRow) ;
        CurTime  = Params[t_Idx] ; 
        NextTime = CurBuf[t_Idx] ;
        err = Integrate(NextTime-CurTime) ; 
        UpdateBuffer(CurBuf) ;
	}

	return 0 ;
}

 
void LowLevelKinMod::ResetInteg(double t, double rtol, double atol) {
    if (nIndeps >0)
        Reset_ISP(ISP,t,rtol, atol) ;
}

int LowLevelKinMod::Solve(void){
    return Solve_SSP((Hybrd1_Scratchpad_t) SSP) ;
}

void LowLevelKinMod::UpdateVals(void){
    Eval(ISP->t, dMets) ;
}

void LowLevelKinMod::SetNthMet(int n, double val) {
    Mets[n] = val ;
}

void LowLevelKinMod::SetNthParam(int n, double val) {
    Params[n] = val ;
}

void LowLevelKinMod::SetVals(char *What, PyObject *InList){

  size_t n, max_n ;
  double *vec ;
      
  if (GetBufInfo(What, &vec, &max_n)){
        for (n = 0 ; n< max_n ; n++){
            vec [n]  = PyFloat_AsDouble(PyList_GetItem(InList, (Py_ssize_t) n)) ;
        }
    }
    else{       
            fprintf(stderr, "\nUnknown request in GetVals %s\n", What) ;
    }
} ;







double LowLevelKinMod::GetSSErr(void) {

    double sum = 0.0;
    int n ;

    UpdateVals() ;
    for (n = 0 ;  n<nIndeps ;n++ )
        sum += dMets[n] * dMets[n] ;

    return sqrt(sum) ;
}


double LowLevelKinMod::GetNthMet(int n) {
    return Mets[n] ;
}

double LowLevelKinMod::GetNthDiff(int n) {
    return dMets[n] ;
}

double LowLevelKinMod::GetNthRate(int n) {
    return Rates[n] ;
}

double LowLevelKinMod::GetNthParam(int n) {
    return Params[n] ;
}

int LowLevelKinMod::GetBufInfo(char *What, double **Buf, size_t *Len){
    
    int OK  = 1;
    
   switch(What[0]){
        case 'P' :
            *Buf = Params ;
            *Len = nParams ;
            break ;
        case 'M' :
            *Buf =  Mets ;
            *Len =  nMets ;
        break ;
        case 'D' :
            *Buf =  dMets ;
            *Len =  nMets ;
        break ;
        case 'R' :
            *Buf =  Rates ;
            *Len =  nRates ;
        break ;
        default:
           OK = 0 ;
    }
    
    return OK ;
}



void LowLevelKinMod::GetVals(char *What, PyObject *OutList){

  size_t n, max_n ;
  double *vec ;

    if (GetBufInfo(What, &vec, &max_n)){      
        for (n = 0 ; n< max_n ; n++){
            PyList_SetItem(OutList, (Py_ssize_t) n, PyFloat_FromDouble(vec[n])) ;
        }
    }
    else{
        fprintf(stderr, "\nUnknown request in GetVals %s\n", What) ;
    }
}


#ifdef LLKM_TEST
int main(int argc, char **argv){

  LowLevelKinMod m ;


    fprintf(stderr,"\nLLKM TEST\n") ;
    fprintf(stderr, "Got m\n");
    m.Load("./test.so") ;
    fprintf(stderr, "Got ./test.so\n");
    m.Integrate(0.01) ;
    m.Integrate(0.01) ;



    return 0 ;

}
#endif
