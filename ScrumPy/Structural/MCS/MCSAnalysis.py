

from Util import  Set

   ### MCS analysis ###
    
def GetMCSForTargs(modes_db, targs, max_s=None, warn = False):
    '''
        pre     :       modes_db is an ModesDB object
                            targs is subset of modes.mo.rnames
                          warn - T|F, passed to modes_db .checkBinaryAndInform(), T - prints warning when modes_db.ModesOf() is called
                          default F.
                          
        post    :       Minimal cut sets (list of list) for abolishing reactions in targs have been returned.
    '''
    block_modes = []
    for targ in targs:
        block_modes.extend(list(modes_db.ModesOf(targ, show_warning = warn).keys()))
    sub_mtx = modes_db.mo.SubSys(block_modes)
    mcs = FindMCS(modes_db, sub_mtx,max_size=max_s)
    return mcs
    
def MCSToPromoteModes(modes_db, modes, max_s=None):
    '''
        pre     :      modes_db an ModesDB object
                            modes is subset modes_db.mo.cnames
        
        post    :       Minimal cut sets for abolishing the subsets of modes_db.mo.cnames that is not in modes have been returned
    '''
    rv = []
    sub_mtx = modes_db.mo.SubSys(Set.Complement(modes_db.mo.cnames,modes))
    modes_reac = modes_db.mo.SubSys(modes).rnames
    cand_rxn = Set.Complement(sub_mtx.rnames,modes_reac)
    mcs = FindMCS(modes_db,sub_mtx,max_size = max_s)
    for cs in mcs:
        if Set.IsSubset(cs, cand_rxn):
            rv.append(cs)
            
    return rv

def FindMCS(modes_db,  subsys, max_size = None,  warn = False):
    ''' MCS algorithm as defiend in Klamt (2004)
        pre     :      modes_db is an ModesDB object
                        subsys is a sub system of modes_db.mo, such that all modes in subsys should be abolished
                         max_size - maximal size of cut any MCS, by default len(modes_db.mo.rnames).
                         warn - passed to modes_db.checkBinaryAndInform(), if T warning is printed whenmodes_db.ModesOf() is called,
                         by default set to F.
                         
        post    :     MCSs up to size max_size have been returned.
    '''
    if max_size == None:
        max_size = len(subsys.rnames)
    mcs = []
    ess = GetEssentials(subsys)
    mcs.extend([[s] for s in ess])
    redundants = Set.Complement(subsys.rnames,  ess)
    precut = MakePreCutSets(redundants)
    i = 2
    new_precuts = [1]   #new_precuts needs to have len > 0 initially, then reset for each iteration
    while len(new_precuts)!=0  and (i < max_size):
        print(i)
        new_precuts =[]
        for reac in redundants:
            RemoveFrPre(reac,  precut)
            mtx_inv = subsys.SubSys(Set.Intersect(list(modes_db.ModesOf(reac, show_warning = warn).keys()),  subsys.cnames))
            temp_presets = []
            for pre_set in precut:
                if not DoesSetCoverAllEMs(mtx_inv,  pre_set):
                    new_precut = pre_set[:]
                    new_precut.append(reac)
                    temp_presets.append(new_precut)
            RemoveSupers(temp_presets,  mcs)
            for cs in temp_presets:
                if DoesSetCoverAllEMs(subsys,  cs):
                    mcs.append(cs)
                else:
                    new_precuts.append(cs)
        i+=1
        if len(new_precuts)!=0:
            precut = new_precuts[:]

    return mcs

def RemoveSupers(new_list,  ref_list):
    '''   private: called by FindMCS() to remove supersets from list of candidate cut sets  '''
    for new in new_list[:]:
        for ref in ref_list:
            if Set.IsSubset(ref,new) and new in new_list:
                new_list.remove(new)

def RemoveFrPre(reac,  precut):
     '''   private: called by FindMCS() to remove sets in precut which contain reac  '''
     for cs in precut[:]:
        if reac in cs:
            precut.remove(cs)


def MakePreCutSets(reacs):
    '''   private: called by FindMCS() to create a list of (singleton) lists each containing an item in reacs  '''
    return [[r] for r in reacs]

def GetEssentials(mtx):
     '''   private: called by FindMCS() to identify all essential reactions (i.e. rows where all elements are non-zero)  '''
     return [row for row in mtx.rnames if all(mtx.GetRow(row))]


def DoesSetCoverAllEMs(mtx, reac_set):
    ''' private: called by FindMCS() to checks if reacs_set if removed abolishes all modes in mtx '''
    mtx_cop = mtx.Copy()
    for r in Set.Complement(mtx.rnames, reac_set):
        mtx_cop.DelRow(r)
    mtx_cop.Transpose()
    mtx_cop.ZapZeroes()
    mtx_cop.Transpose()
    
    return len(mtx_cop.cnames) ==len(mtx.cnames)
