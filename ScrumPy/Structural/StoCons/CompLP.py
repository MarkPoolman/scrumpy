#import Util.Set
from ScrumPy import Util

from . import MtxLP
parent = MtxLP.MtxLp
from . import  Mtx

class CompLp(parent):


    def __init__(self, mtx, direc = 'Min', klass = 'LP', lowBound = 0, upBound = None, name = 'Unnamed transposed stoichiometry matrix problem'):

        parent.__init__(self, mtx, direc = direc, klass = klass, lowBound = lowBound, upBound = upBound, name = name)
        self.Externs = mtx.Externs[:]
        self.Reacs = mtx.rnames[:]

##access

    def GetReacs(self):
        return self.Reacs[:]


    def GetColNames(self, kind = 'Continuous'):
        rv = []
        if kind == 'Metabolite':
            rv = self.GetMets()
        else:
            rv = parent.GetColNames(self, kind)
        return rv            


    def GetExterns(self):
        return self.Externs[:]


    def GetMets(self):
        return self.Cnames[:]



    def GetSolution(self, kind = 'Metabolite', *args, **argd):
        return parent.GetSolution(self, kind = kind, *args, **argd)

        
##deleting

    def DelReacs(self, rnlist):
        parent.DelRows(self, rnlist)
        self.Reacs = Util.Set.Complement(self.Reacs, rnlist)        


    def DelMets(self, metlist):
        parent.DelCols(self, metlist)
        self.Externs = Util.Set.Complement(self.Externs, metlist) 

##conservation analysis


    def IsConserved(self, met):
        return not self.IsIdenticallyZero(met)



    def ConservedMets(self):
        """pre: MIP"""
        self.SetObjDirec('Max')
        return list(self.OptLenSol().keys())



    def UnconservedMets(self):
        """pre: MIP"""
        self.SetObjDirec('Max')
        return Util.Set.Complement(self.GetMets(), self.ConservedMets())    



    def ExternalUncoupled(self, externs = []):
        externs = externs or self.GetExterns()
        rv = Util.Set.Complement(self.GetMets(), externs)
        for intern in rv[:]:
            do, i = True, 0
            while do and i < len(externs):
                if self.GetCouplingType(intern, externs[i]) != '||':
                    do = False
                    rv.remove(intern)
                i += 1                
        return rv


    ###########################
    ##Whole-network consistency and subset consistency

    def IsConsistent(self):
        """pre: N is a transposed stoichiometry matrix.
        post: IsConsistent(N) is True, if N is stoichiometrically consistent
        (i. e. having an all-positive solution x of Nx = 0) and False, otherwise"""
        return self.HasPositiveSol()


    def IsSubsetConsistent(self, rnlist):
        compl = Util.Set.Complement(self.GetReacs(), rnlist)
        stodct = self.ExtractRows(compl)
        rv = self.IsConsistent()
        self.RestoreRows(stodct)
        return rv

    

    def MaxConsistentSet(self, quest = []): #corrects = []):

        rv = []
    
        ##remove and save tested rows
        quest = quest or self.GetReacs()
        corrects = Util.Set.Complement(self.GetReacs(), quest)
##        quest = Util.Set.Complement(self.GetReacs(), corrects)
        stodct = self.ExtractRows(quest)

        ##test the rows
        if self.IsConsistent():
            rv = corrects[:]
            for rn in quest:
                self.AddConstraint(sto = stodct[rn], lowBound = 0, upBound = 0, name = rn)
                if self.IsConsistent():
                    rv.append(rn)
                    del stodct[rn]
                else:
                    self.DelRows([rn])             

        ##restore
        self.RestoreRows(stodct)             

        return rv    


##
##    ####################################################################
##    ##submaximal consistent subsystem
##
                
##
##
##    def MinResolvingSet(self, corrects = []):        
##        maxcons = self.MaxConsistentSet(corrects = corrects)
##        return Util.Set.Complement(self.GetReacs(), maxcons)
##
##
##    ###################################################################
##    ##resolvers
##    
##    def IsResolver(self, rn):
##        sto = self.ExtractRows([rn])
##        rv = self.IsConsistent()
##        self.RestoreRows(sto)
##        return rv
##                   
##
##    def Resolvers(self, suspects = []):
##        rv = []
##        suspects = suspects or self.GetReacs()
##        for rname in suspects:
##            if self.IsResolver(rname):
##                rv.append(rname)
##        return rv    
##        
##
##    def ResolvingTable(self, submin = None, suspects = []):
##
##        rv = matrix(cnames = self.GetReacs())
##        submin = submin or self.MinResolvingSet()
##        suspects = suspects or self.GetReacs()
##
##        stodct = self.ExtractRows(submin)
##
##        for rname in submin:
##            self.AddConstraint(stodct[rname], 0, 0, rname)
##            resolvers = self.Resolvers(Util.Set.Intersect(self.GetRowNames(), suspects))
##            rv.NewRow(map(lambda x: x in resolvers, rv.cnames), rname + '$')
##            self.DelRows([rname])
##
##        self.RestoreRows(stodct)            
##        return rv
##
##
##
##    def IsLocalResolver(self, rn, rnlist):
##        rnlist = rnlist[:]
##        rnlist.remove(rn)
##        return self.IsSubsetConsistent(rnlist)
##
##
#######################################################################
####localisation
##
##    def ExtractedLocus(self, rnlist = [], fun = None):
##        fun = fun or (lambda rn, rnlist: not self.IsLocalResolver(rn, rnlist))
##        return parent.ExtractedLocus(self, fun = fun, rnlist = rnlist)
##
##
##    def DisjointLoci(self, rnlist = [], testfun = None, extractfun = None):
##        extractfun = extractfun or (lambda rn, rnlist: not self.IsLocalResolver(rn, rnlist))
##        testfun = testfun or (lambda rnlist: not self.IsSubsetConsistent(rnlist))
##        return parent.DisjointLoci(self, testfun = testfun, extractfun = extractfun, rnlist = rnlist)        
