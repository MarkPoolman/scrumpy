ZEROROUND = 8
ZEROVAL = pow(10, -ZEROROUND)


def IsZero(val):
    return abs(val) <= ZEROVAL


def IsAllZero(seq):
    bools = [IsZero(x) for x in seq]
    return not False in bools

def RoundFloat(val, places = 3):
    return float('%.*f' % (places, val))

def RoundSto(sto):
    for k in sto:
        sto[k] = RoundFloat(sto[k])

def AreEqual(val1, val2):
    return IsZero(val1 - val2)
