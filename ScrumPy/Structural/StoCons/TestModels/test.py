import ScrumPy


#m = ScrumPy.Model("calvin1.spy")

def Kinetic(m):

    m["LR_vm"] = 3500
    while m["LR_vm"] > 200:
        m["LR_vm"] -= 100
        m.FindSS()
        print(m["LR_vm"], m["Rubisco"], m.NewtSS(), m.IsOK())

    
    while m["LR_vm"] < 3500:
            m["LR_vm"] += 100
            m.FindSS()
            print(m["LR_vm"], m["Rubisco"], m.NewtSS(), m.IsOK())


def Simulate(m):
    
    m["LR_vm"] = 3500
    while m["LR_vm"] > 200:
        m["LR_vm"] -= 100
        m.Simulate(0.01,100)
    
    while m["LR_vm"] < 3500:
            m["LR_vm"] += 100
            m.Simulate(0.01,100)
            
