
from ScrumPy.LP import ScrumPyLP, glpk_ll, glpk
EPSILON =1e-12
class mip(glpk.lp):


    def __init__(self, mtx, direc = 'Min', lowBound = 0, upBound = None, name = 'Unnamed mtx problem'):

        glpk.lp.__init__(self)

       
        self.AddRows(mtx.rnames[:])
        self.AddCols(mtx.cnames[:])
        self.Cnames = mtx.cnames
        
        for rname in mtx.rnames:
            row = [float(v) for v in mtx[rname]]
            self.SetRowVals(rname, row)
            self.SetRowBounds(rname, 0, 0)

        for cname in mtx.cnames:
            self.SetColBounds(cname, lowBound, upBound)

        self.SetObjDirec(direc)

        self.tmp = []


    def SetLenObjective(self, varlist = []):
        """pre: varlist ???
        post: the length (i. e. number of nonzero components) of a solution is optimised"""
        
        ContNames = varlist or self.GetColNames('Continuous')
        NewCols = ["_INT_" + name for name in ContNames]
        NewRows = ["_CNT_" + name for name in ContNames]
        self.AddIntCols(NewCols)
        self.AddRows(NewRows)

        coef = {'Max' : 1, 'Min' : -1}[self.GetObjDirec()]  ##max: int <= cont; min : int >= cont
        
        for Cont,Row,Col in zip(ContNames, NewRows, NewCols):
            constraint = {Cont:coef, Col:-coef}
            self.SetRowFromDic(Row,constraint)
            self.SetRowBounds(Row, 0, None)
            self.SetColBounds(Col,0,1)

        self.ClearObjective()
        self.SetObjCoefsFromLists(NewCols,[1]*len(NewCols))

 

    def UnConservedMets(self):
        
        self.SetObjDirec('Max')
        self.SolveOptLen()
        sol = self.GetMIPSolDic()
        return [k for k in sol if k[0]!="_" and sol[k] == 0]



    def SolveOptLen(self, pos = [], lowBound = 1, *args, **argd):
        self.MakePositive(pos, tmp = True, lowBound = lowBound)
        cont = self.GetColNames('Continuous')
        self.SetLenObjective(cont)
        self.Solve()
        self.MIPSolve()
        

    
    def MakePositive(self, pos = [], tmp = False, lowBound = EPSILON):
        for targ in pos:
            self.AddConstraint({targ : 1}, lowBound, None, tmp = tmp)


    def AddConstraint(self, sto = None, lowBound = 0, upBound = 0, name = None):
        name = name or 'Row ' + str(self.GetNumRows() + 1)
        self.AddRows([name])
        self.SetRowBounds(name, lowBound, upBound)
        if type(sto) == dict:
            self.SetRowFromDic(name, sto)
        elif type(sto) == list:
            self.SetRowVals(name, sto)
      











##
##    def UnconservedMets(self):
##    
##    
##        self.SetObjDirec('Max')
##        return Util.Set.Complement(self.GetMets(), self.ConservedMets())    
##






            
##    
##    def SetLenObjective(self, varlist = []):
##        """pre: varlist ???
##        post: the length (i. e. number of nonzero components) of a solution is optimised"""
##        
##        ContNames = varlist or self.GetColNames('Continuous')
##        NewCols = ["_INT_" + name for name in ContNames]
##        NewRows = ["_CNT_" + name for name in ContNames]
##        self.AddIntCols(NewCols)
##        self.AddRows(NewRows)
##
##        coef = {'Max' : 1, 'Min' : -1}[self.GetObjDirec()]  ##max: int <= cont; min : int >= cont
##        
##        for Cont,Row,Col in zip(ContNames, NewRows, NewCols):
##            constraint = {Cont:coef, Col:-coef}
##            self.SetRowFromDic(Row,constraint)
##            self.SetRowBounds(Row, 0, None)
##            self.SetColBounds(Col,0,None)
##            
##        self.SetObjective(NewCols)

##
##  
##    def AddConstraint(self, sto = None, lowBound = 0, upBound = 0, name = None):
##        name = name or 'Row ' + str(self.GetNumRows() + 1)
##        self.AddRows([name])
##        self.SetRowBounds(name, lowBound, upBound)
##        if type(sto) == dict:
##            self.SetRowFromDic(name, sto)
##        elif type(sto) == list:
##            self.SetRowVals(name, sto)
##      
##        
##    def SetObjective(self, obj = None):
##        self.ClearObjective()
##        objtype = type(obj)
##        if objtype == dict:
##            self.SetObjCoefsFromDic(obj)
##        elif objtype == list:
##            self.SetObjCoefsFromLists(obj, [1] * len(obj))

##     
##    def SetRowFromDic(self, name, dic):#TODO - rethink and move to glpk.py
##        row = [0] * self.GetNumCols()
##        for var in dic:
##            nvar = self.GetColIdxs([var])[0]
##            row[nvar - 1] = dic[var]
##        self.SetRowVals(name, row)
##


        
"""
print the mip sol
for c in mp.GetColNames():
	print (c, mp._glp_mip_col_val(mp.GetColIdxs([c])[0]))
"""
##
##
##    def MinLenSolutions(self, nmax = -1, disjoint = False, *args, **argd):
##        cont = self.GetColNames('Continuous')
##        self.SetLenObjective(varlist = cont, tmp = True)
##        rv = self.MinSolutions(nmax = nmax, disjoint = disjoint, *args, **argd)
##        self.CleanTemps(nlast = 1 + len(cont))
##        return rv
##
##
####positive nullspace
##
##    def GetNonNegNullSpace(self, dim, cnames):
##        """pre: self.GetClass() == MIP, dim is the nullspace dimension, cnames is the list of continuous variables in correct order.
##        post: rv = non-negative nullspace matrix of the LP constraint matrix"""
##                
##        rv = Mtx.StoMat(rnames = cnames, Conv = float)
##
##        self.SetObjDirec('Min')
##        self.SetLenObjective(tmp = True)
##        self.AddSumConstraint(cnames = cnames, tmp = True)
##                
##        while len(rv.cnames) < dim:
##            self.Solve()
##            if self.GetStatusMsg() != 'MIP Optimal':
##                #raise exceptions.Exception("Failed to calculate a non-negative nullspace")
##                print("Failed to calculate a non-negative nullspace")
##                return
##            colmtx = self.GetSolution(kind = 'Continuous', asmatrix = True)
##            rv.NewCol(colmtx.GetCol(0))
##            intsol = self.GetSolution(kind = 'Integer')
##            self.SetIntegerCut(intsol, tmp = True)
##
##        self.CleanTemps()
##        return rv            

