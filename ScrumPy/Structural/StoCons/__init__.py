
from ScrumPy.Structural import StoMat
#from . import Mtx, CompLP
from . import MIP, MtxLP


def UnconservedMets(m):
    return MIP.mip(m.smx.Copy(tx=1)).UnConservedMets()

#############################################################################
##stoichiometric inconsistencies
##
def UnconsMtx(m, uncons_mets = [], gaussjordan = True):
    """pre: Nt is a transposed stoichiometry matrix, uncons_mets is a list of unconserved metabolites.
    post: MinUnconsMtx(Nt) is a matrix of minimal inconsistent net stoichiometries in Nt"""


    
    Nt = m.smx.Copy(tx=1)
    Kt = Nt.NullSpace()
    Kt.Transpose()
    rv = StoMat.StoMat(rnames = Nt.cnames, Conv = float)
    
    
    if len(Kt) > 0:
        P = MtxLP.MtxLp(Kt, 'Min', 'MIP')
        for met in uncons_mets:
            if Mtx.IsAllZero(Kt.GetCol(met)):
                rv.NewCol()
                rv[met, -1] = 1
            else:
                P.MakePositive([met], tmp = True)
                vecs = P.MinLenSolutions(asmatrix = True)
                for col in vecs:
                    col = col.GetCol(0)
                    if not Mtx.ContainsCol(rv, col):
                        rv.NewCol(col)
                P.CleanTemps()
    else:
        for met in uncons_mets:
            rv.NewCol()
            rv[met, -1] = 1
    return rv


def LeakageModeMtx(m, uncons_vecs, all = False):
    '''
        pre     :   smx - external stoichiometry matrix
                    uncons_vecs - matrix of inconsistent net stoichiometries, return-value of
                    UnconsMtx(...)
                    all - calculate all leakage modes based on elementary modes, False by default

        post    :   returns matrix of leakage modes (rv.rnames = smx.rnames, rv.cnames = modes), if
                    all is true, all leakage modes of the model are in rv, otherwise modes are based on
                    nullspace and cannot be guarantied to be complete. smx and uncons_vecs are not altered.
    '''

    smx = m.smx
    rv = StoMat.StoMat(rnames = smx.cnames)
    if type(uncons_vecs) == type(rv):
        uncons_vecs = list(map(uncons_vecs.GetCol, uncons_vecs.cnames))
        #[uncons_vecs.GetCol(c) for c in uncons_vecs.cnames]
        
    tag = '__tx__'
    n = 0
    
    for uncons_vec in uncons_vecs:
        N = smx.Copy()
        N.NewCol(uncons_vec, name = tag)
        if all:
            mtx = Mtx.ElModeMtx(N)
            #mtx.Transpose()
        else:
            mtx = Mtx.NullSpace(N, gaussjordan = True)
        for cname in mtx.cnames[:]:
            if Mtx.IsZero(mtx[tag, cname]):
                mtx.DelCol(cname)
        mtx.DelRow(tag)
        uprange = n + len(mtx.cnames)
        mtx.cnames = list(map(str, list(range(n, uprange))))
        n = uprange
        rv.AugCol(mtx)        
    return rv

##
##def LeakageResolvers(N, uncons_vec):
##    '''
##            pre     :   N - external stoichiometry matrix
##                        uncons_vec - column of matrix of inconsistent net stoichiometries, column of return-value
##                        of UnconsMtx(...)
##
##            post    :   returns list of reactions in N with non-zero coefficients in uncons_vec
##                        that appear in the same subset as transporter of
##                        unconserved metabolite.
##
##    '''
##    rv = []
##    tag = '__tx__'
##    N = N.Copy()
##    Mtx.NewCol(N, uncons_vec, name = tag)
##    try:
##        mtx = Mtx.NullSpace(N, gaussjordan = False)
##    except:
##        mtx = N.NullSpace()
##    subsets = Mtx.ProportionalRows(mtx)
##    for subset in list(subsets.values()):
##        if tag in subset:
##            del subset[tag]
##            rv = list(subset.keys())
##    return rv            
