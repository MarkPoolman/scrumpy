import sys

#import Structural.Model
#import Util.Set
from ScrumPy import Util
from ScrumPy.Util import Seq, Sci
from ScrumPy.Structural import StoMat, EnzSubset
#from ScrumPy import Structural.EnzSubset as 
from ScrumPy.ModelDescription.lexer import t_Rever, t_Irrev, t_BackIrrev


from . import NJ, Set
from . import Relation
from . import Sto
from . import Connect


##ZEROROUND = 8
##ZEROVAL = pow(10, -ZEROROUND)
from .Round import ZEROROUND, ZEROVAL
##
##
##def StoMat(filename = '', **kwargs):
##    rv = StoMat.StoMat(**kwargs)
##    if filename:
##        rv.ReadFile(filename)
##    return rv        
##        

##############################################
##tolerance values

def IsZero(val):
    return abs(val) <= ZEROVAL


def IsAllZero(seq):
    bools = [IsZero(x) for x in seq]
    return not False in bools


def IsMtxZero(M):
    return len(M.cnames) == 0 or IsZero(MaxVal(M))


def MaxVal(M):
    return max([max(list(map(abs, M[rname]))) for rname in M.rnames])
        

def IsSemiPos(seq):
    return not Seq.AllEq(seq, 0) and Seq.AllMatch(seq, lambda x: x >= 0)

def IsSemiNeg(seq):
    return not Seq.AllEq(seq, 0) and Seq.AllMatch(seq, lambda x: x <= 0)


def IsSemiSigned(seq):
    """pre: seq is a sequence of numbers.
    post: IsSemiSigned(seq) is True, if seq contains non-zero values
    and they have all the same sign."""
    
    rv = False
    if Seq.AllEq(seq, 0):
        pass
    elif Seq.AllMatch(seq, lambda x: x >= 0):
        rv = True
    elif Seq.AllMatch(seq, lambda x: x <= 0):
        rv = True
    return rv


def ZeroRnames(M):
    rv = []
    for rname in M.rnames:
        if IsAllZero(M[rname]):
            rv.append(rname)
    return rv

def NonZeroRnames(M):
    rv = []
    for rname in M.rnames:
        if Util.Seq.AllMatch(M[rname], lambda x: not IsZero(abs(x))):
            rv.append(rname)
    return rv


def UnZeroed(M):
    rv = M.Copy()
    for rname in M.rnames:
        if IsAllZero(M[rname]):
            rv.DelRow(rname)
    return rv


def ProportionalRows(K):
    rv = {}
    K = UnZeroed(K)
    
    counter = 1
    while len(K):
        rname = K.rnames[0]
        row = K[rname]
        subset = {rname : 1}

        for rname2 in K.rnames[1:]:
            row2 = K[rname2]
            dep = True
            rat = 0
            i = 0

            while dep and i < len(row):
                if IsZero(row[i]) or IsZero(row2[i]):
                    if not (IsZero(row[i]) and IsZero(row2[i])):
                        dep = False
                else:
                    rat2 = row[i] / row2[i]
                    
                    if rat == 0:
                        rat = rat2
                    else:
                        if not IsZero(rat2 - rat):
                            dep = False

                i += 1
                        
            if dep:
                subset[rname2] = rat

        for rn in subset:
            K.DelRow(rn)

        if len(subset) > 1:
            name = 'Ess_' + str(counter)
            rv[name] = subset
            counter += 1

        else:
            rv[rname] = subset

    return rv

######################
##nullspace calculation and inspection

def OrthNullSpace(M):
    return StoMat(FromMtx = M.OrthNullSpace())


def NullSpace(M, gaussjordan = False):
    if gaussjordan:
        rv = M.NullSpace()
    else:
        try:
            mtx = M.ToSciMtx(float)
            k = Sci.null(mtx)
            zs = Sci.mul(mtx, k, AsList=True)        
            minz, maxz = min(list(map(min,zs))), max(list(map(max,zs)))
            if not IsZero(minz) or not IsZero(maxz):
                raise 0
            rv = StoMat(FromMtx = k, Conv = float)
            rv.rnames = M.cnames[:]
            rv.cnames =["c_" + str(x) for x in range(len(rv[0]))]
        except:
            rv = NullSpace(M, True)
    return rv


def IdenticallyZeroes(M, gaussjordan = True):
    K = NullSpace(M, gaussjordan)
    return ZeroRnames(K)

def LinearDependentSubsets(M, gaussjordan = True):
    K = NullSpace(M, gaussjordan)
    rv = ProportionalRows(K)
    rv['ZERO'] = dict.fromkeys(ZeroRnames(K), 0.0)
    return rv

def IsoColRelation(M):
    d = ProportionalRows(M.Copy(tx=1))
    return Relation.Dict2Relation(d)

def RowRelation(M, gaussjordan = True):
    LS = LinearDependentSubsets(M, gaussjordan = gaussjordan)
    return Relation.Dict2Relation(LS)


def IsoNonZeros(cname, M, cnames = []):
    cnames = cnames or M.cnames    
    fsto = set(M.InvolvedWith(cname))
    for cname2 in cnames:
        if cname != cname2 and set(M.InvolvedWith(cname2)) == fsto:
            rv.append(cname2)
    return rv            


def IsoNonZeroRel(M):
    rv = Utils.GetRelation()
    cnames = set(M.cnames[:])
    while len(cnames):
        cname = cnames[0]
        isos = [cname]
        isos = IsoNonZeros(cname, M, cnames)
        rv.AddList(cname, isos)
        cnames = cnames.difference(isos)
    return rv

def LeftNullSpace(M, gaussjordan = True, core = True):
    rnames = M.rnames[:]
    if core:
        M = Utils.Connect.CoreNetwork(M)
    if len(M) > 0:        
        M = M.Copy(tx = 1)
        rv = NullSpace(M, gaussjordan = gaussjordan)
        for rname in rnames:
            if not rname in rv.rnames:
                rv.NewRow(name = rname)
        rv.RowReorder(rnames)                
    else:
        rv = StoMat(rnames = rnames)
    return rv

def ExtLeftNullSpace(M, core = True):
    rv = LeftNullSpace(M, core = core)
    for met in rv.rnames[:]:
        if not met in M.Externs:
            rv.DelRow(met)
    rv = ScalMul(rv, -1)
    rv.RowReorder(M.Externs)
    return rv            

######################
##row correlation analysis

def CosTheta(seq1, seq2):
    rv = abs(Seq.CosTheta(seq1, seq2))
    if IsZero(rv):
        rv = 0.0
    elif IsZero(1 - rv):
        rv = 1.0
    return rv


def AreInSameSubSys(M, rname1, rname2):
    O = OrthNullSpace(M)
    return not IsAllZero(O[rname1]) and not IsAllZero(O[rname2]) and bool(CosTheta(O[rname1], O[rname2]))


def ConnectedTo(O, rname):
    rv = []
    O = UnZeroed(O)
    if rname in O.rnames:
        for rname2 in O.rnames:
            if CosTheta(O[rname], O[rname2]) != 0:
                rv.append(rname2)
    return rv


def SubSystems(M):
    rv = []
    O = OrthNullSpace(M)
    O = UnZeroed(O)
    rlist = O.rnames[:]
    while rlist:
        seed = rlist[0]
        subsys = ConnectedTo(O, seed)
        rlist = Util.Set.Complement(rlist, subsys)
        rv.append(subsys)
    return rv         


def ONS2Phi(O):
    O = UnZeroed(O)
    return O.RowDiffMtx(fun = CosTheta)
           

def CorrCoefMtx(M):
    O = OrthNullSpace(M)
    return StoMat(FromMtx = ONS2Phi(O))



def RowDiffNJ(M, fun = None):
    M = M.Copy()
    M.ZapZeroes()
    D = M.RowDiffMtx(fun = fun)
    return D.ToNJTree()


def RowDiffTree(M, fun = None):
    nj = RowDiffNJ(M, fun = fun)
    tree = NJ.NJTree(nj)
    tree.MakeCompact()
    return tree


def ClusterRows(M, fun = None):
    if len(M) > 1:
        tree = RowDiffTree(M, fun = fun)
        names = tree.LeafNames()
        M.RowReorder(names)


def SetDist(M, rname1, rname2):
    set1 = list(M.InvolvedWith(rname1))
    set2 = list(M.InvolvedWith(rname2))
    return 1 - Set.Jaccard(set1, set2)


def ClusterBySetDist(M):
    D = StoMat(cnames = M.rnames, rnames = M.rnames)
    nj = D.ToNJTree()
    tree = NJ.NJTree(nj)
    tree.MakeCompact()
    names = tree.LeafNames()
    M.RowReorder(names)    


def SetDistMtx(M):
    rv = StoMat(rnames = M.rnames, cnames = M.rnames)
    for i in range(len(M)):
        for j in range(i + 1, len(M)):
            r1, r2 = M.rnames[i], M.rnames[j]
            rv[i, j] = rv[j, i] = SetDist(M, r1, r2)
    return rv            

def SortRowsByConnectedness(M):
    rnames = M.rnames[:]
    rnames.sort(cmp = lambda x, y: cmp(-len(M.InvolvedWith(x)), -len(M.InvolvedWith(y))))
    M.RowReorder(rnames)                

##def StoAngleNJ(M):
##    tree = RowDiffTree(M)
##    leaves = tree.AllLeaves()
##    for leaf in leaves:
##        rname = leaf.Name
##        sto = M.InvolvedWith(rname)
##        leaf.Name = String.Sto2Equation(sto, rname in M.Irrevs)
##    return tree.ToNJ()
    
    
#############################################################################
##matrix construction

def Dict2Mtx(dct, names = [], tx = False, Conv = float):
    names = names or list(dct.keys())
    rv = StoMat.StoMat(rnames = ['v'], cnames = names, Conv = Conv)
    for k in dct:
        rv[0, k] = dct[k]
    if tx:
        rv = rv.Copy(tx = 1)
    return rv        


#############################################################################
##matrix comparison

def AreEqual(A, B):
    rv = set(A.rnames) == set(B.rnames)
    rv = rv and set(A.cnames) == set(B.cnames)
    rv = rv and set(A.Externs) == set(B.Externs)
    rv = rv and set(A.Irrevs) == set(B.Irrevs)
    if rv:
        B.RowReorder(A.rnames)
        B.ColReorder(A.cnames)
        i = 0
        while rv and i < len(A):
            rv = A[i] == B[i]
            i += 1
    return rv


def ContainsCol(M, col):
    rv = False
    i = 0
    while not rv and i < len(M.cnames):
        if col == M.GetCol(i):
            rv = True
        i += 1
    return rv        


def MapMatrices(M1, M2):
    cnames2 = M2.cnames[:]
    rv = {} 
    for cname1 in M1.cnames:
        i, found = 0, False
        while not found and i < len(cnames2):
            cname2 = cnames2[i]
            if Utils.Sto.AreEqual(M1.InvolvedWith(cname1), M2.InvolvedWith(cname2)):
                rv[cname1] = cname2
                del cnames2[i]
                found = True
            i += 1
    return rv            
                    
def MatDif(M1, M2):
    d = MapMatrices(M1, M2)
    d1 = set(M1.cnames).difference(list(d.keys()))
    d2 = set(M2.cnames).difference(list(d.values()))
    return list(d1), list(d2)


def CmpVecs(seq1, seq2):
    rv = -2
    diff = Util.Seq.Diff(seq1, seq2)
    if Util.Seq.AllMatch(diff, lambda x: x == 0):
        rv = 0
    elif Util.Seq.AllMatch(diff, lambda x: x >= 0):
        rv = 1
    elif Util.Seq.AllMatch(diff, lambda x: x <= 0):
        rv = -1
    return rv
            

def MatUnion(M1, M2):
    rv = M1.Copy()
    NumerateCols(rv)
    n = len(rv.cnames)
    d1, d2 = MatDif(M1, M2)
    for cname in d2:
        n += 1
        NewCol(rv, M2.InvolvedWith(cname), name = str(n))
    return rv        

#############################################################################
##el. modes


def ElModeMtx(N, externs = [], irrevs = []):
    m = Structural.Model.Model()
    m.smexterns = N.Copy()
    m.sm = N.Copy()
    irrv = list(set(irrevs).intersection(m.sm.cnames).difference(IdenticallyZeroes(m.sm)))
    for i in irrv:
        m.sm.RevProps[i]=t_Irrev
    for extern in externs:
        m.sm.DelRow(extern)
    try:        
        rv = m.ElModes().mo.Copy()
        for cname in N.cnames:
            if len(N.InvolvedWith(cname)) == 0:
                rv.SetRow(cname, [0] * len(rv.cnames))
    except:
        sys.stderr.write('Problem claculating elementary modes')
        rv = StoMat(rnames = N.cnames)
    return rv        
            
                


#############################################################################
##editing

def NewCol(N, col, name = 'tx', rev=t_Rever):
    if type(col) == dict:
        N.NewReaction(name,col,rev)
    else:        
        N.NewCol(col, name = name)
    N.RevProps.update({name:rev})
    #Set.UpdateList(N.GetIrrevs(), [name], irrev)


def AugMtx(N1, N2):
    for cname in N2.cnames:
        iw = N2.InvolvedWith(cname)
        NewCol(N1, iw, cname)
    N1.Irrevs = list(set(N1.Irrevs).union(N2.Irrevs))
    

def SaveStoich(N, cname, stoich = {}):
    if cname in N.cnames:
        DelCol(N, cname)
    N.NewReaction(cname, stoich)

def DelCol(N, cname):
    N.DelCol(cname)
    if cname in N.Irrevs:
        N.Irrevs.remove(cname)


def DelRow(N, rname):
    N.DelRow(rname)
    if rname in N.Externs:
        N.Externs.remove(rname)

def DelColWithExclusives(N, cname):
    iw = N.InvolvedWith(cname)
    N.DelCol(cname)                
    for rname in iw:
        if IsAllZero(N[rname]):
                N.DelRow(rname)

def DelNonMinCols(M):
    i = len(M.cnames)
    while i > 0:
        i -= 1
        col = M.GetCol(i)
        j, done = 0, False
        while j < i and not done:
            Cmp = CmpVecs(col, M.GetCol(j))
            if Cmp == -1:
                M.SwapCol(i, j)            
            if Cmp > -2:
                M.DelCol(i)
                done = True
            j += 1

def DelZeroCols(M):
    M.Transpose()
    M.ZapZeroes()
    M.Transpose()

##binary matrices:
def SetList(N, cname, List = []):
    stoich = {}.fromkeys(List, 1)
    SaveStoich(N, cname, stoich)

def AddList(N, cname = None, List = []):
    if cname != None and not cname in N.cnames:        
        N.NewCol(name = cname)
    for rname in List:
        if not rname in N.rnames:
            N.NewRow(name = rname)
        if cname != None:            
            N[rname, cname] = 1


def Update(N, N1):
    for rname in N1.rnames:
        if not rname in N.rnames:
            N.NewRow(name = rname)
    for cname in N1.cnames:
        if not cname in N.cnames:
            N.NewCol(name = cname)
    for cname in N1.cnames:            
        for rname in N1.rnames:
            N[rname, cname] += N1[rname, cname]


def Union(N1, N2):
    cnames = list(set(N1.cnames).union(N2.cnames))
    rnames = list(set(N1.rnames).union(N2.rnames))
    rv = StoMat(cnames = cnames, rnames = rnames)
    for cname in cnames:
        union = set()
        if cname in N1.cnames:
            union.update(N1.InvolvedWith(cname))
        if  cname in N2.cnames:
            union.update(N2.InvolvedWith(cname))       
        AddList(rv, cname, list(union))
    return rv


def DelZeros(M):
    for cname in M.cnames:
        for rname in M.rnames:
            if Utils.Mtx.IsZero(M[rname, cname]):
                M[rname, cname] = 0

def DelIsoCols(M):
    R = IsoColRelation(M).SelectByMinLen(2)
    for k in R.TrueKeys():
        for cname in R[k][1:]:
            M.DelCol(cname)
                



##################################################################
#subsystems

def ExtractCols(N, cnames):
    rv = StoMat(rnames = N.rnames, Conv = N.Conv)
    for cname in cnames:
        rv.NewCol(N.GetCol(cname))
    return rv

def ExtractRows(N, rnames):
    rv = StoMat(cnames = N.cnames, Conv = N.Conv)
    for rname in rnames:
        rv.NewRow(N.GetRow(rname), rname)
    return rv    

def InternalMtx(N):
    rv = N.Copy()
    for x in N.Externs:
        rv.DelRow(x)
    return rv        

##################################################################
#normalise

def NormaliseCols(M):
    """pre: M is an mpq matrix"""
    rv = M.__class__(rnames = M.rnames, Conv = M.Conv)
    for cname in M.cnames:
        col = M.GetCol(cname)
        Seq.Normalise(col)        
        rv.NewCol(col, cname)
    return rv    

def IntegiseCols(M):
    """pre: M is an mpq matrix"""
    rv = M.__class__(rnames = M.rnames)
    for cname in M.cnames:
        col = M.GetCol(cname)
        Seq.Normalise(col)        
        col = Seq.Integise(col)
        rv.NewCol(col, cname)
    return rv


def IntegiseRows(M):
    """pre: M is an mpq matrix"""
    return IntegiseCols(M.Copy(tx=1)).Copy(tx=1)

def DelIsoforms(M):
    isos = M.FindIsoforms()
    for isogroup in isos:
        for cname in isogroup[1:]:
            M.DelCol(cname)
                

############################################################
##renaming

def RenameListAndSublist(List, sublist, newlist = [], fun = None):
    newlist = newlist or list(map(fun, List))
    newsublist = []
    for i in range(len(List)):
        oldval = List[i]
        if oldval in sublist:
            newsublist.append(newlist[i])
    return newlist, newsublist


def RenameRowsAndExterns(M, rnames = [], fun = []):
    M.rnames, M.Externs = RenameListAndSublist(M.rnames, M.Externs, rnames, fun)


def RenameColsAndIrrevs(M, cnames = [], fun = []):
    M.cnames, M.Irrevs = RenameListAndSublist(M.cnames, M.Irrevs, cnames, fun)
    

def NumerateRows(M, start = 1):
    rnames = list(map(str, list(range(start, len(M) + start))))
    RenameRowsAndExterns(M, rnames)
    

def NumerateCols(M, start = 1):
    cnames = list(map(str, list(range(start, len(M.cnames) + start))))
    RenameColsAndIrrevs(M, cnames)
    

###########################################################################
# basic algebra

def ScalMul(M, k):
    rv = M.Copy()
    for cname in rv.cnames:
        rv.MulCol(cname, k = k)
    return rv        

def LinComb(M, sto):
    rv = [0] * len(M)
    for cname in sto:
        col = [x * sto[cname] for x in M.GetCol(cname)] 
        rv = Util.Seq.Add(rv, col)
    return rv

################################################################################

            
