import os
import re
import string
import tkinter.filedialog

from ScrumPy.Util import Tree


def SaveNjAndPs(nj, fname):
    njname = fname + '.nj'
    open(njname, 'w').write(nj)
    os.system('newicktops ' + njname)
    psname = fname + '.ps'
    lines = open(psname).readlines()
    s = ''
    nlen = len(lines)
    for i in range(nlen):
        line = lines[i]
        if line.startswith('(1) title setclip'):
            line = '%%' + line
        elif line.startswith('0.7 setgray'):
            line = '0 setgray\n'
        elif nlen - 10 <= i < nlen - 2:
            line = '%%' + line
        s += line
    open(psname, 'w').write(s)            
    os.system('ps2ps %s out.ps' % psname)
    os.system('mv out.ps ' + psname)



class NJTree(Tree.Tree):

    def __init__(self, nj = ''):
        Tree.Tree.__init__(self)
        self.ReadNJ(nj)


    def Open(self, fname = ''):
        fname = fname or tkinter.filedialog.askopenfilename()
        nj = open(fname).read()
        self.ReadNJ(nj)

    def Save(self, fname = ''):
        fname = fname or tkinter.filedialog.asksaveasfilename()
        open(fname, 'w').write(self.ToNJ())


    def ReadNJ(self, nj):
        """ pre: nj is an NJ format string
           post: constructs tree from nj """
        pn = "\s*(\(.+\))?\s*([^\(\)\[\]:;]+)?\s*:?\s*(\d*.?\d*e?[+-]?\d*)?\s*"
        mo = re.search(pn, nj)
        if mo != None:
            self.Name = mo.groups()[1] or ''
             

            dtp = mo.groups()[2]
            try:
                self.DistToParent = string.atof(dtp or '-1')
            except:
                self.DistToParent = float(-1)
                print(self.Name, dtp)

            #initializing children
            if mo.groups()[0] != None:
                #parsing the substring in brackets
                ch = mo.groups()[0][1 : -1]
                i = pos = balance = 0
                toks = []
                for i in range(0, len(ch)):
                    if ch[i] == ',' and balance == 0:
                        toks += [ch[pos : i]]
                        pos = i + 1
                    elif ch[i] == '(':
                        balance -= 1
                    elif ch[i] == ')':
                        balance += 1
                toks += [ch[pos:]]

                for tok in toks:
                    child = self.__class__()
                    child.ReadNJ(tok)
                    self.AddChild(child)



    def ToNJ(self):
        rv = ''
        if len(self.Children) > 0:
            lst = [child.ToNJ() for child in self.Children]
            rv += "(%s)" %  string.join(lst, ', ')
        if self.Name != None:
            rv += ' ' + self.Name
        if self.DistToParent != None:
            rv += " : " + str(self.DistToParent)
        return rv


    def LeafNames(self):
        return [l.Name for l in self.AllLeaves()]



    def MakeCompact(self):
        iscluster = not self.IsLeaf() and not self.IsRoot()
      
        for child in self.Children:
            if not child.IsLeaf():
                child.MakeCompact()

        if iscluster:
            for child in self.Children[:]:
                if child.IsLeaf() and child.DistToParent == self.DistToParent:#0 and self.DistToParent == 0:
                    self.Parent.AddChild(child)
                    self.Children.remove(child)
                
        if iscluster and len(self.Children) == 0:
            self.Parent.Children.remove(self)                    

