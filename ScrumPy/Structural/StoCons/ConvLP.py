import Utils.Mtx as Mtx
import Utils.Sto

from . import MtxLP
from . import FluxLP
parent = FluxLP.FluxLp

class ConvLp(parent):

    def __init__(self, N, scale):
        self.Scale = scale
        self.N = N
        S = Mtx.InternalMtx(N)

        parent.__init__(self, S, klass = 'MIP')


    ##setting objective
            
    def SetLenObjective(self, externs = [], tmp = True):
        txs = self.AllTransporters(externs)
        parent.SetLenObjective(self, txs, tmp = tmp)

    def SetSumObjective(self, coef = 1, externs = [], tmp = False):
        txs = self.AllTransporters(externs)
        parent.SetSumObjective(self, coef, txs, tmp = tmp)        


    ##adding constraints
    def AddTxConstraint(self, tx, val, tmp = True):
        self.AddConstantConstraints({tx : val}, tmp = tmp)

    def AddExtConstraint(self, x, coef, blockopposite = False, tmp = True):
        coef = float(coef) / self.Scale
        tx = self.GetProperDir(x, coef)
        self.AddTxConstraint(tx, abs(coef), tmp = tmp)
        if blockopposite:
            self.Block(self.GetOpposite(tx), tmp = tmp)

    def AddStoConstraint(self, sto, blockopposite = False, tmp = True):
        for x in sto:
            self.AddExtConstraint(x, sto[x], blockopposite = blockopposite, tmp = tmp)        

    def ExcludeLastSolution(self, tmp = True):
        sol = self.GetSolution()
        self.ExcludeSolution(sol, tmp = tmp, kind = 'Continuous')
  

    ##data access       
    def GetTxName(self, x):
        return list(self.N.InvolvedWith(x).keys())[0]

    def GetProperDir(self, x, coef):
        tx = self.GetTxName(x)
        val = float(coef / self.N[x, tx])
        if val < 0:
            tx = self.GetOpposite(tx)
        return tx            

    def GetOppDir(self, x, coef):
        tx = self.GetProperDir(x, coef)
        return self.GetOpposite(tx)

    def AllTransporters(self, externs = []):
        externs = externs or self.N.Externs
        txs = list(map(self.GetTxName, externs))
        backs = list(map(self.GetOpposite, txs))
        return list(set(txs + backs))

    def GetSolution(self, kind = 'tx', **argd):
        if kind == 'tx':
            rv = {}
            sol = self.GetSolution('Continuous', normalise = False)
            for x in self.N.Externs:
                tx = self.GetTxName(x)
                val = sol.get(tx, 0) * self.Scale
                if val != 0:
                    rv[tx] = val
                txback = self.GetOpposite(tx)
                val = sol.get(txback, 0) * self.Scale
                if val != 0:
                    rv[txback] = val
        else:
            rv = parent.GetSolution(self, kind, **argd)            
        return rv

    def GetConversion(self):
        rv = Mtx.StoMat(rnames = self.N.Externs, cnames = ['v'], Conv = float)
        v = self.GetSolution('Reaction', normalise = False)
        if v:
            for x in self.N.Externs:
                tx = self.GetTxName(x)
                rv[x, 0] = self.N[x, tx] * v.get(tx, 0) * self.Scale
        Utils.Mtx.DelZeros(rv)                
        return rv

    ##el conversions

    def ElVectorInvolving(self, ext, coef = 1):
        self.SetLenObjective(tmp = True)
        self.AddExtConstraint(ext, coef = coef, blockopposite = True, tmp = True)
        self.Solve()
        self.CleanTemps(1 + len(self.GetObjective()) + 2)
        c = self.GetConversion()

        self.SetSumObjective(tmp = True)
        self.AddExtConstraint(ext, coef = coef, tmp = True)
        for x in c.InvolvedWith('v'):
            self.Block(self.GetOppDir(x, c[x, 0]), tmp = True)
        for x in Mtx.ZeroRnames(c):
            self.AddExtConstraint(x, coef = 0, blockopposite = True, tmp = True)            
        self.Solve()
        self.CleanTemps(1 + 1 + 2 * len(c) - len(c.InvolvedWith('v')))

        return self.GetConversion()


    def ElVectorsInvolving(self, ext, coef = 1):
        rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
        go = True
        while go:
            c = self.ElVectorInvolving(ext, coef = coef)
            if not self.IsStatusOptimal():
                go = False                
            else:
                rv.AugCol(c)
                self.ExcludeLastSolution()
        Mtx.NumerateCols(rv)                    
        self.CleanTemps(len(rv.cnames))            
        return rv

    def ElVectors(self):
        rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
        for x in self.N.Externs:
            cc1 = self.ElVectorsInvolving(x, 1)
            rv.AugCol(cc1)
            cc2 = self.ElVectorsInvolving(x, -1)
            rv.AugCol(cc2)
        Mtx.NumerateCols(rv)            
        Mtx.DelIsoCols(rv)
        Mtx.NumerateCols(rv)
        return rv            


####closest vertices
##    def FindClosestVertex(self, c):
##        self.AddStoConstraint(c)
##        self.Solve()
##        self.CleanTemps(len(c))       
##    
##    def GetClosestVertex(self, c):
##        self.FindClosestVertex(c)
##        return self.GetConversion()
##
##    def ClosestVertices(self, c0):
##        rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
##        go = True
##        while go:
##            c = self.GetClosestVertex(c0)
##            if not self.IsStatusOptimal():
##                go = False                
##            else:
##                rv.AugCol(c)
##                self.ExcludeLastSolution()
##        Mtx.NumerateCols(rv)                    
##        self.CleanTemps()            
##        return rv
##
##    def FeasibleVertices(self, c0):
##        c1 = self.ClosestVertices(c0)
##        Utils.Sto.Invert(c0)
##        c2 = self.ClosestVertices(c0)
##        c1.AugCol(c2)
##        return c1
##
##    def FeasibleMtx(self, C0):
##        rv = Mtx.StoMat(rnames = self.N.Externs, Conv = float)
##        for cname in C0.cnames:
##            c0 = C0.InvolvedWith(cname)
##            c = self.FeasibleVertices(c0)            
##            rv.AugCol(c)
##        Mtx.NumerateCols(rv)            
##        Mtx.DelZeroCols(rv)
##        Mtx.DelIsoCols(rv)
##        Mtx.NumerateCols(rv)
##        return rv


##        tx = self.GetTxName(x)
##        val = float(coef / self.N[x, tx])
##        if val < 0:
##            tx = self.GetOpposite(tx)

##        self.AddConstraint({tx : 1}, val, None, tmp = tmp)    

##    def MakeIrrev(self, rnlist):
##        rnlist = set(rnlist).intersection(self.GetReacs())
##        self.Irrevs.update(rnlist)
##        for rn in rnlist:
##            rnback = self.GetOpposite(rn)
##            self.AddConstraint({rn : 1, rnback : -1}, 0, None)        
