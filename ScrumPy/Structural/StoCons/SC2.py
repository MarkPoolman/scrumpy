from . import Mtx, MtxLP, FluxLP, CompLP


#################################################################################

def MtxLp(mtx, direc = 'Min', klass = 'LP', lowBound = 0, upBound = None, name = 'Unnamed mtx problem'):
    return MtxLP.MtxLp(mtx, direc = direc, klass = klass, lowBound = lowBound, upBound = upBound, name = name)


#################################################################################

def FluxLp(mtx, direc = 'Min', klass = 'LP', lowBound = 0, upBound = None, name = 'Unnamed stoichiometry matrix problem'):
    return FluxLP.FluxLp(mtx, direc = direc, klass = klass, lowBound = lowBound, upBound = upBound, name = name)

#################################################################################

def CompLp(mtx, direc = 'Min', klass = 'LP', lowBound = 0, upBound = None, name = 'Unnamed stoichiometry matrix problem'):
    return CompLP.CompLp(mtx, direc = direc, klass = klass, lowBound = lowBound, upBound = upBound, name = name)

#################################################################################

def IsConsistent(N):
    """pre: N is an external stoichiometry matrix.
    post: IsConsistent(Nt) is True iff N is stoichiometrically consistent."""
    Nt = N.Copy(tx=1)    
    return CompLp(Nt).IsConsistent()


def UnconservedMets(N):
    """pre: N is an external stoichiometry matrix.
    post: IsConsistent(N) is the list of unconserved metabolites in N."""
    Nt = N.Copy(tx=1)    
    return CompLp(Nt, 'Min', 'MIP').UnconservedMets()


def UnconsMtx(N, uncons_mets = []):
    """pre: N is an external stoichiometry matrix of a network N,
    uncons_mets is a list of unconserved metabolites in N (will be calculated in the function if an empty list provided).
    post: UnconsMtx(N) is a matrix of minimal inconsistent net stoichiometries in N."""    
    Nt = N.Copy(tx=1)
    rv = Mtx.StoMat(rnames = Nt.cnames, Conv = float)
    uncons_mets = uncons_mets or CompLp(Nt, klass = 'MIP').UnconservedMets()

    Kt = Mtx.NullSpace(Nt, gaussjordan = True).Copy(tx=1)
    if len(Kt) > 0:
        P = MtxLp(Kt, 'Min', 'MIP')
        for met in uncons_mets:
            if Mtx.IsAllZero(Kt.GetCol(met)):
                rv.NewCol()
                rv[met, -1] = 1
            else:
                P.MakePositive([met], tmp = True)
                vecs = P.MinLenSolutions(asmatrix = True)
                for col in vecs:
                    col = col.GetCol(0)
                    if not Mtx.ContainsCol(rv, col):
                        rv.NewCol(col)
                P.CleanTemps()
    else:
        for met in uncons_mets:
            rv.NewCol()
            rv[met, -1] = 1

    return rv


def LeakageMtx(N, uncons_vecs = [], all = False):
    """pre: N is an external stoichiometry matrix of a network N,
    uncons_vecs is a matrix of minimal inconsistent net stoichiometries in N
    (will be calculated in the function if an empty list provided).
    post: LeakageMtx(N) is a matrix, whose columns comprise a complete (if all = True) or
    a spanning (otherwise) set of elementary leakage modes in N."""       
    uncons_vecs = uncons_vecs or UnconsMtx(N)
    return Mtx.LeakageModeMtx(N, uncons_vecs = uncons_vecs, all = all)


def ShortestLeakageMode(N, uncons_vec):
    """pre: N is an external stoichiometry matrix of a network N,
    uncons_vec is some minimal inconsistent net stoichiometry in N.
    post: ShortestLeakageMode(N) is the elementary leakage mode corresponding to uncons_vec,
    with a minimal number of non-zero components, represented as a dictionary."""     
    return FluxLp(N, klass = 'MIP').ShortestLeakageMode(uncons_vec)



