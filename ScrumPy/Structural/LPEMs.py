import math
import ScrumPy
from ScrumPy.Structural import Model, StoMat, ElModes
from ScrumPy.Util import Sci


def SubModel(m, reacs):

    Head = "Structural()\nNoEdit()\n"
    smx = m.smx.SubSys(reacs)
    smx.ToScrumPy("SubModel.spy", Head)
    return ScrumPy.Model("SubModel.spy")

class ModesDB(ElModes.ModesDB):

    def __init__(self, tabl, model,tol=None):

        tabl.ChangeType(float)
        self.model = model
        self.mo = tabl

        # Because the base class implements search and filter functions, and doesn't know about tol
        # We need to make sure we have recorded it here. Instantiating this class should always
        # specify a numeric value. Icky - shurely a better way ?
        if tol is not None:
            self.model.tol = tol
        else:
            tol = self.model.tol
            
        self.sto = sto =  model.smx.Mul(tabl)
        
        sto.ZapZeroes(tol)
        sto.RevProps = tabl.RevProps
        for r in sto.rnames:
            sto[r] = [float("{:.4}".format(v)) if abs(v) > tol else 0.0 for v  in sto[r]]

##    def _rowsearch(self, name, crit, mtx, Select=True,  **kwargs):
##
##        rv = ElModes.ModesDB._rowsearch(self, name, crit, mtx, Select,  **kwargs)
##        rv.tol = self.tol
##        rv.sto.ZapZeroes(rv.tol)
##        return rv
        

def SSErr(m,fd):
    rvec = m.sm.RateVector(fd)
    res = m.sm.Mul(rvec)
    return sum([math.sqrt(x*x) for x in res.GetCol(0)])

        

def FindSmallestInDic(d):
	keys = list(d.keys())
	vals = list(map(abs,d.values()))
	minval = min(vals)
	idx = vals.index(minval)
	key = keys[idx]
	return key, d[key]

    
def DelZeroes(d, tol=1e-12):
    """ pre: GetZeroes(d,tol),
        post: all values <=tol removed """

    #maxv = max([abs(x) for x in d.values()])

    for k in list(d.keys()):
        if abs(d[k]) <tol:
            del d[k]

def SetLimits(lp, fd):
    """pre:
       post: """

    for reac in lp.GetReacNames():
        if reac not in fd:
            lp.SetFixedFlux({reac:0})

    for reac, val in list(fd.items()):
        if val >0:
            bounds = (0,val)
        else:
            bounds = (val,0)
        lp.SetFluxBounds({reac:bounds})


def DecomposeVec(E, v,Irrevs):

    #print "DecomposeVec",E, v,Irrevs

    sciv = Sci.matrix(v)
    E = E.Copy(float)
    sciE = Sci.matrix(E.rows)
    g = Sci.pinv(sciE)

    e = g*sciv # <<<------  the decomposition : e = v.E#
    evals = e.transpose().tolist()[0]

    Again = False
    EMnames = E.cnames[:]
    for i in range(len(evals)):
        if evals[i] <0 and EMnames[i] in Irrevs:
            Again = True
            E.DelCol(EMnames[i])
    if Again:
        #print("Again")
        return  DecomposeVec(E,v, Irrevs)
    else:
        return dict(zip(E.cnames,evals))


def GetDecom(sol,elmo):
    """pre: a vector solution, and the set of elementary modes it needs to be decomposed into
        post: a list of flux dictionaries corresponding the EM modes it was decomposed into
    """
   
    rv = []
    v = elmo.model.sm.RateVector(sol)
    v.ChangeType(float)
    EM_fd = DecomposeVec(elmo.mo, v, elmo.mo.GetIrrevs())
    
    for e in EM_fd:
       
        rd = elmo.mo.InvolvedWith(e)
        fd = {}
        for r in rd:
            fd[r] =  rd[r] * EM_fd[e]
            
        rv.append(fd)   

    return rv


def DecomIfNotEM(m,sol,targ, targets, tol,lp):
    """pre; a vector flus solution
        post; returns a list of dictionaries containing the elementary components of this vector
                if its an em mode the vector will be the only element in the list"""

    sol = dict(sol)
 
    reacs = list(sol.keys())
    sm = m.sm.SubSys(reacs)
    k = sm.OrthNullSpace()
    dimk = k.Dims()[1]
    
    if dimk == 0:  # no solution
        rv = []
    elif dimk == 1:
        rv = [sol]  # sol is em
    else:
        # Dimension>1 => not elementary
 
        minr, minv = FindSmallestInDic(sol)
        if minv < abs(sol[targ]) and not targ in targets:
        # if a flux in the sol is smaller than the target flux and we've not tried to eliminate it previously
            rv = LPEMsAsDics(m, sol, tol=1e-7,lp=lp)[0]   # try to recursively eliminate it
            
        else:                                       # otherwise fall back to Schuster algorithm
            subm = SubModel(m,reacs)

            for reac in subm.sm.RevProps:    
                if sol[reac]>0:
                    subm.sm.RevProps[reac] = '->'
                    subm.smx.RevProps[reac] = '->'
                else:
                    subm.sm.RevProps[reac] = '<-'
                    subm.smx.RevProps[reac] = '<-'

            elmo = subm.ElModes()
            
            elmo.mo.ChangeType(float)
            rv  = GetDecom(sol,elmo) #decomposing the solution into elementary modes
        
  
    return rv
  



def LPEMsAsDics(m, fd, tol=1e-7, lp=None):
    """ pre: m = ScrumPy.Model(...), fd is a dictionary mapping reaction names to stead-state flux values
            optional checks:
                    
        post: A list of reactions eliminated and their associated MIP solution"""

    rv  = []
    errs = []
    targets = set() # reactions we have eliminated

    if lp is None:
        lp = m.GetLP() # generate a linear programming representation of the the model.    
        lp.SetObjective(m.sm.cnames) # the objective is to minimise fluxes in fd
        # because glpk is non-reentrant, and we maybe invoked recursively,
        # we need to ensure lp is created exactly once, and pass it around as needed

    CurSol = dict(fd) # take a copy, because we will over-write this, leave the original unchanged.
    #DelZeroes(CurSol,tol)
    SetLimits(lp, CurSol)
    
    MaxTries = len(CurSol) # sentinel - make sure we don't try to remove more reacs than we started with
    nTries = 0

    while len(CurSol) >0 and nTries<=MaxTries: # while we still have reactions to eliminate in the current
        
        reac, flux =FindSmallestInDic(CurSol)  # find the reaction with the smallest (absolute) flux
        SetLimits(lp, CurSol)
        lp.SetFixedFlux({reac:flux})
        targets.add(reac)
        
        lp.Solve(False) # solve the lp - False -> no msg to stdout
        sol = lp.GetPrimSol() # get the solution
           
        for r in sol:  # subtract the lp solution from the current solution
            CurSol[r] -= sol[r]
        DelZeroes(CurSol, tol) # and remove (near) zero values
   
        if len(sol) != 0:
            dc = DecomIfNotEM(m,sol, reac, targets, tol,lp)
            if len(dc) ==0: # rounding error resulted in non-viable subsys
                errs.append((reac, sol))
            rv.extend(dc)
            
        else:  # rounding error may introduce inconsistant constraints
            dc = DecomIfNotEM(m, CurSol,reac, targets, tol,lp)
            print ("last", len(dc), "sols may have errors")
            rv.extend(dc)
            CurSol={}
                
        nTries += 1
        
        
    return rv, errs


def LPEMs(m, fd, tol=1e-7):

    SolDics,errs = LPEMsAsDics(m, fd, tol)
    reacs = list(fd.keys())

    subm = SubModel(m,reacs)
    
    tabl = StoMat.StoMat(rnames = subm.sm.cnames, Conv = float)
    
    n = 0
    ename = "ElMo_"
    for dic in SolDics:
        cname = ename + str(n)
        n += 1
        tabl.NewCol(name=cname)
        for reac in dic:
            tabl[reac, cname] = dic[reac]
            
    tabl.RevProps = dict(zip(tabl.cnames, ["->"] * len(tabl.cnames)))

    subm.smx.ChangeType(float)
    
    return ModesDB(tabl, subm, tol), errs
        
    


    
CalvinFD={  # Steady-state flux vector for the Calvin cycle model
    "Ru5Pk" : 119.278462267,
    "G3Pdh" : 203.045922616,
    "R5Piso" : 39.7594874223,
    "TPI" : 99.9701090316,
    "SBPase" : 39.7594874222,
    "StSynth" : 9.52539121589,
    "PGM" : -17.9969111613,
    "TKL2" : 39.7594874222,
    "Rubisco" : 119.278462267,
    "PGK!" : 203.045922975,
    "TPT_DHAP" : 38.4480457514,
    "FBPase" : 21.7625758573,
    "TKL1" : 39.7594874212,
    "Ald1" : 21.7625758578,
    "TPT_PGA" : 35.5110019167,
    "Ald2" : 39.7594874274,
    "LReac" : 331.849776099,
    "PGI" : -17.9969119607,
    "X5Piso" : 79.5189748443,
    "TPT_GAP" : 1.79426288397,
    "StPase" : 27.5223027808,
}        
        

      

    
    
    
