

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman
09/04/01 onward
Py2C, convert python objects to c equivalents
(NOT callable directly from python)
***/

#include "Py2C.h"
#include<stdlib.h>

long int *PyIntList2CIntArry(PyObject *pilist){

  long int *rv, len, n ;
  	
	len = PyList_Size(pilist) ;
	if(len > 0){
		rv = calloc(sizeof(long), len) ;
		for( n = 0 ; n < len ; n ++)
			rv[n] = PyLong_AsLong(PyList_GetItem(pilist, n)) ;
	}
	else rv = 0 ;
		
	return rv ;
}

float *PyNumList2CFltArry(PyObject *pilist){

 	float *rv ; 
	int len, n ;
  	
	len = PyList_Size(pilist) ;
	if(len > 0){
		rv = calloc(sizeof(float), len) ;
		for( n = 0 ; n < len ; n ++)
			rv[n] = PyFloat_AsDouble(PyList_GetItem(pilist, n)) ;
	}
	else rv = 0 ;
		
	return rv ;
}
		 


ArbRat_t PyInt2ArbRat(PyObject *pint){

  return ArbRatNew(PyLong_AsLong(pint), 1) ;
}



extern ArbRat_t PyRat2ArbRat(PympqObject *prat){
 
	return ArbRatDup(prat->q) ; 

}
	
	
	


ARVec_t PyNumList2ARVec(PyObject *pilist){


  ARVec_t rv ;
  ArbRat_t val ;
  int n, len ;
  
  	len = PyList_Size(pilist) ;
	rv = ARVecNew(len) ;
	for(n = 0 ; n < len ; n++){
		/*val = PyRat2ArbRat((PympqObject*)PyList_GetItem(pilist, n)) ;*/
		val = ((PympqObject*)PyList_GetItem(pilist, n))->q ;
		ARVecSetEl(rv, n, val) ;
	}

	return rv ;
}



		
ARVecList_t PyListList2ArVecList(PyObject *pilistlist){

  PyObject *pilist ;
  ARVecList_t rv ;
  
  int len, n ;
  
	rv = ARVLNew() ;
	len = PyList_Size(pilistlist) ;
	for(n = 0 ; n < len ; n++){
		pilist = PyList_GetItem(pilistlist,n) ;
		ARVLApp(rv, PyNumList2ARVec(pilist)) ; 
	}
	return rv ;
}


IntVecList_t PyListList2IntVecList(PyObject *pilistlist){

	PyObject *pilist ;
  	IntVecList_t rv ;
  	IntVec_t vec ;
  	long int *tmp_vec ;
  	int len, n ;
  	ulint idx, nest_len ;
  
	rv = IntVLNew() ;
	len = PyList_Size(pilistlist) ;
	for(n = 0 ; n < len ; n++){
		pilist = PyList_GetItem(pilistlist,n) ;
		nest_len = PyList_Size(pilist) ;
		vec = IntVecNew(nest_len) ;
		tmp_vec = PyIntList2CIntArry(pilist) ;
		for (idx = 0 ; idx < nest_len ; idx++){
			IntVecSetEl(vec, idx, tmp_vec[idx]) ;
		}
		IntVLApp(rv, vec) ; 
	}
	return rv ;
}


/* FltVecList_t PyListList2FltVecList(PyObject *pilistlist){

	PyObject *pilist ;
  	FltVecList_t rv ;
  	FltVec_t vec ;
  	float *tmp_vec ;
  	int len, n ;
  	ulint idx, nest_len ;
  
	rv = FltVLNew() ;
	len = PyList_Size(pilistlist) ;
	for(n = 0 ; n < len ; n++){
		pilist = PyList_GetItem(pilistlist,n) ;
		nest_len = PyList_Size(pilist) ;
		vec = FltVecNew(nest_len) ;
		tmp_vec = PyNumList2CFltArry(pilist) ; 
		for (idx = 0 ; idx < nest_len ; idx++){
			FltVecSetEl(vec, idx, tmp_vec[idx]) ;
		}
		FltVLApp(rv, vec) ; 
	}
	return rv ;
} */


int MaxInPyIntList(PyObject *pilist) {

  int len , max, n, cur ;
  
  	len = PyList_Size(pilist) ;
	max = PyLong_AsLong(PyList_GetItem(pilist, 0)) ;
	for( n = 1 ; n < len  ; n ++){
		cur = PyLong_AsLong(PyList_GetItem(pilist, n)) ;
		max = max > cur ? max : cur ;
	}
	return max ;
}




Set_t PyIntList2Set(PyObject *pilist){

  Set_t rv ;
  int n, len ;
  
	len = PyList_Size(pilist) ;
	rv = SetNew(MaxInPyIntList(pilist)) ;
	
	for(n = 0 ; n < len ; n++)
		SetAddMem(rv, PyLong_AsLong(PyList_GetItem(pilist, n))) ;
		
	return rv ;
}
	
	
	
PyObject *Describe(PyObject *self, PyObject *args){

	PyObject *MyObj ;
	
	Set_t s ;

	
	PyArg_ParseTuple(args, "O", &MyObj) ;
	s = PyIntList2Set(MyObj) ;
	fprintf(stderr, "\n set = \n%s\n", SetStr(s)) ; 


	return Py_None ;
		
}



