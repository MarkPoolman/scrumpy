

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/**
M.G.Poolman 05/04/01 onwards
ARVecList - lists of ARVec_t
**/

#include<stdio.h>
#include <stdlib.h>
#include"ARVecList.h"




/*************** low level node stuff  ************/


struct Node  {
	struct Node *Next ;
	ARVec_t Vec ;
}  ;

typedef struct Node *Node_p ;

static Node_p NewNode(void){

	return (Node_p) calloc(sizeof(struct Node), 1 ) ;
} 


static void DelNode(Node_p n){

	if(n->Vec)
		ARVecDel(n->Vec) ;
		
	free(n) ;
} ;


static Node_p ParNode(Node_p Adam, Node_p targ){ /* find parent in list */

	if(Adam->Next == targ)
		return Adam ;
		
	return ParNode(Adam->Next, targ) ;
}



static void PrintNodes(Node_p n){

	if(n){
		/* fprintf(stderr, "%x\t%x\t| %x\n", n, n->Next, n->Vec) ;*/
		printels(n->Vec) ;
		PrintNodes(n->Next) ;
	}
}


static void DelNodes(Node_p n){

	/* fprintf(stderr, "deln %x\n", n) ; */ 
	if(n){
		DelNodes(n->Next) ;
		DelNode(n) ;
	}
}




/***********  list stuff  ***************/


struct ARVecList{

	Node_p Top, Cur, Bot ;
	int len ;
} ;



/*** create and destroy  ***/


ARVecList_t ARVLNew() {
	/* pre: True
	  post: returns a usable ARVecList_t 
	 */
	 
	 return (ARVecList_t) calloc(sizeof(struct ARVecList), 1) ;
} ;
	 
	 
	 
void ARVLDel(ARVecList_t l){
	/* pre: l usable
	  post: l' !usable, resources freed 
	 */
	 
	 DelNodes(l->Top) ;
	 free (l) ;
}

	/***** info about lists *****/

ulint ARVLLen(ARVecList_t l) {
	/* pre: l usable
	  post: returns length of l 
	*/
	 return(l->len) ;
}


	 /***** retrieve items in situ ******/
	 
ARVec_t ARVLTop(ARVecList_t l) {
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns topmost item
	*/
	
	return(l->Top->Vec) ;
}
	
		
	
ARVec_t ARVLCur(ARVecList_t l){
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns current item
	*/
	
	return(l->Cur->Vec) ;
}
	
	
	
ARVec_t ARVLBot(ARVecList_t l) {
	/* pre: ARVLLen(l) > 0
	  post: l' == l, returns bottom item
	*/
	 	
	return(l->Bot->Vec) ;
}


ARVec_t ARVLGetCol(ARVecList_t l, ulint idx) {
	
	ulint n ;
	ARVec_t rv ;
	ARVLCur2Top(l) ;
	rv = ARVecNew(ARVLLen(l)) ;
	for (n = 0 ; n < ARVLLen(l) ; n++) {
		ARVecSetEl(rv, n, ARVecGetElDup(ARVLCur(l), idx)) ;
		ARVLCurNext(l) ;
	}
	
	return rv ;
}


	/**** Add items ****/

void ARVLIns(ARVecList_t l, ARVec_t v) {
	/* pre: usable(l,v), !(ARVLApp(l, v) || ARVLIns(l, v))
	  post: ARVLTop(l') == v
	 */

  Node_p new ;
  
 	new = NewNode() ;
	new->Vec = v ;
	
	new->Next = l->Top ;
	l->Top = new ;
	l->len++ ;
	if(l->len ==1)
		l->Cur = l->Bot = l->Top ;
}


	 
void ARVLApp(ARVecList_t l, ARVec_t v) {
	/* pre: usable(l,v) !(ARVLApp(l, v) || ARVLIns(l, v)) 
	  post: ARVLBot(l') == v
	 */
	 

  Node_p new ;
  
  	if(l->len == 0)
		return(ARVLIns(l,v)) ;
  
 	new = NewNode() ;
	new->Vec = v ;
	
	l->Bot->Next = new ;
	l->Bot = new ;
	l->len++ ;
}
	 
	 
	 
	/**** Remove items ***/
	 
ARVec_t ARVLPop(ARVecList_t l){
	/* pre: ARVLLen(l) > 0
	  post: ARVLLen(l') == ARVLLen(l) -1,
	  		ARVLTop(l') != ARVLTop(l),
			ARVLTop(l)  == ARVLPop(l) ;
	*/
	
  ARVec_t rv ;
  Node_p old ;
  
  	old = l->Top ;
	l->Top = l->Top->Next ;
	
	if(l->Cur == old)
		l->Cur = l->Cur->Next ;
	
	if(l->Bot == old)
		l->Bot = 0 ; /* i.e.  removed last item */
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}
	
	

	
ARVec_t ARVLChop(ARVecList_t l) {
	/* pre: ARVLLen(l) > 0
	  post: ARVLLen(l') == ARVLLen(l) -1,
	  		ARVLBot(l') != ARVLBot(l),
			ARVLChop(l) == ARVLBot(l) ;
	*/
	
		
  ARVec_t rv ;
  Node_p old ;
  
  	if(l->Bot == l->Top)
		return(ARVLPop(l)) ;
  
  	old = l->Bot ;
	l->Bot = ParNode(l->Top, l->Bot) ;
	l->Bot->Next = 0 ;
	
	
	if(l->Cur == old)
		l->Cur = l->Bot ;
		
	l->len-- ;
	rv = old->Vec ;
	old->Vec = 0 ;
	free(old) ;
	return rv ;
}
	
	



ARVec_t ARVLExtract(ARVecList_t l) {
	/* pre: ARVLLen(l) > 0
	  post: v = ARVLExtract(ARVecList_t l) => v not present in l
	  		ARVLCurAtTop(l) => ARVLCurAtTop(l')
			!ARVLCurAtTop(l) => ARVLCurPrev(l) */

	Node_p parent ;
	ARVec_t rv ; 
	
	if(l->Cur == l->Top){
		return(ARVLPop(l)) ;
	}
	if(l->Cur == l->Bot){
		return ARVLChop(l) ;
	} 
		
	parent = ParNode(l->Top, l->Cur) ;  /* chop out the current node */
	parent->Next = l->Cur->Next ;  

	rv = l->Cur->Vec ;					/* extract the information from it */	
	free(l->Cur) ;						/* destroy the old node */
	l->Cur = parent ;					/* point l->Cur somewhere useful */
	l->len-- ;	
	return (rv) ;
}
	
	
	
	
	
	
	
	
	
 	/**** Navigate  ***/
	
void ARVLCur2Top(ARVecList_t l) {
	/* pre: usable(l)
	  post: ARVLCur(l') == ARVLTop(l')
	*/
	
	l->Cur = l->Top ;
}
	
	
	
void ARVLCur2Bot(ARVecList_t l){
	/* pre: usable(l)
	  post: ARVLCur(l') == ARVLBot(l')
	*/ 
	
	l->Cur = l->Bot ;
}
 
void ARVLCurNext(ARVecList_t l) {
	/* pre: ARVLCur(l) != ARVLBot(l) 
	  post: current moved to next in sequence
	*/ 
 	l->Cur = l->Cur->Next ;
}


void ARVLCurPrev(ARVecList_t l) {
	/* pre: ARVLCur(l) != ARVLTop(l) 
	  post: current moved to previous in sequence
	*/ 
 
 	l->Cur = ParNode(l->Top, l->Cur) ;
}
 


Bool ARVLCurAtTop(ARVecList_t l){
	/* pre: True
	  post: ARVLCurAtTop(l) => ARVLCur(l) == ARVLTop(l) */ 
 
 	return(l->Cur == l->Top) ;
}
 
 
Bool ARVLCurAtBot(ARVecList_t l){
	/* pre: True
	  post: ARVLCurAtTop(l) => ARVLCur(l) == ARVLTop(l) */ 
 
 	return(l->Cur == l->Bot) ;
}







 
void PrintARVL(ARVecList_t l){

	fprintf(stderr, "%p\t%d\t%p\t%p\t%p\n\n", l, l->len, l->Top, l->Cur, l->Bot) ;
	PrintNodes(l->Top) ;
}
 




#ifdef TEST

 
 
 main(){
 
 
 
 /********* test basic creation and destruction ****/
 
 #ifdef NOT_DEF
 
   ARVecList_t l ;
   ARVec_t v ;

	v = ARVecNew(5) ;
	l = ARVLNew() ;
	
	fprintf(stderr, "l = %x\t, len = %d\n", l, ARVLLen(l)) ;
	
	PrintARVL(l) ;
	
	ARVLDel(l) ;
	fprintf(stderr, "del\n") ;
	l = ARVLNew() ; 
 	ARVLIns(l, v) ;
	fprintf(stderr, "insert l = %x\t, len = %d\n", l, ARVLLen(l)) ;
	fprintf(stderr, "vpointer %x\t%x\t%x\t%x\n", 
		v,
		ARVLTop(l),
		ARVLCur(l),
		ARVLBot(l)) ;
		
	ARVLDel(l) ;
	fprintf(stderr, "done\n") ;



/**** longer list this time ****/


  ARVecList_t l ;
  ARVec_t v ;
  int len,n ;
  
  	len = 5 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		ARVLIns(l, ARVecDup(v)) ;
		ARVLApp(l, ARVecDup(v)) ;
	}
	PrintARVL(l) ;
	ARVLDel(l);	


/******** remove items -1 ARVLPop ************/


  ARVecList_t l ;
  ARVec_t v ;
  int len,n ;
  
  	len = 5 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		ARVLIns(l, ARVecDup(v)) ;
		ARVLApp(l, ARVecDup(v)) ;
	}
	PrintARVL(l) ;


	while(ARVLLen(l) != 0){
		v = ARVLPop(l) ;
		fprintf(stderr, "\npopped %x\n", v) ;
		PrintARVL(l) ;
	}


	ARVLDel(l);	


/******** remove items -2 ARVLChop ************/


  ARVecList_t l ;
  ARVec_t v ;
  int len,n ;
  
  	len = 20 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		ARVLIns(l, ARVecDup(v)) ;
		ARVLApp(l, ARVecDup(v)) ;
	}
	PrintARVL(l) ;


	while(ARVLLen(l) != 0){
		v = ARVLChop(l) ;
		fprintf(stderr, "\nchopped %x\n", v) ;
		PrintARVL(l) ;
	}


	ARVLDel(l);	



  ARVecList_t l ;
  ARVec_t v ;
  int len,n ;
  
  	len = 2 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		ARVLIns(l, ARVecDup(v)) ;
		ARVLApp(l, ARVecDup(v)) ;
	}
	
	ARVLCur2Top(l) ;
	while(ARVLBot(l) != ARVLCur(l)){
		fprintf(stderr, "cur = %x\n",ARVLCur(l)) ; 
		ARVLCurNext(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",ARVLCur(l)) ; ;
	
	while(ARVLTop(l) != ARVLCur(l)){
		fprintf(stderr, "cur = %x\n",ARVLCur(l)) ;
		ARVLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",ARVLCur(l)) ;
	
	ARVLCur2Bot(l) ;
	while(ARVLTop(l) != ARVLCur(l)){
		fprintf(stderr, "cur = %x\n",ARVLCur(l)) ;
		ARVLCurPrev(l) ;
	}
	fprintf(stderr, "cur = %x\n\n",ARVLCur(l)) ;
	
	ARVLDel(l);	



  ARVecList_t l ;
  ARVec_t v ;
  int len,n ;
  
  	len = 20 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	for(n = 0 ; n < len ; n++){
		ARVecAddSc(v, ArbRatNew(1,1),0) ;
		ARVLIns(l, ARVecDup(v)) ;
	}
	
	ARVLCur2Bot(l) ;
	do{
		v = ARVLExtract(l) ;
		fprintf(stderr, "\nvvvvvvvvv\n") ;
		printels(v) ;
		fprintf(stderr, "\n") ;
		PrintARVL(l) ;
		fprintf(stderr, "\n^^^^^^^^\n") ;
		if(!ARVLCurAtTop(l))
			ARVLCurPrev(l) ;
	}while(!ARVLCurAtTop(l)) ;

	exit(0) ;
	
	#endif
	
	
	ARVecList_t l ;
  	ARVec_t v, v_ext ;
  	int len,n ;
  
  	len = 5 ;
	v = ARVecNew(len) ;
	l = ARVLNew() ;
	
	
	for(n = 0 ; n < len ; n++){
		ARVecSetEl(v, n, ArbRatNew(n,1)) ;
		ARVLIns(l, ARVecDup(v)) ;
		ARVLApp(l, ARVecDup(v)) ;
	}
	
	
	PrintARVL(l) ;
	
	v_ext = ARVLGetCol(l, 4) ;
	printels(v_ext) ;
	
	PrintARVL(l) ;
	
	ARVLDel(l);	
	

}


#endif

 
 
 
 
 















