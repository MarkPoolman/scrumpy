/**
H. Hartman
IntVec - vectors of integers
**/

#include <stdio.h>
#include <stdlib.h>

#include "IntVec.h"


struct IntVec {
	ulint len ;
	int *vec ;
} ;



/* printing for testing  */

void printEls(IntVec_t v){
	
 	ulint n ;  
  	for (n = 0 ; n < v->len-1 ; n++)
		fprintf(stderr, "%d  ", v->vec[n]) ;
		
	fprintf(stderr, "%d\n", v->vec[n]) ;
}

void printintv(IntVec_t v){
	
	fprintf(stderr, "%p %lu %p\n", v, v->len, v->vec) ;
}

/***** Create and destroy, NB: check that memory allocation is done properly  ****/


IntVec_t IntVecNew(ulint len) {
	/* pre: len > 0
	  post: returns a usable instance of IntVec_t with elements init to zero
	*/
	
	IntVec_t intvec ;
	ulint n ;
	
	intvec = (IntVec_t) malloc(sizeof(struct IntVec)); 
	intvec->len = len ;
	intvec->vec = (int *) calloc(sizeof(int), len) ;
	
	for (n = 0; n < len; n++){
		intvec->vec[n] = 0 ;
	}
	
	return intvec ;
}


IntVec_t IntVecDup(IntVec_t intv) {
	/* pre: intv usabl
	  post: returns a usable copy of intv
	*/
	
	IntVec_t copy ;
	ulint n ;
	
	copy = IntVecNew(intv->len) ;
	for (n = 0 ; n < intv->len ; n++){
		copy->vec[n] = intv->vec[n] ;
	}
	return copy ;
}


void IntVecDel(IntVec_t intv) {
	/* pre: intv usable
	  post: intv not usable, resources freed
	*/
	
	if (intv){
		free(intv->vec) ;
		free(intv) ;
	}
}


void IntVecCat(IntVec_t intv1, IntVec_t intv2) {
	/* pre: intv1, intv2 usable, intv1 != intv2
	  post: (copy of) intv2 appended to intv1
	*/
	
	ulint n ;
	struct IntVec temp ; /* temp is a IntVec structure, not a pointer to one */
	
	temp.len = intv1->len ;
	temp.vec = intv1->vec ;
	
	intv1->len += intv2->len ;
	intv1->vec = (int *) calloc(sizeof(int), intv1->len) ;
	
	for (n = 0 ; n < temp.len ; n++){
		intv1->vec[n] = temp.vec[n] ;
	}
	
	for ( ; n < intv1->len ; n++){
		intv1->vec[n] = intv2->vec[n - intv2->len] ;
	}
	free(temp.vec) ;
}


/**** scalar ******/

ulint IntVecLen(IntVec_t intv) { 
	/* pre: intv usable
	  post: returns length of intv
	*/

	return intv->len ;
}


ulint IntVecMaxIdx(IntVec_t intv) {
	/* pre: intv usable
	  post: returns max value usable as an index into intv
	*/
	return intv->len - 1 ;
}


int IntVecGetEl(IntVec_t intv, ulint idx) {
	/* pre: idx <= IntVecMaxIdx(intv) 
	  post: returns idxth element in intv
	*/ 

	return (intv->vec[idx]) ;
}


int IntVecGetElDup(IntVec_t intv, ulint idx) { 
	/* NB: check if valid
	   pre: idx <= IntVecMaxIdx(intv) 
	  post: returns copy of idxth element in intv
	*/ 
	
	int rv = intv->vec[idx] ;
	
	return rv ;
}	

void IntVecSetEl(IntVec_t intv, ulint idx, int i) {
	/* pre: idx <= IntVecMaxIdx(intv), i usable
	  post: r == IntVecGetEl(intv, idx)
	*/
	
	intv->vec[idx] = i ;
}


void IntVecNegate(IntVec_t intv) { 
	/* pre: intv usable
	  post: intv->vec' == intvec->vec(*-1)
	*/
	
	ulint n ;
	for (n = 0; n < intv->len ; n++){
		intv->vec[n] *= -1 ;
	}
}


void IntVecAddSc(IntVec_t intv, int i, ulint Offset) {  
	/* pre: intv usable, Offset <= IntVecMaxIdx(intv)
	  post: i added to elements of intv between offset and IntVecMaxIdx(intv) inclusive
	*/
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] += i ;
	}
}


void IntVecSubSc(IntVec_t intv, int i, ulint Offset) { 
	/* pre: intv usable
	  post: i subtracted from elements intv between offset and IntVecMaxIdx(intv) inclusive
	*/
	
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] -= i ;
	}
}


void IntVecMulSc(IntVec_t intv, int i, ulint Offset) {  
	/* pre: intv usable
	  post: elements of intv between offset and IntVecMaxIdx(intv) inclusive multiplied by r
	*/
	
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] *= i ;
	}
}


void IntVecDivSc(IntVec_t intv, int i, ulint Offset) { 
	/* pre: intv usable
	  post: elements of intv between offset and IntVecMaxIdx(intv) inclusive divided by i
	*/

	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] /= i ;
	}
}


/********** vector ***********/


void IntVecAdd(IntVec_t intv, IntVec_t intv1, ulint Offset) { 
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec + intv1->vec
	  	elements in intv1 Offset..IntVecMaxIdx(intv1) added to intv 
	*/
	
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] += intv1->vec[n] ;
	}
}


void IntVecSub(IntVec_t intv, IntVec_t intv1, ulint Offset) { 
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec - intv1->vec
	  		elements in intv1 Offset..IntVecMaxIdx(intv1) subtracted from intv 
	*/
	
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] -= intv1->vec[n] ;
	}
}



void IntVecMul(IntVec_t intv, IntVec_t intv1, ulint Offset) { 
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec * intv1->vec
	  		elements in intv Offset..IntVecMaxIdx(intv1) multiplied by intv 
	*/

	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] *= intv1->vec[n] ;
	}
}


void IntVecDiv(IntVec_t intv, IntVec_t intv1, ulint Offset) { 
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec * intv1->vec
	  		elements in intv Offset..IntVecMaxIdx(intv) divided by intv
	*/
	
	ulint n ;
	for (n = Offset ; n < intv->len ; n++){
		intv->vec[n] /= intv1->vec[n] ;
	}
}


/********* qualitative functions *********/

void IntVecZeros(Set_t s, IntVec_t intv, ulint Offset) {
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices > Offset of intv w/ zero value 
	*/ 

	ulint n ;
	
	SetMakeEmpty(s) ;
	for (n = Offset ; n < intv->len ; n++){
		if(!intv->vec[n]){ /* check this! */
			SetAddMem(s, n) ;
		}
	}
}


void IntVecNonZeros(Set_t s, IntVec_t intv, int Offset) {
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ non-zero value 
	*/
	
	IntVecZeros(s, intv, Offset) ;
	SetCompl(s, s) ;
}

void IntVecPos(Set_t s, IntVec_t intv){
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ +ve value 
	*/ 

	ulint n ;
	
	SetMakeEmpty(s) ;
	for (n = 0 ; n < intv->len ; n++){
		if(intv->vec[n] > 0){ 
			SetAddMem(s, n) ;
		}
	}
}


void IntVecNeg(Set_t s, IntVec_t intv){
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ -ve value 
	*/ 

	ulint n ;
	
	SetMakeEmpty(s) ;
	for (n = 0 ; n < intv->len ; n++){
		if(intv->vec[n] < 0){ 
			SetAddMem(s, n) ;
		}
	}
}



/********** string  **************/

/* 
 do we need this? 
 */









#ifdef TEST

/** testing, not done yet... ***/

main(){

#ifdef DONT_COMPILE_THIS


/********* create and destroy ****/

	IntVec_t v ;
	ulint n, len ;
	len = 5 ;
	
	v = IntVecNew(len) ;
	
	printintv(v) ;
	
	IntVecDel(v) ;
	


	
/******* print content of empty vector  *******/
	ulint len, n ;
	IntVec_t v ;
	len = 5 ;
	
	v = IntVecNew(len) ;
	printEls(v) ;
	
	IntVecDel(v) ;



/****** test IntVecSetEl ********/
	
	ulint len, n ;
	IntVec_t v ;
	len = 5 ;
	
	v = IntVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
	}
	printEls(v) ;
	IntVecDel(v) ;



	
/******* test IntVecDup ************/
	
	ulint len, n ;
	IntVec_t v, v1 ;
	len = 5 ;
	
	v = IntVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
	}
		
	v1 = IntVecDup(v) ;
	
	
	printEls(v) ;
	printEls(v1) ;
	
	IntVecDel(v) ;
	IntVecDel(v1) ;

	
	/******* test IntVecAddSc  ************/
	
	ulint len, n ;
	IntVec_t v ;
	int i ;
	len = 10 ;
	i = 5 ;
	
	v = IntVecNew(len) ;
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
	}
		
	IntVecAddSc(v, i, 5) ;

	printEls(v) ;
	IntVecDel(v) ;





	/******* test IntVecSubSc  *********/
	
	ulint len, n ;
	IntVec_t v ;
	int i ;
	len = 10 ;
	i = 5 ;
	
	v = IntVecNew(len) ;
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
	}
		
	IntVecSubSc(v, i, 5) ;

	printEls(v) ;
	IntVecDel(v) ;
	



	/******* test IntVecAdd and IntVecSub  ************/
	
	ulint len, n, off ;
	IntVec_t v, v1 ;
	len = 10 ;
	
	v = IntVecNew(len) ;
	v1 = IntVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecSetEl(v1,n,n+1) ;
	}
	
	
	printEls(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			IntVecAdd(v,v1,off) ;
			printEls(v) ;
		}
	}
	printEls(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			IntVecSub(v,v1,off) ;
			printEls(v) ;
		}
	}

	IntVecDel(v) ;
	IntVecDel(v1) ;


	/******* test IntVecMul and IntVecDiv  ************/
	
	ulint len, n, off ;
	IntVec_t v, v1 ;
	len = 3 ;
	
	v = IntVecNew(len) ;
	v1 = IntVecNew(len) ;
		
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecSetEl(v1,n,n+1) ;
	}
	
	printEls(v) ;
	printEls(v1) ;
	
	IntVecMul(v,v1,0) ;
	
	printEls(v) ;
	
	IntVecDel(v) ;
	IntVecDel(v1) ;
	
	
	
	/*********/
	
	ulint len, n, off ;
	IntVec_t v, v1 ;
	len = 3 ;
	
	v = IntVecNew(len) ;
	v1 = IntVecNew(len) ;
		

	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecSetEl(v1,n,n+1) ;
	}
	
	
	printEls(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			IntVecMul(v,v1,off) ;
			printEls(v) ;
		}
	}
	printEls(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			IntVecDiv(v,v1,off) ;
			printEls(v) ;
		}
	}

  	IntVecDel(v) ;
	IntVecDel(v1) ;
#endif	

	/***************** test the set functions  **********/
	
	ulint len, n, off ;
	IntVec_t v ;
	Set_t s ;
  
  	len = 64 ;
	s = SetNew(len) ;
	v = IntVecNew(len) ;
	
	
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecNonZeros(s, v, 0) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		IntVecZeros(s, v, 0) ;
		fprintf(stderr, "%s\n\n", SetStr(s)) ; 
	}
	
	for (n = -10 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecPos(s, v) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		IntVecPos(s, v) ;
		fprintf(stderr, "%s\n\n", SetStr(s)) ; 
	}
		
	IntVecDel(v) ;
	
#ifdef DONT_COMPILE_THIS	
		/***************** test ARVecCat  **********/
	
	
	
  	ulint len, n ;
  	IntVec_t v, v1 ;
    
  	len = 32 ;
	
	v = IntVecNew(len) ;
	v1 = IntVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		IntVecSetEl(v,n,n) ;
		IntVecSetEl(v1,n,n) ;
	}
	
	IntVecCat(v,v1) ;
	printEls(v) ;
	
	#endif
	
	exit(0) ;

}
#endif


	
	
