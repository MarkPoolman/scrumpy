

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman
27/03/01 Onward
ARVec - an ADT representing vectors of ArbRats 
****/


#ifndef ARVEC_H
#define ARVEC_H

#include "ArbRat.h"
#include "Set.h" 


typedef struct ARVec *ARVec_t ;



/***** Create and destroy  ****/


ARVec_t ARVecNew(ulint len) ;
	/* pre: len > 0
	  post: returns a usable instance of ARVec_t with elements init to zero
	*/
	 

ARVec_t ARVecDup(ARVec_t arv) ;
	/* pre: arv usabl
	  post: returns a usable copy of arv
	*/


void ARVecDel(ARVec_t arv) ;
	/* pre: arv usable
	  post: arv not usable, resources freed
	*/
	
void ARVecCat(ARVec_t arv1, ARVec_t arv2) ;
	/* pre: arv1, arv2 usable, arv1 != arv2
	  post: (copy of) arv2 appended to arv1
	*/
	


ulint ARVecLen(ARVec_t arv) ;
	/* pre: arv usable
	  post: returns length of arv
	*/

ulint ARVecMaxIdx(ARVec_t arv) ;
	/* pre: arv usable
	  post: returns max value usable as an index into arv
	*/

ArbRat_t ARVecGetEl(ARVec_t arv, ulint idx) ;
	/* pre: idx <= ARVecMaxIdx(arv) 
	  post: returns idxth element in arv
	*/ 

ArbRat_t ARVecGetElDup(ARVec_t arv, ulint idx) ;
	/* pre: idx <= ARVecMaxIdx(arv) 
	  post: returns copy of idxth element in arv
	*/ 

void ARVecSetEl(ARVec_t arv, ulint idx, ArbRat_t r) ;
	/* pre: idx <= ARVecMaxIdx(arv), r usable
	  post: ArbRatEq(r, ARVecGetEl(arv, idx))
	*/


/**** Scalar ops  *********/


void ARVecNegate(ARVec_t arv) ;
	/* pre: arv usable
	  post: all elements in arv negated */

void ARVecAddSc(ARVec_t arv, ArbRat_t r, ulint Offset) ; 
	/* pre: arv usable, Offset <= ARVecMaxIdx(arv)
	  post: r added to elements of arv between offset and ARVecMaxIdx(arv) inclusive
	*/

void ARVecSubSc(ARVec_t arv, ArbRat_t r, ulint Offset) ; 
	/* pre: arv usable
	  post: r subtracted from elements arv between offset and ARVecMaxIdx(arv) 						inclusive
	*/

void ARVecMulSc(ARVec_t arv, ArbRat_t r, ulint Offset) ; 
	/* pre: arv usable
	  post: elements of arv between offset and ARVecMaxIdx(arv) inclusive multiplied by r
	*/

void ARVecDivSc(ARVec_t arv, ArbRat_t r, ulint Offset) ; 
	/* pre: arv usable
	  post: elements of arv between offset and ARVecMaxIdx(arv) inclusive divided by r
	*/


/****** Vector ops ************/


void ARVecAdd(ARVec_t arv, ARVec_t arv1, ulint Offset) ; 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv1 Offset..ARVecMaxIdx(arv1) added to arv 		  
	*/

void ARVecSub(ARVec_t arv, ARVec_t arv1, ulint Offset) ; 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv1 Offset..ARVecMaxIdx(arv1) subtracted from  arv 
	*/

void ARVecMul(ARVec_t arv, ARVec_t arv1, ulint Offset) ; 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv Offset..ARVecMaxIdx(arv1) multiplied by arv 
	*/

void ARVecDiv(ARVec_t arv, ARVec_t arv1, ulint Offset) ; 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv Offset..ARVecMaxIdx(arv1) divided by arv
	*/



/********* qualitative functions *********/
void ARVecZeros(Set_t s, ARVec_t arv, ulint Offset) ;
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices > Offset of arv w/ zero value 
	*/ 

void ARVecNonZeros(Set_t s, ARVec_t arv, int Offset) ;
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ non-zero value 
	*/ 


void ARVecPos(Set_t s, ARVec_t arv) ;
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ +ve value 
	*/ 

void ARVecNegs(Set_t s, ARVec_t arv) ;
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ -ve value 
	*/ 



/********** string  **************/

void ARVec2Strs(ARVec_t arv, char **strs) ;
	/* pre: arv usable, strs = calloc(sizeof *char,  ARVecMaxIdx(arv)+1) 
	  post: strs'[0..ARVecMaxIdx(arv)] newly calloc()'d str repn of arv 
	*/


void printels(ARVec_t v) ;


#endif
