/**
H. Hartman
IntVec - vectors of integers 
**/

#ifndef INTVEC_H
#define INTVEC_H

#include "Set.h" 

typedef struct IntVec *IntVec_t ;

/***** Create and destroy **/

IntVec_t IntVecNew(ulint len) ;
	/* pre: len > 0
	  post: returns a usable instance of IntVec_t with elements init to zero
	*/

IntVec_t IntVecDup(IntVec_t intv) ;
	/* pre: intv usabl
	  post: returns a usable copy of intv
	*/
	
void IntVecDel(IntVec_t intv) ;
	/* pre: intv usable
	  post: intv not usable, resources freed
	*/
	
void IntVecCat(IntVec_t intv1, IntVec_t intv2) ;
	/* pre: intv1, intv2 usable, intv1 != intv2
	  post: (copy of) intv2 appended to intv1
	*/

/**** scalar ********/

ulint IntVecLen(IntVec_t intv) ;
	/* pre: intv usable
	  post: returns length of intv
	*/

ulint IntVecMaxIdx(IntVec_t intv) ;
	/* pre: intv usable
	  post: returns max value usable as an index into intv
	*/

int IntVecGetEl(IntVec_t intv, ulint idx) ;
	/* pre: idx <= IntVecMaxIdx(intv) 
	  post: returns idxth element in intv
	*/ 


int IntVecGetElDup(IntVec_t intv, ulint idx) ; 
	/* pre: idx <= IntVecMaxIdx(intv) 
	  post: returns copy of idxth element in intv
	*/ 


void IntVecSetEl(IntVec_t intv, ulint idx, int i) ;
	/* pre: idx <= IntVecMaxIdx(intv), i usable
	  post: r == IntVecGetEl(intv, idx)
	*/

void IntVecNegate(IntVec_t intv) ;
	/* pre: intv usable
	  post: intv->vec' == intvec->vec(*-1)
	*/
	
void IntVecAddSc(IntVec_t intv, int i, ulint Offset) ;  
	/* pre: intv usable, Offset <= IntVecMaxIdx(intv)
	  post: i added to elements of intv between offset and IntVecMaxIdx(intv) inclusive
	*/
	
void IntVecSubSc(IntVec_t intv, int i, ulint Offset) ;
	/* pre: intv usable
	  post: i subtracted from elements intv between offset and IntVecMaxIdx(intv) inclusive
	*/


void IntVecMulSc(IntVec_t intv, int i, ulint Offset) ;  
	/* pre: intv usable
	  post: elements of intv between offset and IntVecMaxIdx(intv) inclusive multiplied by r
	*/


void IntVecDivSc(IntVec_t intv, int i, ulint Offset) ;
	/* pre: intv usable
	  post: elements of intv between offset and IntVecMaxIdx(intv) inclusive divided by i
	*/


void IntVecAdd(IntVec_t intv, IntVec_t intv1, ulint Offset) ; 
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec + intv1->vec
	  	elements in arv1 Offset..ARVecMaxIdx(arv1) added to arv 
	*/

void IntVecSub(IntVec_t intv, IntVec_t intv1, ulint Offset) ;
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec - intv1->vec
	  		elements in arv1 Offset..ARVecMaxIdx(arv1) subtracted from arv 
	*/


void IntVecMul(IntVec_t intv, IntVec_t intv1, ulint Offset) ;
	/* pre: IntVecMaxIdx(intv) == IntVecMaxIdx(intv1), Offset <= IntVecMaxIdx(intv)
	  post: intv->vec' = intv->vec - intv1->vec
	  		elements in intv Offset..IntVecMaxIdx(intv1) multiplied by intv 
	*/

/********* qualitative functions *********/

void IntVecZeros(Set_t s, IntVec_t intv, ulint Offset) ;
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices > Offset of intv w/ zero value 
	*/ 


void IntVecNonZeros(Set_t s, IntVec_t intv, int Offset) ;
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ non-zero value 
	*/
	

void IntVecPos(Set_t s, IntVec_t intv);
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ +ve value 
	*/ 

void IntVecNeg(Set_t s, IntVec_t intv);
	/* pre: IntVecMaxIdx(intv) <= SetMax(s)
	  post: s' contains only the indices of intv w/ -ve value 
	*/
	
/*** string *****/

void printEls(IntVec_t v) ;
#endif

