

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman
27/03/01 Onward
ARVec - an ADT representing vectors of ArbRats 
****/

#include <stdio.h>

#include <stdlib.h>



#include "ARVec.h"






struct ARVec{
	ulint len ;
	ArbRat_t *vec ;
} ;




void printels(ARVec_t v){
	
  char **strs ;
  ulint n ;
  
	strs = (char**) calloc(sizeof(char*), ARVecMaxIdx(v)+1) ;   
  
	ARVec2Strs(v, strs) ;
  	for (n = 0 ; n < v->len-1 ; n++)
		fprintf(stderr, "%s  ", strs[n]) ;
	
	fprintf(stderr, "%s\n", strs[n]) ;
	for (n = 0 ; n < v->len-1 ; n++)
		free(strs[n]) ;
	free(strs[n]) ;
	free(strs) ;


}





/***** Create and destroy  ****/


ARVec_t ARVecNew(ulint len) { 
	/* pre: len > 0
	  post: returns a usable instance of ARVec_t with elements init to zero
	*/
	 
  ARVec_t rv ;
  ulint n ;
  
	rv = (ARVec_t) malloc(sizeof(struct ARVec)) ;
  	rv->len = len ;
	rv->vec = (ArbRat_t *) calloc(sizeof(ArbRat_t), len) ;

	for(n = 0 ; n < len ; n++)
		rv->vec[n] = ArbRatCreate()	;
  	 
	 return rv ;
}

	 

ARVec_t ARVecDup(ARVec_t arv) { 
	/* pre: arv usabl
	  post: returns a usable copy of arv
	*/

  ARVec_t rv ;
  ulint n ;
  
	rv = ARVecNew(arv->len) ;
  	for(n = 0 ; n < arv->len ; n++)
		ArbRatSet(rv->vec[n], arv->vec[n])  ;

	return rv ;

} 
	

void ARVecDel(ARVec_t arv) { 
	/* pre: arv usable
	  post: arv not usable, resources freed
	*/
	
  ulint n ; 

	if(arv){		/* arv might have been null */
	
  	
	  	for(n = 0 ; n < arv->len ; n++)
			ArbRatDel(arv->vec[n]) ;
	
		free(arv->vec) ;
		free(arv) ;
	}
}

	
void ARVecCat(ARVec_t arv1, ARVec_t arv2) {
	/* pre: arv1, arv2 usable, arv1 != arv2
	  post: (copy of) arv2 appended to arv1
	*/

  ulint n ;
  
  	struct ARVec tmp ; 
	
	tmp.len = arv1->len ;		/* a copy of the stuff that would otherwise */
	tmp.vec = arv1->vec ;		/* get overwritten */
	
  	arv1->len += arv2->len ;	/* length of the appended vec */	
	arv1->vec = (ArbRat_t *)    /* allocate the  vec */
		calloc(sizeof(ArbRat_t), arv1->len) ;

	for(n = 0 ; n < tmp.len ; n++)	/* copy the orginal vals back */
		arv1->vec[n] = tmp.vec[n]	;
		
	for( ; n < arv1->len ; n++)		/* duplicate the values in arv2 */
		arv1->vec[n] = ArbRatDup(arv2->vec[n - arv2->len]) ;
  	 
	free (tmp.vec) ;	/* nothing else wants free()ing */
}
	

/**** Scalar ops  *********/


ulint ARVecLen(ARVec_t arv) { 
	/* pre: arv usable
	  post: returns length of arv
	*/

	return arv->len ;
}
	
ulint ARVecMaxIdx(ARVec_t arv) { 
	/* pre: arv usable
	  post: returns max value usable as an index into arv
	*/

	return arv->len -1 ;
}



ArbRat_t ARVecGetEl(ARVec_t arv, ulint idx) {
	/* pre: idx <= ARVecMaxIdx(arv) 
	  post: returns idxth element in arv
	*/ 

	return (arv->vec[idx]) ;

}

ArbRat_t ARVecGetElDup(ARVec_t arv, ulint idx) { 
	/* pre: idx <= ARVecMaxIdx(arv) 
	  post: returns copy of idxth element in arv
	*/ 

	return ArbRatDup(arv->vec[idx]) ;
}
	
	
void ARVecSetEl(ARVec_t arv, ulint idx, ArbRat_t r) {
	/* pre: idx <= ARVecMaxIdx(arv), r usable
	  post: ArbRatEq(r, ARVecGetEl(arv, idx))
	*/
	
	
	ArbRatSet(arv->vec[idx], r) ;
}
	



void ARVecNegate(ARVec_t arv) { 
	/* pre: arv usable, Offset <= ARVecMaxIdx(arv)
	  post: r added to elements of arv between offset and ARVecMaxIdx(arv) inclusive
	*/

  ulint n ;
  for(n = 0 ; n < arv->len ; n++)
		ArbRatNeg(arv->vec[n]) ;
}


void ARVecAddSc(ARVec_t arv, ArbRat_t r, ulint Offset) {  
	/* pre: arv usable, Offset <= ARVecMaxIdx(arv)
	  post: r added to elements of arv between offset and ARVecMaxIdx(arv) inclusive
	*/

  ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatAdd(arv->vec[n], r) ;
} ;


	
void ARVecSubSc(ARVec_t arv, ArbRat_t r, ulint Offset) { 
	/* pre: arv usable
	  post: r subtracted from elements arv between offset and ARVecMaxIdx(arv) 						inclusive
	*/
	
	
  ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatSub(arv->vec[n], r) ;
} ;

void ARVecMulSc(ARVec_t arv, ArbRat_t r, ulint Offset) {  
	/* pre: arv usable
	  post: elements of arv between offset and ARVecMaxIdx(arv) inclusive multiplied by r
	*/
	
  ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatMul(arv->vec[n], r) ;
} ;
	

void ARVecDivSc(ARVec_t arv, ArbRat_t r, ulint Offset) { 
	/* pre: arv usable
	  post: elements of arv between offset and ARVecMaxIdx(arv) inclusive divided by r
	*/
	
  ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatDiv(arv->vec[n], r) ;
}

/****** Vector ops ************/


void ARVecAdd(ARVec_t arv, ARVec_t arv1, ulint Offset) { 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv1 Offset..ARVecMaxIdx(arv1) added to arv 
	*/
	
  ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatAdd(arv->vec[n], arv1->vec[n]) ;
}

	
	

void ARVecSub(ARVec_t arv, ARVec_t arv1, ulint Offset) { 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv1 Offset..ARVecMaxIdx(arv1) subtracted from  arv 
	*/


 ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatSub(arv->vec[n], arv1->vec[n]) ;
}




void ARVecMul(ARVec_t arv, ARVec_t arv1, ulint Offset) { 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv Offset..ARVecMaxIdx(arv1) multiplied by arv 
	*/


 ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatMul(arv->vec[n], arv1->vec[n]) ;
}


void ARVecDiv(ARVec_t arv, ARVec_t arv1, ulint Offset) { 
	/* pre: ARVecMaxIdx(arv) == ARVecMaxIdx(arv1), Offset <= ARVecMaxIdx(arv)
	  post: arv1' = arv1
	  		elements in arv Offset..ARVecMaxIdx(arv1) divided by arv
	*/

 ulint n ;
  
  	for(n = Offset ; n < arv->len ; n++)
		ArbRatDiv(arv->vec[n], arv1->vec[n]) ;
}



/********* qualitative functions *********/

void ARVecZeros(Set_t s, ARVec_t arv, ulint Offset) {
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices > Offset of arv w/ zero value 
	*/ 
 
 ulint n ;
 
 	SetMakeEmpty(s) ;
	for(n = Offset ; n <  arv->len ; n ++)
		if(ArbRatEqZero(arv->vec[n])){
			SetAddMem(s, n) ;
		}
}


void ARVecNonZeros(Set_t s, ARVec_t arv, int Offset) {
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ non-zero value 
	*/ 

	ARVecZeros(s, arv, Offset) ;
	SetCompl(s, s) ;
}

void ARVecPos(Set_t s, ARVec_t arv){
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ +ve value 
	*/ 


 ulint n ;
 
 	SetMakeEmpty(s) ;
	for(n = 0 ; n <  arv->len ; n ++)
		if(ArbRatIsPos(arv->vec[n]))
			SetAddMem(s, n) ;
			
}

void ARVecNegs(Set_t s, ARVec_t arv) {
	/* pre: ARVecMaxIdx(arv) <= SetMax(s)
	  post: s' contains only the indices of arv w/ -ve value 
	*/ 

 ulint n ;
 
 	SetMakeEmpty(s) ;
	for(n = 0 ; n <  arv->len ; n ++)
		if(ArbRatIsNeg(arv->vec[n]))
			SetAddMem(s, n) ;
			
}



/********** string  **************/

void ARVec2Strs(ARVec_t arv, char **strs){
	/* pre: arv usable, strs = calloc(sizeof *char,  ARVecMaxIdx(arv)+1) 
	  post: strs'[0..ARVecMaxIdx(arv)] newly calloc()'d str repn of arv 
	*/
	
  ulint  n ;
  
	for(n = 0 ; n < arv->len ; n++)
		ArbRat2Str(arv->vec[n], strs+n) ;
}




#ifdef TEST
/* void printels(ARVec_t v){
	
  char **strs ;
  int n ;
  
	strs = (char**) calloc(sizeof(char*), ARVecMaxIdx(v)+1) ;   
  
	ARVec2Strs(v, strs) ;
  	for (n = 0 ; n < v->len-1 ; n++)
		fprintf(stderr, "%s ", strs[n]) ;
	
	fprintf(stderr, "%s\n", strs[n]) ;
	for (n = 0 ; n < v->len-1 ; n++)
		free(strs[n]) ;
	free(strs[n]) ;
	free(strs) ;


}
  */



void printarv(ARVec_t v){
	
	fprintf(stderr, "%p %lu %p\n", v, v->len, v->vec) ;
}


main(){

#ifdef DONT_COMPILE_THIS	
/******* test new str  *******/
	ulint len, n ;
	char *str ;
	ARVec_t v ;
	len = 5 ;
	
	v = ARVecNew(len) ;
	for (n = 0 ; n < len ; n++){
		ArbRat2Str(ARVecGetEl(v,n), &str) ;
		fprintf(stderr, "%s\n", str) ;
	}
	
	
/****** test ARVecSetEl ********/
	
	ulint len, n ;
	char *str ;
	ARVec_t v ;
	len = 5 ;
	
	v = ARVecNew(len) ;
	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		ArbRat2Str(ARVecGetEl(v,n), &str) ;
		fprintf(stderr, "%s\n", str) ;
	}

	
/******* test ARVecDup and ARVecDel  ************/
	
	ulint len, n ;
	char *str ;
	ARVec_t v, v1 ;
	len = 5 ;
	
	v = ARVecNew(len) ;
	for (n = 0 ; n < len ; n++) 
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		
	v1 = ARVecDup(v) ;
	for (n = 0 ; n < len ; n++) {
		ArbRat2Str(ARVecGetEl(v1,n), &str) ;
		fprintf(stderr, "%s\n", str) ;
	}

	ARVecDel(v)  ; 
	ARVecDel(v1) ;  


	
	
	/******* test ARVecAddSc  ************/
	
	ulint len, n ;
	char *str, **strs ;
	ARVec_t v ;
	ArbRat_t r ;
	len = 10 ;
	
	v = ARVecNew(len) ;
	r = ArbRatNew(1,5) ;
	for (n = 0 ; n < len ; n++) 
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		
	ARVecAddSc(v,r,5) ; 

	for (n = 0 ; n < len ; n++) {
		ArbRat2Str(ARVecGetEl(v,n), &str) ;
		fprintf(stderr, "%s\n", str) ;
	}

	/******* and ARVecSubSc with ARVec2Strs  *********/
	
	ARVecSubSc(v,r,5) ;
	strs = (char**) calloc(sizeof(char*), ARVecMaxIdx(v)+1) ; 
	/*
	ARVec2Strs(v, strs) ;
	for (n = 0 ; n < len ; n++) 
		fprintf(stderr, "%s\n", strs[n]) ;
	*/
	printels(v) ;
	

	/******* test ARVecAdd and ARVecSub  ************/
	
	ulint len, n, off ;
	char *str, **strs ;
	ARVec_t v, v1 ;
	len = 10 ;
	
	v = ARVecNew(len) ;
	v1 = ARVecNew(len) ;

	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		ARVecSetEl(v1,n, ArbRatNew(len,n+1)) ;	
	}
	
	
	printels(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			ARVecAdd(v,v1,off) ;
			printels(v) ;
		}
	}
	printels(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			ARVecSub(v,v1,off) ;
			printels(v) ;
		}
	}

    
	/******* test ARVecMul and ARVecDiv  ************/
	
	
	
	ulint len, n, off ;
	char *str, **strs ;
	ARVec_t v, v1 ;
	len = 3 ;
	
		
	v = ARVecNew(len) ;
	v1 = ARVecNew(len) ;

	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		ARVecSetEl(v1,n, ArbRatNew(len,n+1)) ;	
	}
	printels(v) ;
	printels(v1) ;
	ARVecMul(v,v1,0) ;
	printels(v) ;
	
	
	ulint len, n, off ;
	char *str, **strs ;
	ARVec_t v, v1 ;
	len = 3 ;
	
	v = ARVecNew(len) ;
	v1 = ARVecNew(len) ;

	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		ARVecSetEl(v1,n, ArbRatNew(len,n+1)) ;	
	}
	
	
	printels(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			ARVecMul(v,v1,off) ;
			printels(v) ;
		}
	}
	printels(v) ;
	for (n = 0 ; n < len ; n++){
		for(off = 0 ; off < len ; off++){
			ARVecDiv(v,v1,off) ;
			printels(v) ;
		}
	}

	
	
	/***************** test the set functions  **********/
	
	
	
  ulint len, n ;
  ARVec_t v ;
  Set_t s ;
  
  	len = 64 ;
	s = SetNew(len) ;

	v = ARVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(n,len)) ;
		ARVecNonZeros(s, v) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		ARVecZeros(s, v) ; 
		fprintf(stderr, "%s\n\n", SetStr(s)) ; 
	}
	
	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v,n, ArbRatNew(-n,len)) ;
		ARVecPos(s, v) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		ARVecNegs(s, v) ; 
		fprintf(stderr, "%s\n\n", SetStr(s)) ; 
	}
		


	
	
		/***************** test ARVecCat  **********/
	
	
	
  ulint len, n ;
  ARVec_t v1,v2 ;
    
  	len = 32 ;
	
	v1 = ARVecNew(len) ;
	v2 = ARVecNew(len) ;
	
	for (n = 0 ; n < len ; n++){
		ARVecSetEl(v1,n, ArbRatNew(n,len)) ;
		ARVecSetEl(v2,n, ArbRatNew(-n,len)) ;
	}
	
	ARVecCat(v1,v2) ;
	printels(v1) ;
	
	#endif
	
	/********** testcombine *******/
	
	int v_neg[] = {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,-2,3,0,-5,-1,5,2,2,-3,-3,-3,0,-3,-3,1} ;
        int v_pos[]=  {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,5,-17,0,27,3,-23,-5,-5,17,17,17,0,31,31,-3} ; 
	
	ARVec_t neg, pos, rv ;
	
	ulint nreacs = 31, n ;
	


	neg = ARVecNew(nreacs) ;
	pos = ARVecNew(nreacs) ;
	
	for(n = 0; n < nreacs; n++ ) {
		if (n == 17 || n == 20 || n == 21 || n == 24 || n == 28 || n == 29) {
			ARVecSetEl(neg, n, ArbRatNew(v_neg[n],2)) ;
		}
		else {
			ARVecSetEl(neg, n, ArbRatNew(v_neg[n],1)) ;
		}
		if (n == 16 || n == 22 || n == 23) {
			ARVecSetEl(pos, n, ArbRatNew(v_pos[n],2)) ;
		}
		else if (n == 17 || n == 20 || n == 21 || n == 24 || n == 28 || n == 29){
			ARVecSetEl(pos, n, ArbRatNew(v_pos[n],8)) ;
		}
		else if (n == 17 || n == 20 || n == 21 || n == 24 || n == 28 || n == 29) {
			ARVecSetEl(pos, n, ArbRatNew(v_pos[n],8)) ;
		}
		else if (n == 19 || n == 25 || n == 26 || n == 27 || n == 30) {
			ARVecSetEl(pos, n, ArbRatNew(v_pos[n],4)) ;
		}
		else {
			ARVecSetEl(pos, n, ArbRatNew(v_pos[n],1)) ;
		}
	}

	
	/* printels(neg) ;
	printels(pos) ;	 */
	
	rv = CombineNumVecs(16, pos, neg) ;
	
	exit(0) ;

}
#endif
		


