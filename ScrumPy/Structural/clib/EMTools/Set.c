

/*****************************************************************************

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


*****************************************************************************/
/***
M.G.Poolman 27/03/01 onward
Set  -- An ADT handling sets of integers
***/


#include<stdio.h>
#include<limits.h>
#include<stdlib.h>
#include "Set.h"


typedef ulint Chunk ;
/* Chunks are bit strings with no numerical significance (whatever the compiler thinks) */

const ulint BitsPerChunk = CHAR_BIT * sizeof(Chunk)  ;
const ulint One = 1 ;


struct Set {
	ulint Max,			/* max value of a member */ 
		  n_chunks ;    /* number of Chunks we have */
	Chunk *Chunks ;		
} ;


	/************** create and destroy  *****************/
	

Set_t SetNew(ulint len) {
	/* pre: True
	  post: SetMax(SetNew(len)) >= len, 
	  		for all valid m: !SetHasMem(s,m) 
	*/

  Set_t s ;
  
	s =  (Set_t) malloc(sizeof (struct Set)) ;	
	s->n_chunks = One + (len - One) / BitsPerChunk ;
	s->Max = (s->n_chunks * BitsPerChunk) -One ;
	s->Chunks = (Chunk *) calloc(s->n_chunks, sizeof(Chunk)) ;
	return s ;
} 
	
	

void SetDel(Set_t s) { 
	/* pre: s usable
	  post: s not usable, resources freed 
	*/
	
	free(s->Chunks) ;
	free(s) ;
}



	/************* get some useful info ****************/

ulint nthCh(Set_t s, ulint n){
  return s->Chunks[n] ;
 }

char *SetStr(Set_t s) { 
	/* pre: s usable
	  post: SetStr(s) == a newly alloc()'d string representation of s
	*/
	
  char *rv ;
  Chunk BitMask ; 
  ulint nchunk, nbit, nchar  ; /* count bits, chunks and chars */
  
  
  	rv = (char *) calloc(s->Max + One, One) ;
	nchar = 0 ;
	for (nchunk = 0 ; nchunk < s->n_chunks ; nchunk++){
		BitMask = One ;
		for(nbit = 0 ; nbit < BitsPerChunk ; nbit++){
			rv[nchar] = (s->Chunks[nchunk] & BitMask) ? '1' : '0' ;
			
			BitMask <<= One ;		/* move the BitMask on 1 */
			nchar++ ;			/* increment character index */
		}
	}
	return rv ;
}
	
	
ulint SetMax(Set_t s) {
	/* pre: s usable
	  post: SetMax(s) == maximum value a member of s may take
	*/


	return s->Max ;
}



Bool SetIsEmpty(Set_t s){
	/* pre: s usable
	  post: SetIsEmpty(s) => s is empty   :-) */

  ulint n ;
  
  	for(n = 0 ; n < s->n_chunks ; n++)
		if(s->Chunks[n])
			return FALSE ;
			
	return TRUE ;
}


	/************** Set operations  *******************/ 
	
	/*** with members  ***/
	
Bool SetHasMem(Set_t s, ulint m) { 
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s,m) => m present in s
	*/

  ulint Chu_idx, Bit_idx ; 	/* chunk index, Bit index */
  Chunk BitMask ;
  
  	Chu_idx = m / BitsPerChunk ;
	Bit_idx = m % BitsPerChunk ;
	BitMask = One << Bit_idx ;
		
	return s->Chunks[Chu_idx] & BitMask;
} ;
	
	
	

void SetAddMem(Set_t s, ulint m) { 
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s', m)
	*/

  ulint Chu_idx, Bit_idx ; 	/* chunk index, Bit index */
  Chunk BitMask ;
    
  	Chu_idx = m / BitsPerChunk ;
	Bit_idx = m % BitsPerChunk ;
	BitMask = One << Bit_idx ;
	s->Chunks[Chu_idx] |= BitMask ;
}
	

void SetDelMem(Set_t s, ulint m) { 
	/* pre: m <= SetMax(s)
	  post: !SetHasMem(s',m)
	*/

  int Chu_idx, Bit_idx ; 	/* chunk index, Bit index */
  Chunk BitMask ;
  
  	Chu_idx = m / BitsPerChunk ;
	Bit_idx = m % BitsPerChunk ;
	BitMask = ~(One << Bit_idx) ;
	
	s->Chunks[Chu_idx] &= BitMask ;
}



void SetAddMemBool(Set_t s, ulint m, Bool b){
	/* pre: m <= SetMax(s)
	  post: SetHasMem(s', m, b) == b
	 */

	b ? SetAddMem(s,m) : SetDelMem(s,m) ;
	/** 
		I'm sure there's an elagent bit twid64dle instead of this,
		Any ideas ?
	**/
}


void SetMakeEmpty(Set_t s){
	/* pre: s usable
	  post: for m[0..SetMax(s): !SetHasMem(s',m)
	*/ 

  ulint n ;
  
	for (n = 0 ; n < s->n_chunks ; n++)
		s->Chunks[n] = 0 ;
		
}  	



	
	
	/*** with other sets ***/



Set_t SetDup(Set_t s){
	/* pre: s usable
	  post: returns a duplicate of s */

  Set_t rv ;
  ulint n ;
  
  	rv = SetNew(s->Max) ;
	for(n = 0 ; n < s->n_chunks ; n++)
		rv->Chunks[n] = s->Chunks[n] ;
		
	return rv ;
} 



	
Bool SetIsSub(Set_t s1, Set_t s2) { 
	/* pre: SetMax(s1) == SetMax(s2)
	  post: SetIsSub(s1,s2) => Sub is a Subset of Set
	*/

  ulint Chu_idx ;
  
 	for(Chu_idx = 0 ; Chu_idx < s1->n_chunks ; Chu_idx++)
		if((s1->Chunks[Chu_idx] & s2->Chunks[Chu_idx]) != s1->Chunks[Chu_idx])
			return FALSE ;
					
	return TRUE ;
}
	

Bool SetIsEqual(Set_t s1, Set_t s2) { 
	/* pre: SetMax(s1) == SetMax(s2)
	  post: SetIsEqual(s1, s2) => 
	  	for all valid m: SetHasMem(s1, m) == SetHasMem(s2, m)
	*/
	
  ulint Chu_idx ;
  
	for(Chu_idx = 0 ; Chu_idx < s1->n_chunks ; Chu_idx++)
		if(s1->Chunks[Chu_idx] != s2->Chunks[Chu_idx])
			return FALSE ;
					
	return TRUE ;
}
	
	
void SetInter(Set_t out, Set_t s1, Set_t s2) {
	/* pre: SetMax(out) >= (SetMax(s1) == SetMax(s2))
	  post: out == the intersection of s1 with s2
	*/

  ulint Chu_idx ;
  
	for(Chu_idx = 0 ; Chu_idx < out->n_chunks ; Chu_idx++)
		out->Chunks[Chu_idx] = s1->Chunks[Chu_idx] & s2->Chunks[Chu_idx] ;	
}



void SetUnion(Set_t out, Set_t s1, Set_t s2) {
	/* pre: SetMax(out) >= (SetMax(s1) == SetMax(s2))
	  post: out == the union of s1 and s2
	*/

  ulint Chu_idx ;

	for(Chu_idx = 0 ; Chu_idx < out->n_chunks ; Chu_idx++)
		out->Chunks[Chu_idx] = s1->Chunks[Chu_idx] | s2->Chunks[Chu_idx] ;	
}



void SetCompl(Set_t out, Set_t s) {
	/* pre: SetMax(out) == SetMax(s)
	  post: for m[0..SetMax(s)]: 
				SetHasMem(out,m) <=> !SetHasMem(s ,m)
				SetHasMem(s ,m) <=> !SetHasMem(out,m) 
	*/
	
  ulint Chu_idx ;

	for(Chu_idx = 0 ; Chu_idx < out->n_chunks ; Chu_idx++)
		out->Chunks[Chu_idx] = ~s->Chunks[Chu_idx] ;	
}

	

	
	
#ifdef TEST

main(){


#ifdef DONT_COMPILE_THIS


/****** test SetNew, SetMax, and SetStr */
  Set_t s ;
					
	s = SetNew(32) ;
	fprintf(stderr, "%d : %s\n", SetMax(s), SetStr(s)) ;

/*#ifdef NOT_DEF*/
/****** test SetAddMem and SetStr */
  Set_t s ;
  ulint max, n ;
	
	max = 63 ;
	s = SetNew(max) ;
	fprintf(stderr, "%s\n", SetStr(s)) ;
	for(n = 0 ; n <= max ; n++){
		SetAddMem(s, n) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
	}

/****** test SetDelMem  ************/
	
	for(n = 0 ; n <= max ; n++){
		SetDelMem(s, n) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
	}


/***** test SetHasMem *************/
	
  Set_t s ;
  ulint max, n ;
	
	max = 63 ;
	s = SetNew(max) ;
	fprintf(stderr, "%s\n", SetStr(s)) ;
	for(n = 0 ; n <= max ; n++){
		SetAddMem(s, n) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		if (!SetHasMem(s,n)){
			fprintf(stderr,"!!!!! SetHasMem 1 (%d) inconsistent !!!!!\n",n) ;
			/*exit(1) ;*/
		}
	}
	
	for(n = 0 ; n <= max ; n++){
		SetDelMem(s, n) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
		if (SetHasMem(s,n)){
			fprintf(stderr,"!!!!! SetHasMem 2 inconsistent !!!!!\n") ;
			exit(1) ;
		}
		
	}
	
	for(n = 1 ; n <= max ; n++){
		SetAddMem(s, n) ;
		SetDelMem(s, n-1) ;
		
		fprintf(stderr, "n-1 = %d ?\t%d\tn = %d ?\t%d\n", 
			n-1, SetHasMem(s,n-1) ? 1:0, n, SetHasMem(s,n)?1:0) ;
		
	}	
		

	
	/*********** test IsSubSet ***************/
	
  int max, s1dat[] = {1,3,500}, s2dat[] = {1,3,500,9}, n ;	
  Set_t s1, s2 ;
	
  s1 = SetNew(500) ;
  s2 = SetNew(500) ;
  
  	for(n = 0; n < 3 ; n++)
  		SetAddMem(s1, s1dat[n]) ;
	
	for(n = 0; n < 4 ; n++)
  		SetAddMem(s2, s2dat[n]) ;
	
  	fprintf(stderr, "\nIsSubSet(s1,s2) %d\tIsSubSet(s2,s1)%d\n",
		SetIsSub(s1,s2)?1:0, SetIsSub(s2,s1)?1:0) ;




  
/******** test IsEqual ****/	
	
  int max, s1dat[] = {1,3,500}, s2dat[] = {1,3,500,9}, n ;	
  Set_t s1, s2 ;
	
  s1 = SetNew(500) ;
  s2 = SetNew(500) ;
  
  	for(n = 0; n < 3 ; n++)
  		SetAddMem(s1, s1dat[n]) ;
	
	for(n = 0; n < 4 ; n++)
  		SetAddMem(s2, s2dat[n]) ;
	
  	fprintf(stderr, "\nSetIsEqual(s1,s2) %d\tSetIsEqual(s2,s1)%d\n",
		SetIsEqual(s1,s2)?1:0, SetIsEqual(s2,s1)?1:0) ;

	SetDelMem(s2, 9) ;
	
	fprintf(stderr, "\nSetIsEqual(s1,s2) %d\tSetIsEqual(s2,s1)%d\n",
		SetIsEqual(s1,s2)?1:0, SetIsEqual(s2,s1)?1:0) ;

	

	
/******** test SetInter ****/	
	
  int n, size ;	
  Set_t s1, s2, out ;

	size = 64 ;

	s1 = SetNew(size) ;
	s2 = SetNew(size) ;
  	out = SetNew(size) ;
	
  	for(n = 0; n < size ; n++)
  		SetAddMemBool(s1, n, (n%2)!=0) ;
	
	for(n = 0; n < size ; n++)
  		SetAddMemBool(s2, n, (n%3)!=0) ;
	
	SetInter(out, s1, s1) ;
  	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s1),
			SetStr(out)) ;

	SetInter(out, s1, s2) ;
  	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s2),
			SetStr(out)) ;

	SetMakeEmpty(s1) ;
	SetInter(out, s1, s2) ;
	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s2),
			SetStr(out)) ;

	exit(0) ;

/***** test SetUnion  *********/
	
  int n, size ;	
  Set_t s1, s2, out ;

	size = 64 ;

	s1 = SetNew(size) ;
	s2 = SetNew(size) ;
  	out = SetNew(size) ;
	
  	for(n = 0; n < size ; n++)
  		SetAddMemBool(s1, n, (n%2)==0) ;
	
	for(n = 0; n < size ; n++)
  		SetAddMemBool(s2, n, (n%3)==0) ;
	
	SetUnion(out, s1, s1) ;
  	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s1),
			SetStr(out)) ;

	SetUnion(out, s1, s2) ;
  	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s2),
			SetStr(out)) ;

	SetMakeEmpty(s1) ;
	SetUnion(out, s1, s2) ;
	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s2),
			SetStr(out)) ;

	exit(0) ;

	
/*****  test SetCompl ***/

  int n, size ;	
  Set_t s1, s2, out ;

	size = 64 ;

	s1 = SetNew(size) ;
	s2 = SetNew(size) ;
  	out = SetNew(size) ;
	
  	for(n = 0; n < size ; n++)
  		SetAddMemBool(s1, n, (n%2)==0) ;
	
	for(n = 0; n < size ; n++)
  		SetAddMemBool(s2, n, (n%3)==0) ;
	
	SetCompl(out, s1) ;
  	fprintf(stderr, "\n%s\n%s\n",
			SetStr(s1),
			SetStr(out)) ;

	SetCompl(out, s2) ;
  	fprintf(stderr, "\n%s\n%s\n%s\n",
			SetStr(s1),
			SetStr(s2),
			SetStr(out)) ;

	SetMakeEmpty(s1) ;
	SetCompl(out, s1) ;
  	fprintf(stderr, "\n%s\n%s\n",
			SetStr(s1),
			SetStr(out)) ;
			
	SetCompl(out, out) ;
  	fprintf(stderr, "\n%s sc\n",SetStr(out)) ;


	exit(0) ;
	



/********* test SetMakeEmpty  **********/



  Set_t s ;

  ulint size,n ;
  size = 64 ;
  
  s = SetNew(size) ;
  
  
  	for(n = 0; n < size ; n++){
  		SetAddMem(s, n) ;
		fprintf(stderr, "\n%s\n", SetStr(s)) ;
		SetMakeEmpty(s) ;
		fprintf(stderr, "%s\n", SetStr(s)) ;
	}
	
	
	for(n = 0; n < size ; n++)
  		SetAddMem(s, n) ;
		
	fprintf(stderr, "\n%s\n", SetStr(s)) ;
	SetMakeEmpty(s) ;
	fprintf(stderr, "%s\n", SetStr(s)) ;
		  
  exit(0) ;

	
	

/********* test SetDup  **********/



  Set_t s ;

  ulint size,n ;
  size = 64 ;
  
  s = SetNew(size) ;
  
  
  	for(n = 0; n < size ; n++){
  		SetAddMem(s, n) ;
		fprintf(stderr, "\n%s\n%s\n", SetStr(s), SetStr(SetDup(s))) ;
	}
	
	exit(0) ;



/*** test SetIsEmpty  ****/



  Set_t s ;

  ulint size,n ;
  size = 64 ;
  
  s = SetNew(size) ;
  
  	for(n = 0; n < size ; n++){
		SetAddMem(s, n) ;
		fprintf(stderr, "\n->%s\nSet Is %s<-\n", SetStr(s), 
			SetIsEmpty(s) ? "Empty" : "Not Empty") ; 
  		SetMakeEmpty(s) ;
		fprintf(stderr, "->%s\nSet Is %s<-\n", SetStr(s), 
			SetIsEmpty(s) ? "Empty" : "Not Empty") ; 
	}
	
	exit(0) ;


#endif	
	/*********** test IsSubSet ***************/
	
  int max, s1dat[] = {1,3,500}, s2dat[] = {1,3,500,9}, n ;	
  Set_t s1, s2 ;
	
  s1 = SetNew(500) ;
  s2 = SetNew(500) ;
  
  	for(n = 0; n < 3 ; n++)
  		SetAddMem(s1, s1dat[n]) ;
	
	for(n = 0; n < 4 ; n++)
  		SetAddMem(s2, s2dat[n]) ;
	
  	fprintf(stderr, "\nIsSubSet(s1,s2) %d\tIsSubSet(s2,s1)%d\n",
		SetIsSub(s1,s2)?1:0, SetIsSub(s2,s1)?1:0) ;



/*#endif*/



	
}	

#endif	
