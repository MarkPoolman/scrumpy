


"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2001 - 2018 Copy

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
"""
M. Poolman 10/01/01
Dynamic matrix class
"""


import  math,sys, random
Pi = math.pi
PiOver2 = Pi/2


from . import  Sci,Seq,Sorter,Types,LaTeX, Tree, Format
#TODO: tree won't import at startup, error from internal ete2 imports
# however is OK if imported after startup
# see

import ScrumPy


DefaultConv = Types.ArbRat
DynMatrixError = "Dynamic Matrix Error"
Tol = 1e-9 # tolerance for floating point error checks etc.

def DepWarn(Str):
    sys.stderr.write( "!! Deprecated " + Str+" !!" )


def GetIdent(n, Conv=DefaultConv, mul=1):
    rv = matrix(nrow=n,ncol=n,Conv=Conv)
    rv.MakeIdent(mul)
    return rv

def FromVec(vec, Conv=None):

    if not Conv:
        vtype = type(vec[0]).__name__
        try:
            Conv = {
                'float' : float,
                'int' : int,
                'long int' : int
                }[vtype]
        except KeyError:
            Conv = DefaultConv

    size = len(vec)
    rv = matrix(size,size,Conv)
    for n in range(size):
        rv[n,n] = Conv(vec[n])

    return rv






def Float2Str(f,sf):
    return format(f, ".SFg".replace("SF", str(sf)))
        





class matrix:

    El2Str = str
    PrintSF = 3 # sig figs to print if elements are floats
    
    def __init__(self, nrow=0, ncol=0,rnames=[],cnames=[], Conv=DefaultConv, FromMtx=None):

        self.Conv = Conv
        self.rnames = []
        self.cnames = []
        self.rows = []
        
        if FromMtx is not None:  
            self.FromDynMatrix(FromMtx)

        else:

            if len(rnames) != 0:
                self.rnames = rnames[:]
            else:
                for r in range(nrow):
                    self.rnames.append("row"+str(r))

            if len(cnames) != 0:
                self.cnames = list(cnames)
            else:
                for c in range(ncol):
                    self.cnames.append("col"+str(c))

            Zero = self.Conv(0)
            lenr = len(self.cnames)
            for r in range(len(self.rnames)):
                self.rows.append([Zero] *lenr)

        #if type(self.Conv(1))==float: # self.Conv is a function, so check what it returns
        #    self.El2Str = self.Float2Str
        self.El2Str = str # users or subclasses can modify as needed.
                          # We stick to a safe default

        self.ui = ScrumPy.DefaultUIClass()



    
    def _getc(self, c):
        if type(c) ==int:
            return c
        return  self.cnames.index(c)
    
    def _getr(self, r):
        if type(r)==int:
            return r
        return self.rnames.index(r)
    
    def _getrc(self,rc):
        if type(rc) == tuple:
            r, c = rc
            c = self._getc(c)
        else:
            r = rc
            c = None
        r = self._getr(r)
            
        return r,c
        

    def __len__(self):
        return len(self.rows)


    def __getitem__(self,rc):
        
        r,c = self._getrc(rc)

        row =  self.rows[r]
        
        if c is None:
            return row
        return row[c]

            

    def __setitem__(self,rc,  val=0):
        
        r, c = self._getrc(rc)
        
        if  c is  None:
            self.rows[r] = [self.Conv(v) for v in val]
        else:
            self.rows[r][c] = self.Conv(val)


    def __getslice__(self,lo,hi):
        rv = matrix(Conv=self.Conv)
        rv.rows = self.rows[lo:hi]
        rv.rnames = self.rnames[lo:hi]
        rv.cnames = self.cnames[:]
        return rv


    def __str__(self):
        return self.AsString()

    def __repr__(self):
        try:
            return "{:d}x{:d} matrix of with elements of {:s}".format(len(self),len(self.cnames),str(self.Conv))
        except:
            return "can't generate string rep'n for this matrix"



    def Float2Str(self,el):
        return format(el, ".SFg".replace("SF", str(self.PrintSF)))
            

    def SetEl2Str(self, func):
        
        try:
            func(self[0, 0])
            self.El2Str = func
        except:
            print(func,  "not compatible with",  type(self[0, 0]))



    def AsString(self, ElDelim = " ", LnDelim = "\n",RowHead=""):
            
        def GetWidth(StrList):
            return max(map(len, StrList))

        def Format2Widths(StrList, Widths):
            return [sw[0].rjust(sw[1]) for sw in zip(StrList, Widths)]

        Widths = [GetWidth(self.rnames)]
        for c in self.cnames:
            ColStr = [c] + [self.El2Str(el) for el in self.GetCol(c)]
            Widths.append(GetWidth(ColStr))

        HeadLine = Format2Widths([RowHead]+self.cnames, Widths)
        RetList = [ElDelim.join(HeadLine)]

        for r in self.rnames:
            RowStr = [r]+[self.El2Str(el) for el in self[r]]
            Row = Format2Widths(RowStr, Widths)
            RetList.append(ElDelim.join(Row))

        return LnDelim.join(RetList)
        

    def AsLaTeX(self,AsLaTeXObj=False,RowHead=""):
        "return a string representation in Latex tabular form"

        #e2s = self.El2Str
        #e2l = lambda x: LaTeX.FloatToTex(x, self.PrintSF)
        #self.SetEl2Str(e2l)  
        ## THIS NEEDS MORE THOUGHT -assumes all el are float

        rnames = self.rnames
        self.rnames = [LaTeX.DeTex(r) for r in rnames]

        cnames = self.cnames
        self.cnames = [LaTeX.DeTex(c) for c in cnames]

        rv = LaTeX.LaTeX()
        tab = rv.AddTabular(ncols=len(self.cnames)+1, Format="r")
        Body = self.AsString(ElDelim=LaTeX.tabsep, LnDelim=LaTeX.eol+"\n",RowHead=RowHead)
        tab.AddContent(Body)

        self.cnames=cnames
        self.rnames = rnames
        #self.SetEl2Str(e2s)

        if AsLaTeXObj:
            return rv
        return str(rv)

    def View(self):
        self.ui.ViewMatrix(self)
        

    def index(self,row):

        for i in range(len(self.rows)):
            if Seq.AreEqual(self.rows[i], row):
                return i

        return None
        



    def Dims(self):
        """ pre: True
           post: (nRows, nCols)
        """

        return len(self),  len(self.cnames)




    def ResetRowNames(self):
        """ pre: True
           post: self.rnames==["Row_0".."Row_n"] """

        self.rnames = ["Row_"+str(n) for n in range(len(self))]


    def ChangeType(self, Conv):
        self.Conv = Conv
        self.FunAllRows(Conv,True)

    def MakeSeqSelfType(self, seq):
        #TODO: list comprehension

        return list(map(self.Conv, seq))


    def MakeZero(self):
        #TODO: do we need this ?

        Zero = self.Conv(0)
        LenR = len(self[0])
    
        for n in range(len(self.rnames)):
            self.rows[n] = [Zero]*LenR


    def Clear(self):
        "erase all row and col data"
        #TODO: do we need this ?
        self.cnames, self.rnames, self.rows = [],[],[]



    def Copy(self, Conv=None,tx=False):
        """ pre: True
           post: Copy of self, with Element type Conv (self.Conv if Conv is  None)
                    tx => Copy is transposed """

        if Conv is  None:
            Conv=self.Conv


        rv = self.__class__(Conv=Conv)
        rv.cnames = self.cnames[:]
        rv.rnames = self.rnames[:]
        if Conv==self.Conv:
            for row in self.rows:
                rv.rows.append(row[:])
        else:
            for row in self.rows: # TODO: list comprehension
                rv.rows.append(list(map(Conv, row)))

        if tx:
            rv.Transpose()

        return rv
        
        
        
    def SubMtx(self, names,Conv=None):
        """pre: IsSubset(names, self.rnames) OR IsSubset(names,self.rnames)
           post: Sub-matrix of self comprised of the rows or columns in names"""

        if Conv is  None:
            Conv=self.Conv
       
        if names[0] in self.rnames:
            rv = matrix(Conv=Conv, cnames = self.cnames)
            Get = lambda n:self[n]
            Set = rv.NewRow
        else:
            rv = matrix(Conv=Conv, rnames = self.rnames)
            Get = self.GetCol
            Set = rv.NewCol
        
        for name in names:
            Set(Get(name), name)
       
        return rv
               


    def ReadFile(self, File,  Delim=None):
        """ pre: File is a readable stream or name of a file
           post: contents of self are those of FileName  """

        if type(File) == str:
            f = open(File, "r")
        else:
            f = File

        self.Clear()
        Titles = f.readline().strip().split(Delim)
        #self.RowTitle = Titles[0]
        #for t in Titles[1:]:
        for t in Titles:
            self.NewCol(name=t)

        row = f.readline().strip()
        n = 1
        while row != "\n" and row != "" and row[0]!="#":
            row = row.split(Delim)
            try:
                self.NewRow(row[1:], row[0])
            except:
                print("problem at line", n, "ignoring it", row)
            row = f.readline().strip()
            n+=1



    def WriteFile(self,File,WithHash=False,WithRNames=True,Delim="\t"):
        """ pre: (ColOrder is  None) || (complete and exclusive list of column headings of self)
           post: FileName contains FileSpec representation of self
                 (ColOrder is not None) => Columns in ColOrder
                 ELSE Exception """

        if type(File) == bytes:
            f = open(File, "w")
        else:
            f = File

        first = {True:["#"],
                 False:[""]
                 }[WithHash]

        print(Delim.join(first+self.cnames), file=f)

        if WithRNames:
            for n in range(len(self)):
                print(Delim.join([self.rnames[n]]+list(map(str,self.rows[n]))), file=f)
        else:
            for n in len(self):
                print(Delim.join(map(str,self.rows[n])), file=f)


    def Filter(self, func, cname=None):
        """ pre: func(self[r,c]) -> Bool
                 cname is  None OR cname in self.cnames
            post: return copy of self st func(el) == True for all el in rv.GetCol(cname)
                  func applied to rownames if cname is  None
        """

        rv = self.__class__()
        rv.Conv = self.Conv          # note: work on data members in case methods are overloaded by subclasses
        rv.El2Str = self.El2Str
        rv.cnames = self.cnames[:]

        if cname  is  None:
            col = self.rnames
        else:
            col = self.GetCol(cname)

        for n in range(len(self)):
            if func(col[n]):
                rv.rnames.append(self.rnames[n])
                rv.rows.append(self.rows[n])

        return rv


    def ToFile(self, fname):
        f = open(fname,"w")
        f.write(str(self))


    def ToList(self):
        """ return a list of lists of tuples. Each list corresponds to a  row,
             each tuple is (name,value) of the column and value of the non zero elements within that row """

        rv = []
        for row in self:
            curlist = []
            curidxs = Seq.IdxListOfMatches(row, lambda x: x!=0)
            for idx in curidxs:
                curlist.append((self.cnames[idx], row[idx]))
            rv.append(curlist)

        return rv



    def ToDict(self):
        """ return dictionary in which keys are row names and values lists of tuples (colnames, elval) for elval != 0 """

        l = self.ToList()
        rv = {}
        for idx in range(len(self)):
            rv[self.rnames[idx]] = l[idx]
        return rv
    
        
    def UpTri(self):
        """ return upper triangular elements of self as a single list """

        rv = []
        for i in range(len(self)):
            rv.extend(self[i][i+1:])
        return rv


    def FromDicDic(self,d):
        """ pre: d is a dictionary of rows,
        in which each value is a dictionary mapping col names to values
            post: self'[r,c] == d[r,c] for non-zero values, all other values in self are zero """

        self.rnames = []
        self.rows = []
        self.cnames = []

        for kr in list(d.keys()):              # kr - key of row
            self.NewRow(name=kr)
            rd = d[kr]                      # rd - row dictionary
            for kc in list(rd.keys()):        # kc - key of col
                if not kc in self.cnames:
                    self.NewCol(name=kc)

                self[kr,kc] = d[kr][kc]




    def ToColList(self):
        """ as ToList, but returns list of columns """

        rv = []
        for cn in self.cnames:
            col = self.GetCol(cn)
            curidxs = Seq.IdxListOfMatches(col, lambda x: x!=0)
            curlist = []
            for idx in curidxs:
                curlist.append((self.rnames[idx],col[idx]))
            rv.append(curlist)
        return rv


    def ToColDict(self):
        """ return dictionary in which keys are col names and values lists of tuples (rownames, elval) for elval != 0 """

        l = self.ToColList()
        rv = {}
        for idx in range(len(self.cnames)):
            rv[self.cnames[idx]] = l[idx]
        return rv


    def ColDict(self, col):
        return dict(zip(self.rnames, self.GetCol(col)))


    def RowDict(self, row):
        return dict(zip(self.cnames, self[row]))


    def ToDicDic(self):
        ''' return a dictionary compatible with pre of FromDicDic(...) '''
        rv = {}
        for r in self.rnames:
            rv[r] = self.RowDict(r)
        return rv


    def ToSciMtx(self,Conv=None):

        if not Conv:
            return Sci.matrix(self.rows)

        return Sci.matrix(self.Copy(Conv).rows)
    

    def ToNumPyMtx(self):
        
        print("ToNumPyMtx() is deprecated - use ToSciMtx()")
        return self.ToSciMtx()
 

    def FromSciMtx(self, mtx, Conv=None):

        nrow,ncol = mtx.shape
        if Conv is not None:
            self.Conv = Conv
        self.cnames = ["col"+str(x) for x in range(ncol)]
        self.rnames = ["row"+str(x) for x in range(nrow)]

        self.rows = mtx.tolist()






    def ToOctaveMtx(self, name, Conv = float):
        """ pre: True
           post: returns a string that when printed assigns the octave matrix, name, to current contents
                    using Conv as a type converter
        """
        rv = name + "= ["
        for row in self.rows:
            rv += "\n  ["
            for el in  row[:-1]:
                rv += str(Conv(el)) + ", "
            rv += str(Conv(row[-1])) + "]"
        rv += "\n]\n"

        return rv


    def ToCMtx(self, name, Conv = float):
        """ pre: True
           post: returns a string that when printed declares a C matrix, name, of current contents
                    type will need editing for non-default Conv, which as ever, is the type converter
        """

        c_nrows = name + "_nRows"
        c_ncols = name + "_nCols"
        strlenrow  = str(len(self.rows))
        strlencol = str(len(self.rows[0]))
        rv = "  const int "

        rv += c_nrows + " = " + strlenrow + ", " +   c_ncols + " = " + strlencol + ";"

        rv += "\n  double " +  name + "[" + c_nrows + "]" + "[" + c_ncols + "] = {\n"
        for row in self.rows[:-1]:
            rv += "\t{"
            for el in row[:-1]:
                rv += str(Conv(el)) + ", "
            rv += str(Conv(row[-1])) + "},\n"

        row = self.rows[-1]
        rv += "\t{"
        for el in row[:-1]:
            rv += str(Conv(el)) + ", "
        rv += str(Conv(row[-1])) + "} \n  } ;"

        return rv


 

                       
        
 


    def FromDynMatrix(self,other):

        self.Conv = other.Conv
        self.cnames = other.cnames[:]
        self.rnames = other.rnames[:]
        self.rows = []
        for r in other.rows:
            self.rows.append(r[:])




    def InitRows(self, rnames):
        """ pre: rnames is a list of strings
           post: self is an empty (r x 0) matrix, using rnames as row names """
        self.rnames = rnames[:]
        self.rows = []



    def Transpose(self):
        NewRows = []
        for c in range(len(self.cnames)):
            NewRows.append(self.GetCol(c))
        self.rows = NewRows

        self.rnames, self.cnames = self.cnames, self.rnames


    def RowReorder(self, NewOrder):
        tmp = matrix(Conv=self.Conv,ncol = len(self.cnames))

        for n in NewOrder:
            if type(n)==int:
                name = self.rnames[n]
            else:
                name = n
            tmp.NewRow(self[n], name)

        self.rows = tmp.rows
        self.rnames = tmp.rnames


    def ColReorder(self, NewOrder):
        self.Transpose()
        self.RowReorder(NewOrder)
        self.Transpose()

    def RandRowOrder(self):
        self.RowReorder(Seq.RandOrder(self.rnames))

    def RandColOrder(self):
        self.ColReorder(Seq.RandOrder(self.cnames))

    def RandOrder(self):
        self.RandRowOrder()
        self.RandColOrder()


    def SortBy(self,cname,Descend=False):

        origc = self.GetCol(cname)
        sortc = sorted(origc,reverse=Descend)
        
        mark = random.random()  # ie a number known not to be in the list
        while mark in origc:    # protect against duplicate entries
            mark = random.random()
            # unlikely that it's already there, but it doesn't hurt to check
            

        neworder=[]
        for x in sortc:
            idx = origc.index(x)
            neworder.append(idx)
            origc[idx] = mark

        self.RowReorder(neworder)


    def BlockDiag(self):
        """ (Attempt to) block diagonalise self,
            bring self closer to BD if not block diagonalisable
        """

        EqZero = lambda x: x==0

        #
        ## FIXME: doesn't work if no zero in row or col
        #
        RowKey = lambda r: Seq.PosOfFirst(self[r],EqZero)[1]
        ColKey = lambda c: Seq.PosOfFirst(self.GetCol(c),EqZero)[1]
        

        RNames = self.rnames[:]
        CNames = self.cnames[:]

        RNames.sort(key=RowKey)
        CNames.sort(key=ColKey)

        self.RowReorder(RNames)
        self.ColReorder(CNames)
        




    def RowNamesFromIdxs(self,idxs=[]):
        rv = []
        for i in idxs:
            rv.append(self.rnames[i])
        return rv


    def ColNamesFromIdxs(self,idxs=[]):
        rv = []
        for i in idxs:
            rv.append(self.cnames[i])
        return rv


    def RowNZIdxs(self,r,Abs=True):
        "list of indices to nonzero elements of r, sorted by ascending (Abs => absolute) element value "

        return Seq.NZIdxs(self[r],Abs)


    def ColNZIdxs(self,c,Abs=True):
        "list of indices to nonzero elements of c, sorted by ascending (Abs => absolute) element value "

        return Seq.NZIdxs(self.GetCol(c),Abs)
        
    def UpTriNames(self):
        """pre: self is square,
          post: list of tuples of row column names forming the upper triangular (excludes leading diagonal)"""
          
        Offset = 0
        rv = []
        for r in self.rnames:
            Offset +=1
            for c in self.cnames[Offset:]:
                rv.append((r,c))
        return rv
        
    def LoTriNames(self):
        """pre: self is square,
          post: list of tuples of row column names forming the lower triangular (excludes leading diagonal)"""
              
        Offset = 0
        rv = []
        for c in self.cnames:
            Offset +=1
            for r in self.rnames[Offset:]:
                rv.append((r,c))
        return rv  
        
    def ReplaceNames(self, RepDic):
        """ replace row and col names with their corresponding value in ReplDic, if present"""
        
        Seq.MultiLRepl(self.rnames, RepDic)
        Seq.MultiLRepl(self.cnames, RepDic)


    def Sort(self, fun, **fundat):
        """ sort rows in ascending order of fun(row,mtx,fundat) where:
            fun returns a numeric type
            row is an index into mtx
            mtx is self
            fundat is optional named args"""

        s = Sorter.Sorter()
        for r in self.rnames:
            s.append(r)
        self.RowReorder(s.Sort(fun, mtx=self, **fundat))




    def MakeIdent(self, mul):

        r = 0
        for row in self.rows:
            row = [0 for x in row]
            row[r] = mul
            self.rows[r] = self.MakeSeqSelfType(row)
            r = r + 1



    def NewCol(self,  col=None, name=None):

        if name is  None:
            name = "col_" + str(len(self.cnames)+1)
        self.cnames.append(name)

        if col is  None:
            col = [0] *len(self.rnames)
        
        col = [self.Conv(x)for x in col]

        if len(self.rnames)==0:
            for ridx,  val in enumerate(col):
                self.rows.append([val])
                self.rnames.append("row_" + str(ridx))

        elif len(col) != len(self.rnames):
            raise IndexError("wrong length for new col")

        else:
            for ridx, val in enumerate(col):
                self.rows[ridx].append(val)




    def DelCol(self, c=0):

        if type(c) != int:
            c = self.cnames.index(c)

        del self.cnames[c]
        for row in self.rows:
            del row[c]


    def GetCol(self, c):
        
        c = self._getc(c)
        return[row[c] for row in self.rows]

    def ColAsNP(self,c):
        return Sci.array(self.GetCol(c))
    

    def SetCol(self, c, vals):
        c= self._getc(c)
        vals = [self.Conv(val) for val in vals]
        
        for idx, row in enumerate(self.rows):
            row[c] = vals[idx]



    def AddCol(self, c, vals=None, k = None):
        
        # TODO:list comprehension
        if vals  is not None:
            self.SetCol(c, list(map(lambda x,y: x+y, self.GetCol(c), vals)))
        else:
            self.SetCol(c, [x+k for x in self.GetCol(c)])

    def SubCol(self, c, vals=None, k=None):
        # TODO:list comprehension
        if vals is not None:
            self.SetCol(c, list(map(lambda x,y: x-y, self.GetCol(c), vals)))
        else:
            self.SetCol(c, [x-k for x in self.GetCol(c)])

    def MulCol(self, c, vals=None, k=None):
        #TODO:list comprehension
        if vals is not None:
            self.SetCol(c, list(map(lambda x,y: x*y, self.GetCol(c), vals)))
        else:
            self.SetCol(c, [x*k for x in self.GetCol(c)])

    def DivCol(self, c, vals=None, k=None):
        #TODO:list comprehension
        if vals is not None:
            self.SetCol(c, list(map(lambda x,y: x/y, self.GetCol(c), vals)))
        else:
            self.SetCol(c, [x/k for x in self.GetCol(c)])


    def FunCol(self, c, Fun):
        # TODO:re-write as FunRow()
        self.SetCol(c, list(map(Fun, self.GetCol(c))))

    def MaxInCol(self, c):
        return max(self.GetCol(c))

    def MinInCol(self, c):
        return min(self.GetCol(c))

    def PosOfValsInCol(self, c, val):
        return Seq.PosOfValsInSeq(self.GetCol(c), val)

    def NZeroesInCol(self,c):
        return len(self.PosOfValsInCol(c,0))


    def NonZeroesInCols(self):
        """ returns a list vector, one element per col,
        each element being the number of non-zero elements in the coresponding col """

        rv = []
        for c in range(len(self.cnames)):
            rv.append(self.NZeroesInCol(c))

        return rv

    def NamesNonZeroesInCols(self):
        """ return a list (one el per col) lists of row names that have no zero element in corresponding col """

        rows = self.rows
        rnames = self.rnames
        rv = []

        for c in range(len(self.cnames)):
            res = []
            for r in range(len(self)):
                if rows[r][c] != 0:
                    res.append(rnames[r])
            rv.append(res)
        return rv
        
    def Range(self, name):
        """pre: name in self.rnames OR name in self.cnames
           post: max - min of row or col, name """
       
        if name in self.rnames:
            seq = self[name]
        else:
            seq = self.GetCol(name)
           
        return max(seq) - min(seq)
       
        
        


    def SwapCol(self, c1, c2):

        if type(c1).__name__ == 'string':
            c1 = self.cnames.index(c1)

        if type(c2).__name__ == 'string':
            c2 = self.cnames.index(c2)

        tmp = self.GetCol(c1)
        self.SetCol(c1, self.GetCol(c2))
        self.SetCol(c2, tmp)

        tmp = self.cnames[c1]
        self.cnames[c1] = self.cnames[c2]
        self.cnames[c2] = tmp


    def AugCol(self, m):
        for c in range(len(m.cnames)):
            self.NewCol(m.GetCol(c),m.cnames[c])

    def MakeLastCol(self, c):
        if type(c) == bytes:
            cname = c
            c = self.cnames.index(c)
        else:
            cname = self.cnames[c]

        col = self.GetCol(c)
        self.DelCol(c)
        self.NewCol(col, cname)


    def MakeFirstCol(self, cname):

        cidx = self._getc(cname)
        cname = self.cnames[cidx]

        col = self.GetCol(cidx)
        lencol = len(col)
        self.DelCol(cidx)
        
        self.cnames.insert(0,cname)
        for idx in range(lencol):
            self.rows[idx].insert(0,col[idx])





    def NewRow(self,  r=None, name=None):

        if name is  None:
            name = "row" + str(len(self.rows)+1)

        self.rnames.append(name)

        if r is  None:
            r = [0] *len(self.cnames)
        elif len(r) != len(self.cnames):
            raise IndexError("wrong length for new row")

        self.rows.append(self.MakeSeqSelfType(r))


    def DelRow(self, r):
        if type(r) != int:
            r = self.rnames.index(r)

        del self.rows[r]
        del self.rnames[r]




    def FunRow(self, r, fun, InPlace=False):
        row =  [self.Conv(fun(x)) for x in self[r]]
        if not InPlace:
            return row
        self[r] = row

    def FunAllRows(self, fun, InPlace=False):
        rows =  [[self.Conv(fun(x)) for x in row] for row in self.rows]

        if not InPlace:
            return rows
        self.rows =rows

    def AddRow(self, r, vals=None, k=None):
        """ Pre: NOT(vals is None AND k is None) """
        if vals is None:
            self.FunRow(r,  lambda x: x+k, InPlace=True)
        else:
            if not k is None:
                vals = [x*k for  x in vals]
            row = self[r]
            for i, v in enumerate(vals):
                row[i] += v
                
            


    def MulRow(self, r, vals=None, k=None):
        
        if vals is None:
            self.FunRow(r,  lambda x: x*k, InPlace=True)
        else:
            if not k is None:
                vals = [x*k for  x in vals]
            row = self[r]
            for i, v in enumerate(vals):
                row[i] *= v
        
        
        
 #       if vals and k:
  #          self[r] = list(map(lambda x,y: x*y, self[r], list(map(lambda x,mul=k: x*mul,vals))))
   #     elif vals:
     #       self[r] = list(map(lambda x,y: x*y, self[r], vals))
       # else:
        #    self[r] = list(map(lambda x,mul=k: x*mul,self[r]))


    def DivRow(self, r, vals=None, k=None):
        
        if vals is None:
            self.FunRow(r,  lambda x: x/k, InPlace=True)
        else:
            if not k is None:
                vals = [x/k for  x in vals]
            row = self[r]
            for i, v in enumerate(vals):
                row[i] /= v
        

    def InsertRow(self, pos, Val=0, Name=None):

        r = list(map(lambda x,v=Val: v, list(range(0,len(self.cnames)))))
        self.rows.insert(pos, self.MakeSeqSelfType(r))
        if Name:
            self.rnames.insert(pos,Name)


    def SubRow(self, r, vals):
        DepWarn("SubRow")
        self.rows[r] = list(map(lambda x,y: x-y, self.rows[r], vals))


    def AddAndRemoveRows(self, rows):
        """ pre: rows is subset of self.rnames
           post: self'[rows[0]] """

        r0 = rows[0]
        for r in rows[1:]:
            self.AddRow(r0,self[r])
        for r in rows[1:]:
            self.DelRow(r)


    def MaxInRow(self, r):
        return max(self[r])

    def MinInRow(self, r):
        return min(self[r])

    def PosOfValsInRow(self, r, val):
        return Seq.PosOfValsInSeq(self.rows[r], val)


    def RowsMatching(self,  fun,  col):
        """ pre: fun(self[r,col]) =>Bool for all rows in self
           post: list of row indices, r, st fun(self[r,col)==True
        """

        col = self.GetCol(col)
        rv = []
        for i in range(len(self.rnames)):
            if fun(col[i]):
                rv.append(i)

        return rv

    def RowAsNP(self,r):
        return Sci.array(self[r])

        
    def RoundOff(self, ndigits,InPlace=False):
        return self.FunAllRows(lambda x: round(x, ndigits))

        
    def DupRows(self):
        """ pre: True
           post: self.DupRows()  a (possibly empty) list of lists of names of rows with identical elements
        """

        rv = []
        rows = self.rows
        ridxs = list(range(len(self.rows)))
        while len(ridxs)>0:
            r = ridxs[0]
            duplist = [r]
            for r2 in ridxs[1:]:
                if Seq.AreEqual(rows[r], rows[r2]):
                    duplist.append(r2)

            for d in duplist:
                del ridxs[ridxs.index(d)]
            if len(duplist)> 1:
                rv.append(list(map(lambda x,rn = self.rnames: rn[x],duplist)))

        return rv



    def DelDupRows(self):
        """pre: True
          post: len(self.DupRows())==0
        """

        for dups in self.DupRows():
            for d in dups[1:]:  # leave the first one !!
                self.DelRow(d)


    def MaxCountRow(self, val):
        """ return the row name with the greatest number of elements of value val
            in the event of a tie the first matching row name is returned
            if no rows contain val the first row name is returned """

        rv = self.rnames[0]
        best = 0
        for r in self.rnames:
            hits = self[r].count(val)
            if hits > best:
                best = hits
                rv = r
        return rv



    def RowsWithSingCols(self):
        "return list of idxs of rows with at least one non-zero element unique to the column"
        rv = []
        for c in self.cnames:
            col = self.GetCol(c)
            idx = Seq.IdxOfUnique(col, lambda x: x!=0)
            if idx is not None and not idx in rv:
                rv.append(idx)
        return rv


    def SwapRow(self, r1, r2):

        if type(r1).__name__ == 'string':
            r1 = self.rnames.index(r1)

        if type(r2).__name__ == 'string':
            r2 = self.rnames.index(r2)

        tmp = self.rows[r1]
        self.rows[r1] = self.rows[r2]
        self.rows[r2] = tmp

        tmp = self.rnames[r1]
        self.rnames[r1] = self.rnames[r2]
        self.rnames[r2] = tmp


    def MakeFirstRow(self, r):

        row = self[r]
        print( type(r))
        if type(r)==bytes or type(r)==str:
            name = r
        else:
            name = self.rnames[r]

        self.DelRow(r)
        self.rows.insert(0,row)
        self.rnames.insert(0,name)


    def MakeLastRow(self, r):

        row = self[r]
        if type(r)==bytes:
            name = r
        else:
            name = self.rnames[r]

        self.DelRow(r)
        self.rows.append(row)
        self.rnames.append(name)


    def AugRow(self, m):
        for r in range(len(m.rnames)):
            self.NewRow(m.rows[r], m.rnames[r] )


    def NonZeroR(self, seed=[], thresh=1e-6):
        """pre: mtx has no empty rows, and numrical elements
          post: return a seed + list of list of rownames for which r(a,b)> thresh
                  where r is sample correlaation coeff,
                  ** self is empty  -DESTRUCTIVE ! ** """


        lenself = len(self)

        if lenself==0:
            return seed
        elif lenself==1:
            return seed+self.rnames[:]

        FirstRow = self.rnames[0]
        rv = [FirstRow]
        for ThisRow in self.rnames[1:]:
            if abs(Sci.CosTheta(self[FirstRow],self[ThisRow])) > thresh:
                rv.append(ThisRow)
                self.DelRow(ThisRow)
        self.DelRow(FirstRow)
        return self.NonZeroR(seed+[rv], thresh)



    def RowDiffMtxFull(self, fun= None, Conv=float):

        rv = matrix(rnames=self.rnames, cnames=self.rnames, Conv=Conv)
        lenself = len(self)
        for r1 in range(lenself):
            for r2 in range(lenself):
                rv[r1,r2] = fun(self[r1],self[r2])

        return rv




    def RowDiffMtx(self, fun=None,Conv=float):
        """ pre:  fun(self[r1],self[r2]) for 0 <= r1,r2  < len(self) returns type Conv
                     fun(r1,r2) == fun(r2,r1)
           post: returns M such that M[r1,r2] = fun(self[r1],self[r2])
            """

        if fun is  None:
            fun = lambda x,y: abs(Sci.CosTheta(x,y))

        rv = matrix(rnames=self.rnames, cnames=self.rnames, Conv=Conv)
        lenself = len(self)
        for r1 in range(lenself):
            for r2 in range(r1, lenself):
                rv.rows[r1][r2] = rv.rows[r2][r1] = fun(self[r1],self[r2])

        return rv


    def RowDiffMtx2(self,fun):
        """ as above, but fun takes self, r1, and r2 as args, and no type conversion is done """

        rv = matrix(rnames=self.rnames, cnames=self.rnames, Conv=self.Conv)
        lenself = len(self)
        for r1 in range(lenself):
            for r2 in range(r1+1, lenself):
                rv[r1,r2] = rv[r2,r1] =  fun(self,r1,r2)

        return rv


    def ToNJTree(self):
        """pre:  self is square difference matrix
          post:  A string representing the NJ tree, based on row differences, as calculated by fun """

       
        def FindNearestRows(mtx):

            rv = mtx.rnames[0] ,mtx.cnames[1],mtx[0,1]
            if len(mtx) == 2:
                return rv

            for r in range(len(mtx)):
                for c in range(r):
                    el =  mtx[r,c]
                    if el == 0:
                        return mtx.rnames[r],mtx.cnames[c],0
                    else:
                        if el < rv[2]:
                            rv = mtx.rnames[r],mtx.cnames[c],mtx[r,c]
            return rv

        nodes = {}
        mtx = self.Copy()
        for r in mtx.rnames:
            nodes[r] = Tree.Tree(name=r)
        while len(mtx) >1:
            print(".", end=' ')
            m0,m1,dist = FindNearestRows(mtx)
            ch0, ch1 = nodes[m0], nodes[m1]
            ch0.dist = ch1.dist = dist
            
            p = Tree.Tree()
            p.add_child(ch0)
            p.add_child(ch1)

            pname = m0+m1
            nodes[pname] = p

            mtx.NewRow(name=pname)
            mtx.NewCol(name=pname)

            for i in range(len(mtx)):
                mtx[pname,i] = mtx[i,pname] = (mtx[m0,i]+mtx[m1,i])/2

            for m in m0, m1:
                mtx.DelRow(m)
                mtx.DelCol(m)
                del nodes[m]

        return list(nodes.values())[0]





    def ToNJTreeOld(self):
        """pre:  self is square difference matrix
          post:  A string representing the NJ tree, based on row differences, as calculated by fun """

        def FindNearestRows(mtx):

            rv = 0,1,mtx[0,1]
            if len(mtx) == 2:
                return rv

            for r in range(len(mtx)):
                for c in range(r):
                    el =  mtx[r,c]
                    if el == 0:
                        return r,c,0
                    else:
                        if el < rv[2]:
                            rv = r,c,mtx[r,c]
            return rv

        while len(self) >1:
            m0,m1,v = FindNearestRows(self)
            strv = ":"+str(v)
            self.rnames[m0] = "(" + self.rnames[m0] + strv + "," + self.rnames[m1] + strv+")"
            for i in range(len(self)):
                self[m0,i] = self[i,m0] = (self[m0,i]+self[m1,i])/2

            self.DelRow(m1)
            self.DelCol(m1)

        return self.rnames[0] + ";"


    def IntegiseR(self):
        """pre: Sci.Integise(r) for all r in self.rows
           post:  convert elements to integers whilest mainiting ratios across  rows
        """

        self.Conv=int
        self.rows = list(map(Sci.Integise,  self.rows))
       

    def IntegiseC(self):
        """pre: Sci.Integise(c) for all c in self.GetCol(c)
           post:  convert elements to integers whilest mainiting ratios along columns
        """
        
        self.Conv=int
        for c in self.cnames:
            self.SetCol(c, Sci.Integise(self.GetCol(c)))


    def ZapZeroes(self,  thresh=0.0):

        if thresh == 0.0:
            Zap = lambda s: Seq.AllEq(s, 0)
        else:
            Zap = lambda s: Seq.AllMatch(s,  lambda x: abs(x)<thresh)

        for r in range(len(self)-1, -1, -1):
            if Zap(self[r]):
                self.DelRow(r)


    def FlattenZeroes(self, thresh=1e-10):

        nc = range(self.Dims()[1])
        Zero = self.Conv(0)

        for row in self.rows:
            for c in nc:
                if abs(row[c]) <= thresh:
                    row[c] = Zero


    def Mul(self, other):
        
        rvcnames = ["col_0"]  # TODO: use isinstance etc, not atributes
        if hasattr(other, "rows"):                    # other is instance or subclass of DynMatrix.matrix
            sciother = Sci.matrix(other.rows)
            rvcnames = other.cnames
            
        elif hasattr(other,  "IsFluxDesc") :  # instance of Structural.FluxDesc.FluxDesc or subclass thereof
            sciother = Sci.matrix(other.fluxes).transpose()
            
        elif  type(other) in (list,  tuple): # other is list or tuple
            sciother = Sci.matrix(other).transpose()
            
        else:
            sciother = other # if other is not anything else we recognise, assume it's a scipy.matrix
            
        try:
            rows = (Sci.matrix(self.rows) * sciother).tolist()
            rv = self.__class__(rnames=self.rnames, cnames=rvcnames,Conv=self.Conv)
            rv.rows  = rows
            return rv
        
        except: # problem in Sci.mul, fall back to slower method
            self._Mul(other)
            
            
            
    def _Mul(self, other):
        """ redundant fall-back for Mul, don't invoke directly """
    
        print("""
        Fall back to slow Mul.
        If you see this having directly  invoked matrix.Mul(other) there is probably
        a problem with one or other of the matrices, and this may still fail.
        If you didn't directly invoke matrix.Mul(other), please report it.
        """)
        
        selfr = len(self)
        otherr = len(other)
        selfc = len(self.cnames)
        otherc = len(other.cnames)

        if  selfc != otherr:
            raise ArithmeticError("inconsistant matrices for multiplication")

        rv = self.__class__(selfr,otherc,Conv=self.Conv)
        other.Transpose()
        for i in range(selfr):
            for j in range(otherc):
                rv.rows[i][j] = sum(map(lambda x,y:x*y,self.rows[i],other.rows[j]))
        other.Transpose()
        rv.cnames = other.cnames[:]
        rv.rnames = self.rnames[:]
        return rv


    def OrthNullSpace(self):
        "orthogonal nullspace as matrix of floats"

        try:
            N = Sci.matrix(self.rows,float)
            K = Sci.null(N)
            Z = N*K
            Fail = Z.max() > Sci.TOL or Z.min() < -Sci.TOL
            if Fail:
                print("OrthNullSpace out of tol")
        except:
            Fail = True   # possbility of unknown exceptions from low level math libs
            print("Sci.null() fails !")

        if not Fail:
            rv = matrix(Conv=float)
            rv.rnames = self.cnames[:]
            rv.cnames =["c_"+str(x) for x in range(len(K[0]))]
            rv.rows = K.tolist()
        else:
            print("falling back to (slow) GaussJ + GramS\n")
            if not self.Conv == Types.ArbRat:
                rv = self.Copy(Conv=Types.ArbRat).NullSpace()
            else:
                rv = self.NullSpace()
            rv.Orthogonalise()

        return rv


    def Orthogonalise(self):
        """ Use Gram-Schmidt orthogonalisation to make self orthognal"""

        self.Transpose() # transposing lets us use more efficient row (aot col) operations
        self.ChangeType(float)

        for j in range(len(self)):
            vj = self[j][:]
            for i in range(j):
                ui = self[i]
                vjui = -Sci.DotProd(vj,ui)
                self.AddRow(j, [x*vjui for x in ui])
            NormJ = math.sqrt(sum([x*x for x in self[j]]))
            self.DivRow(j,k=NormJ)
        self.Transpose()


    def PseudoInverse(self,  AsSci=False):

        if self.Conv != float:
            rows = self.Copy(float).rows
        else:
            rows = self.rows

        rvrows = Sci.pinv(rows)
        if AsSci:
            rv = rvrows
        else:
            nrows, ncols = len(rvrows), len(rvrows[0])
            rv = self.__class__(nrows, ncols, Conv=float)
            rv.rows = rvrows.tolist
        return rv


    def SetPivot(self,Row):

        GotOne = 0
        vals=self.GetCol(Row)

        #vals = map(abs, vals)[Row:]
        vals = [abs(val) for val in vals[Row:]]
        PivVal = max(vals)
        if PivVal != 0:
            GotOne = 1
            PivIdx = Row+vals.index(PivVal)
            if PivIdx != Row:
                self.SwapRow(PivIdx,Row)

        return GotOne


    def ElimCol(self, Row):
        "pre: self.GetEl(Row,Row)==1"

        RowVals = self.rows[Row]
        for r in range(len(self.rows)):
            if r != Row and self.rows[r][Row] != 0:
                mul = -self.rows[r][Row]
                self.AddRow(r, RowVals, mul)


    def RedRowEch(self):
        """ put self in reduced row echelon form,
        uses col swaps in pivoting """

        CurRow = 0
        done = 0
        if self.Conv != Types.ArbRat:
            print ("! Warning RedRowEch for typesother than ArbRat may be unstable", file = sys.stderr)
       
        #Flatten = lambda x,y: 0

        while not done:
            if Seq.AllEq(self.rows[CurRow], 0):
                self.DelRow(CurRow)
            else:
                if self.SetPivot(CurRow):
                    self.DivRow(CurRow,vals=None, k=self.rows[CurRow][CurRow])
                    self.ElimCol(CurRow)
                    #Flatten(self[CurRow], 1e-6)
                    CurRow += 1
                else:
                    self.MakeLastCol(CurRow)

            done = CurRow == len(self.rows)


    def NullSpace(self):

        rv = self.Copy(Conv=DefaultConv)
        rv.RedRowEch()
        nsrnames = rv.cnames[:]   # because we do col xchs in RedRowEch names get reordered and rv is othrewise mangled
        Slice = len(rv.rows)
        if len(rv.cnames) != Slice:
            for r in range(len(rv.rows)):
                rv.rows[r] = rv.rows[r][Slice:]
            rv.cnames = ["c_"+str(x) for x in range(len(rv[0]))]
            ident = GetIdent(len(rv.cnames),Conv=self.Conv, mul = -1)
            rv.AugRow(ident)
            rv.rnames = nsrnames
            rv.RowReorder(self.cnames) # so now  (self * rv) == 0
        else:
            # special case, invertible matrix, zero dimensional NullSpace
            rv.cnames = []
            rv.rnames = self.cnames[:]
            rv.rows = [[] for x in rv.rnames]
        return rv
        
    
    def LNullSpace(self):
        
        self.Transpose()
        rv = self.NullSpace()
        self.Transpose()
        rv.Transpose()
        
        return rv

 
    def LOrthNullSpace(self):
        
        self.Transpose()
        rv = self.OrthNullSpace()
        self.Transpose()
        rv.Transpose()
        
        return rv


