
"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


# Kali - devours all


class Kali:
    def __init__(self, ident=None,KaliVerbose=False,*args,**kwargs):
        self.ident = ident
        self.Verbose =KaliVerbose
        
    def __str__(self):
        return "Kali from " + str(self.ident) + "\n"

    def __repr__(self):
        return str(self)

    def __getitem__(self, i):
        return self

    def __setitem__(self, i):
        pass
        
    def __call__(self, *args,  **kwargs):
        return self

    def __getattr__(self, a):
        if self.Verbose:
            print("Kali  ", self.ident, a)
        return self
    
