
import os, types

def StrDate():
    d = os.popen("date").read().split()
    return "_".join([d[1],d[2],"".join(d[3].split(":")),d[-1]])

def StrIsNum(s):
    try:
        float(s)
        return True
    except:
        return False

def RoundedFloat(x, sf=3):
    """return x as a string rounded to sf sig. figs"""
    
    try:
        return"{:.sfg}".replace("sf", str(sf)).format(x)
    except:
            try:
                return str(round(x, sf))
            except:
                return str(x)


def EngMantExp(x, sf):

    mant, exp = format(x, " .SFe".replace("SF", str(sf-1))).split("e")
    mant=float(mant)
    exp=int(exp)

    if abs(exp) >=3:
        diff = exp%3
        exp-=diff
        mant*=10**diff
    else:
        mant = x
        exp = 0

    return str(round(mant,sf)),str(exp)
        

            
    
def AsRoundedFloat(x, sf=3):
    return RoundedFloat(float(x),  sf)

def RoundedList(List, sf=3):
    return [RoundedFloat(item,sf) for item in List]

def RoundedListOfLists(List, sf=3):
    return [RoundedList(item, sf) for item in List]


def FindFieldWidths(rows):

    rv = [0]*len(rows[0])
    
    for row in rows:
        for idx, val in enumerate(row):
            lenval = len(val)
            if rv[idx] < lenval:
                rv[idx] = lenval
        
    return rv


def PadRow(Row, Widths, PadChar=" "):
    return [s.ljust(w,PadChar) for s,w in zip(Row, Widths)]


def PadRows(Rows,Widths=None, PadChar=" "):
    if Widths is None:
        Widths = FindFieldWidths(Rows)
    return [PadRow(row,Widths) for row in Rows]




def IsQuotedStr(i):
    if type(i) != str:
        return False
    if len(i)<2:
        return False
    if i[0] == "'" or i[0] == '"':
        return i[0] == i[-1]
    return False


def QuoteStr(i,*args,**kwargs):
    if IsQuotedStr(i):
        return i 
    else:
        return str(i).join(('"','"'))

def StripQuote(i):
    if IsQuotedStr(i):
        return i[1:-1]
    else:
        return i
    


def FormatListOrTuple(l, pad, depth=0,Delims=True,Quoted=True):

    curtype=type(l)

    if curtype == tuple:
        opend,closed,isep = {True:("(\n", ")",",\n"), False:("\n","","\n")}[Delims]
    else:
        opend,closed,isep = {True:("[\n", "]",",\n"), False:("\n","","\n")}[Delims]

    rv = opend
    depth+=1
    for i in l:
        rv += pad*depth + FormatNested(i,pad,depth,Delims=Delims,Quoted=Quoted)+isep
            
    return rv  + pad*(depth-1)+closed
  

def FormatDic(d, pad,depth=0,Delims=True,Quoted=True):

    opend,closed,isep = {True: ("{\n","}",",\n"), False:("\n","","\n")}[Delims]
   
    rv = opend
    depth+=1
    for k in list(d.keys()):
        rv += pad*depth + QuoteStr(k) + " : " + FormatNested(d[k], pad, depth,Delims=Delims,Quoted=Quoted) + isep
        
    return rv +pad*(depth-1)+closed




def FormatNested(struct, pad="    ", depth=0, AsType=None,Delims=True,Quoted=True):

    FormatLookup = { dict : FormatDic,
                     list : FormatListOrTuple,
                     tuple: FormatListOrTuple,
                     str: QuoteStr
                    }

    struct_t = type(struct)
    
    if AsType==None:
        if struct_t not in FormatLookup:
            if hasattr(struct,"keys"):
                AsType=dict
            elif hasattr(struct, "index"):
                AsType=tuple
        else:
            AsType=struct_t
                
   
    if AsType in FormatLookup:
        return FormatLookup[AsType](struct, pad,depth,Delims,Quoted=Quoted)
    elif Quoted:
        return  QuoteStr(struct)
    else:
        return str(struct)
            
    
