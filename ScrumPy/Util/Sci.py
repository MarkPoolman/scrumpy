

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 2006 -

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

"""
Common entry point for "scientific" functions, especially on vectors and matrices.
Currently using scipy, but this may change in future.
"""


import math,  fractions

import scipy,  numpy

from . import Seq

from numpy import array,  zeros, linspace
import scipy




from scipy.linalg  import svd,  pinv,  eigvals
from scipy import special,optimize, matrix
# NOTE: ignore warnings about unused imports, they are for re-export to other modules.

#
## FIXME: scipy.matrix is deprecated - (replace w/ 2D array?)
#



from numpy import inf as INF

TOL=1e-12 # smallest difference between two numbers that can be considered non-zero


def HistoLists(data,  **kwargs):
    
    counts,  bins = numpy.histogram(data, **kwargs)
    return bins, counts


def gcd(a,b):
  while b:
    a,b = b, a%b
  return a

def Gcd(nums):
    """ pre: nums is a sequence of numbers, len(nums) >1
        post: greatest common denominator of nums
    """

    g = gcd(nums[0], nums[1])
    if len(nums)>2:
        return Gcd([g]+ nums[2:])
    return g


def Integise(nums,  KeepSign=True):
    """ pre: Gcd(nums)
         post: copy of nums converted to integer whilst maintaining ratios
         
         Note: beware of rounding error if nums contain floats - especially recurring decimals
    """
    
    if max(nums) == min(nums): # special cases of all the same and/or all zero

        if nums[0] == 0:
            rv = nums[:]
        else:
            if KeepSign:
                one = abs(nums[0])/nums[0]
            else:
                one = nums[0]/nums[0]
            
            rv = [one] * len(nums)
            
    else:
        g = Gcd(nums)
        if KeepSign:
            g = abs(g)
            
        rv = [int(x/g) for x in nums]
        
    return rv




def PolyFit(Xdat, Ydat, deg=1):
    "polynomial fit of X,Y of degree deg"

    return scipy.polyfit(Xdat,Ydat,deg)


def CurveFit(f, xs, ys, p_init=None, p_bounds=None):

    rv = {}
    if len(xs) == 0:
        raise ValueError("Cant't fit no data!")

    for i in reversed(range(len(xs))):
        if scipy.isnan(ys[i]):
            del ys[i], xs[i]

    xs,ys = array(xs), array(ys)
    if p_bounds is None:
        p_bounds=(-INF, INF)

    #try:
    params, cov = optimize.curve_fit(f, xs, ys, p0=p_init, bounds=p_bounds)
    rv["Params"] = params
    rv["Cov"] = cov
    rv["Stdevs"] = scipy.sqrt(scipy.diag(cov))
    xs_unq = array(sorted(list(set(xs))))
    rv["Fitted"] = xs_unq, f(xs_unq, *params)
    rv["Resids"] = resids = xs, f(xs, *params) - ys
    return rv
    #except:
    #    pass # ie return none if curve_fit fails



def null(A,tol=TOL):
    "orthonormal nullspace, elements are floats"
    return scipy.linalg.null_space(A, tol)
        # tol == rcond - check appropriate value
    
##
### old version - scipy.null_space now available in more recent versions
##
##    u,w,vt=svd(A,full_matrices=1)
##    v,i = Seq.PosOfFirst(w.tolist(),lambda x: x <= tol) #TODO  avoid using Seq and w.tolist
##    if i == -1:
##        i = len(w)
##    rv = vt[i:,].transpose()
##
##    if AsList:
##        return rv.tolist()
##    return rv

def mul(a,b):

    return a*b


def Mod(vec):
    return math.sqrt(scipy.vdot(vec,vec))

def DotProd(a, b):
    return scipy.vdot(a, b)
    
def RelLen(a, b):
    return Mod(a)/Mod(b)
    
    
def EucDist(a,b):
    " the Euclidean distance between a,b "

    moda = Mod(a)
    modb = Mod(b)
    cosab =CosTheta(a, b)

    return math.sqrt(abs(moda*moda + modb*modb - 2*moda*modb*cosab))    

def SumSq(seq):

    return sum([x*x for x in seq])
    
def RootSumSq(seq):
    return math.sqrt(SumSq(seq))


def SumSqDiff(vals, const):

        return(SumSq([x-const for x in vals]))

def Deriv(x,y):
    """ pre: len(x) == len(y)==3, all x and y numeric
       post: Deriv(x,y) == slope at x[1],y[1], by three point linear derivation """
    return 0.5 * (
        ((y[1] - y[0]) / (x[1] - x[0])) +
        ((y[2] - y[1]) / (x[2] - x[1]))
    )


def NumDeriv(x,y):
    """ pre: len(x) == len(y) >=3, all x and y numeric
       post: Deriv(x,y) -> array of 1st derivative dy/dx (including end points) """
    
    if not type(x) is numpy.ndarray:
        x = numpy.array(x, dtype=float)
    if not type(y) is numpy.ndarray:
        y = numpy.array(y, dtype=float)
        
    return numpy.gradient(y,x)


def CosTheta(veca,vecb):
    veca = list(map(float, veca)) 
    vecb = list(map(float, vecb)) 
    
    rv = scipy.vdot(veca,vecb)/(Mod(veca)*Mod(vecb))
    if rv >1.0:
        rv = 1.0
    elif rv < -1.0:
        rv = -1.0

    return rv


AbsCosTheta = lambda a,b: abs(CosTheta(a,b))


def AreParallel(a,b, tol=TOL):
    return 1-AbsCosTheta(a,b) <= tol

def AreOrthogonal(a,b,tol = TOL):
    return AbsCosTheta(a,b) <= tol


Theta    = lambda a,b : math.acos(CosTheta(a,b))
AbsTheta = lambda a,b : abs(Theta(a,b))
Q1Theta  = lambda a,b : math.acos(AbsCosTheta(a,b))

Log2 = lambda x: math.log(x, 2)



def SafeLog(x, base, MinVal=-42):

    if x <=0.0:
        return MinVal
    return math.log(x,base)

    

def AbsFFT(y):
    """ pre: y iterable of numeric types
       post: AbsFFT(y) => absolute values of the positive frequency components of y
    """

    len2 = len(y)//2
    yft = scipy.fft.fft(y)

    return numpy.abs(yft[:len2]) * 1/len2
    

def AbsFFTx(y, Interval=1):

    if type(y) is int:
        leny = y
    else:
        leny = len(y)

    return scipy.fft.fftfreq(leny, Interval)[:leny//2]




