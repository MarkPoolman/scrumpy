
#
##
### Parallel processing for ScrumPy
### The heavy lifting is done by the python 3 component
### of the ParalellPython library in pplib
### The full package and other details can be found here: 
### https://www.parallelpython.com/
##
#

import os, sys, subprocess

import ScrumPy

pplib = "pp-1.6.4.4"
Path = __file__.rsplit(os.sep,1)[0]
ppPath = os.sep.join((Path, pplib))
ppcmd  = os.sep.join((ppPath,"ppserver.py -p 35000 -i 127.0.0.1"))
ppservers=("node-1:35000",)
sys.path.append(ppPath)
import pp


__PServer = None
__JServer = None


def GetPServer():
    global __PServer
    if __PServer is None:
        __PServer = subprocess.Popen(ppcmd.split())
    return __PServer


def GetJServer():
    global __JServer
    GetPServer()
    if __JServer is None:
      __JServer =  pp.Server(ppservers=ppservers)
    return __JServer


def Finnish():

    global __PServer, __JServer

    if  __JServer is not None:
        __JServer.destroy()
        __JServer = None

    if  __PServer is not None:
        __PServer.kill()
        __PServer.wait()
        __PServer = None

















#
##
### Testing stuff below here  #######
##
#

import time
res =[]
def cb(S):
    res.append(S)

def Dir():
    return dir(ScrumPy)[:3]

def Test(s):
    t0 = time.time()    
    MAX = 10
    for x in range(MAX):
        for y in range(x,MAX):
            s.submit(Dir, callback=cb, globals=globals())

    s.wait()
    t1 = time.time()
    #for p in AllPairs:
    #    print(p)

    for x in range(MAX):
        for y in range(x,MAX):
            s.submit(Dir, callback=cb, globals=globals())
    s.wait()
    t2 = time.time()

    print("First", t1-t0, "Second", t2-t1)

#s.destroy()
