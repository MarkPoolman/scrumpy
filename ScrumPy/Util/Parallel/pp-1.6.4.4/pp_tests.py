import os, subprocess
import random
import pp
import time

import sys
sys.path.append("/usr/local/lib/python3/dist-packages")
import ScrumPy


def GetPid():
    return os.getpid()


def Dir():
    return(dir(ScrumPy.Util)[:2], GetPid())

def Count():

    n = 1e7
    while n>0:
        n -= 1
    return GetPid()


def GotNums(x,y):

    return " ".join((str(GetPid()), str(x), str(y)))



def StartServer():
    return subprocess.Popen("./ppserver.py -p 35000 -i 127.0.0.1".split())

def GetJobServer():
    ppservers=("node-1:35000",)
    return pp.Server(ppservers=ppservers)

##./ppserver.py -p 35000 -i 127.0.0.1
##Jobs = [s.submit(Count, globals=globals()) for n in range(20)]
##for j in Jobs:
##    print(j())
##
##
##Jobs = [s.submit(Dir, globals=globals()) for n in range(20)]
##for j in Jobs:
##    print(j())
##
##Jobs = [s.submit(GetPid, globals=globals()) for n in range(20)]
##for j in Jobs:
##    print(j())
##
##    
##
##AllPairs = []
##
##def cb(S):
##    AllPairs.append(S)
##
##MAX = 10
##for x in range(MAX):
##    for y in range(x,MAX):
##        s.submit(GotNums,(x,y), callback=cb, globals=globals())

res =[]
def cb(S):
    res.append(S)

def Test(s):
    t0 = time.time()    
    MAX = 10
    for x in range(MAX):
        for y in range(x,MAX):
            s.submit(Dir, callback=cb, globals=globals())

    s.wait()
    t1 = time.time()
    #for p in AllPairs:
    #    print(p)

    for x in range(MAX):
        for y in range(x,MAX):
            s.submit(Dir, callback=cb, globals=globals())
    s.wait()
    t2 = time.time()

    print("First", t1-t0, "Second", t2-t1)

#s.destroy()
