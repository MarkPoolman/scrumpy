#
##
### Generic gnuplot interface
##
#

from subprocess import Popen, PIPE,  STDOUT

import time
import threading
import queue
import os

GnuplotPath = "/usr/bin/gnuplot-qt"
# TODO: Allow other options here



class AsyncReader(threading.Thread):
    '''

    from http://stefaanlippens.net/python-asynchronous-subprocess-pipe-reading
    
    Helper class to implement asynchronous reading of a file
    in a separate thread. Pushes read lines on a queue to
    be consumed in another thread.

    ?? probably do away with this and use select.poll() or select.select() for Gnuplot.__call__()
    ?? if we don't do replace gnuplot with pyqtgraph entirely
    '''
 
    def __init__(self, fd):

        threading.Thread.__init__(self)
        self._fd = fd
        self.Q =queue.Queue()
        self._StopNow = False
 
    def run(self):
        '''The body of the tread: read lines and put them on the queue.'''
        for line in iter(self._fd.readline, ''):
            if self._StopNow:
                return
            self.Q.put(line)
            
    def stop(self):
        self._StopNow =True
 
    def eof(self):
        '''Check whether there is no more content to expect.'''
        return not self.is_alive() and self.Q.empty()
        
    def Get(self):
        '''return the last line read from fd - None if queue empty'''
        
        if self.Q.empty():
           rv = None
           
        else:
            rv = self.Q.get().decode().strip()
           
        return rv
    

class Gnuplot(Popen):
    
    WaitTime = 0.1 # time to wait before re-prompting gnuplot for a response
    MaxWait = 0.5  # max time to wait before assuming gnuplot has finnished

    def __init__(self):
      
        Popen.__init__(self, GnuplotPath, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
      
        self.FromGP =  AsyncReader(self.stdout)
        self.FromGP.start()
        
        self.WinNos = []
        
    def NewWinNo(self):
        
        rv = 0
        while rv  in self.WinNos:
            rv += 1
        self.WinNos.append(rv)
        return rv
        
    def FreeWinNo(self, n):
        # TODO: FIXME: should be 'set term qt 0 close' 
        self.WinNos.remove(n)
        self('set term "" ' + str(n) + " close")
        
        
        
    def __del__(self):

        self.stdout.flush()
        self.stdin.close()
        os.waitpid(self.pid,0)
        self.FromGP.stop()

  
    def _snd(self, stuff):
        self.stdin.write(stuff.encode())
        self.stdin.flush()

              
    def __call__(self,cmd):
        
        rv = []
        
        self._snd(cmd + ' ; print "DONE"\n')
        
        Waited = 0.0
        
        line = self.FromGP.Get()
        while line != "DONE" and Waited < self.MaxWait:
            
            if line == None:
                time.sleep(self.WaitTime)
                Waited += self.WaitTime
            elif line.endswith(":"):
                self._snd("\n")
            else:
                rv.append(line)
                Waited = 0
            line =  self.FromGP.Get()
            #print line
        
        return rv
        
    def Terminals(self):
        """ pre: True
           post: Terminals()-> list of available terminals"""
           
        return  [s.split()[0] for s in self("set term")[1:]]
        

GlobalGP = None
def GetGlobal():

    global GlobalGP
    if GlobalGP is None:
        GlobalGP = Gnuplot()

    return GlobalGP
   


