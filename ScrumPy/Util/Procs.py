import os, sys

from subprocess import Popen, PIPE


def GetChildren(pid=os.getpid()):
    
    cmd = ["ps", "--ppid", str(pid), "-o",  "pid"]
    ps = Popen(cmd, stdout=PIPE)
    res = ps.stdout.read()
    rv = [int(i) for i in res.split()[1:]]
    for pid in rv:
        rv.extend(GetChildren(pid))
    return rv
    

def GreedyWait(NoHang=True):
    
    if NoHang:
        opts = os.WNOHANG
    else:
        opts = 0

    print("GW", file=sys.__stderr__)

    try:
        Wres = os.wait3(opts)
        while Wres[0] != 0:
            print("GW chpid", Wres[0], file=sys.__stderr__)
            # while we have children that have terminated
            Wres = os.wait3(opts)
    except:
        pass # wait3 raises an exception if we have no children
    
        
def KillChildren():
    """ invoke this on exit to ensure we don't leave
        zombies hanging around """
    print("KCh", file = sys.stderr)
    
    for pid in GetChildren():
        print("KC:", pid)
        try:
            os.kill(pid, 9)

        except:
            pass
            
    GreedyWait(NoHang=False)

        
    

