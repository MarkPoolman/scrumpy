

"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
"""
M.G. Poolman 10/01/01 onwards
Seq.py
some useful things to do to sequences, mainly numeric
"""

import random

from . import Sorter



##
## more generalised sequence functions
##



def AllEq(seq, val):
    """
    pre : a[n] != val valid for (n <= 0 < len(a))
    post: AllEq(seq, val) => all elements in Seq are equal to val
    """
    for el in seq:
        if el != val:
            return False

    return True


def AllSame(seq):

    return len(set(seq))==1
    

def AreEqual(s1, s2):
    lens1 = len(s1)

    if lens1 != len(s2):
        return False

    for i in range(lens1):
        if s1[i] != s2[i]:
            return False

    return True


def PosOfFirst(seq, fun):
    """pre: fun is a boolean function which can accept any element of seq as input
      post: returns(val,idx) where val and index are the first value and index where fun is true
               returns (None, -1) if no matches """

    for el in seq:
        if fun(el):
            return el, seq.index(el)
    return None,-1



def PosOfValsInSeq(seq, val):
    """
    pre : all elements in seq can be compared with val
    post: PosOfValsInSeq == list of indices to elements in seq of value val
    """
    rv = []
    for idx in range(len(seq)):
        if seq[idx] == val:
            rv.append(idx)

    return rv




def LRepl(L,  old,  new):
    """ pre: L is a list
        post: all old items replaced with new"""
    for i in L:
        if i == old:
            L[L.index(i)] = new
        
        
def MultiLRrepl(L,  RepDic):
    """ replace all items equal to heys of RepDic with their corresponding value"""
    
    for old,  new in list(RepDic.items()):
        LRepl(L, old,  new)
        
    
    

def Abs(seq):
    return list(map(abs, seq))


def HasMatches(seq,fun):
    for el in seq:
        if fun(el):
            return True
    return False


def IdxListOfMatches(seq, fun):
    """
    pre: all elements in seq can be passed to fun which returns a boolean
    post: returns a list of indices for which fun(seq[idx])==1 (true)
    """
    rv = []
    for idx in range(len(seq)):
        if fun(seq[idx]):
            rv.append(idx)

    return rv



def IdxOfUnique(seq, fun):
    """
    pre: all elements in seq can be passed to fun which returns a boolean
    post: returns idx for which only fun(idx) is true, else returns None
    """

    match = IdxListOfMatches(seq, fun)
    if len(match) == 1:
        return match[0]
    return None


def NZIdxs(seq,Abs=True):
    "list of indices to nonzero elements of seq, sorted by ascending (Abs => absolute) element value "

    idxs = []
    for i in range(len(seq)):
        if seq[i] !=0:
            idxs.append(i)

    s = Sorter.Sorter(idxs)
    if Abs:
        def fun(a):
            return abs(seq[a])
    else:
        def fun(a):
            return seq[a]

    return s.Sort(fun)


def IdxOfMax(seq):
    return list(seq).index(max(seq))

def IdxOfMin(seq):
    return list(seq).index(min(seq))



def NumOfMatches(seq, fun):
     """
    pre: all elements in seq can be passed to fun which returns a boolean
    post: returns the number of elements for which fun(seq[idx])==1 (true)
    """

     return len(IdxListOfMatches(seq, fun))


def AllMatch(seq, fun):
    """ pre: NumOfMatches(seq,fun)
        post: fun(seq[i]) == True for i = [0..len(seq)-1]"""
        
    return NumOfMatches(seq, fun) == len(seq)
        




##
##
##
##sequences of rationals
##


def LargestDen(seq):
    """ pre: all items in Seq have GetDen() method, returning comparable +ve values
       post: LargestDen(seq) = largest denominator in seq """

    rv = seq[0].denominator
    for i in seq[1:]:
        d = i.denominator
        if d > rv:
            rv = d

    return rv *1


def RandOrder(seq):
    """ pre: true
       post: return a list with items in random order wrt seq"""


    return random.sample(seq,len(seq))


def Integise(seq):
    """ pre: LargestDen(seq) is defined
       post: seq' = Integise(seq)
             seq' items are integer, maintaining ratios of seq """

    den = LargestDen(seq)

    while den > 1:
        seq = [x* den for x in seq]
        den = LargestDen(seq)

    #for id in range(len(seq)):
     #   seq[id] = int(seq[id])

    return seq


### needed by StoiCons


def Normalise(seq, nidx=-1, KeepSign=1):

    abseq = Abs(seq)

    if nidx < 0:
        nidx = abseq.index(max(abseq))

    val = seq[nidx] * 1# * 1 : force creation of new instance for val
    if KeepSign:
        val = abs(val)

    if val == 0:
        raise ZeroDivisionError

    if val != 1:
        for i in range(len(seq)):
            seq[i] /= val






def QSort(L, GreaterThan=lambda a,b:a>b):

    def Swap(A, x,y):
        A[x],A[y] = A[y], A[x]
        
    def part(A,lo,hi):

        pval = A[hi]
        pidx = hi    

        idx = lo
        while idx < pidx:
            if GreaterThan( A[idx],pval):
                if pidx>idx+1:
                    Swap(A, pidx-1, pidx)
                Swap(A, idx, pidx)
                pidx-=1
            else:
                idx+=1
                        
        return A.index(pval)
                       

    def qsort(A, lo, hi):

        if lo >= 0 and hi >= 0 and lo < hi:
            # Partition and get pivot index
            p = part(A,lo,hi)
            
            # Sort the two partitions
            qsort(A, lo, p - 1) # Left side of pivot
            qsort(A, p + 1, hi) # Right side of pivot

    qsort(L, 0, len(L)-1)

















