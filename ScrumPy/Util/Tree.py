import os, sys

from ScrumPy.ThirdParty import ete3

   
def FromDeltaMtx(mtx):

    print ("From dm")

    #t0 = time.time()

    mtx = mtx.Copy()
    Nodes = {r:Tree(name=r) for r in mtx.rnames}
    nParents = 0

    while len(Nodes) >1:

        ch0, ch1, dist = FindNearestRows(mtx)
        pName = "P"+str(nParents)
        p = Tree(name=pName)
        Nodes[pName] = p
        p.add_child(Nodes[ch0], dist=dist)
        p.add_child(Nodes[ch1], dist=dist)
        nParents+=1

        mtx.NewRow(name=pName)
        mtx.NewCol(name=pName)
    
        for i in range(len(mtx)-1):
            mtx[pName,i] = mtx[i,pName] = (mtx[ch0,i]+mtx[ch1,i])/2
    
        for ch in ch0, ch1:
            mtx.DelRow(ch)
            mtx.DelCol(ch)
            del Nodes[ch]
            
    p.dist=0
    #print("Time:", time.time() - t0)
    return p

          
def FindNearestRows(mtx):
    """ pre: mtx is a square, symetric matrix
       post: -> row, col, val of smallest element in the upper triangular
        ( helper for FromDeltaMtx )
    """

    LenMtx = len(mtx)

    MinVal = min(mtx[0][1:])
    MinRow = 0
    MinCol = mtx[0][1:].index(MinVal) + 1

    for r in range(1,LenMtx-1):
        
        CurChunk = mtx[r][r+1:]
        CurMin = min(CurChunk)
        
        if CurMin < MinVal:
            MinVal = CurMin
            MinRow = r
            MinCol = CurChunk.index(MinVal) + r + 1
     

    #print(mtx, "\n\n",mtx.rnames[MinRow], MinCol, mtx.cnames[MinCol], MinVal,"!!\n\n")
            
    return mtx.rnames[MinRow], mtx.cnames[MinCol], MinVal
                     



class Tree(ete3.Tree):

                
    def Consolidate(self,Thresh = 1e-7):

        for l in self.get_leaves():
            if l.up.dist < Thresh:
                if not l.up.is_root():
                    l.up.delete()
            
        self.ladderize()

    
    def Consolidate2(self, Thresh= 1e-7):
        """ recursive verision of Consolidiate, traverses *whole* tree
            removing internal nodes with distance < thresh
        """

        def RecCons(n, thresh):
            if n.dist <= thresh and not n.is_root():
                RecCons(n.up,thresh)
                n.delete()

        for leaf in self.get_leaves():
            RecCons(leaf.up, thresh)

        self.ladderize()

        

    def NWStr(self, format=1):

        return self.write(format=format)



    def Copy(self):

        return Tree(self.NWStr(),format=1)


    def IsInternal(self):
        return True not in (self.is_leaf(), self.is_root(), not self.name in DefaultNames)


    def HasChild(self, node):

        for ch in self.traverse():
            if ch == node:
                return True
        return False


    def IsModule(self):
        """ A module is assumed to be an internal node
            with a non-default name """
        return True not in (self.is_leaf(), self.is_root(), self.name in DefaultNames)


    def HasModule(self):

        for n in self.traverse():
            if n.IsModule() and n != self:
                return True
        return False


    def IsTerminalModule(self):
        """  is a module that has no modules in descendants """

        return self.IsModule() and (not self.HasModule())


    def GetModules(self):

        rv =  {}
        for n in self.traverse():
            if n.IsModule():
                mod = Tree(n.NWStr())       # work with a COPY of the subtree
                mod.dist = n.MeanDist()
                mod.name = n.name           # better way to do this re self.name = in __init__
                mod.Rename()
                rv[n.name] = mod

        return rv




    def GetNodesByName(self,Names):
        """ pre: Names is a list of strings
           post: GetNodesByName(Names) == list of nodes with name in Names
        """

        searchres = [self.search_nodes(name=n) for n in Names]  # list of lists
        searchits = [L for L in searchres if len(L)>0]
        
        return [L[0] for L in searchits]

    def GetLeafNamesInOrder(self):
        rv = []
        for ch in self.children:
            if ch.IsLeaf():
                rv.append(ch.name)
            else:
                rv.extend(ch.GetLeafNamesInOrder())
        return rv
        

        
    def GetCommonAncestor(self, Names):

        targs   = self.GetNodesByName(Names)
        current = targs[0].up
        hits    = current.GetNodesByName(Names)
        
        while len(hits) < len(targs):
            current = current.up
            hits = current.GetNodesByName(Names)
            
        return current



    def PruneNodesMissing(self, Names):

        for ch in self.children[:]:
            if len(ch.GetNodesByName(Names)) == 0:
                self.children.remove(ch)
            else:
                ch.PruneNodesMissing(Names)
                

    def GetMinTreeHolding(self, Names):

        rv = self.GetCommonAncestor(Names).Copy()
        rv.PruneNodesMissing(Names)
        rv.MergeSingles()
        rv.SetName("-")

        return rv


    def HasOnlyLeaves(self):

        return False not in [ch.IsLeaf() for ch in self.children]


    def DistToFurthest(self):
        return self.get_farthest_leaf()[1]

    def DistToFurthestChild(self):

        dists = [node.dist for node in self.get_children()]
        return max(dists)


    def MeanDist(self):

        return mean([n.dist for n in self.traverse()])



    def MergeSingles(self):

        for ch in self.children[:]:
            if len(ch.children) == 1:
                ch.MergeSingles()
                self.dist += ch.dist
                ch.delete()
                
        for ch in self.children[:]:
            ch.MergeSingles()
                


    ### v   output and display  v




    def Save(self, features=None, outfile=None, format=1):

        if outfile==None:
            outfile=self.name + ".nw"

        ete4.Tree.write(self, features, outfile, format)


    def Rename(self):

        for n in self.traverse():
            if n.name == "NoName":
                n.name="-"

    def SetName(self, name):

        self.name = name


    def nLeafStr(self):
        """ num leaves as a string """

        return "(" + str(self.nLeaf) + ")"

    def GetNodeType(self):

        raise NotImplemented("GetNodeType")
        

        #return {
        #    self.IsRoot()    : TreeLayouts.Root,
        #    self.IsInternal(): TreeLayouts.Internal,
        #    self.IsModule()  : TreeLayouts.Module,
        #    self.IsLeaf()    : TreeLayouts.Leaf
        #}[True]


    def _ExpandAll(self):

        for node in self.traverse():
            node.collapsed = False


    def _SetnLeaf(self):

        self._ExpandAll()

        for node in self.traverse():
            node.nLeaf = len(node.get_leaves())


    def _CopyDists(self):
        """ copy node distances, so that they can be restored if modified by layout functions """

        for n in self.traverse():
            n._Dist = n.dist


    def _RestoreDists(self):
        """ restore dists modified by layout functions """

        for n in self.traverse():
            n.dist = n._Dist



    def SetZeroLeafMean(self):

        dist = self.MeanDist()
        for leaf in self.get_leaves():
            if leaf.dist == 0:
                leaf.dist = dist

    def SetAllLeafMean(self):

        dist = self.MeanDist()
        for leaf in self.get_leaves():
            leaf.dist = dist

    def SetAllDists(self,dist):

        for n in self.traverse():
            n.dist = dist


    def SetAllDistsMean(self):
        self.SetAllDists(self.MeanDist())




    def _Render(self, Layout=None, KeepCollapsed=False, FileName=None, Width=None, Height=None):

        Header = None

        self._CopyDists()
        self._SetnLeaf()

        layout = TreeLayouts.Layouts[Layout]
        if FileName == None:
            self.show(layout = layout) 
        else:
            self.render(file_name = FileName,
                        layout    = layout,
                        w         = Width,
                        h         = Height
                    )

        if not KeepCollapsed:
            self._ExpandAll()
        self._RestoreDists()


    def Show(self, Layout="Explore",KeepCollapsed=False):

        print ("!! Show not yet implemented, using tree.show() instead !!")
        self.show()

        #self._Render(Layout=Layout,KeepCollapsed=KeepCollapsed)


    def SavePDF(self, fname=None, Layout="Explore",KeepCollapsed=False):

        ete4Warn("SavePDF", "Falling back to ete4.tree.render(fname), othetr options ignored")
        self.render(fname)
        return

        if fname == None:
            fname = self.name

        fname += "_" + Layout + ".pdf"
        fname = fname.replace(" ","_")
        self._Render(Layout=Layout, FileName=fname,KeepCollapsed=KeepCollapsed)




