
import sys


class BaseArray:
    """ Base class for arrays.
        Pythonesque interface to low level swig/c arrays.
        Subclasses must provide:
            _new(size)   => new array
            _get(index)  => array[index]
            _cast(value) => type(_cast(value)) == type(array[i]) | exception
            _set(index,value) => array[index] == _cast(value)
            _del(array)  => free array

        array is assumed to have a 1 offset index, and BaseArray provides mapping
        from the more usual python [0..size-1] <> [1..size]

        BaseArray (not the subclass) is responsible for array bound and
        other sanity checks, except for _cast which should raise ValueError
        if type conversion not possible.

    """
    
    def _NotImplemented(BaseArray, *a, **k):
        raise NotImplementedError
    
    _new  = _NotImplemented
    _get  = _NotImplemented
    _cast = _NotImplemented
    _set  = _NotImplemented
    _del  = _NotImplemented

    _offset=1
    
    
    def __init__(self,SizeOrList):
        if type(SizeOrList) == int:
            self.size = SizeOrList
            self._array = self._new(self.size+self._offset)
        else:
            self.FromList(SizeOrList)

    def __idx_check__(self,i):
        if i <self._offset or i>self.size+self._offset:
            raise IndexError

    def __getitem__(self, i):
        i+=self._offset
        self.__idx_check__(i)
        return self._get(self._array,i)

    def __setitem__(self, i, v):
        i+=self._offset
        self.__idx_check__(i)
        v = self._cast(v)
        return self._set(self._array,i,v)

    def __del__(self):
        self._del(self._array)

    def __len__(self):
        return self.size

    def ToList(self):
        rv = []
        for i in range(self.size):
            rv.append(self[i])
        return rv

    def FromList(self,l):
        l = list(map(self._cast,l))  # if can't cast l, leave self unchanged
        if hasattr(self, "_array"):
            self._del(self._array)
        self.size = len(l)
        self._array = self._new(self.size+self._offset)
        for i in range(self.size):
            self[i] = l[i]

    def __repr__(self):
        return repr(self.ToList())

    def __str__(self):
        return str(self.ToList())



