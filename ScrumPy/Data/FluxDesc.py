from . import DataSets

class FluxDesc(DataSets.DataSet):

    def __init__(self, model):
        self.model = model


    def Update(self,fd=None,Name=None):

        if fd is None:
            self.NewCol(
                self.model.GetRates(),
                Name
            )
        else:
            self.NewCol(name=Name)
            for reac in fd:
                self[reac,-1] = fd[reac]

        
       
