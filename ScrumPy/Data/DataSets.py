
"""

ScrumPy -- Metabolic Modelling with Python

Copyright Mark Poolman 1995 - 2002

 This file is part of ScrumPy.

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""

import sys, types,  math, string, csv

from scipy import stats # TODO move into Stats


from ScrumPy.Util  import DynMatrix, Sci,  Set

from . import Stats, Plotter, Histogram


def StrOrFloat(x):
    try:                # unfortunately there is no str.isfloat()
        return float(x) # so if it converts to a float, it's a float
    except:
        return str(x)   # otherwise it's a string


class  DataSet(DynMatrix.matrix):

    def __init__(self,
                 FromFile=None, # X
                 ItemNames=[],  # X
                 SrcDic=None,   # X
                 FromMtx=None,  # X
                 rnames = None, cnames = None, # X
                 Conv=StrOrFloat,
                 Delim="\t",
                 UseQtPlotter=True):
        """ pre: maximum 1 non-default X labeled parameter.
            (FileName != None) -> (File conforms to FileSpec)
           post: Usable """

        self.Delim = Delim
        self.FileName=None
        
        if FromMtx != None:
            DynMatrix.matrix.__init__(self,Conv=Conv, FromMtx=FromMtx)
        elif rnames != None:
            DynMatrix.matrix.__init__(self, rnames=rnames, cnames=cnames)
        elif FromFile != None:
            DynMatrix.matrix.__init__(self,Conv=Conv)
            self.Delim=Delim
            self.ReadFile(FromFile)
        elif SrcDic != None:
            DynMatrix.matrix.__init__(self,cnames=ItemNames,Conv=Conv)
            self.SrcDic = SrcDic
        elif ItemNames != []:
            DynMatrix.matrix.__init__(self,Conv=Conv,cnames=ItemNames)
        else:
            DynMatrix.matrix.__init__(self,Conv=Conv)

        self.Plotter = None
        self.PlotX   = None # whether or not we have added a plotter we need the attr here, in case one is added later
        if self.FileName is None:
            self.Caption = "Unamed"
        else:
            self.Caption = self.FileName

        if UseQtPlotter:
            self.AddPlotter = self.AddQtPlotter
        
        
            

    def __del__(self):
        if self.Plotter is not None:
            try:
                self.Plotter.Close()
            except:
                pass # just in case something else has closed it


    def AddPlotter(self):
        """ the defualt (old) gnuplot plotter """
    
        if self.Plotter is None:
            self.Plotter = Plotter.Plotter(Title = self.FileName)
            self.Plotter.SetMissing()
            if self.PlotX is None:
                self.PlotX = 0
            #self.PlotY = 1


    def AddQtPlotter(self):
        """ the new (soon to be default) plotter """

        if (self.Plotter is None) or (not self.Plotter.IsOpen()):

            self.Plotter = Plotter.QtPlotter("ScrumPy Dataset", self.Caption)
            if self.PlotX is None:
                self.PlotX = 0


    def AddUIPlotter(self):

        self.Plotter = self.ui.GetPlotter()


    def ClosePlotter(self):
        self.Plotter.Close()
        self.Plotter = None


    def Copy(self):

        return   DataSet(FromMtx=self, WithPlotter=not self.Plotter is None)

    def ClearData(self):
        """ remove all rows, leave cols and other info intact"""

        self.rows = []
        self.rnames =[]


    def Subset(self, names):
        rv = DataSet()
        for name in names:
            rv.NewCol(self.GetCol(name),name)
        rv.rnames  = self.rnames[:]
        return rv




    def ReadFile(self, FileName):

        lines = open(FileName).readlines()
        if len(lines) > 10:
            first = lines[:10]
        else:
            first = lines
            
        sniffer = csv.Sniffer()
        dialect = sniffer.sniff("".join(first))
        reader  = csv.reader(lines, dialect)
        rows = [row for row in reader]
        
        self.cnames = rows.pop(0)
        self.rows = [[self.Conv(i) for i in row] for row in rows]
        self.rnames = ["Row_" + str(n) for n in range(len(self.rows))]
        
        self.FileName = FileName
        


    def WriteFile(self,File=None):

        if File == None and self.FileName == None:
            print("No file to write to !")

        else:
            if type(File) is str:
                self.FileName = File
                File = open(File, "w")

            else:
                File = open(self.FileName, "w")

            print(self.Delim.join(self.cnames), file=File)

            for r in self:
                print(self.Delim.join(map(str, r)), file=File)



    def WriteFileAsColPairs(self, File,ref):

        if type(File)==bytes:
            File = open(File, "w")

        refc = self.GetCol(ref)

        for cname in self.cnames:
            if cname != ref:
                File.write("#" + ref+" "+cname+"\n")
                col = self.GetCol(cname)
                for i in range(len(refc)):
                    File.write(str(refc[i])+"\t"+str(col[i]) + "\n")
                File.write("\n")


    def XYLists(self, XCol, YCols, SliLo=None, SliHi=None):

        if type(YCols)==int or type(YCols)==str:
            YCols=[YCols]
            
        Xs =self.GetCol(XCol)[SliLo:SliHi] * len(YCols)
        Ys = []
        for YCol in YCols:
            Ys += self.GetCol(YCol)[SliLo:SliHi]

        return Xs, Ys

        



    def Update(self, label=None):
        #
        # TODO: self.UpdateFromDic(self.SrcDic) ??
        #
        if label==None:
            label = "Row_" + str(len(self))
        row = [0]*len(self.cnames)
        self.NewRow(row,label)
        try:
            for k in self.cnames:
                self[label,k]= self.SrcDic[k]
        except:
            print(""""
            !!! DataSet.Update failed - either not created with source dctionary
            !!! or source dictionary keys has new keys
            """)


    def UpdateFromDic(self, dic, name=None):

        self.NewRow()
        if name != None:
            self.rnames[-1] = name
        for k in list(dic.keys()):  
		# iterate explicitly on keys because some "dictionary-like" classes might not support direct iteration
            if not k in self.cnames:
                self.NewCol(name=k)
            self[-1,k] = dic[k]



    def _get(self, Name):
        try:
            return self[Name]
        except:
            return self.GetCol(Name)


    def SelectCols(self, cols, rows=None):

        cidxs = [self._getc(col) for col in cols]
        cnames = [self.cnames[c] for c in cidxs] # ensure cnames are strings even if ints in cols
        rv = DataSet(ItemNames=cnames)
        
        if rows is None:
            rows = self.rnames[:]

        for row in rows:
            rv.NewRow([self[row][c] for c in cidxs], str(row))

        return rv


    def DescribeCols(self, cols, Indep=None):

        rv = DataSet(ItemNames=["Mean", "SEM"])
        cidxs = [self._getc(c) for c in cols]
        sqrtnobs = len(cols)**.5
        
        for rname in self.rnames:
            row = self[rname]
            data = [row[idx] for idx in cidxs]
            desc = stats.describe(data)
            sem = desc.variance**.5/sqrtnobs # stderr of mean
            rv.NewRow([desc.mean, sem], rname)

        if Indep is not None:
            if type(Indep) is int:
                Indep = self.cnames[Indep]
            rv.NewCol(self.GetCol(Indep), Indep)
            rv.MakeFirstCol(Indep)

        

        return rv
            
        
                     
        

    

    

        
        
        
        

        

        

        

    def Sum(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Sum(Name) == Arithmetic sum of self[Name] """

        return sum(self._get(Name))

    def Mean(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Mean(Name)    == arithmetic mean of self[Name] """

        return Stats.Mean(self._get(Name))


    def ColMeans(self):
        """ pre: len(self) >0
          post: list of mean values of items in self """

        return list(map(self.Mean,  self.cnames))


    def Variance(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.Variance(Name)== variance self[Name] """

        return Stats.Var(self._get(Name))


    def StdDev(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.StdeDev(Name) == standard deviation of self[Name] """

        return Stats.StdDev(self._get(Name))

    def StdErr(self, Name):
        """ pre: len(self[Name]) > 1
            post: self.StdErr(Name)  ==  standard error of the mean of self[Name] """

        return self.StdDev(Name)/math.sqrt(len(self._get(Name)))


    def Lower_n_ile(self, Name, n):
        """ pre: len(self[Name]) > 0, 0 < n <= 1
            post: self.Lower_n_ile(self, Name, n) == value of self[Name] which is the limit of the
            lower nth portion of self[Name] """

        return Stats.Lower_n_ile(self._get(Name),n)


    def Upper_n_ile(self, Name, n):
        """ pre: len(self[Name]) > 0, 0 < n <= 1
            post: self.Lower_n_ile(self, Name, n) == value of self[Name] which is the limit of the
            upper nth portion of self[Name] """

        return Stats.Upper_n_ile(self._get(Name),n)


    def Median(self, Name):
        """ pre: len(self[Name]) > 0
            post: self.Median(Name) == Median value of self[Name] """

        return Stats.Median(self._get(Name))


    def TTest(self, Name_a, Name_b):
            """ Student's (two tailed) t and p(t) """
            return Stats.TTest(self._get(Name_a),self._get(Name_b))


    def FTest(self, Name_a,  Name_b):
            """ F and p(F) """
            return Stats.FTest(self._get(Name_a),self._get(Name_b))


    def Pearsons(self,  ref,  targs=[]):
        """ pre: ref and targs in self.cnames
           post: dictionary of {targ: Pearsonsr(ref, targ)}"""

        rv = {}
        ref = self.GetCol(ref)
        for t in targs:
           rv[t] = Stats.Pearson_r(ref, self.GetCol(t))

        return rv


    def Spearmans(self,  ref,  targs=[]):
        """ pre: ref and targs in self.cnames
           post: dictionary of {targ: Pearsonsr(ref, targ)}"""

        rv = {}
        ref = self.GetCol(ref)
        for t in targs:
           rv[t] = Stats.Spearman_r(ref, self.GetCol(t))

        return rv


    def Deriv(self, wrt, ynames=None):
        """ pre: wrt in self.cnames,
                 ynames != None => ynames id a subset of self.cnames
            post: Deriv(...) -> matrix of derivatives of y columns wrt to x column
                  ynames == None => derivs of all columns except x
        """
                 
        rv = DataSet()
        x = self.GetCol(wrt)
        rv.NewCol(x, wrt)

        if ynames is None:
            ynames = self.cnames 
        ynames = [n for n in ynames if n != wrt]

        for y in ynames:
            col = self.GetCol(y)
            dcol = Sci.NumDeriv(x,col)
            rv.NewCol(dcol, "d_"+y)

        return rv
        
    

    def MeanDuplicates(self,  RefCol, SetPlotXRef=True, OrderByRef=True ):
        """ pre: self.GetCol(RefCol)
           post: Data set of average values for which RefCol value is duplicated in self
        """

        rv = DataSet(ItemNames=self.cnames)
        if SetPlotXRef:
            rv.SetPlotX(RefCol)

        Uniques = Set.MakeSet(self.GetCol(RefCol))
        if OrderByRef:
            Uniques.sort()


        for u in Uniques:
            uds = self.Filter(lambda x: x==u,  RefCol)
            rv.NewRow(uds.ColMeans())

        return rv


    def CurveFit(self, x, ys, f, p_init=None, p_limits=None, title=None):

        xdata = self.GetCol(x)
        ydata = []
        for y in ys:
            ydata += self.GetCol(y)

        rv = Sci.CurveFit(f, xdata, ydata, p_init, p_limits)

        if title is not None:
            self.NewCol([f(x, *rv["Params"]) for x in xdata], title)
            
        return rv


    #
    ##
    ###  Plotting functions #########################
    ##
    #

    def SetPlotX(self, x):
        """"pre: True
           post:    if self has a column of name or index x, this will used as x axis for plotting
                  else warning message on terminal"""

        if type(x) is int and x <= 0 < len(self.cnames):
            x = self.cnames[x]
            
        if x in self.cnames: # redundant if x was valid int but simpler logic
            self.PlotX = x
            self.Plot()
        else:
            print("!! ",  x,  "not found - ignoring !!")


    def SetPlotY(self,  y):
        """"pre: True
           post:    if: self has a column of name or index y, this will used as the y axis for 3d surface plots
                  else: warning message on terminal"""

        if y in self.cnames:
            self.PlotY = y



    def SetLogAxis(self, Axis="x", On=True):
        """ pre: Axis="x"|"y", On boolean """

        self.AddPlotter()

        self.Plotter.SetLog(Axis, On)
        self.Plot()


    def SetPlotStyle(self, Data=[], Style="linesp"):
        
        self.AddPlotter()
        
        if Data == []:
            Data = self.GetPlotItems()
        elif type(Data) == bytes:
            Data = [Data]

        for d in Data:
            self.Plotter.SetStyle(d, Style)

        self.Plot()




    def AddToPlot(self, ys, style="linespoints",  AutoPlot=True,  XclX=True, Opts={}, Plotter=None):
        """ pre:  ys is a string or list of strings
                  style is a valid gnuplot style
            post:   if: present items in ys are added to the current plot
                  else: warning(s) on terminal
                  XclX => independent variable ignored if in ys
                  AutoPlot => plot is updated immediatly (applies to all other methods with AutoPlot parameter
        """

        self.AddPlotter() # if a plotter is already defined, this does notthing

        if not type(ys) == list:
            ys = [ys]
            
        if XclX and self.PlotX in ys:
            ys = ys[:]
            ys.remove(self.PlotX) 

        for y in ys:
            if y in self.cnames:
                self.Plotter.AddData(y, [self.GetCol(self.PlotX), self.GetCol(y)],style, Opts)
            else:
                print("! ignoring attempt to plot non-existent value ",y, " !", file=sys.stderr)

        if AutoPlot:
            self.Plot()

    def UpdatePlot(self):

        xdat = self.GetCol(self.PlotX)
        for k in self.Plotter.keys():
            self.Plotter.Update(k, xdat, self.GetCol(k))


    def AddToSurfPlot(self, zs, style="lines"):

        raise NotImplementedError

        if not type(zs) == list:
            zs = [zs]

        for z in zs:
            if z in self.cnames:
                self.Plotter.AddData(z, [self.GetCol(self.PlotX), self.GetCol(self.PlotY),  self.GetCol(z)],style)
            else:
                print("! ignoring attempt to plot non-existent value ",z, " !", file=sys.stderr)

        #if AutoPlot:
        #    self.SurfPlot()


    def AddMatchesToPlot(self, substr,  style="linespoints",   AutoPlot=True,  XclX = True):
        """ pre:  substr is a string
                  style is a valid gnuplot style
            post: item names  containing substr are added to the current plot """

        self.AddToPlot([col for col in self.cnames if substr in col], style,  AutoPlot,  XclX)



    def RemoveFromPlot(self, ys, AutoPlot=False):
        """ pre: ys is a string or list of strings
           post: ys is/are not in the current plot"""

        if self.Plotter is None:
            print("No plots defined!")
        else:
            if not type(ys) == list:
                ys = [ys]
            self.Plotter.Remove(ys)
##
##            for y in ys:
##                self.Plotter.Remove(y)

            if AutoPlot:
                self.Plot()
            
 


    def RemoveMatchesFromPlot(self, substr,  AutoPlot=True):
        """ pre: substr is a string
           post: items whose name matches gsubstr are removed from the plot """
        
        if self.Plotter is None:
            print("No plots defined!")
        else:
            self.RemoveFromPlot([key for key in list(self.Plotter.keys()) if substr in key], AutoPlot)



    def RemoveAllFromPlot(self):
        """ pre: True
           post: self.GetPlotItems==[]"""
        
        if self.Plotter is None:
            print("No plots defined!")
        else:
            self.RemoveFromPlot(list(self.Plotter.keys()))




    def ReplacePlotItems(self, Items,AutoPlot=True, XclX=True):
        """ pre: Util.Set.IsSubset(Items, self.cnames)
           post: Util.Set.IsEqual(Items, self.GetPlot)"""

        self.RemoveAllFromPlot()
        self.AddToPlot(Items, AutoPlot,  XclX)


    def GetPlotItems(self):
        """ return a list of names of data currently being plotted"""

        return list(self.Plotter.keys())


    def GetPlotData(self, x, y):
        """ return a Plotter.Plotdata object, corresponding to col x and y,
            useful for combining data from more than one data set in a single plot.
            see the Data.Plotter module """

        return Plotter.Plotdata([self.GetCol(self.PlotX), self.GetCol(y)])


    def SavePlot(self, filename):
        """ save the current plot in gnuplot readable format to filename """

        self.Plotter.SavePlot(filename)


    def SavePlotPDF(self, filename):

        self.Plotter.SaveA4PDF(filename)


    def Plot(self, MaxPoints=0):
        """ update the current plot window or plot in new one if not present"""

        self.AddPlotter()

        if MaxPoints ==0:
            start=0
        else:
            lenself = len(self)
            if lenself > MaxPoints:
                start= lenself - MaxPoints

        if self.PlotX is None:
            self.PlotX = self.cnames[0]

        x=self.GetCol(self.PlotX)[start:]
        for k in list(self.Plotter.keys()):
            y = self.GetCol(k)[start:]
            self.Plotter[k].UpdateData([x[:], y])
            # Ensure *copy* of x as update will change if nans present in y

        self.Plotter.Plot()


    def SurfPlot(self):

        raise NotImplemented

        self.Plotter.SurfPlot()



    def Histogram(self, Name, nbins=None, Plot=True):
        """ pre: Name in self.cnames
                 nbins is None OR, type(nbins) is int AND nbins >1
            post: -> instance of Histogram.Histogram representing the data in the column named Name
                  Plot == TRUE, histogram will be plotted on self.Plotter
        """

        rv = Histogram.Histogram(self.GetCol(Name), nbins)
        if Plot:
            self.AddPlotter()
            self.Plotter.Histogram(Name,rv.Bounds, rv.Dens)
        return rv
    
##
##TODO: re-write to use new Histogram, not Histo, module
##
##    def HistogramAll(self, nbins, **kwargs):
##        data = []
##        for r in self.rows:
##            data.extend(r)
##        return Histo.Histo(data, nbins, **kwargs)
##
##
##    def HistogramUD(self, nbins,Diag=True, **kwargs):
##        """ pre: self is square """
##        data = []
##        if Diag:
##            for i in range(len(self)):
##                data.extend(self[i][i:])
##        else:
##            for i in range(len(self)-1):
##                data.extend(self[i][i+1:])
##
##        return Histo.Histo(data, nbins, **kwargs)
##
##
    def FFT(self, xcol=0):
        """ pre: self contains evenly sampled timecourse data with time in xcol
           post: => dataset containing the absolute +ve freqeuncy components of self.columns
        """

        if type(xcol) is int:
            xcol = self.cnames[xcol]

        rv = DataSet()
        interval = self[1, xcol] - self[0,xcol] 
        Freqs =  Sci.AbsFFTx(len(self), interval)
        rv.NewCol(list(Freqs),"Freq")
        
        cols = self.cnames[:]
        cols.remove(xcol)
        for col in cols:
            fft = Sci.AbsFFT(self.GetCol(col))
            rv.NewCol(list(fft), col) 

        return rv
                      

def TopAbsNFromDic(dic,  n,  cname):
     
    sortedvals = sorted(list(dic.values()), lambda x,y: cmp(abs(x), abs(y)), reverse=True)
    aminval = abs(sortedvals[n-1])

    rv = DataSet(ItemNames=[cname])
    for k in dic:
        v = dic[k]
        if abs(v)>= aminval:
            rv.NewRow([v], k)
            
    rv.SortBy(cname,Descend=True)
    return rv
    



