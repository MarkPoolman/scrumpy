import math
import numpy

from ScrumPy.UI.QtUIComponents import PlotClient
Plotter =  PlotClient.SimplePlotWinCli



from scipy.stats import iqr    # TODO: move this to ScrumPu.Util.Sci
def FreeDiac(data,nBins=True):  # Equivalent(ish) of r's FD directive for histograms
    """ Freedman-Diaconis method to deternine optimal bin width 
        (or number of bins if nBins is True) to construct a histogram
        for the given data"""
    
    width = 2*iqr(data)/len(data)**(1/3)
    if nBins:
        return int ( (max(data) - min(data))/width )
    return width


class Histogram:

    def __init__(self, data, nbins=None, WithCDF=True):
        
        if nbins is None:
            self.nBins = nbins = FreeDiac(data, nBins=True)
            # int(1+ math.log2(len(data)))  Sturges' rule
  

        nCounts = self.nCounts = len(data)

        self.Counts, self.Bounds =  Counts, Bounds = numpy.histogram(data, nbins)
        
        self.BinWidth = BinWidth =  Bounds[1] - Bounds[0]
        self.Mids     = Mids     =  list(Bounds[:-1] + BinWidth/2 )
        self.Dens     = Counts/(BinWidth*nCounts)

        if WithCDF:
    
            data = sorted(data)
            unique =numpy.unique(data)
            wcounts = [data.count(u)/nCounts for u in unique]
            for n in range(1,len(unique)):
                wcounts[n] += wcounts[n-1]       

            self.CDF_x = unique
            self.CDF_y = wcounts
            self.SF_y  = [1-c for c in self.CDF_y] # "survival function" consistent naming w/ scipy.stats

        self.Plotter = None


    def SetupPlotter(self, WinTitle="ScrumPy Histogram", Caption="Data", Legend="data"):

        self.Plotter = Plotter(WinTitle=WinTitle, Caption=Caption)
        self.Legend = Legend


    def Plot(self, Legend=None, Dens=True, Plotter=None):

        if Dens:
            ydat = self.Dens
        else:
            ydat = self.Counts

        if Plotter is None:
            if self.Plotter is None:
                self.SetupPlotter()
            Plotter = self.Plotter

        if Legend is None:
            Legend = self.Legend
            
        Plotter.Histogram(Legend, self.Bounds, ydat)
        



    def PlotCDF(self, Legend=None, Plotter=None, SF=False):

        if Plotter is None:
            if self.Plotter is None:
                self.SetupPlotter()
            Plotter = self.Plotter

        if Legend is None:
            Legend = self.Legend

        if SF:
            y_dat = self.SF_y
        else:
            y_dat = self.CDF_y
            
        Plotter.ScatterPlot(Legend, self.CDF_x, y_dat,{"symbolSize":6})
        if Plotter is self.Plotter:
            Plotter.Show()

    def PlotSF(self, *args, **kwargs):
        self.PlotCDF(*args, **kwargs, SF=True)



class FixedHisto(Histogram):
    def __init__(self, data, Hi, Lo=0,  Width=1):

        self.Range=(Lo,Hi)
        nbins= self.nbins = int((Hi-Lo)/Width)

        self.Counts, self.Bounds =  Counts, Bounds = numpy.histogram(data, nbins, range=(Lo,Hi))
        
        self.BinWidth = Width
        self.Mids     = Mids     =  list(Bounds[:-1] + Width/2 )
        self.Dens     = Counts/(Width*len(Counts))

        self.Plotter = None


        
            
            
    
