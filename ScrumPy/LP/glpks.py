


"""
glpk.py - (c) Mark Poolman 2008 onwards.
A high(ish) level interface between python and the glpk library of
Andrew Makhorin (mao AT gnu DOT org) which can be found at www.gnu.org/software/glpk


 This file is part of ScrumPy - Metabolic Modelling in Python

    ScrumPy is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ScrumPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ScrumPy; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""





import os, sys,  random


import swiglpk as glpk_ll

from ..Util import  DynMatrix


RevDic = lambda d: d.update(zip(list(d.values()),  list(d.keys())))


ColKinds = {
    glpk_ll.GLP_CV : "Continuous",
    glpk_ll.GLP_IV : "Integer",
    glpk_ll.GLP_BV : "Binary",
}
RevDic(ColKinds)


ObjKinds = {
    glpk_ll.GLP_MIN : "Min",
    glpk_ll.GLP_MAX : "Max"
}
RevDic(ObjKinds)



########################## mappings between various constants


Num2Str = {             # map constant status values to human strings
    glpk_ll.GLP_MIN : "Min",
    glpk_ll.GLP_MAX : "Max",
}

Str2Num = dict(zip(Num2Str.values(), Num2Str.keys()))

Status2Str = {
    glpk_ll.GLP_UNDEF :"undefined",
    glpk_ll.GLP_FEAS  : "feasible",
    glpk_ll.GLP_INFEAS :"infeasible",
    glpk_ll.GLP_NOFEAS  :"no feasible",
    glpk_ll.GLP_OPT     :"optimal",
    glpk_ll.GLP_UNBND   :"unbounded"
}             







########################## freindly interface to low level c arrays

swiglpk=glpk_ll


class Array:

    EType=None # array and element types to be supplied by the sub-class

    def __init__(self,SizeOrList):
        if type(SizeOrList) ==int:   # we're initing an undetlying malloc'd C array
            size = SizeOrList
            vals = [0] * size        # ensure all elements are iniitialised
        else:
            size = len(SizeOrList)
            vals = SizeOrList

        self.size = size
        super().__init__(size+1)
        for i in range(size):
            self[i+1] = self.EType(vals[i])
        #
        ## better to overload __get/setitem__ for the +1 offset ?
        #  also range check

    def __len__(self):
        return self.size


class IntArray(Array,swiglpk.intArray):

    EType = int


class DoubleArray(Array, swiglpk.doubleArray):

    EType = float
##
##
##class BaseArray:
##
##    AType=None
##    EType=None # array and element types to be supplied by the sub-class
##
##    #_offset=1 by default
##
##    def __init__(self,SizeOrList):
##        if type(SizeOrList) ==int:   # we're initing an undetlying malloc'd C array
##            size = SizeOrList
##            vals = [0] * size        # ensure all elements are iniitialised
##        else:
##            size = len(SizeOrList)
##            vals = SizeOrList
##
##        self.size = size
##        self._array = AType.__init__(self,size+1)
##        for i in range(size):
##            self[i+1] = self.EType(vals[i])
##        #
##        ## better to overload __get/setitem__ for the +1 offset ?
##        #
##
##    def __getitem__(self, i):  # TODO: range check on i for set and get
##        return self._array[i]
##
##    def __setitem__(self, i, v):
##        self._array[i]  = self.EType(v)
##
##    def __len__(self):
##        return self.size
##
##
##class IntArray(BaseArray):
##
##    AType =  swiglpk.intArray
##    EType = int
##
##class DoubleArray(BaseArray):
##
##    AType = swiglpk.doubleArray
##    EType = float
##
####
##class IntArray(swiglpk.intArray):
##
##    #_offset=1 by default
##
##    def __init__(self,SizeOrList):
##        if type(SizeOrList) ==int:   # we're initing an undetlying malloc'd C array
##            size = SizeOrList
##            vals = [0] * size        # ensure all elements are iniitialised
##        else:
##            size = len(SizeOrList)
##            vals = SizeOrList
##
##        self.size = size
##        swiglpk.intArray.__init__(self,size+1)
##        for i in range(size):
##            self[i+1] = int(vals[i])
##
##    def __len__(self):
##        return self.size
##
##    
##
##
##class DoubleArray(swiglpk.doubleArray):
##
##    
##    def __init__(self,SizeOrList):
##        if type(SizeOrList) ==int:   # we're initing an underlying malloc'd C array
##            size = SizeOrList
##            vals = [0.0] * size        # ensure all elements are iniitialised
##        else:
##            size = len(SizeOrList)
##            vals = SizeOrList
##            
##        self.size = size    
##        swiglpk.doubleArray.__init__(self,size+1)
##        for i in range(size):
##            self[i+1] = float(vals[i])
##
##
##    def __len__(self):
##        return self.size
##
##    



########################## mappings between arrays/lists etc

def AddOne(l):
    return [x+1 for x in l]

def SubOne(l):
    return [x-1 for x in l]

def Vec2Lists(vec):
    """ pre: vec indexable type with elements comparable to 0
       post: list of indices and vals of non-zereos in vec """

    d = {}
    for i in range(len(vec)):
        val = vec[i]
        if val != 0:
            d[i] = val
    return list(d.keys()), list(d.values())

def Lists2Vec(idxs,vals,size):
    """ pre: len(vals) == len(idxs), max(idx)<size
       post: rv[indxs[i]] = vals[i], i[0..size-1] else 0.0 """


    rv = [0.0] * size
    for i in range(len(idxs)):
        idx = idxs[i]
        if idx >=0:
            rv[idx] = vals[i]
    return rv



#########################


VerySmall = 1e-12
def IsZero(number, tol=VerySmall):
    return abs(number) < tol





#
# class that executes a glpk_ll function with fixed first arg
# and arbitrary subsequent args
#
class exec_ll:
        def __init__(self,name, arg0):
            self.name = name
            self.arg0 = arg0

        def __call__(self, *args):
            return getattr(glpk_ll, self.name)(self.arg0,*args)




#
# IO utils
#
#
def F2Str(fun):
    """ pre: fun(filename) writes to a file
       post: returns as a string what fun was going to write """

    dest = str(random.randint(0,100000))
    fun(dest)
    rv = open(dest).read()
    os.remove(dest)
    return rv


#
##
### ##################### main user interface to glpk
##
#
class lp:


    _Readers = {      # map humanoid format descriptions to glpk function names
        "FixedMPS"    : glpk_ll.glp_read_mps,
    #    "FreeMPS"     : glpk_ll.glp_read_freemps,
    #    "CPLEX"       : glpk_ll.glp_read_cpxlp,
    #    "GnuMathProg" : glpk_ll.glp_read_model - no longer in glpk?
    }

    _MethodsMap = {   # map methods to names of functions that have trivial equivalence
                      # methods can't be bound until we have an lpx instance - see __init__()
        "SetName":"glp_set_prob_name",
        "GetName":"glp_get_prob_name",

        "SetObjDir":"glp_set_obj_dir",
        "GetObjDir":"glp_get_obj_dir",

        "GetObjVal" :"glp_get_obj_val",
        "GetMIPObjVal":"glp_mip_obj_val",

        "SetObjName":"glp_set_obj_name",
        "GetObjName":"glp_get_obj_name",

        "GetNumRows":"glp_get_num_rows",
        "GetNumCols":"glp_get_num_cols"
    }


    _StatusGettersMap = {  # as above, these query problem status
        "Generic": "glp_get_status",
        "Primal" : "glp_get_prim_stat",
        "Dual"   : "glp_get_dual_stat"
    }
    _StatusGetters = {} # will be bound here on __init__()


    _WritersMap = {  # map nice warm fuzzy pythonic string format descriptions to glpk functions
        "Human" :"glp_print_prob",
        "FixedMPS" : "glp_write_mps",
        "FreeMPS"  : "glp_write_freemps",
        "CPLEX"    : "glp_write_cpxlp",
        "Solution" : "glp_print_sol"
    }
    _Writers = {} # as previous maps




    def __init__(self, name="Unamed"):

        # setup the low level module
        self.lpx = glpk_ll.glp_create_prob()
        
        self.smcp = smcp  = glpk_ll.glp_smcp()
        glpk_ll.glp_init_smcp(smcp)
        smcp.presolve = glpk_ll.GLP_ON
        smcp.msg_level = glpk_ll.GLP_MSG_ON # | _OFF | _ERR | _ALL
        
        

        self.iocp = iocp  = glpk_ll.glp_iocp()
        glpk_ll.glp_init_iocp(iocp)
        iocp.presolve = glpk_ll.GLP_ON

        # finnish setup using methods from this module
        self.__setup()
        self.SetName(name)



    def __setup(self):
        self.rnames={}
        self.cnames={}
        self.cidxs = {}
        self.ridxs = {}

        self.__map_ll()


        for method in self._MethodsMap:
            setattr(self, method, exec_ll(self._MethodsMap[method],self.lpx))

        for writer in self._WritersMap:
            self._Writers[writer] = exec_ll(self._WritersMap[writer],self.lpx)

        for getter in self._StatusGettersMap:
            self._StatusGetters[getter] =  exec_ll(self._StatusGettersMap[getter],self.lpx)


    def __repr__(self):
        return self.GetName()+" (instance of glpk.lp)"

    def __str__(self):
        return self.Write()


    def __map_ll(self):

        for i in dir(glpk_ll):
            if i.startswith("glp_") and not i.startswith("glp_read"):
                setattr(self, "_"+i, exec_ll(i,self.lpx))
            


    #def __getattr__(self, attr):
    #    return
        # "just-in-time" alternative to __map_ll
        # the approach, and variants thereof, will almost certainly
        # cause more problems than it solves.
        # left here for your entertainment.
        #if attr.startswith("_glp_lpx"):
        #    a = exec_ll(attr[1:], self.lpx)
        #    setattr(self, attr,a)
        #    return a



    def __diccheck(self, dic, k):
        if not k in dic:
            sys.stderr.write(k+" is invalid - options are: "+", ".join(list(dic.keys())))
            raise KeyError
        return dic[k]


    def __rowcheck(self, r):
        if r<0 or r>=self.GetNumRows():
           raise IndexError(str(r))
        return r+1

    def __colcheck(self, c):
        if c<0 or c>=self.GetNumCols():
           raise IndexError(str(c))
        return c+1


    def __getrow(self,r):
        if type(r)==str:
            if r in self.ridxs:
                r = self.ridxs[r]
            else:
               raise KeyError(str(r))

        return self.__rowcheck(r)


    def __getcol(self,c):
        if type(c)==str:
            if c in self.cidxs:
                c = self.cidxs[c]
            else:
               raise KeyError(str(c))

        return self.__colcheck(c)



    def __getbnds(self, lo, hi):

        if   lo == None and hi == None:
            flag, lo, hi = glpk_ll.GLP_FR, 0.0, 0.0
        elif lo != None and hi == None:
            flag, lo, hi = glpk_ll.GLP_LO, lo, 0.0
        elif lo == None and hi != None:
            flag, lo, hi = glpk_ll.GLP_UP, 0.0, hi
        elif lo != None and hi != None:
            if lo < hi:
                flag =  glpk_ll.GLP_DB
            elif lo==hi:
                flag = glpk_ll.GLP_FX
            else:
               raise ValueError("lo > hi !")

        return flag, lo, hi



    def __RebuildNameDics(self):

        self.rnames = {}
        self.cnames = {}
        self.ridxs = {}
        self.cidxs = {}
        nr,nc = self.GetDims()

        for r in range(nr):
            rname = self._glp_get_row_name(r+1)
            self.rnames[r] =  rname
            self.ridxs[rname] = r

        for c in range(nc):
            cname = self._glp_get_col_name(c+1)
            self.cnames[c] =  cname
            self.cidxs[cname] = c


    def __del__(self):
            self._glp_delete_prob()

    #
    ## # Public methods start here
    #


    #
    ## define the problem
    #

    def SetObjDirec(self, direc="Min"):
        """ pre: direc = "Min" | "Max" """

        self._glp_set_obj_dir(ObjKinds[direc])

    def GetObjDirec(self):
        return ObjKinds[self._glp_get_obj_dir()]
    
    def AddRows(self,rows):

        tr = type(rows)

        if tr == str:
            self.AddRows([rows])
            
        if tr == int:
            if rows <1:
               raise ValueError("Can't add less then one row !")
            self._glp_add_rows(rows)

        elif tr == list:
            oldnr = self.GetNumRows()
            newnr = len(rows)
            if True in list(map(self.HasRow,rows)):
               raise ValueError("Can't add duplicate row index !")

            self._glp_add_rows(newnr)
            for ridx in range(newnr):
                self.SetRowName(ridx+oldnr, rows[ridx])

        else:
           raise TypeError("rows must be int or list")



    def AddCols(self,cols):

        tc = type(cols)
        if tc == int:
            if cols <1:
               raise ValueError("Can't add less then one col !")
            self._glp_add_cols(cols)

        elif tc == list:
            oldnc = self.GetNumCols()
            newnc = len(cols)
            if True in list(map(self.HasCol,cols)):
               raise ValueError("Can't add duplicate col index !")

            self._glp_add_cols(newnc)
            for cidx in range(newnc):
                self.SetColName(cidx+oldnc, cols[cidx])

        else:
           raise TypeError("cols must be int or list")


    def AddIntCols(self, cols):

        self.AddCols(cols)
        for c in cols:
            self.SetColKind(c, "Integer")

    
    def AddBinCols(self, cols):

        self.AddCols(cols)
        for c in cols:
            self.SetColKind(c, "Binary")

        


    def GetRowIdxs(self,rows):
        return list(map(self.__getrow, rows)) #TODO: list comprehension

    def GetColIdxs(self,cols):
        return list(map(self.__getcol, cols))#TODO: list comprehension


    def DelRows(self,rows):
        n = len(rows)
        rows = self.GetRowIdxs(rows)
        idxs = IntArray(rows)
        self._glp_del_rows(n,idxs._array)
        self.__RebuildNameDics()


    def DelCols(self,cols):
        n = len(cols)
        cols = self.GetColIdxs(cols)
        idxs = IntArray(cols)
        self._glp_del_cols(n,idxs._array)
        self.__RebuildNameDics()


    def SetRowName(self, r, name):

        if name in self.ridxs:
           raise KeyError("duplicate row name, "+name)

        gr = self.__getrow(r)
        self._glp_set_row_name(gr,name)
        self.ridxs[name] = r
        self.rnames[r] = name


    def SetColName(self, c, name):

        if name in self.cidxs:
           raise KeyError("duplicate col name,"+name)

        gc = self.__getcol(c)
        self._glp_set_col_name(gc,name)
        self.cidxs[name] = c
        self.cnames[c] = name



    def SetRowBounds(self, r, lo=None, hi=None):

        r = self.__getrow(r)
        flag, lo, hi = self.__getbnds(lo,hi)
        self._glp_set_row_bnds(r, flag, lo, hi)


    def SetColBounds(self, c, lo=None, hi=None):

        c = self.__getcol(c)
        flag, lo, hi = self.__getbnds(lo,hi)
        self._glp_set_col_bnds(c,flag,lo,hi)


    def SetRowVals(self, row, vals):

        row = self.__getrow(row)
        lenv = len(vals)
        if len(vals) != self.GetNumCols():
           raise IndexError("incorrect row length")
        zs = []
        nzs = []
        for i in range(lenv):
            if vals[i] == 0:
                zs.append(i)
            else:
                nzs.append(i)
        zs.reverse()
        if len(zs) !=0:
            vals = vals[:] # copy, because we are going to change the contents
        for i in zs:
            del vals[i]

        nzs = IntArray(AddOne(nzs))
        vals = DoubleArray(vals)
        self._glp_set_mat_row(row, len(vals), nzs, vals)

      
    def SetRowFromDic(self, name, dic):
        row = [0] * self.GetNumCols()
        for var in dic:
            vidx = self.__getcol(var) - 1
            row[vidx] = dic[var]
        self.SetRowVals(name, row)


    def SetColKind(self, col, kind):
        
        cidx = self.__getcol(col)
        if kind not in ColKinds:
            raise KeyError("Unknown kind %s" % kind)
        
        if type (kind) == str:
            kind = ColKinds[kind]
        
        self._glp_set_col_kind(cidx, kind)
        

    def GetColKind(self, col):

        cidx = self.__getcol(col)
        kind = self._glp_get_col_kind(cidx)
        return ColKinds[kind]


    def LoadMtxFromLists(self, RowIdxs, ColIdxs, ElVals):

        nels = len(ElVals)
        nr,nc = self.GetDims()

        if not len(RowIdxs) == len(ColIdxs) == nels:
           raise IndexError("Inconsistant indices and/or values")

        try:
            RowIdxs = list(map(self.__getrow, RowIdxs))
        except:
           raise IndexError("Bad row index")

        try:
            ColIdxs = list(map(self.__getcol, ColIdxs))
        except:
           raise IndexError("Bad col index")

        tupdic = {}
        for i in range(len(RowIdxs)):
            tup = str((RowIdxs[i],ColIdxs[i]))
            if tup in tupdic:
               raise IndexError("Duplicate indices not allowed")
                # don't ask me why not, I didn't write gplk.c !
            else:
                tupdic[tup]=1


        zs = []                     # remove any zero element values, which for reasons unknown
        for i in range(nels):       # will cause precipitate termination in glpk
            if ElVals[i] ==0:
                zs.append(i)
        if len(zs) != 0:
            RowIdxs, ColIdxs, ElVals = RowIdxs[:], ColIdxs[:], ElVals[:] # copy, keep orignals untouched
            zs.reverse()
            for i in zs:
                for l in RowIdxs, ColIdxs, ElVals:
                    del l[i]


        ia = IntArray(RowIdxs)
        ja = IntArray(ColIdxs)
        ar = DoubleArray(ElVals)

        self._glp_load_matrix(
            len(ElVals),
            ia,
            ja,
            ar)


    def LoadMtxFromDic(self,dic, KeysAreRowNames=True):

        if KeysAreRowNames:
            for k in list(dic.keys()):
                self.SetRowVals(k,dic[k])
        else:
            print("D'oh !!")


    def SetObjCoef(self, c, coef):

        if c==-1 or c=="shift":
            self._glp_set_obj_coef(0, coef)

        c = self.__getcol(c)
        self._glp_set_obj_coef(c, coef)


    def SetObjCoefsFromLists(self, cs, coefs):

        lenc = len(cs)
        if lenc != len(coefs):
           raise IndexError("cs and coefs not of same length !")

        cs = list(map(self.__getcol,cs))
        for i in range(lenc):
            self._glp_set_obj_coef(cs[i], coefs[i])


    def SetObjCoefsFromDic(self, d):
        self.SetObjCoefsFromLists(list(d.keys()), list(d.values()))
        

    def ClearObjective(self):
        for c in range(self._glp_get_num_cols()):
            self._glp_set_obj_coef(c+1, 0.0)

    def GetObjCoef(self, c):
        c = self.__getcol(c)
        return(self._glp_get_obj_coef(c))
        
        
        



    #
    ##  problem information
    #

    def GetDims(self):
        return self.GetNumRows(), self.GetNumCols()

    def HasRow(self, r):
        """ pre: True
           post: HasRow(r) => r is a valid row index """
        try:
            self.__getrow(r)
            return True
        except:
            return False


    def HasCol(self, c):
        """pre: True
          post: HasCol(c) => c is a valid col index """
        try:
            self.__getcol(c)
            return True
        except:
            return False


    def GetRow(self, r):
        """ self.HasRow(r) => return row r as a list
            else: exception """

        r = self.__getrow(r)
        lenr = self.GetNumCols()
        inds = IntArray(lenr)
        vals = DoubleArray(lenr)
        self._glp_get_mat_row(r, inds._array, vals._array)
        inds = SubOne(inds.ToList())
        vals = vals.ToList()

        return Lists2Vec(inds, vals, lenr)


    def GetRowsAsLists(self):

        nr = self.GetDims()[0]
        return list(map(self.GetRow, list(range(nr))))




    def GetCol(self, c):

        c = self.__getcol(c)
        lenc = self.GetNumCols()
        inds = IntArray(lenc)
        vals = DoubleArray(lenc)
        self._glp_get_mat_col(c, inds._array, vals._array)
        inds = SubOne(inds.ToList())
        vals = vals.ToList()

        return Lists2Vec(inds, vals, lenc)


    def GetRowName(self, r):
        self.__getrow(r) # sanity check, exception if r invalid

        if r in self.rnames:
            return self.rnames[r]
        return "row_"+str(r)


    def GetRowNames(self):
        return list(map(self.GetRowName, list(range(self.GetDims()[0]))))


    def GetColNames(self, kind = 'All'): # TODO: get rid of tring literals in args
        
        cnames = list(self.cnames.values())
        if kind == 'All':
            return cnames
        else:
            return [c for c in cnames if self.GetColKind(c) == kind]


    def GetSimplexAsMtx(self):

        rnames = self.GetRowNames()
        cnames = self.GetColNames()
        rows   = self.GetRowsAsLists()

        rv = DynMatrix.matrix(rnames=rnames, cnames=cnames, Conv=float)
        rv.rows =rows

        return rv


    

    #
    ## Solve the problem
    #

    def Solve(self):

        self._glp_std_basis()
        #rv = self._glp_simplex(None)
        rv = self._glp_simplex(self.smcp)
        self.smcp.preseolve = glpk_ll.GLP_OFF

        return rv


    def MIPSolve(self):
        
        if self._glp_get_num_int() == 0:
            print ("MIPSolve: Ignoring - no integer columns defined!", file=sys.stderr)
        else:
            return self._glp_intopt(self.iocp)


    #
    ## Status information
    #

    def GetStatus(self, Stype="Generic"):
        """pre: Stype in ["Generic", "Primal", "Dual"]
          post: numerical value of current status """

        return self.__diccheck(self._StatusGetters, Stype)()


    def GetStatusMsg(self,Stype="Generic"):
        """pre: self.GetStatus(Stype)
          post: human string describing current status """
        return Status2Str[self.GetStatus(Stype)]


    def IsStatusOptimal(self):
        return self.GetStatus() == glpk_ll.GLP_OPT


    #
    ## Solution information
    #
    # see also: GetObjVal and GetObjName defined by __init__()

    def GetRowPrimal(self, r):
        r = self.__getrow(r)
        return self._glp_get_row_prim(r)

    def GetRowDual(self, r):
        r = self.__getrow(r)
        return self._glp_get_row_dual(r)

    def GetColPrimal(self, c):
        c = self.__getcol(c)
        return self._glp_get_col_prim(c)

    def GetColDual(self, c):
        c = self.__getcol(c)
        return self._glp_get_col_dual(c)


    def GetPrimSolDic(self):
        
        rv = {}
        for c in self.cidxs:
            rv[c] =  self._glp_get_col_prim(self.cidxs[c]+1)
        return rv
        

    def GetMIPSolDic(self):
        
        rv = {}
        for c in self.cidxs:
            rv[c] =  self._glp_mip_col_val(self.cidxs[c]+1)
        return rv
        


    #
    ##  I/O methods
    #

    def Read(self, src, format='CPLEX'): #TODO - review/re-write
        """    *** NOT IMPLEMENTED ***

        pre: format in ['GnuMathProg', 'FixedMPS', 'CPLEX', 'FreeMPS']
                src is readable filename in format format
          post: self represents problem in src """

        raise NotImplementedError


        self._glp_delete_prob()
        self.lpx = self.__diccheck(self._Readers, format)(src)
        self.__setup()
        self.__RebuildNameDics()

##
##
##    def Write(self, format="Human", dest=None):
##        """ pre: format in ['FixedMPS', 'CPLEX', 'Human', 'FreeMPS', 'Solution']
##                 Dest writable file name OR None
##            post: Dest != None -> problem written in format to Dest
##                  Dest == None -> returns proble as a string in format """
##
##
##        writer = self.__diccheck(self._Writers, format)
##        nr,nc = self.GetDims()
##
##        if (nc < 1 or nr <1) and format != "Human":
##           raise IndexError("must have at least one row and column")
##
##        if dest==None:
##            return F2Str(writer)
##        else:
##            writer(dest)
##            rv = None
##        return rv
##
##


    def WriteProb(self,FName=None):
        """ pre: FName writable file name OR None
            post: FName != None -> problem written in cplex format to FName
                  FName == None -> returns problem as a string in cplex format

            see the glp_write_lp description in the glpk manual for further details """

        if FName is None:
            FName = str(random.randint(0,100000))
            self.WriteProb(FName)
            rv = open(FName).read()
            os.remove(FName)
            return rv

        self._glp_write_lp(None, FName)


    def PrintProb(self):
        print(self.WriteProb())


    def WriteSol(self,FName=None):
        """ pre: FName writable file name OR None
            post: FName != None -> solution info written to FName
                  FName == None -> solution info returned as a string

            see the glp_print_sol description in the glpk manual for further details"""


        if FName is None:
            FName = str(random.randint(0,100000))
            self.WriteSol(FName)
            rv = open(FName).read()
            os.remove(FName)
            return rv

        self._glp_print_sol(FName)


    def PrintSol(self):
        print(self.WriteSol())


    def WriteMIPSol(self, FName=None):

        """ pre: FName writable file name OR None
            post: FName != None -> solution info written to FName
                  FName == None -> solution info returned as a string

            see the glp_print_sol description in the glpk manual for further details
        """

        if FName is None:
            FName = str(random.randint(0,100000))
            self.WriteMIPSol(FName)
            rv = open(FName).read()
            os.remove(FName)
            return rv

        self._glp_print_mip(FName)


    def PrintMIPSol(self):
        print(self.WriteMIPSol())

  


    def PrintMtx(self):
        """ print current constraint matrix on stdout """

        print(self.GetSimplexAsMtx())

   
