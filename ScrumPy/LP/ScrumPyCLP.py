


"""
MIP (cplex) implementation by Hassan Hartman based on ScrumPyLP by Mark Poolman and ScrumPyQP by Thomas Pfau

"""





import sys

print("!! ScrumPyCLP untested in ScrumPy3 !!",  file=sys.stderr)

  
from ScrumPy.Util import Set, DynMatrix,  Kali
from ScrumPy.Structural import StoMat
from ScrumPy.Data import DataSets


NullF =  open("/dev/null", "w")


try:
    import cplex
except:
    print("!! Warning: cplex not available !!",  file=sys.stderr)
    cplex = Kali.Kali()
    ObjectiveMap = dict()
  
   
ObjectiveMap = {
    "Max" : cplex.Cplex.objective.sense.maximize,
    "Min" : cplex.Cplex.objective.sense.minimize,
    cplex.Cplex.objective.sense.minimize : "Min" ,
    cplex.Cplex.objective.sense. maximize: "Max"
    }


#Some definitions of static variables, all inequaleties are inclusive (>=, <=) 
GREATER_THAN = "G"
LESS_THAN = "L"
EQUAL_TO = "E"
RANGED = "R"

constr_tag = 'constr_'



########################### Mappings for human readability
Status2Str = {           # map symbolic status constants to human strings
    1    : "Optimal",
    23   : "Feasible",
    3 : "Infeasible",
    5 : "No feasible",
    2  : "Unbounded!",
    4  : "Unbounded!",
    101 : "MIP Optimal",
    102 : "MIP Optimal (tol)", #within tolerance
    103 : "MIP Infeasible",
    
    }

###############################

VerySmall = 1e-12
def IsZero(number, tol=VerySmall):
    return abs(number) < tol

###############################
def SplitRev(sm,Conv=float): # copy of sm with reversible reacs split into forward and back irreversible pairs

    rv = sm.Copy(Conv)
    rv.Reversed = reversed = []
    rv.Backs = backs = []

    for r in sm.cnames:
        if sm.RevProps[r] == StoMat.t_Rever:
            sto = sm.InvolvedWith(r)
            rback = r+"_back"
            rv.NewReaction(rback,sto)
            rv.MulCol(rback,k=-1)
            backs.append(rback)

        elif sm.RevProps[r] == StoMat.t_BackIrrev:
           rv.MulCol(r,  k=-1)
           reversed.append(r)

    return rv

def _fix_back(sol, threshold=VerySmall):
    for reac in list(sol.keys()):
        if reac.endswith("_back"):
            val = sol[reac]
            if not IsZero(val,threshold):
                r2 = reac[:-5]
                if r2 in sol:
                    sol[r2]-=val
                else:
                    sol[r2] = -val
            del sol[reac]
    return sol
    
def UndoSplit(sm):
    for b in sm.Backs:
        back_row = sm.GetRow(b)
        org_row = sm.GetRow(b.split('_back')[0])
        for i in range(len(back_row)):
            bool = (back_row[i]!=org_row[i])
            sm[b.split('_back')[0],i] = sm.Conv(bool)   
        sm.DelRow(b)

    

class clp(cplex.Cplex):

    def __init__(self, model,infinity = cplex.infinity,  Direc='Min',  sm=None):
    
    #modified to fitt linear
        cplex.Cplex.__init__(self)
        self.SetObjDirec(direc=Direc)
        self.infinity = infinity
        self.updateActive = True
        self.set_problem_name("Linear Programming Problem")
        self.set_problem_type(self.problem_type.LP) 
        self.Klass='CLP'
        
        self.set_results_stream(sys.__stdout__)
        if sm==None:
            self.sm = SplitRev(model.sm)
        else:
            self.sm=sm
            self.sm.Backs=[]
            self.sm.Reversed=[]
        self.model=model
        self.variables.add(names=self.sm.cnames[:],lb=len(self.sm.cnames)*[0],ub = len(self.sm.cnames)*[self.infinity])
        self.variables_names = self.sm.cnames[:] # print self.variables.get_names(). 
        #setup the rows (i.e. constraints)
        self.linear_constraints.add(names=self.sm.rnames[:])
        self.linear_constraints_names = self.sm.rnames[:] 
         
        for row in self.sm.rnames:
  #          print "Setting up Row: " + row
            self.SetRowVals(row, list(map(float, self.sm[row])))
            self.SetRowBounds(row, 0,0)
        self.rnames = {}
        self.ridxs = {}
        self.cnames = {}
        self.cidxs = {}
        self.updateRowDics()
        self.updateColDics()
  

        
    def __del__(self):
        cplex.Cplex.__del__(self)

    def __getCol(self, c):
        if c in self.variables_names:
            return self.variables_names.index(c)
        else:
            raise KeyError(str(c))
        
    def __getRow(self, r):
        if r in self.linear_constraints_names:
            return self.linear_constraints_names.index(r)
        else:
            raise KeyError(str(r))
      
    #compatability functions, these are not really necessary
    def updateColDics(self):
        if not self.updateActive:
            return
        self.cnames = dict(list(zip(list(range(len(self.variables_names))),self.variables_names)))
        self.cidxs =  dict(list(zip(self.variables_names,list(range(len(self.variables_names))))))

    def updateRowDics(self):
        if not self.updateActive:
            return
        """Be aware, that this function is only for linear constraints!!"""
        self.rnames = dict(list(zip(list(range(len(self.linear_constraints_names))),self.linear_constraints_names)))
        self.ridxs = dict(list(zip(self.linear_constraints_names,list(range(len(self.linear_constraints_names))))))
        
    def GetClass(self):
        return self.Klass 
    ##
    ##
    #   Objective Modifications:
    ##
    ##

    ##
    #   General
    ##

    def AddToObjective(self,reacs):
       self.SetLinObjective(reacs)
    
    def SetObjective(self,reacs):
        self.ClearObjective()
        self.SetLinObjective(reacs)


    def ClearObjective(self):
        self.objective.set_linear(list(zip(self.variables_names[:],[0]*len(self.variables_names))))
        
    
    def SetObjCoefsFromDic(self, d):
        
        self.SetLinObjCoefsFromDic(d)  
        
    def SetObjCoefsFromLists(self, cs, coefs):
    
        self.SetLinObjCoefsFromLists(cs, coefs)
        

    def SetObjDirec(self,direc="Min"):
        if not (direc=="Max" or direc=="Min"):
            raise ValueError(direc)
        self.objective.set_sense(ObjectiveMap[direc])


    def SetObjCoef(self,c,coef):
        
        self.SetLinObjCoef(c,coef)

    def SetObjName(self,name):
        self.objective.set_name(name)

    def GetObjName(self):
        return self.objective.get_name()
    ##
    #   Access
    ##
    def GetObjective(self):
        res={}
        res.update(dict(list(zip(self.variables_names,self.objective.get_linear()))))
        for r in list(res.keys()):
            if res[r] == 0:
                del res[r]
        return res
    
    ##
    #   Linear
    ##

    def SetLinObjective(self,reacs):
        if type(reacs)==dict:
            target = {}
            for reac in list(reacs.keys()):
                target[reac] = reacs[reac]
                back = self._BackReac(reac)
                if back != None:
                    target[back] = reacs[reac]
            self.SetObjCoefsFromDic(target)

        else:
            target = reacs[:]
            for reac in target[:]:
                back = self._BackReac(reac)
                if not self.HasCol(reac):
                    raise IndexError("Column " + str(reac) + " not in the Problem")
                if back != None and back not in target:
                    target.append(back)
                else:
                    del back
                
            self.SetObjCoefsFromLists(target[:],[1]*len(target[:]))
            del target

    def SetLinObjCoefsFromLists(self, cs, coefs):
        lenc = len(cs)
        if lenc != len(coefs):
            raise IndexError("cs and coefs not of same length !")                                               
        self.objective.set_linear(list(zip(cs[:], coefs[:])))


    def SetLinObjCoefsFromDic(self, d):
        self.SetObjCoefsFromLists(list(d.keys()), list(d.values()))


    def SetLinObjCoef(self, c, coef):
        if c in self.variables_names: #or c in self.indicator_var_names:
            self.objective.set_linear(c,coef)
        

    
    ##
    ##
    #   Variable Modifications
    ##
    ##
        
    ##
    #   Variable Constraint Modifications
    ##
    
    
    def SetFixedFlux(self, reacs):

        for reac in reacs:
            rate = reacs[reac]
            if reac in self.sm.Reversed:
                rate *= -1
            rback = reac + "_back"
            hasback = rback in self.sm.Backs
            if rate >= 0:
                self.SetColBounds(reac, rate, rate)
                if hasback:
                    self.SetColBounds(rback, 0,0)
            else:
                if not hasback:
                    sys.stderr.write("! Setting inconsitant flux in irreversible reaction %s\n" % reac)
                    self.SetColBounds(reac, rate, rate)
                else:
                    self.SetColBounds(reac, 0, 0)
                    self.SetColBounds(rback, -rate, -rate)
                    
                    
    def SetColBounds(self, c, lo=None, hi=None):
        back = self._BackReac(c)
        if not c in self.variables_names:
            raise ValueError(c + "is not part of this problem")
        if lo != None:
            if lo < 0 :
                if back == None:
                    raise ValueError("trying to set negative flux to irreversable reaction " + c)
                else:
                    self.variables.set_upper_bounds(back,-lo)
                self.variables.set_lower_bounds(c,0)
            else:
                if back != None:
                    self.variables.set_upper_bounds(back,0)
                self.variables.set_lower_bounds(c,lo)
        else: 
            self.variables.set_lower_bounds(c,0)
            if back != None:
                self.variables.set_upper_bounds(back,self.infinity)
                
        if hi != None:
            if hi < 0 :
                if back == None:
                    raise ValueError("trying to set negative flux to irreversable reaction " + c)
                else:
                    self.variables.set_lower_bounds(back,-hi)
                self.variables.set_upper_bounds(c,0)
            else:
                if back != None:
                    self.variables.set_lower_bounds(back,0)
                self.variables.set_upper_bounds(c,hi)
        else: 
            self.variables.set_upper_bounds(c,self.infinity)
            if back != None:
                self.variables.set_lower_bounds(back,0)
                
    

    def UnboundFlux(self, reac):

        print("! UnboundFlux() is deprecated, use ClearFluxConstraint(reac) instead !", file=sys.stderr)
        self.ClearFluxConstraint(reac)
    
    
    def ClearFluxConstraint(self, reac):
        """ pre: reac in self.GetReacNames
           post: flux constraint(s) removed from reac """

        self.SetColBounds(reac, 0, None)

        rback = self._BackReac(reac)
        if rback != None:
            self.SetColBounds(rback, 0, None)


    def ClearFluxConstraints(self, reacs=[]):
        """ pre: reacs ==[] OR reacs is sibset of self.GetReacNames()
           post: reacs ==[] => all reactions constraints cleared
                 ELSE constraints of reacs cleared """

        if reacs == []:
            reacs = self.GetReacNames()
            
        for reac in reacs:
            self.ClearFluxConstraint(reac)
            
            
    def FiniteBoundFlux(self, reac, lo, hi):

        if hi < lo:
            lo,hi = hi,lo

        rback = self._BackReac(reac)
        if rback == None:
            if lo < 0:
                raise ValueError("attempt to set -ve bound on irreversible "+reac)
            self.SetColBounds(reac, lo,hi)
        else:
            if hi < 0:
                self.SetColBounds(reac,0,0)
                self.SetColBounds(rback, -hi, -lo)
            elif lo < 0:
                self.SetColBounds(reac,0,hi)
                self.SetColBounds(rback, 0, -lo)
            else:
                self.SetColBounds(reac,lo,hi)
                self.SetColBounds(rback, 0, 0)


    def SetFluxBounds(self, reacs):
        """ Pre: reacs = {name:(lo,hi)...}, (lo<=hi OR hi==None) """

        for reac in reacs:
            lo, hi = reacs[reac]
            if lo == hi == None:
                self.ClearFluxConstraint(reac)
            elif lo == hi:
                self.SetFixedFlux({reac:lo})
            elif not None in (lo,hi):
                self.FiniteBoundFlux(reac, lo, hi)
            else:                                  # lo == None xor hi == None
                rback = self._BackReac(reac)
                if rback == None:                  # irreversible
                    self.SetColBounds(reac, lo, hi)
                else:                              # reversible reaction
                    if lo == None:                 # lo == None
                        if hi < 0.0:
                            self.SetColBounds(reac, 0,0)
                            self.SetColBounds(rback, -hi, None)
                        else:
                            self.SetColBounds(reac,  0, hi)
                            self.SetColBounds(rback, 0, None)
                    else:
                        if lo < 0.0:                # hi == None
                            self.SetColBounds(reac, 0,None)
                            self.SetColBounds(rback, 0, -lo)
                        else:
                            self.SetColBounds(reac, lo,None)
                            self.SetColBounds(rback, 0, 0)
                            

    
    
    ##
    #   Adding variables
    ##

    def AddCols(self,cols):

        tc = type(cols)
        if tc == int:
            if cols <1:
                raise ValueError("Can't add less then one col !")
            else:
                for i in range(cols):
                    name = "col_"+str(len(self.variables_names))
                    self.AddVar(names=[name])
#                    self.variables_names.append(name)

        elif tc == list:
            if True in list(map(self.HasCol,cols)):
                raise ValueError("Can't add duplicate col index !")
            
            self.AddVar(names=cols)
#            self.variables_names.extend(cols)

        else:
            raise TypeError("cols must be int or list")

    def AddVar(self, obj=[], lb=[], ub=[], types='', names=[], columns=[]):
        if len(names) < 1:
            print("Need variable names")
            return
        else:
            for r in names:
                if r in self.variables_names:
                    raise ValueError("Duplicate variable name! " + r)
                else:
                    self.variables_names.append(r)
                 
            self.variables.add(obj, lb, ub, types, names, columns)
            self.updateColDics()  # update self.cnames
            
        
    ##
    #   Removing Variables
    ##
    
    def DelCols(self,cols):
        self.variables.delete(cols)
        for r in cols:
            del self.variables_names[self.variables_names.index(r)]
        self.updateColDics()  # update self.cnames
            
    ##
    #   Variable Modifications
    ##
    
    def SetColName(self,c,name):
        if self.HasCol(c):
            self.variables.set_names([(c,name)])
            self.variables_names[self.variables_names.index(c)] = name
            self.updateColDics()  # update self.cnames
        else:
            raise IndexError("Bad column index, " + str(c) + " not a variable name")

    ##
    ##
    #   Linear Constraint Modifications
    ##
    ##

    ##
    #   Adding Constraints
    ##
#    def SetSumFluxConstraint(self,  reacs, total,  name):
#
#        self.AddRows([name])
#        reacs = reacs[:]
#        for r in reacs[:]:
#            rback = self._BackReac(r)
#            if rback != None:
#                reacs.append(rback)
#        vals = [1]*len(reacs)
#        self.linear_constraints.set_linear_components(name,[reacs,vals])
#        self.SetRowBounds(name,  total, total)
#
#        print "SetSumFluxCostraint testing !"
#        
    

    def AddRows(self,rownames):
        if True in list(map(self.HasRow,rownames)):
            raise ValueError("Can't add duplicate row index !")
        self._add_lin_constraint(names=rownames)
#        self.linear_constraints.add(names=rownames[:])
#        self.linear_constraints_names.extend(rownames[:])
#        self.rnames[len(self.rnames)] = row   # add constraint in self.rnames


            

    def _add_row_constraint(self, sto, lowBound  = 0, upBound = 0,  name = None):
        """ sto is a list of tuples representing the constraint with each entry being: (var,coef)  e.g. [("Reaction1", 37)]  ([["Reaction1"], [37]] )
                      where var is the variable referenced and coef is the coefficient for the variable in this constraint.
            lowBound is the lower bound of the constraint
            upBound is the upper bound of the constraint
            name is constraint name or 'Row'+ numbe of current rows +1
        """
        name = name or 'Row ' + str(self.GetNumRows() + 1)
        if name in self.linear_constraints_names:
                raise ValueError("Duplicate constraint name! " + name)
        else:
                flag,range,rhs = self.__getbnds(lowBound,upBound)
                #self.linear_constraints_names.append(r)
                self._add_lin_constraint(lin_expr = [sto], senses=[flag],  rhs = [rhs], range_values = [range], names=[name])
                
        

    def _add_lin_constraint(self, lin_expr=[], senses='', rhs=[], range_values=[], names=[]):
        if len(names) < 1:
            print("Need constraint names")
            return
        else:
            for r in names:
                if r in self.linear_constraints_names:
                    raise ValueError("Duplicate constraint name! " + r)
                else:
                    self.linear_constraints_names.append(r)
            self.linear_constraints.add(lin_expr, senses, rhs, range_values, names)
        self.updateRowDics() #update rnames

    def AddSumConstraint(self, lowBound = 1.0, upBound = 1.0, cnames = [],  lin_expr=None,  constr_name=None):
    
        cnames = cnames or self.GetAllVarNames()
        if lin_expr==None:
            lin_expr=[cnames, [1]*len(cnames)]
        if constr_name==None:
            constr_name = constr_tag+str(self.linear_constraints.get_num())
        self._add_row_constraint(lin_expr, lowBound, upBound, constr_name) 
       
    def CheckReac(self, reac):
        
        if not reac in self.model.sm.cnames:
            print("! %s doesn't exist ! " %reac, file=sys.stderr) 
            return False
            
        return True
        
    def CleanReacDic(self,  dic):
        " remove keys in dic that are not reactions in the model, print warning if any removed"
    
        NotFound = Set.Complement(list(dic.keys()),  self.model.sm.cnames)
        if len(NotFound) != 0:
            print("! ignoring non-existent reaction(s)",  ", ".join(NotFound), file=sys.stderr)
            for r in NotFound:
                del dic[r]
                
                
    def CleanReacList(self, l):
        " remove items in l that are not reactions in the model, print warning if any removed"
        NotFound = Set.Complement(l,  self.model.sm.cnames)
        if len(NotFound) != 0:
            print("! ignoring non-existant reacation(s)",  ", ".join(NotFound), file=sys.stderr)
            for r in NotFound:
                l.remove(r)
    ##
    #   Removing Constraints
    ##

    def DelRows(self,rows):
#        self.linear_constraints.delete(rows)
        for r in rows:
            self.DelRow(r)
#            if type(1) == type(r):
#                del self.linear_constraints_names[r]
#            if r in self.linear_constraints_names:
#                del self.linear_constraints_names[self.linear_constraints_names.index(r)]
#        updateRowDics() # update rnames

    def DelRow(self,id):
        self.linear_constraints.delete(id)
        if type(1) == type(id):
            del self.linear_constraints_names[id]
        if id in self.linear_constraints_names:
            del self.linear_constraints_names[self.linear_constraints_names.index(id)]
        self.updateRowDics()
                
    ##
    #   Modifing Constraints
    ##
    def SetRowVals(self,rowname,values):
        """pre: values is a list containing a number of values equal to the number of total variables in this QP."""
        if not rowname in self.linear_constraints_names:
            raise IndexError("Bad col index " + str(rowname) + " not a valid identifier")

        vals = []
        idx = []
        for i in range(len(values)):
            if values[i] != 0:
                vals.append(values[i])
                idx.append(i)
        self.linear_constraints.set_linear_components(rowname,[idx,vals])

    def SetRowBounds(self,rowname,lo=None,hi=None):
       #get upper and lower bounds
        flag,range,rhs = self.__getbnds(lo,hi)
        #if a constraint is to be freed it will be removed.
        if flag == "FREE":
            if rowname in self.linear_constraints_names:
                self.linear_constraints.delete(rowname)
                del self.linear_constraints_names[self.linear_constraints_names.index(rowname)]
        else:
            self.linear_constraints.set_rhs(rowname,rhs)
            self.linear_constraints.set_range_values(rowname,range)
            self.linear_constraints.set_senses(rowname,flag)

    def LoadMtxFromDic(self,dic, KeysAreRowNames=True):
        
        if KeysAreRowNames:
            
            for k in list(dic.keys()):
                self.SetRowVals(k,dic[k])

    def LoadMtxFromLists(self, RowIdxs ,ColIdcs ,ElVals):

        #check if all rows and columns exist
        if not len(RowIdxs) == len(ColIdcs) == len(ElVals):
             raise IndexError("Inconsistant indices and/or values")

        for r in RowIdxs:
            if not r in self.linear_constraints_names:
                raise IndexError("Bad row index, " + str(r) + " not a constraint name")
        #remove all linear constraints
        self.linear_constraints.delete()
        self.linear_constraints_names = []
        
        for c in ColIdcs:
            if not c in self.variables_names:
                 raise IndexError("Bad column index, " + str(c) + " not a variable name")
        #set up the new constraints
        for r in RowIdxs:
            if not self.HasRow(r):
                self.linear_constraints.add(names=[r])
                self.linear_conatraints_names.append(r)
        
        for i in range(len(RowIdxs)):
            self.linear_constraints.set_linear_components(RowIdxs[i],[[ColIdcs[i]],[ElVals[i]]])

    def SetRowName(self,r,name):
        if self.HasRow(r):
            self.linear_constraints.set_names([(r,name)])
            self.linear_constraints_names[r] = name
        else:
            raise IndexError("Bad row index, " + str(r) + " not a constraint name")
        self.updateRowDics()
   


 
    
  
 

    ##
    ##
    #   Solving and Solution access
    ##
    ##
    
    def Solve(self,PrintStatus=True):
        self.solve()
        if PrintStatus:
            print(self.GetStatusMsg())    

    def GetStatusMsg(self):
        if self.solution.get_status() in Status2Str:    
            return Status2Str[self.solution.get_status()]
        else:
            return "CPLEX Solution Status Code: " + str(self.solution.get_status())
 
 #modeified to fitt linear
    def GetPrimSol(self, IncZeroes=False,FixBack=True,AsMtx=False,threshold=VerySmall,  variables=None):
        sol={}
        if variables==None:
            variables = self.GetColNames()
        if len(variables)==0:
            return sol
        sol = dict(list(zip(variables,self.solution.get_values(variables))))
        cols = list(sol.keys())  
        for r in cols:
            if IsZero(sol[r],threshold) and not IncZeroes:
                del sol[r]
        for rev in Set.Intersect(self.sm.Reversed,  list(sol.keys())):
            sol[rev] *= -1
        if FixBack:
            sol=_fix_back(sol)
        if  AsMtx:
            rv = StoMat.StoMat(cnames=["lp_sol"],rnames=list(sol.keys()),Conv=float)
            for r in sol:
                rv[r][0] = sol[r]
        else:
            rv = sol
        return rv



#modeified to fitt linear
    def GetRowDual(self,r):
        """get the Dual of constraint r """
        if self.GetStatusMsg() =='Optimal':
            return self.solution.get_dual_values(r)
        else:
            print("There is no optimal Solution")
            return None
    
#modeified to fitt linear
    def GetColDual(self,c):
        """get the Dual of variable r IMPORTANT: This will not return the Flux value but the variable value (i.e. irrespectible of the reverse flux """
        if self.GetStatusMsg() =='Optimal':
            return self.solution.get_reduced_costs(c)
        else:
            print("There is no optimal Solution")
            return None
    
#modeified to fitt linear
    def GetColPrimal(self, c):
        """get the Primal of variable r IMPORTANT: This will not return the Flux value but the variable value (i.e. irrespectible of the reverse flux """
        if self.GetStatusMsg() =='Optimal':
            return self.solution.get_values(c)
        else:
            print("There is no optimal Solution")
            return None
    
#modeified to fitt linear
    def GetRowPrimal(self, r):
        """get the Primal of constraint r """
        if self.GetStatusMsg() =='Optimal':
            return self.solution.get_linear_slacks(r)
        else:
            print("There is no optimal Solution")
            return None
    

    def GetObjDir(self):
        """get the Directionality of the objective (either Min or Max)"""
        return ObjectiveMap[self.objective.get_sense()]
#modeified to fitt linear

    def GetObjVal(self):
        """get the Objective Value if there is a valid solution """
        val = 0.0
        if self.GetStatusMsg() =='Optimal':
            val = self.solution.get_objective_value()
        return val

#modeified to fitt linear

#not needed
#    def GetFluxValue(self,flux):
#        if self.GetStatusMsg() =='Optimal':
#            val = self.solution.get_values(flux)
#            back = _BackReac(flux)
#            if back != None:
#                val -= self.solution.get_values(back)
#            return val
#        else:
#            print "There is no optimal Solution"
#            return None

#modeified to fitt linear
    def IsStatusOptimal(self):
        return self.GetStatusMsg() =='Optimal'

    
    ##
    ##
    #   General Access and Information Methods
    ##
    ##

    ##
    #   Matrix Information Methods
    ##
    def GetSimplexAsMtx(self):

        rnames = self.GetRowNames()
        cnames = self.GetColNames()
        rows   = self.GetRowsAsLists()

        rv = DynMatrix.matrix(rnames=rnames, cnames=cnames, Conv=float)
        rv.rows =rows

        return rv
    def GetRowsAsLists(self):
        #create an empty matrix
        matrix = []
        for row in self.linear_constraints_names:
            matrix.append(self.GetRow(row)[:])
        return matrix

    ##
    #   Variable/Constraint info retrieving
    ##
    
  #modeified to fitt linear  
  #redundant
#    def GetNames(self):
#        return self.GetColNames()

#modeified to fitt linear
    def GetColNames(self):
        return self.variables.get_names()
        
#redundant, same as above
#    def GetAllVarNames(self):
#        return self.variables.get_names()

    def GetColIdx(self,c):
        return self.variables_names.index(c)
    
    def GetColIdxs(self,cols):
        return list(map(self.variables_names.index,cols)) # incompatible with ScrumPyLP
        
    

    def GetCol(self,c):
        col = []
        cidx = self.GetColIdx(c)
        for r in self.linear_constraints.get_rows():
            if cidx in r.ind:
                col.append(r.val[r.ind.index(cidx)])
            else:
                col.append(0)
                
        return col
    
    def GetRowNames(self):
        return self.linear_constraints_names

    def GetRowIdx(self,r):
        return self.linear_constraints_names.index(r)
    
    def GetRowIdxs(self,rows):
        return list(map(self.linear_constraints_names.index,rows)) # incompatible with ScrumPyLP
 
    def GetRow(self,row):
        comps = self.linear_constraints.get_rows()[self.linear_constraints.get_indices(row)]
        rv = [0.0]*len(self.variables_names)
        for j in range(len(comps.ind)):
            rv[comps.ind[j]] = comps.val[j]
            
        return rv
    
    ##
    #   General property retrieving
    ##
    
    
    def GetNumCols(self):
        return len(self.variables_names)

    def GetNumRows(self):
        return len(self.linear_constraints_names)

    def HasRow(self,rowname):
        if rowname in self.linear_constraints_names:
            return True
        else:
            return False

    def HasCol(self, c):
        """pre: True
          post: HasCol(c) => c is a valid col index """
        if c in self.variables_names:            
            return True
        else:
            return False

    def GetColName(self, c):
        #must be smaller then the number of variables, larger than zero and a integer)
        if c < self.variables.get_num() and c >= 0 and type(c) == type(1):
            return self.variables_names[c]

    def GetRowName(self, r):
        #must be smaller then the number of variables, larger than zero and a integer)
        if r < self.linear_constraints.get_num() and r >= 0 and type(r) == int:
            return self.linear_constraints_names[r]
        
    def GetDims(self):
        return (self.linear_constraints.get_num(), self.variables.get_num())

    def SetName(self,name):
        self.set_problem_name(name)

    def GetName(self):
        return self.get_problem_name()
                
    def PrintMtx(self):
        """ print current constraint matrix on stdout """

        nr,nc = self.GetDims()
        print(" "*10, end=' ')
        for c in range(nc):
            print(self.GetColName(c).ljust(6), end=' ')
        print()
        for r in range(nr):
            print(self.GetRowName(r).ljust(10), " ".join([("%3.3g"%x).ljust(6) for x in self.GetRow(r)]))
            

    def GetReacNames(self):
        """pre: TRUE
          post: list of reaction names used to generate this LP"""

        return [s for s in self.sm.cnames if not s.endswith("_back")]
    
    
    ##
    ##
    #   Helper Functions
    ##
    ##
    def __getbnds(self,lo,hi):
        if   lo == None and hi == None:
            flag, range, rhs = "FREE", 0.0, 0.0
        elif lo != None and hi == None:
            flag, range, rhs = GREATER_THAN, 0.0, lo
        elif lo == None and hi != None:
            flag, range, rhs = LESS_THAN, 0.0, hi
        elif lo != None and hi != None:
            if lo < hi:
                flag, range, rhs = RANGED, -(hi-lo) , hi
            elif lo==hi:
                flag, range, rhs = EQUAL_TO, 0.0, hi
            else:
                raise ValueError("lo > hi !")

        return flag, range, rhs

    
    def _BackReac(self, reac):

        rback = reac + "_back"
        if rback in self.sm.Backs:
            return rback
        return None 
   
    def FixedFluxScan(self, reaction, lo, hi, n_p):

        rv = DataSets.DataSet(ItemNames=[reaction,"ObjVal"])
        lo = float(lo)
        hi = float(hi)
        inc = (hi - lo)/(n_p-1)
        cur = lo

        for n in range(n_p):
            self.SetFixedFlux({reaction:cur})
            self.Solve(False)

            sol = self.GetPrimSol()

            if len(sol)==0:
                obval = float("NaN") # indicate fail
            else:
                obval =  self.GetObjVal()

            sol["ObjVal"] = obval
            sol[reaction] = cur
            rv.UpdateFromDic(sol)

            cur += inc

        return rv

    

 
    

    
    
    
