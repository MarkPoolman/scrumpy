


"""
MIP (cplex) implementation by Hassan Hartman based on ScrumPyLP by Mark Poolman and ScrumPyQP by Thomas Pfau

"""



import sys

print("!! ScrumPyCMIP untested in ScrumPy3 !!",  file=sys.stderr)


NullF =  open("/dev/null", "w")


#Some definitions of static variables, all inequaleties are inclusive (>=, <=) 
GREATER_THAN = "G"
LESS_THAN = "L"
EQUAL_TO = "E"
RANGED = "R"

#internal definitions for problem types
lin = 'linear'
#quad = 'quadratic'
indi = 'indicator'
bina = 'binary'  #not currenly in use

indivar_tag = 'indi_var_'
indiconstr_tag = 'indi_constr_'
constr_tag = 'constr_'

from . import ScrumPyCLP
cplex = ScrumPyCLP.cplex


from ScrumPy.Structural import StoMat
from ScrumPy.Data import DataSets

########################### Mappings for human readability
Status2Str = ScrumPyCLP.Status2Str
ObjectiveMap = ScrumPyCLP.ObjectiveMap

###############################

VerySmall =  ScrumPyCLP.VerySmall
IsZero = ScrumPyCLP.IsZero
SplitRev=ScrumPyCLP.SplitRev
_fix_back=ScrumPyCLP._fix_back
UndoSplit=ScrumPyCLP.UndoSplit


def RemoveIsoRevs(sm):
    isos = sm.FindIsoforms()
    if len (isos)>0:
        for iso in isos:
            if len(iso) == 2:
                sm.DelCol(iso[1])
            elif len(iso)>2:
                sys.stderr.write("len"+str(iso)+"> 2! \n")
        for i in range(len(sm.cnames[:])):
            cname = "ElMo_" + str(i)
            sm.cnames[i] = cname


class cmip(ScrumPyCLP.clp):

    def __init__(self, model,infinity = cplex.infinity, Direc='Min',  sm=None):
    
        ScrumPyCLP.clp.__init__(self, model,infinity, Direc,  sm)
        
        self.set_problem_name("Mixed Integer Linear Problem")
        self.set_problem_type(self.problem_type.MILP) 
        self.Klass='CMIP'
        self.indicator_constraints_names = [] 
        
    
    #   Indicator constraint addition
    
    def AddIndiConstraint(self, lin_expr=cplex.SparsePair(ind = [], val = []), sense=EQUAL_TO, rhs=0.0, indvar=None, complemented=0, name=None ):
       
        if name==None:
            name = indiconstr_tag+str(self.indicator_constraints.get_num())
            if name not in self.indicator_constraints_names:
                self.indicator_constraints_names.append(name)
        if  indvar==None:
            indvar = indivar_tag+str(len(self.variables_names))
        if indvar not in self.variables_names:
            self.variables_names.append(indvar)
            self.variables.add(names=[indvar], types=['B'])
        self.indicator_constraints.add(lin_expr=lin_expr, sense=sense, rhs=rhs, indvar=indvar, complemented=complemented, name=name)

    
    def SetInt2ContConstr(self,vars=[], coeff=1,  tol=1):
        '''
            pre     :       vars - continuous variables to constraint to indicator variables, default all cont. varisables.
                              coeff - coefficient of vars in constraint, default 1.
                              tol - cutoff cont. flux below which a reaction is concidered to be off, defualt 1.
            post    :      For each vars (r_i) indicator constraint added: z_i=0 -> r_i =0.0 and z_i =1 -> r_i >= tol
        '''
        vars = vars or self.GetColNames()
        for var in vars:
            self.AddIndiConstraint(lin_expr=[[var], [coeff]], sense=GREATER_THAN, rhs=tol, indvar=indivar_tag+var,complemented=0) 
            self.AddIndiConstraint(lin_expr=[[var], [coeff]], sense=EQUAL_TO, rhs=0, indvar=indivar_tag+var,complemented=1)
        
    def AddRevConstr(self, revs=[]):
        ''' pre: self.SetInt2ContConstr() has been executed
            post: z_i_forward + z_i_rev <= 1, for indicator variables for reversible reaction i.
        '''
#FIXME: revs is somehow becoming persistent - the default is becoming set to that of a previous invocation
# even for new instances
        if len(revs)==0:
            for r in self.GetColNames():
                if r.endswith('_back'):
                    revs.append(r[:-5])
        for r in revs:
            name= constr_tag+str(self.linear_constraints.get_num())
            self.linear_constraints_names.append(name)
            flag, range, rhs=self._clp__getbnds(0.0, 1.0)
            self.linear_constraints.add(lin_expr=[[[indivar_tag+r, indivar_tag+r+'_back'], [1,1]]],senses=[flag], rhs=[rhs],range_values=[range], names=[name])
    
    def AddNonZeroConstr(self,  vars=[]):
        '''
                pre      :      vars - list of indicator variables to constrain to a nonzero sum, defult empty implying all indicator variables.     
                post    :       Linear constraint sum(vars) >=1 added.
        
        '''
        vars = vars or self.GetIndiVarNames()
        name= constr_tag+str(self.linear_constraints.get_num())
        self.linear_constraints_names.append(name)
        flag, range, rhs=self._clp__getbnds(1.0, None)
        self.linear_constraints.add(lin_expr=[[vars, [1]*len(vars)]],senses=[flag], rhs=[rhs],range_values=[range], names=[name])
        
    
    #   Indicator constraint deletion
    
    def DelIndiRow(self,r):
        if r in self.indicator_constraints_names:
            self.indicator_constraints.delete(r)
            del self.indicator_constraints_names[self.indicator_constraints_names.index(r)]
            
    def DelIndiVar(self, var):
        if var in self.variables_names:
            self.variables.delete(var)
        else:
            print(var+' not in variables list!')
    
    ##
    ##
    #   Solving and Solution access
    ##
    ##
    
 
    def GetPrimSol(self, IncZeroes=False,FixBack=True,AsMtx=False,threshold=VerySmall, kind=lin):
        variables = self.GetColNames(kind)
        rv = ScrumPyCLP.clp.GetPrimSol(self, IncZeroes,FixBack,AsMtx,threshold, variables)
        return rv
    

    
    def GetRowDual(self,r):
        """get the Dual of constraint r """
        if self.GetStatusMsg() in ['Optimal', 'MIP Optimal']:
            return self.solution.get_dual_values(r)
        else:
            print("There is no optimal Solution")
            return None
    

    def GetColDual(self,c):
        """get the Dual of variable r IMPORTANT: This will not return the Flux value but the variable value (i.e. irrespectible of the reverse flux """
        if self.GetStatusMsg() in ['Optimal', 'MIP Optimal']:
            return self.solution.get_reduced_costs(c)
        else:
            print("There is no optimal Solution")
            return None
    

    def GetColPrimal(self, c):
        """get the Primal of variable r IMPORTANT: This will not return the Flux value but the variable value (i.e. irrespectible of the reverse flux """
        if self.GetStatusMsg() in ['Optimal', 'MIP Optimal']:
            return self.solution.get_values(c)
        else:
            print("There is no optimal Solution")
            return None
    

    def GetRowPrimal(self, r):
        """get the Primal of constraint r """
        if self.GetStatusMsg() in ['Optimal', 'MIP Optimal']:
            return self.solution.get_linear_slacks(r)
        else:
            print("There is no optimal Solution")
            return None
    

    def GetObjVal(self):
        """get the Objective Value if there is a valid solution """
        val = 0.0
        if self.GetStatusMsg() in ['Optimal', 'MIP Optimal']:
            val = self.solution.get_objective_value()
        return val


    def GetFluxValue(self,flux):
            print("GetFluxValue not implemented")
    
    def IsStatusOptimal(self):
        return self.GetStatusMsg() in ['Optimal', 'MIP Optimal']
       
    
    ##
    ##
    #   General Access and Information Methods
    ##
    ##


    ##
    #   Variable/Constraint info retrieving
    ##
    def GetColNames(self, var_type=lin):
        if var_type==lin:
            return self.GetLinColNames()
        elif var_type==indi:
            return self.GetIndiVarNames()
        elif var_type == 'all':
            return self.GetAllVarNames()
    
    def GetLinColNames(self):
        return [i for i in self.variables_names[:] if not i.startswith(indivar_tag)]
        
    def GetIndiVarNames(self):
        return [i for i in self.variables_names[:] if i.startswith(indivar_tag)]
        
    def GetAllVarNames(self):
        return self.variables.get_names()


############ cplex-specific methods #######
    
    
    
    def AddLenConstr(self, size, indinames = [],  lin_expr=None,  constr_name=None):
        '''
            pre     :     self.SetInt2ContConstr() has been applied.
                            size - number of active reactions in feasible solution
                            indinames - indicator variables to include in problem, defaults to all indivar
                            lin_exp - stoichiometry of constraint, all vars given coefficient 1 by default
                            constr_name - name of constraint, defaults to 'constr_' +(number of constraints + 1)
            
            post    :   Number of active reactions in feasible solution equal to size.
        '''
        
        indinames = indinames or self.GetIndiVarNames()
        self.AddSumConstraint(size, size, indinames, lin_expr, constr_name)


    def SetLenObjective(self, varlist = []):  
        '''
            pre     :       varlist - continuous variables to include in objective, defaults to all continuous variables.
            post   :        objective set to optimise (min or max) number or variables in solution, as defined by varlist
        '''
        
        obj = {}
        lin_vars = varlist or self.GetColNames() #
        self.SetInt2ContConstr()
        for var in lin_vars:
            indi_name = indivar_tag  + var
            obj[indi_name] = 1.0
        self.SetObjective(obj)
    
 
    def SetIntegerCut(self, indi_names):
        """pre: self.GetObjDirec() == 'Min'; self.SetInt2ContConstr() has been applied.
        post: any solution involving all variables in intnames is excluded."""
        cut = len(indi_names) - 1
        self.AddSumConstraint(lowBound = None, upBound = cut, cnames = indi_names)


    def ExcludeSolution(self, vars):
        '''
        pre      :       self.SetInt2ContConstr() has been applied.
                           vars -  list of continuous variables to exclude.
        post    :       variables in vars excluded from feasible solution
        '''
        indinames = [indivar_tag + var for var in vars]
        self.SetIntegerCut(indinames)
        
        
    def FindAltSols(self, same_objval = True, tol=VerySmall):
        '''
            pre     :     self.SetInt2ContConstr() has been applied. 
                            self.IsStatusOptimal()==True
                            same_objval - collect only alternative solution with the same objective value as current value, default true
                            tol  - if difference in objval between two solutions > tol, they have different objval.
                            
            post    :   returns a dataset of all alternative solutions to the initial solution.
        '''
        rv = DataSets.DataSet(ItemNames=self.GetColNames())
        if same_objval:
            ref_obj = self.GetObjVal()
            go_func = lambda self: (self.GetObjVal() - ref_obj) <= tol
        else:
            go_func = lambda self: True
        while go_func(self) and self.IsStatusOptimal():
            this_sol = self.GetPrimSol()
            excl_sol = self.GetPrimSol(FixBack=False)
            rv.UpdateFromDic(this_sol)
            self.ExcludeSolution(excl_sol)
            self.Solve()
        
        return rv
        
    def PopulateSolutionPool(self, abs_gap=0.0,  intesity=4):
        '''
            pre     :       True, self is MILP/MIQP
                               abs_gap - internal cplex parameter [0.0, 1e+75], which sets an absolute tolerance on the objective 
                                                value for the solutions in the solution pool. Default 0.0 to get all solutions.
                               intesity - internal cplex parameter [0, 4], which set sthe balance between number of solutions obtained 
                                              and the computational resources allocated. Default (full enumeration) is 4.
            post    :       Solution pool (self.solution.pool) is polulated
        '''
        self.parameters.mip.pool.apsgap=abs_gap
        self.parameters.mip.pool.intesity=intesity
        self.populate_solution_pool()
    
    
    def GetSolPool(self, pres_type='dict', sol_type=lin, FixBack=True):
        '''
            pre          :      self.solution.pool is populated with self.PopulateSolutionPool().
                                  pres_type - 'dict'||'ds', default 'dict'
                                  sol_type -  lin||indi, default lin
                                  FixBack - if False {rxn_i_back:X}, else {rxn_i:-X}, default True.
            post        :       solution pool returned in accordence with pres_type, default as nested dictionary.
        '''
        if pres_type=='ds':
            return self.GetSolPoolAsDS(sol_type, FixBack)
        elif pres_type=='dict':
            return self.GetSolPoolAsDict(sol_type, FixBack)
            
    
    def GetSolPoolAsDS(self, sol_type=lin, FixBack=True):
        '''
            pre          :     self.solution.pool is populated with self.PopulateSolutionPool()
                                  pres_type - 'dict'||'ds', default 'dict'
                                  sol_type -  lin||indi, default lin
                                  FixBack - if False {rxn_i_back:X}, else {rxn_i:-X}, default True.
            post        :       solution pool returned as a data set
        '''
        vars = self.GetColNames(sol_type)
        ds= DataSets.DataSet(ItemNames=vars)
        for sol in self.solution.pool.get_names():
            sol_dict=dict(list(zip(vars, self.solution.pool.get_values(sol, vars))))
            if FixBack:
                sol_dict=_fix_back(sol_dict)
            ds.UpdateFromDic(sol_dict)
        return ds
        
    def GetSolPoolAsDict(self,  sol_type=lin, FixBack=True, tol=VerySmall):
        '''
            pre          :      self.solution.pool is polulated with self.PopulateSolutionPool()
                                  pres_type - 'dict'|'ds', default 'dict'
                                  sol_type -  lin|indi, default lin
                                  FixBack - if False {rxn_i_back:X}, else {rxn_i:-X}, default True.  
            post        :      solution pool returned as a nested dictionary
        '''        
        sols = {}
        vars = self.GetColNames(sol_type)
        for sol in self.solution.pool.get_names():
            sol_dict=dict(list(zip(vars, self.solution.pool.get_values(sol, vars))))
            for i in list(sol_dict.keys()):
                if IsZero(sol_dict[i],tol) :
                    del sol_dict[i]
            if FixBack:
                sol_dict=_fix_back(sol_dict)
            sols[sol]=sol_dict
        return sols
    
        
    def GetElMoBySize(self, max_mode=None,  var_type=lin):
        '''
            pre     :       True. Only linear constraints added.
                              max_size - largest possible mode, default all reactions in the model.
                              var_type - lin || indi 
            post    :     Elementary modes up to max_size returned as a dataset.
        '''
        self.SetLenObjective()
        vars = self.GetColNames(var_type)
        rv = DataSets.DataSet(ItemNames=vars)
        if max_mode==None:
            max_mode = len(vars)

        self.SetInt2ContConstr()
        self.AddRevConstr(self.model.sm.GetRevs())
        self.AddNonZeroConstr()
        self.Solve(False)
        ref_sol = self.GetPrimSol()
        size = len(ref_sol)
        #rv.UpdateFromDic(ref_sol)
        self.ClearObjective()
        self.Solve(False)
        while size <= max_mode and self.IsStatusOptimal():
            self.AddLenConstr(size, constr_name='size_constr')
            self.PopulateSolutionPool()
            pool = self.GetSolPool(sol_type=var_type, FixBack=False)
            for sol in pool:
                rv.UpdateFromDic(pool[sol])
                if var_type == lin:
                    self.ExcludeSolution(list(pool[sol].keys()))
                else:
                    self.SetIntegerCut(list(pool[sol].keys()))
            self.DelRow('size_constr')
            size+=1
            self.Solve(False)
        return rv
    
        
    def PostProcess(self, ds):
        '''
                pre     :       ds - rv of self.GetElMoBySize()
                post   :        StoMat object where cnames are elementary modes and rows are reaction from self has been returned.
        '''
        #TODO it would more logical to implement this in the ElModes.XTableau class
        dicdic=ds.ToDicDic()
        for sol in dicdic:
            dicdic[sol]=_fix_back(dicdic[sol])
        ds.FromDicDic(dicdic) 
        
        for i in range(len(ds.cnames)):
            ds.cnames[i]=ds.cnames[i].split(indivar_tag)[-1]
        rv = StoMat.StoMat(c=len(ds.cnames), cnames=ds.cnames)
        rv.RevProps = {}
        for r in range(len(ds.rnames)):
            em_name='ElMo_'+str(r)
            rv.NewRow(r=ds.GetRow(ds.rnames[r]), name=em_name)
            rv.RevProps[em_name] = StoMat.t_Irrev
        dicdic=rv.ToDicDic()
        for sol in dicdic:
            dicdic[sol]=_fix_back(dicdic[sol])
        rv.FromDicDic(dicdic)
        rv.Transpose()
        stodict={}
        for c in rv.cnames:
            key=str(list(rv.InvolvedWith(c).keys()))
            inv=list(rv.InvolvedWith(c).keys())
            if key in stodict:
                stodict[key]['modes'].append(c)
            else:
                stodict[key]={'modes':[c], 'reacs':inv}
        for key in stodict:
            if len(stodict[key]['modes'])==2:
                all_rev=all([self.model.sm.RevProps[r]==StoMat.t_Rever for r in stodict[key]['reacs']])
                if all_rev:
                    rv.RevProps[stodict[key]['modes'][0]]=StoMat.t_Rever
            for mode in stodict[key]['modes'][1:]: #note this deletes all isos including those with len(iso)>2, which are assumed to be non-elementary and caused by cplex populate
                rv.DelReac(mode)
        
        return rv
    
   
    
    
    
