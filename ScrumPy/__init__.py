
"""
NOTE:
 apt-get install libxml2-dev libz-dev libbz2-dev python-pip
 pip install python-libsbml to get libsbml 
 
 If not otherwise in repositories.

see http://sbml.org/Software/libSBML/docs/python-api/libsbml-downloading.html#dl-python
"""


import os, sys,  atexit


from .ModelDescription.ModelDescription import ModelDescription as ModelDesc
from .ModelDescription import Parser
from .Kinetic.Model import Model as  KineticModel
from .Structural.Model import Model as StructuralModel


from .Util import Procs
atexit.register(Procs.KillChildren)
#NB interactive UIs will need to call Procs.KillChildren when the main loop terminates
# the above is for batch mode running

from .UI.BaseUI import BaseUI as DefaultUIClass
from .UI import BasicInfo

def About():
    print(BasicInfo.Greeting)


    


ExtraPaths=["/usr/local/ScrumPy"]
home =os.getenv("HOME")
if home is not None:
    UserMods =  home +"/.ScrumPy"
    if not os.path.exists(UserMods):
        os.mkdir(UserMods)
    ExtraPaths.insert(0, UserMods)
    Parser.SetTabPath(UserMods) # tell ply.yacc where to save intermediate parsetab files
else:
    print ("!! Warning: No user home directory found, expect trouble !!", file = sys.stderr)
        

for p in ExtraPaths:
    if not p in sys.path:
        sys.path.append(p)
    

def SetUI(UIClass):

    global DefaultUIClass
    DefaultUIClass = UIClass
        

 
def Model(FileName=None, uiclass=None):

    m = None

    if uiclass is None:
        uiclass = DefaultUIClass

    ui = uiclass()

    if FileName is None:
        FileName = ui.ChooseFile()

    if type(FileName) is str:

        md = ModelDesc(FileName)
        if md.IsKinetic():
            m = KineticModel(md,ui)
        else:
            m = StructuralModel(md,ui)
            
        ui.BindModel(m) # Better if the model itself did this? no/maybe
    else:
        ui.WarnMsg("Must supply a valid  file name!")

    return m

    
