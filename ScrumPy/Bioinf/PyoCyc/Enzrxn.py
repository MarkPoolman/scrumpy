
from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):
    
    ParentFields = {Tags.Enz,Tags.Cats}
    ChildFields = {Tags.Reac}
    RecordTag = Tags.EnzReac
    
  

class DB(DBBase.DB):

    FileName =  "enzrxns.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag
