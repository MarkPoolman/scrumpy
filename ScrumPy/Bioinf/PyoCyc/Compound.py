from . import RecordBase, DBBase, Tags, OrderedList



class Record(RecordBase.Record):
    
    RecordTag = Tags.Compound
    ParentFields = {Tags.Reac}
    
    def __init__(self, UID, OrgDB=None):
        RecordBase.Record.__init__(self, UID, OrgDB)
        self.EmpForm = {}
        self.Atoms = []
        self.Parents = [] # 

    def __setitem__(self,key,val):
        if key == Tags.ChemForm:
            self.__NewAtoms__(val)
        RecordBase.Record.__setitem__(self, key, val)


    def __NewAtoms__(self, val):
        AtomValue = val[1:-1].split()
        Atom =  AtomValue[0]
        if len(AtomValue) > 1:
            self.EmpForm[Atom] = int(AtomValue[1])
        else:
            self.EmpForm[Atom] = 1
        self.Atoms.append(Atom)


    def NumAtoms(self,atom):
        try:
            return  self.EmpForm[atom]
        except:
            return 0


    def NetCharge(self):
        rv = 0
        if Tags.AtomCharges in self:
            rv =  sum([ eval(c.replace(" ",","))[1] for c in self[Tags.AtomCharges] ])
        return rv

 
    def GetReactions(self):
        """get a list of reactions this metabolite is involved in"""
        return self.GetParents()

    def Copy(self):
        rv = RecordBase.Record(self)
        rv.EmpForm = dict(self.EmpForm)

    def update(self, other):
        RecordBase.Record.update(self,other)
        self.EmpForm = dict(other.EmpForm)



class DB(DBBase.DB):

    FileName = "compounds.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag
        
    def  __init__(self,*args, **kwargs):
        """  see DBBase.DB for args """
        
        DBBase.DB.__init__(self, *args, **kwargs)
        self.InitMWs()


    def InitMWs(self):

        self.MassList = []
        self.MassDic = {}

        for compound in self.values():
            if Tags.MolWt in compound:
                try:
                    mw = float(compound[Tags.MolWt][0])
                except:
                    print("! Bad MW, ", compound[Tags.MolWt], " for ", str(compound))
                    mw = -1
                OrderedList.Insert(self.MassList, mw)
                if mw in self.MassDic:
                    self.MassDic[mw].append(compound)
                else:
                    self.MassDic[mw] = [compound]

    def MWSearch(self, targ,lo,hi):

        print(targ,lo,hi)
        res =  self.InMassRange(targ+lo, targ+hi)
        print(res)
        return res
        
    def NearestByMass(self, m):
        idx = OrderedList.FindNearest(self.MassList, m)
        return self.MassDic[self.MassList[idx]]

    def InMassRange(self,lo,hi):
        if lo > hi: lo,hi = hi,lo
        lidx = OrderedList.FindNearest(self.MassList,lo)
        hidx = OrderedList.FindNearest(self.MassList,hi)

        if self.MassList[hidx] > hi: hidx -= 1
        if self.MassList[lidx] < lo: lidx += 1

        rv = []
        span = self.MassList[lidx:hidx+1]
        for i in span:
            rv += self.MassDic[i]
        return rv
        
        
    def AtomImbal(self, StoDic):
        """ NET atomic stochiometry described by the dictionary StoDic.
            Will try to handle bad names gracefully.
            rv is a dictionary unbalanced atoms to coeffs.
        """

        #print ("AtomImbal not tested in python 3 vrsion")
            
        rv = {}
  
        for s in StoDic:
                        
            coeff = StoDic[s]
            if not s in self:
                s = s.split("_")[-1] # remove any compartmental prefixes 
                # TODO: now that "_" can appear in native biocyc uids this can still fail
                # need to return to this
            if s not in self:
                empform = {"Unknown compound " +s:coeff} # TODO remove string literals
            else:
                empform = self[s].EmpForm
                if empform == {}:
                    empform = {"Unknown composition "+s:coeff}

            if isinstance(coeff, str):
                rv[s] = "Non numeric stoichiometry"
            else:
                for atom in empform:                    
                    natoms = coeff * empform[atom]
                    if atom in rv:
                        rv[atom] += natoms
                    else:
                        rv[atom] = natoms

      
        for k in list(rv.keys()):
            if rv[k] == 0:
                del rv[k]

        return rv
                
                
        
        

        



        


   
        
        
            
#
