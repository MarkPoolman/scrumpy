##Organism.py
##defines the class Organism to integrate the various BioCyc databases

import types, os

from ScrumPy.Util import Set

#from . import Base
from . import RecordBase,Compound, Reaction, Enzrxn, Protein, Gene,Regulation, Transunit, Tags, Info


from . import Types, Promoter, Regulon, Transporter, Pathway


Modules = [ # modules defining relevant dbs
  Compound,
 Reaction,
 Enzrxn,
 Protein,
Gene,
Pathway,
Types,
 Regulation,
 Promoter,
 Transunit,
 Transporter,
    # no longer present in recent PGDBs ?
## Regulon
# TODO: Regulon UIDs are a subset of PROTEIN UIDs so the regulon db needs seperate consideration
  Info
]
DefaultPath = "/usr/local/share/bio/db/biocyc/"


class Organism(dict):
    def __init__(self, data="MetaCyc",Path=DefaultPath):
        
        self.data=data
        self.path=Path
        self.dbs = {}
       
        for Mod in Modules:

            db = Mod.DB(Path=Path+data,OrgDB=self)
            dbname = db.RecordTag#.replace("-", "_") # ensure dbname is a valid python attribute id
            #setattr(self, dbname, db)
            self.dbs[dbname]=db # convenient to access individual dbs in a dic - eg. WhereIs()
            
            self.update(db)
            print (dbname, len(db))

        self.update(self.dbs[Tags.Reac].ECDic)


    def __getitem__(self,k):

        if k in self:
            return dict.__getitem__(self,k)
        
        return RecordBase.NullRecord(k)


    def WhereIs(self,k):

        for db in self.dbs:
            if k in self.dbs[db]:
                return db
            
        return "Nowhere!"

    def GetPaths(self, key):
        pass

    def GetReacs(self, key):
        pass

    def GetAASeq(self,UID):
        return self.dbs["PROTEIN"].GetAASeq(UID)

    def GetGenes(self, targ):
        if targ in self:
            return self[targ].GetGenes()
        return []
            



    def GetGenesAndOr(self,targ):
        '''
        pre     :       targ in self

        post    :      returns list of gene-UIDs related to targ such that between each item
                        in genes there is an OR relationship, between each element of any nested lists there is an AND
                        relationship.
        '''
        if type(targ) == bytes:
            targ = self[targ]
        genes=[]
        if targ.RecordClass!='Gene' and 'COMPONENTS' not in targ:
            non_gene=[]
            for item in targ.GetParents():
                non_gene.extend(self.GetGenesAndOr(item.UID))
            genes.extend(non_gene) 
        elif 'COMPONENTS' in targ:        
            complex=[]
            for comp in targ['COMPONENTS']:
                complex.extend(self.GetGenesAndOr(comp))
            genes.append(complex)  
        elif targ.RecordClass=='Gene':
            genes.append(targ.UID)
        return genes


    def AtomImbal(self, model, reaction, IsCompartmented=False):

        stod = model.smx.InvolvedWith(reaction)

        ##  FIXME: need to to properly
        ##  deal with transport processes
        ##
        
        #if IsCompartmented:
        #    stod = {k.rsplit("_",1)[0]:stod[k] for k in stod}
            # remove compartmental suffixes

            #TODO: no, self.dbs["Compound"] should do this

    
        return self.dbs["Compound"].AtomImbal(stod)


    def AtomImbalDic(self, m, IsCompartmented=False):
        
        rv = {}
        for reac in m.sm.cnames:
            imb = self.AtomImbal(m, reac, IsCompartmented)
            if imb != {}:
                rv[reac] = imb
                
        return rv
    

    def UnknownCompounds(self, m, IsCompartmented=False):
        
        if IsCompartmented:
            mets = [met.rsplit("_",1)[0] for met in m.smx.rnames]
        else:
            mets = m.smx.rnames
            
        return Set.Complement(mets, self.dbs["Compound"])


#
def Paer():
    return Organism("Paeru_PAO1")
