from . import RecordBase, DBBase, Tags


#TODO: transporters.dat no longer present in recent PGDBS ??


class Record(RecordBase.Record):
    
    ChildFields = set() #{Tags.ReacList}
    ParentFields = set() #{Tags.SuPath}
    RecordTag = Tags.Transporter



class DB(DBBase.DB):

    FileName="transporters.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag



#
