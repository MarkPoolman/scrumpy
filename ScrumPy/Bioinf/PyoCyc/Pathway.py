from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):
    
    ChildFields = {Tags.ReacList}
    ParentFields = {Tags.SuPath}
    RecordTag = Tags.Pathway

    def GetReactions(self):
        """ (recursiveley) identify all reaction uids in a (super) pathway """

        rv = set()
        for r in self[Tags.ReacList]:
            if r[:4] == "PWY-":
                rv.update(self.OrgDB[r].GetReactions())
            else:
                rv.add(r)
                
        return rv


    def ToScrumPy(self, FName):
        """ pre: FName is a writable file
           post: reactions in self written to FName as a valid ScrumPy file
        """

        dst = open(FName, "w")
        print("# ", self.OrgDB.data, "\n# ", self.UID, "\n\nStructural()\n", file=dst)

        for reac in self.GetReactions():
            print(self.OrgDB[reac].AsScrumPy(), "\n", file=dst)

        


class DB(DBBase.DB):

    FileName="pathways.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag



#
