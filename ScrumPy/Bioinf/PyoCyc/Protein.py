
import os, sys
from . import RecordBase, DBBase, Tags

#### VVVV dev - move to more global location later - VVVV
try:
    import Bio
    from Bio import SeqIO
    Bio.SeqIO = SeqIO
except:
    Bio = None
    print("Bio python not available - install it separately", file = sys.stderr)

ProtseqFile = "protseq.fsa"
FastaFmt = "fasta"
#### ^^^ dev - move to more global location later - ^^^^


class Record(RecordBase.Record):
    
    ChildFields={Tags.Cats,Tags.CompOf}
    ParentFields={Tags.Gene, Tags.CompOf, Tags.Comps}
    RecordTag=Tags.Prot
    #
    # TODO: This will need rethinking if we need to consider non-enzyme proteins
    #

    def GetMolWt(self):
        """ mol wt (in kD) or None if not present """
        # prots can have multiple estimates of mw or None, return the average if available

        rv = None
        mw = 0
        n = 0
        
        for key in [k for k in self if Tags.MolWt in k]:
            for val in self[key]:
                mw += float(val)
                n += 1

        if n > 0:
            rv = mw/n

        return rv

    def GetAASeq(self):
        return self.OrgDB.GetAASeq(self.UID)
            
    
    
       
    
class DB(DBBase.DB):
    
    FileName="proteins.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag

    def __init__(self,*args, **kwargs):
        
        DBBase.DB.__init__(self, *args,**kwargs)
        self.LoadProtSeq()
        self.MakeUniProtMap()



    def LoadProtSeq(self):
        """ dev version"""

        if Bio is None:
            self.SeqDic = {}
        else:
            pdf         = self.OrgDB.path, self.OrgDB.data,  ProtseqFile
            File        = os.sep.join(pdf)
            seqs        = Bio.SeqIO.parse(File, FastaFmt)
            keyfunc     = lambda seq: seq.id.split("|")[-1]
            self.SeqDic = Bio.SeqIO.to_dict(seqs, key_function=keyfunc)


    def GetAASeq(self, UID):
        """ dev version """

        rv = ""
        if UID in self.SeqDic:
            rv  =  str(self.SeqDic[UID].seq)
        return rv


    
    def MakeUniProtMap(self):

        UPMap = {}
        for rec in self.values():
            if Tags.DBLinks in rec:
                for link in rec[Tags.DBLinks]:
                    link = link[1:-1].split() #[1:-1] remove brackets
                    if link[0]  == Tags.UniProt:
                        up = link[1][1:-1] # [1:-1] remove quotes
                        if up in UPMap:
                            UPMap[up].append(rec)
                        else:
                            UPMap[up]  = [rec]
        self.update(UPMap)
                    
                
                
