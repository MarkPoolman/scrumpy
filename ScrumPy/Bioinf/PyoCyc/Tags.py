Delim =          " - "
RecEnd =        "//"


AtomCharges = "ATOM-CHARGES"
Accn_1 = "ACCESSION-1"
Accn_2 = "ACCESSION-2"
Synons = "SYNONYMS"

Cats     = "CATALYZES"
ChemForm = "CHEMICAL-FORMULA"

Comment = "COMMENT"
ComName = "COMMON-NAME"
CompOf  = "COMPONENT-OF"
Comps   = "COMPONENTS"

Compartment = "^COMPARTMENT"

DBLinks = "DBLINKS"
UniProt = "UNIPROT"

Coeff =  "^COEFFICIENT"
EC    =  "EC-NUMBER"
InPath =          "IN-PATHWAY"                                # the pathway(s) in which a reaction is found
Left =              "LEFT"                                             # Left (substrate) of a reaction
LeftEnd =         "LEFT-END-POSITION"                   # leftmost base in a gene
MolWt =           "MOLECULAR-WEIGHT"
Prod=              "PRODUCT"
Reac =             "REACTION"
ReacEq =         "REACTION-EQUATION"
ReacList =       "REACTION-LIST"                              # reactions in a pathway
ReacDir =        "REACTION-DIRECTION"


Enz =              "ENZYME"
EnzReac =       "ENZYMATIC-REACTION"
Gene =            "GENE"




RegBy=          "REGULATED-BY"
Regul =         "REGULATOR"
Regulates =    "REGULATES"
RegEnt =       "REGULATED-ENTITY"

RegMode =     "MODE"
Right =           "RIGHT"                                             # Right (product) of reaction
RightEnd =     "RIGHT-END-POSITION"                     # rightmost base in a gene
Smiles = "SMILES"
SuPath =         "SUPER-PATHWAY"                            # defined but not used in ecocyc ? future expansion ?
TransDir=       "TRANSCRIPTION-DIRECTION"
Types  =     "TYPES"
UID =              "UNIQUE-ID"
dbComment = "#"
Continue = "/"

IrrR2L = 'IRREVERSIBLE-RIGHT-TO-LEFT'
PhyR2L = 'PHYSIOL-RIGHT-TO-LEFT'     
L2R    = 'LEFT-TO-RIGHT'             
Rev    = 'REVERSIBLE'                 
R2L    = 'RIGHT-TO-LEFT'              
IrrL2R = 'IRREVERSIBLE-LEFT-TO-RIGHT' 
PhyL2R = 'PHYSIOL-LEFT-TO-RIGHT' 
Prot   = "PROTEIN"
Import = "#IMPORT"    # not part of ecocyc - use to identify files imported from 3rd party input


                       ## tagged comments, generated when converting non-biocyc - biocyc
TCReac=         "COMMENT <Reaction>"

NR = "Not reported"   # not strictly a Tag, default value for missing essential (parent or child) fields
Null = "Not found"       # not strictly a Tag, return value  for primary search for non-existent  key.
Compound = "Compound" # not a PGDB tag as compounds get reported as LEFT, RIGHT etc, but useful to identify record types
TransUnit = "TransUnit"  # 
Pathway = "Pathway"
Promotor = "Promoter"
Transporter = "Transporter"
Info = "Info" # meta information - strain, creation date of pgdb etc.
Version = "Version"

