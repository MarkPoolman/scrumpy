
from . import RecordBase, DBBase, Tags, OrderedList

class Record(RecordBase.Record):
    
    ChildFields={Tags.Prod}
    ParentFields={Tags.CompOf}
    RecordTag = Tags.Gene

    def SetMidPos(self):
    
        try:
            self.MidPos = (int(self[Tags.LeftEnd][0])+int(self[Tags.RightEnd][0]))//2
        except KeyError:
            #print (self.UID, ": missing positional information")
            self.MidPos = 0
        

class DB(DBBase.DB):

    FileName="genes.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag

    def __init__(self,*args, **kwargs):
        DBBase.DB.__init__(self, *args,**kwargs)

        ## create the gene position infomation
        self.PosList = []     # ordered list of mid positions
        self.PosDic = {}      # map positions -> UIDs
        self.GenList = []     # list of UIDs orderd by position on chromosome
        self.NoPosList = []   # list of UIDs of genes with no positional information

        self.MakePosInfo()
        self.MakeAuxKeys()


    def MakePosInfo(self):
         
        for gene  in self.values():
            gene.SetMidPos()
            mp = gene.MidPos
            uid = gene.UID
            if mp ==0:
                self.NoPosList.append(uid)
            else:
                self.PosDic[mp] = uid   # we know that mps are unique, so don't check for multiples
                OrderedList.Insert(self.PosList, mp)

        for mp in self.PosList:
            self.GenList.append(self.PosDic[mp])


    def MakeAuxKeys(self):
        for gene in list(self.values()):  # list because len(self) will change
            for tag in Tags.ComName, Tags.Accn_1, Tags.Accn_2, Tags.Synons:
                if tag in gene:
                    for key in gene[tag]:
                        if key != gene.UID:
                            if key in self:
                                try:
                                    self[key].append(gene)
                                except:
                                    print("UID", gene.UID, "and aux key", key, "clash!")
                                     # if a common name in  one gene record is the uid of another
                                    # seems only to be an issue with metacyc, not individual pgdbs
                            else:
                                self[key] = [gene]
                    

    def BPSearch(self, targ=0, lo=0, hi=0, targ_t="Mid"):
        """ search by base pair pos of midpoint """
        geneidx = OrderedList.FindNearest(self.PosList,targ)
        gene = self.PosDic[self.PosList[geneidx]]
        uids =  self.Neighbours_b(gene, lo,hi)
        rv = []
        for uid in uids:
            rv.append(self[uid])
        return rv


    def GPSearch(self, targ=0, lo=0, hi=0,**ignore):
        
        lo = int(targ - lo)
        if lo < 0:
            lo = 0
            
        hi = int(targ+hi)
        if hi >= len(self):
            hi = len(self -1)

        rv = []
        for gene in self.GenList[lo:hi+1]:
            rv.append(self[gene])
        return rv
            


    def Neighbours_g(self, uid, lo, hi):
        "uids of neighbouring lo-hi genes, determined by gene order"

        def minmax(x, lolim,hilim):
            if x > hilim: return hilim
            if x < lolim: return lolim
            return x

        if lo >hi: lo,hi=hi,lo
        maxidx = len(self.PosList)-1
        idx = self.GenList.index(uid)
        top = minmax(idx+hi,0,maxidx)
        bot = minmax(idx+lo,0,maxidx)
      
        return self.GenList[bot:top+1]


    def Neighbours_b(self, uid, lo, hi):
        "uids of neighbouring lo-hi genes, determined by base pair"
        if lo >hi: lo,hi = hi,lo
        mp = self[uid].MidPos
        loidx = OrderedList.FindNearest(self.PosList,mp+lo)
        hiidx = OrderedList.FindNearest(self.PosList,mp+hi)
        rv = []
        
        for gene in self.PosList[loidx:hiidx+1]:
            rv.append(self.PosDic[gene])
        return rv





#
