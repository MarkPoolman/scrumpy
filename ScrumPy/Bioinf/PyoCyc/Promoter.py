from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):
    
    ChildFields = set()
    ParentFields = {Tags.RegBy, Tags.CompOf}
    RecordTag = Tags.Promotor



class DB(DBBase.DB):

    FileName="promoters.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag

