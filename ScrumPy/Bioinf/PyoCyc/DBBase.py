
import os, sys, traceback
from . import RecordBase, Tags

DefaultPath = "/usr/local/share/bio/db/biocyc/"


class DB(dict):

    Path = DefaultPath

    FileName = None # name and class must be over ridden by sub class
    RecordClass=RecordBase.Record
    RecordTag = RecordClass.RecordTag
    
    def  __init__(self, Path=None, FileName=None, OrgDB=None, ReadFile=True):
        
        self.OrgDB = OrgDB

        if  Path is not None:
            self.Path=Path

        if FileName is not None:  
            self.FileName=FileName
        if  ReadFile and self.FileName is not None:
            self.FileName = os.sep.join((self.Path,self.FileName))
            self.ReadFile(self.FileName)

    def __repr__(self):
        return " ".join((
            str(self.__class__),
            "(",
            str(len(self)),
            "records)"
            ))


    def ReadFile(self, FName):

        #decouple error handling from the actual logic of reading the file
        if not os.access(FName, os.R_OK):
            print ("Unable to open", FName, self.RecordTag, "DB will be empty")
            
        else:
            f = open(FName, errors="ignore")
            try:
                self._ReadFile(f)
            except Exception as ex:
                tb = sys.exc_info()[2]
                print (ex, " reading", FName, len(self), "records read, traceback follows")
                traceback.print_tb(tb)
                
            f.close()
                        

    def _ReadFile(self, f):

        for line in f:

            line = line.strip()

                 
            if line.startswith(Tags.RecEnd) or line.startswith(Tags.dbComment):
                pass  #because  Tags.RecEnd  is a superset of Tags.Continue, we must check this first
            
            elif line.startswith(Tags.UID):
                uid = line.split(Tags.Delim)[1]
                self[uid] = cur = self.RecordClass(uid,OrgDB=self.OrgDB)
                
            elif line.startswith(Tags.Continue):
                 cur[tag].append(line)
           
            else:
                try:
                    tag, value = line.split(Tags.Delim,1)                
                    value = value
                    cur[tag] = value
                except ValueError:
                    print (uid,"missing value: ", line)

    def AllRecordKeys(self):
        """ AllRecordKeys() -> set of keys used in all records inself """

        rv = {}
        for rec in self.values():
            rv |= rec.keys()
        return rv

    def Copy(self,**kwargs):

        rv = self.__class__(ReadFile=False,**kwargs)
        rv.update(self)

        return rv

        
                        
                    
                    
            

            
    
