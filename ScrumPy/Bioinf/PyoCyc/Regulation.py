
from . import DBBase, RecordBase, Tags

# Every record will a regulator (ie the agent that acts to regulate something)
# A regulated entity acted upon by the regulator
# And a mode indicating + or - regulation


class Record(RecordBase.Record):

    ChildFields={Tags.RegEnt}
    ParentFields={Tags.Regul}
    RecordTag = Tags.Regul



class DB(DBBase.DB):

    FileName="regulation.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag
