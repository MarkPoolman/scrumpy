
from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):

    ChildFields={Tags.Comps}
    ParentFields={Tags.CompOf, Tags.RegBy}
    # TODO: is Tags.CompOf really a parent for this class?
    RecordTag = Tags.TransUnit


class DB(DBBase.DB):

    FileName="transunits.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag
    
