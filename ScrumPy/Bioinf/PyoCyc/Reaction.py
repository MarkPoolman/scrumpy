import sys
from . import RecordBase, DBBase, Tags
from . ECEquivs import ECEquivs
from ...Util import Seq


#from ModelDescription.lexer import  t_Irrev,  t_BackIrrev,  t_Rever
IrrR2LSpy = "<-" # avoiding circular dependency at the expense
RevSpy    = "<>" # of redundant string literals
IrrL2RSpy = "->"

Quote = '"'
Space   = " "
Indent  = Space*4
Comment = "##"+Indent
Colon = ":"
MissingMsg = " ! Reaction has missing reactants or non-numeric stoichiometry !"


DirecMap = {
    Tags.IrrR2L : IrrR2LSpy,
    Tags.PhyR2L : IrrR2LSpy,
    Tags.L2R    : RevSpy,
    Tags.Rev    : RevSpy,
    Tags.R2L    : RevSpy,
    Tags.IrrL2R : IrrL2RSpy,
    Tags.PhyL2R : IrrL2RSpy
}
DefaultDirec = DirecMap[Tags.Rev]

def Enquote(s):
    if s[0] != Quote:
        return s.join((Quote,Quote))
    return s


def Stod2Spy(Name, Stod, Direc=DefaultDirec, Kinetic="~"):

    Name = Enquote(Name)+Colon
    Kinetic = Indent+Kinetic

    Stod = dict(Stod)

    try:
        Left = list(filter(lambda met: Stod[met] <0, Stod))
        Right = list(filter(lambda met: Stod[met] >0, Stod))
    except TypeError:
        Left = Right  = [] # one or more non-numeric coeffs
        Stod = {}

    for met in Stod:
        Stod[met] = str(abs(Stod[met]))
        if Stod[met] == "1":
            Stod[met] = ""
        
    lhs = " + ".join(map(lambda met: Space.join((Stod[met], Enquote(met))), Left)) + Space
    rhs = " + ".join(map(lambda met: Space.join((Stod[met], Enquote(met))), Right))
    Sto = Indent + Direc.join((lhs,rhs))

    if len(Left)==0 or len(Right)==0:

        Name = Space.join((Comment, Name, MissingMsg))
        Sto = Comment + Sto
        Kinetic = Comment + Kinetic
    
    return "\n".join((Name, Sto, Kinetic))
              
    
                       

    
    

class Record(RecordBase.Record):
    
    ParentFields = {Tags.EnzReac}
    ChildFields = {Tags.Left, Tags.Right} # , Tags.InPath}
    RecordTag=Tags.Reac
    NeedCoeff = False

    def __init__(self, *args, **kwargs):
        self.StoDict = {}
        RecordBase.Record.__init__(self, *args,  **kwargs)
        
  

    def __str__(self):

        rvl = []
        mets = self[Tags.Left] + self[Tags.Right]
        
        for k in  self.OrderedKeys:
            if not k in (Tags.Left, Tags.Right, Tags.Coeff):
                rvl.append(self.Field2Str(k))
                
            # ensure that ^COEFFICIENT are printed in the correct order
            # Coeff field folllows a left or right field if, and only if, abs(coeff) !=1
            elif k != Tags.Coeff:  # ie k is a reactant
                for met in self[k]:
                    rvl.append(Tags.Delim.join((k,met)))
                    coeff = self["^COEFFICIENT"][mets.index(met)]
                    if isinstance(coeff, int):
                        coeff = str(abs(coeff))
                    if coeff != "1":
                        rvl.append(Tags.Delim.join((Tags.Coeff,coeff)))
            #try: 
            #    self.StoDict[self.LastMet] *= int(v)
            #except ValueError:
            #

        return "\n".join(rvl)
    

    def __setitem__(self, k,v):
        RecordBase.Record.__setitem__(self, k, v)
     
        if (k != Tags.Coeff) and self.NeedCoeff:
            RecordBase.Record.__setitem__(self, Tags.Coeff, "1")
            self.NeedCoeff = False
            
        elif k == Tags.Coeff:
            self.NeedCoeff = False
            
        if k in (Tags.Left, Tags.Right):
            self.NeedCoeff = True
        


            
            


    def IsTransporter(self):
        return Tags.Compartment in self and not Seq.AllSame(self[Tags.Compartment])


    def GetStod(self):

        mets = self[Tags.Left] + self[Tags.Right]
        if len(self[Tags.Coeff]) != len(mets):
            self[Tags.Coeff] = "1" 
            # if the last line of the record was a metabolite, coeff won't be set

        coeffs = self[Tags.Coeff][:]
        for cidx in range(len(coeffs)):
            try:
                coeffs[cidx] = int(coeffs[cidx])
            except: pass

        for cidx in range(len(self[Tags.Left])):
            try:
                 coeffs[cidx] = -coeffs[cidx]
            except: pass
           
        
        if Tags.Compartment in self:
            comps = self[Tags.Compartment][:]
        else:
            comps = []
        diff = len(mets) - len(comps)
        if diff >0:
            comps.extend(["CCO-IN"]*diff)
            # ensure number of compartments = no of mets

        stod = dict(self.StoDict)
        for met, coeff, comp in zip(mets, coeffs, comps):
            if comp == "CCO-OUT":
                met = "x_" + met
            stod[met] = coeff
                
        return stod

     
    def AsScrumPy(self,Pfx="",CheckTX=True):


        if Tags.ReacDir in self:
            Direc = DirecMap[self[Tags.ReacDir][0]]
        else:
            Direc = DefaultDirec

        return Stod2Spy(Pfx+self.UID, self.GetStod(), Direc)

     

    def ImBal(self):
        return self.OrgDB.Compound.AtomImbal(self.StoDict)

        
    def GetPathwayIDs(self):
        if Tags.InPath in self:
            return self[Tags.InPath]
        return []

    def GetReactants(self):
        rv = []
        if Tags.Left in self:
            rv += self[Tags.Left]
        if Tags.Right in self:
            rv += self[Tags.Right]
        return rv
        
    




CoeffWarnMsg = "Warning: (ScrumPy) Number of reactants(%d) != number of coeffs(%d) in original record - Coeffs adjusted to match"

class DB(DBBase.DB):

    FileName = "reactions.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag


    def __init__(self, *args, **kwargs):
        
        DBBase.DB.__init__(self, *args, **kwargs)

        self.ECDic = {}
        self.MultiECs = []
        self.BadRecords = []
        
        self.InitECDic()
        self.ConnectChildren()
        self.CheckCoeffs()


    def InitECDic(self):
        
        for reaction in self.values():
            if Tags.EC in reaction:
                ecs = reaction[Tags.EC]
                if len(ecs) > 1:
                    self.MultiECs.append(reaction)
                ec = ecs[0]
                if ec in self.ECDic:
                    self.ECDic[ec].append(reaction)
                else:
                    self.ECDic[ec] = [reaction]
        print (len(self.MultiECs), "reactions have multiple ECNumbers")
                
                
    def ConnectChildren(self):
        # because compounds (children) don't define their parents (reactions)
        # themselves, we need to do it here
        # print("ODB", type(self.OrgDB),file=sys.stderr)
        # return

        if self.OrgDB is not None: # make sure we are part of a an Organism

            db =  self.OrgDB.dbs[Tags.Compound]
            for reaction in self.values():
                for reactant in reaction.GetReactants():
                    if reactant in db:
                        db[reactant].RegisterParent(reaction)


##r = db['PYRUVDEH-RXN']
##>>> for k in r:
##	print(k, r[k])

    def CheckCoeffs(self):

        nBad = 0
        tags = set((Tags.Coeff, Tags.Left, Tags.Right))

        for reac in self.values():
            if tags.issubset(reac):
                coeffs = reac[Tags.Coeff]
                ncoeffs = len(coeffs)
                mets = reac[Tags.Right] + reac[Tags.Left]
                nmets = len(mets)

                if nmets != ncoeffs:
                    nBad += 1
                    msg = CoeffWarnMsg%(nmets, ncoeffs)
                    reac[Tags.Comment] = msg
                    
                    diff = nmets - ncoeffs
                    if diff > 0:
                        coeffs.extend(["1"] * diff) 
                    else:
                        reac[Tags.Coeff] = coeffs[:-diff]
            else:
                print(reac.UID, "Bad reaction record!")
                self.BadRecords.append(reac.UID)
                if not Tags.Left in reac:
                    reac[Tags.Left] = "!!NoSubstrate!!"
                    reac[Tags.Coeff] = 1
                if not Tags.Right in reac:
                    reac[Tags.Right] = "!!NoProduct!!"
                    reac[Tags.Coeff] = 1
                    
                
                

        print ("%d reactions have missmatched coeffs"%nBad)

                
                                            
                
            


        

    def __getitem__(self,k):

        if k in self:
            return DBBase.DB.__getitem__(self,k)

        if k in self.ECDic: # Better to do at Org, rather than DB, level??
            return self.ECDic[k]

        raise  KeyError
    
