from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):
    
    ChildFields = set()
    #TODO: Type records only record the parents, traversing down to find
    # children in the Types or other dbs will require some thought
    ParentFields = {Tags.Types}
    RecordTag = Tags.Types

    def __init__(self, *args, **kwargs):
        RecordBase.Record.__init__(self, *args, **kwargs)
        self.Children = set()

    def AddChildren(self, Children):

        self.Children |= Children
        



class DB(DBBase.DB):

    FileName="types.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag



#
