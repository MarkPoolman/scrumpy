import os
from . import  Tags

class Record(dict):  # base class for BioCyc Records

    ChildFields = set()
    ParentFields = set()
    
    RecordTag = Tags.Null
    
    
    def __init__(self, ID, OrgDB=None):
        # id is the biocyc unique id
        self.UID = ID
        self.OrderedKeys = []
        self[Tags.UID] = ID
        if OrgDB is None:
            OrgDB=dict()
        self.OrgDB = OrgDB
        #self.Parents=[]
       


    def __setitem__(self, k,v):

        if k in self:
            self[k].append(v)
        else:
            dict.__setitem__(self,k, [v])
            self.OrderedKeys.append(k)
        self.LastVal = v


    def __str__(self):
        rv = ""
        return "\n".join(
            list(
                map(lambda k: self.Field2Str(k), self.OrderedKeys)
            )
        )
    
    def __repr__(self):
        return self.UID

    def __eq__(self,other):
        return isinstance(other, Record) and (other.UID == self.UID)

    def __lt__(self, other):
        if isinstance(other, Record):
            return self.UID < other.UID
        raise TypeError()
        
    def __gt__(self, other):
        if isinstance(other, Record):
            return self.UID > other.UID
        raise TypeError()
    
    def __geq__(self, other): # doesn't bind to ">=" why not?
        return not self < other

    def __leq__(self, other):# doesn't bind to "<=" why not?
        return self.UID > other

    def Field2Str(self, k):
        return "\n".join(
            list(
                map(lambda val: k + " - " + val, self[k])
            )
        )


    def Copy(self, OrgDB=None):
        """ copy constructor
            don't link back to the parent db by default
        """

        rv = self.__class__(self.UID, OrgDB)
        rv.OrderedKeys = self.OrderedKeys[:]
        dict.update(rv, self) # because __setitem__ is overloaded

        return rv


    def update(self, other):

        for k in other.keys() - Tags.UID:
            dict.__setitem__(self,k, other[k])

        for k in other.OrderedKeys:
            if not k in self.OrderedKeys:
                self.OrderedKeys.append(k)
            
        



    def write(self, f):
        """ pre: f = (FileName, "w")
           post: contents of self written to FileName """

        f.write(str(self)+Tags.RecEnd+"\n")

    def IsNull(self):
        return self.UID == Tags.Null


    def ValStr2AssocKeys(self, ValStr):
        """ convert string val into a set of keys for the association dict.
            Subclasses can overload this to fine tune the keys that get into
            the assoc dic."""

        # Drop this, reimplement later if we really need it.


    def GetTypes(self):
        """Get all Types this entry belongs to. This includes supertypes of direct Types"""
        rv = []
        if Tags.Types in self:
            for t in self[Tags.Types]:
              rv.append(t)
              if self.OrgDB.HasType(t):
                  rv.extend(self.Org[t].GetTypes())
        return rv  # TODO: check for duplicates, shouldn't be if DB is logical

    def GenChildren(self):
       
        self.Children = []
        OrgDB = self.OrgDB
        for cf in self.ChildFields & self.keys():
            for c in self[cf]:
                if c not in OrgDB:
                    OrgDB[c] = NRRecord(c)
                self.Children.append(OrgDB[c])
                
    # if a sublcass of Record overloads GetChildren, it must also overload TravChildren,
    # passing the new GetChildren to Base.Record.TravChildren, ditto GetParents/TravParents
    def GetChildren(self):
        if not hasattr(self, "Children"):
            self.GenChildren()
        return self.Children.copy()
        
    def TravChildren(self):
        seen = {}
        return self._travc(seen)

    def _travc(self, Seen):

        rv = []
        Seen[self.UID]=1
        for c in self.GetChildren():
            if c.UID not in Seen:     # prevent cyclic recursion
                rv.append(c)
                rv.extend(c._travc(Seen))

        return rv

    def GenParents(self):
   
        self.Parents = []
        OrgDB = self.OrgDB
        
        for pf in self.ParentFields & self.keys():
            for p in self[pf]:
                if p not in OrgDB:
                    OrgDB[p] = NRRecord(p)
                self.Parents.append(OrgDB[p])
                

    def GetParents(self):
        if not hasattr(self,"Parents"):
            self.GenParents()
        return self.Parents[:]

    
    def RegisterParent(self, Parent):
        # allow other records to register themselves as a parent (esp. reactions)
        if not Parent in self.Parents:
            self.Parents.append(Parent)



        
    def TravParents(self):


        Seen = set()
        return self._travp(Seen)
        

    
    def _travp(self, Seen):

        rv = []
        Seen.add(self.UID)
        for p in self.GetParents():
            if not p.UID in Seen :     # prevent cyclic recursion
                rv.append(p)
                rv.extend(p._travp(Seen))
        return rv




    def MultiTrav(self, GoingUp=True, bounces=0):

        if GoingUp:
            rv = self.TravParents()
        else:
            rv = self.TravChildren()

        if bounces >0:
            rv2 = rv[:]
            for rec in rv2:
                if hasattr(rec,"MultiTrav"):
                    rv.extend(rec.MultiTrav(not GoingUp, bounces-1))
        return rv


    def Traverse(self, GetRels="GetParents", **kwargs):  # kwargs ignored at present
        """ pre: GetRels = ["GetParents" | "GetChildren"]
            post: returns a traversed tree in list form [Parent[Child]] in direction of GetRels"""

        rv = []
        rels = getattr(self, GetRels)()
        for r in rels:
           rv.append(r)
           try:
               more = r.Traverse(GetRels)
               if len(more) > 0:
                   rv.append(more)
           except:
               pass

        return rv

    def GetReactions(self):
        recs = self.TravChildren() # + self.TravParents()
        return [r for r in recs if r.RecordTag == Tags.Reac]
        
    def GetReacUIDs(self):
        return [rec.UID for rec in self.GetReactions()]

    def GetGenes(self):
        #raise AttributeError("GetReactions implemented by the db, not the record")
        return [r for r in self.TravParents() if r.RecordTag==Tags.Gene]




class NRRecord(Record):
    """ A record to indicate expected, but missing (Not Reported) data """

    RecordTag = Tags.NR

    def __init__(self, *args, **kwargs):

        Record.__init__(self, *args, **kwargs)

        self[Tags.Comment] =  Tags.NR
        self[Tags.Types] =  Tags.NR



class NullRecord(Record):
    """ Default return if we can't return anything else """


    RecordTag = Tags.Null

    def __init__(self, k):

        Record.__init__(self, Tags.Null)

        self[Tags.Comment] = k
        self[Tags.Comment]=  Tags.Null
        self[Tags.Types] = Tags.Null



#
