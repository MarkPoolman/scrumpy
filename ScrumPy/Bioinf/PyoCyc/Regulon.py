from . import RecordBase, DBBase, Tags
# TODO: Regulon UIDs are a subset of PROTEIN UIDs so the regulon db needs seperate consideration

class Record(RecordBase.Record):
    
    ChildFields = {Tags.Regulates}
    ParentFields = {Tags.Gene}
    RecordTag = "Regulon" #Tags.Pathway



class DB(DBBase.DB):

    FileName="regulons.dat"
    RecordClass = Record
    RecordTag = Record.RecordTag



#
