import os

from . import RecordBase, DBBase, Tags


class Record(RecordBase.Record):
    pass
    
    
    
       
    
class DB(DBBase.DB):
    
    FileName="species.dat"
    RecordClass = Record
    RecordTag = Tags.Info
    VerFile = "version.dat" # different format from all other .dat files, so treated ifferently
    
    def __init__(self, **kwargs):

        DBBase.DB.__init__(self, **kwargs)

        VersionRec = self[Tags.Version] =  Record(Tags.Version, self.OrgDB)
        if os.path.exists(DB.VerFile):
            VerLines = open(os.sep.join((self.Path, self.VerFile))).read().split("\n")
          
            for line in VerLines:
                if not line.startswith(";") and "\t" in line:
                     k,v = line.split("\t",1)
                     VersionRec[k] = v
        else:
            print("Version data not found")
        
            


    def __str__(self):

        rv = ""
        for k in self:
            rv += str(self[k])
        return rv
        
                      
