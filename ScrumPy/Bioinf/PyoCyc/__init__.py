from . import Organism as OrganismModule

Organism = OrganismModule.Organism

#FIXME - horrible and breaks reload()

#Organism = OrganismModule.Organism
#from  .AnnotatedOrganism import AnnotatedOrganism

from . import DBBase
import os

def Avail():

    rv = []

    for localpath in os.listdir(DBBase.DefaultPath):
        report   = localpath
        abspath  = DBBase.DefaultPath + localpath
        realpath = os.path.realpath(abspath)
        #print localpath,  os.path.islink(abspath)

        if os.path.isdir(realpath) and "genes.dat" in os.listdir(realpath):
            if os.path.islink(abspath):
                report += "".join((" (-> ", realpath.rsplit(os.sep)[-1], ")" ))

            rv.append(report)

    rv.sort()


    return rv



def PrintAvail():

    for a in Avail():
        print(a)
