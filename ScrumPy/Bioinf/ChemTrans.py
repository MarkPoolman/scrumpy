#
##
### ChemTrans - interface to the chemical translation service http://cts.fiehnlab.ucdavis.edu/
### See http://cts.fiehnlab.ucdavis.edu/services for details
##
#

import urllib.request, urllib.error, urllib.parse
import json
JsonDecode = json.JSONDecoder().decode

BaseURL = "http://cts.fiehnlab.ucdavis.edu/rest/convert/"

def CleanHTTP(http):
    return http.replace(" ", "%20")

def ReadURL(TransReq):

    url = BaseURL+CleanHTTP(TransReq)
    req = urllib.request.Request(url)
    resp = urllib.request.urlopen(req).read()   
    return JsonDecode(resp.decode()) # .decode() because req.read() -> binary
    



#FromValues = ReadURL("conversion/fromValues")
#ToValues = ReadURL("conversion/toValues")


def ConvertName(From,To,Name):

    Req = "/".join((From,To,Name))
    return ReadURL(Req)


