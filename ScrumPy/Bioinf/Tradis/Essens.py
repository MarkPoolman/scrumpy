#!/usr/bin/python3
import sys, math 
import scipy
from scipy import stats
from scipy.signal import savgol_filter


from ScrumPy.Data.DataSets import DataSet
from ScrumPy.Data import Histogram
from ScrumPy.Util.Seq import IdxOfMax, IdxOfMin
from ScrumPy.Util.Sci import SafeLog

from ScrumPy.UI.QtUIComponents import PlotClient





def SmooFunc(data, winlen=None, order=1): # instead of loess, other aproaches are available

    try:

        if winlen is None:
            winlen = int(2+ len(data)/5)

        return savgol_filter(data, winlen, order)
    except:
        print ("Warning - not enough data to smooth")
        return data



    

def LogRatios(PGGam, PGExp):

    PGGam = list(PGGam)
    PGExp = list(PGExp)

    _PGGam = [1.0 - p for p in PGGam]
    _PGExp = [1.0 - p for p in PGExp]
    
    idxs = list(range(len(PGGam)))
     
    Numer = [PGGam[i] * _PGExp[i] for i in idxs]
    Denom = [PGExp[i] * _PGGam[i] for i in idxs]
    
    return [SafeLog(Numer[i]/Denom[i],2) for i in idxs]




#
##
###  python using scipy/numpy where possible rather than r2py
###  line num in comments refers to line numbers in original r code
##
#

class Essens:

    Info = "Orig"
    
    def __init__(self,FName = None):

        self.HaveCalc = False

        if FName is not None:
            self.Calculate(FName)

    def Calculate(self, FName):
        """ Perform the TraDIS essentiality calculation
            but don't save or report any results. The method
            may be called repeatedly, but this will overwrite
            any previous data """

        self.ReadData(FName)
        self.CalculateBP()        # find the breakpoint
        self.CalculateDistribs()  # calculate the exponential (<BP) and gamma(>=BP) distributions
        self.CalculateThresholds()      # determine from which distribution IIs are most like to have been drawn
        self.CalculateGoodness()
        

    def ReadData(self, FName):
        
        self.FName = FName
        self.AllData = DataSet(FName)                           # line 20
        self.nGenes =  len(self.AllData)
        self.AllInsIdx = self.AllData.GetCol("ins_index" )      # line 21
        self.AllInsIdx.sort()  # for later cdf calculations


    def CalculateBP(self):
        
        self.HistoAll = Histogram.Histogram(self.AllInsIdx, nbins=200)   # line 24
        self.maxindex = 8+IdxOfMax(self.HistoAll.Dens[8:])                 # 25 the valley will be before this
        self.maxval   = self.HistoAll.Mids[self.maxindex ]               # 26
        
        
        self.LoVals = [i for i in self.AllInsIdx if i < self.maxval ]    # 32 (actual values rather than bool)
        nbins  = math.floor(self.maxval *2000)                           # 31
        self.HistoLower = Histogram.Histogram(self.LoVals, nbins=nbins)
        
        self.LowDens     = self.HistoLower.Dens         # Dens = probability density
        self.SmooLowDens = list(SmooFunc(self.LowDens)) # 34
        self.LowMids     = self.HistoLower.Mids         # midpoint values of histogram bins
        
        self.BreakPointIdx =  IdxOfMin(self.SmooLowDens)   # 37
        self.BreakPointVal =  self.LowMids[self.BreakPointIdx]
   
        self.InsIsLT_BP = [i for i in self.AllInsIdx if 0 <= i <self.BreakPointVal]   # 38 (actual values rather than bool)

    
    def CalculateDistribs(self):

        self.Histo   = Histogram.Histogram(self.AllInsIdx) # 45
        self.UniqueIIs = self.Histo.CDF_x
        self.ObsCDF  = self.Histo.CDF_y
        Counts = self.Histo.Counts
        
        CountGT5Idxs =  [i for i in range(len(Counts)) if Counts[i] >5]
        
        self.MaxMid = self.Histo.Mids[CountGT5Idxs[-1]]
        self.InsIsGTE_BP = [i for i in self.AllInsIdx if i >= self.BreakPointVal and i<self.MaxMid] # 46(actual values rather than bool)

        self.ExpParams = stats.fit(stats.expon, self.InsIsLT_BP,((-0.1,-0.0001),(0,10))).params    # 51
        self.GamParams = stats.fit(stats.gamma,self.InsIsGTE_BP,((0,20),(0,0), (0,1))).params # 52
        # NB fixes the location to be zero for exp and gamma as in original R version

        self.HypothObs = HypothObs = [n/500 for n in range(200)] #
        
        f1 = (len(self.InsIsLT_BP) + self.AllInsIdx.count(0)) / self.nGenes # 48 Incorrect, as in original
        f2 = len(self.InsIsGTE_BP)/self.nGenes                              # 49 left for now for consistency
        
        self.ExponPDF = [f1 * x for x in stats.expon.pdf(HypothObs,*self.ExpParams)]
        self.GammaPDF = [f2 * x for x in stats.gamma.pdf(HypothObs, *self.GamParams)]
        
        Indeps = [n/10000 for n in range(1000)]# 64/65
        self._GammaCDF = stats.gamma.cdf(Indeps, *self.GamParams)
        self._ExponSF  = stats.expon.sf(Indeps , *self.ExpParams)

        # not part of original algorithm, used later for goodness of fit calcs
        # and comparison with other algorithms
        self.ExponCDF  = stats.expon.cdf(self.UniqueIIs, *self.ExpParams)
        self.GammaCDF  = stats.gamma.cdf(self.UniqueIIs, *self.GamParams)
        self.RatParam = f1 = 1 - f2  # this is htecorrect calculation
        self.CalcCDF  = f1 * self.ExponCDF + (1-f1) * self.GammaCDF

        
    def CalculateThresholds(self):
        
        self.LogRats = LogRatios(self._GammaCDF, self._ExponSF)
        lower = max([self.LogRats.index(r) for r in self.LogRats if r < -2])
        upper = min([self.LogRats.index(r) for r in self.LogRats if r > 2]) # 64/65

        self.EssenInsIdx = lower/10000
        self.AmbigInsIdx = upper/10000
        
        
    def CalculateGoodness(self):

        self.Resids = self.ObsCDF - self.CalcCDF
        self.RelResids = self.Resids/self.ObsCDF
        self.HistoResids = Histogram.Histogram(self.Resids, self.Histo.nBins) 
        self.SSResids = sum(self.Resids **2)
        self.ResidsStats = stats.describe(self.Resids)
        
               
    def Plot(self, KeepOpen=False, Tail=1):

        self.PlotWin = w = PlotClient.PlotWinCli(WinTitle="ScrumPy - Tradis Essentiality " + self.Info)
        self.DistribPlotter = w.NewPlotter(Name="Insertion Indices " + self.FName)
        self.ResiCDFPlotter = w.NewPlotter(Row=1,Name="CDF Residuals")
        self.ResiPDFPlotter = w.NewPlotter(Row=2,Name="CDF Residual Distribution")
        #w.Command("resize", 750, 1000)
        w.SndRcv("resize", 750, 1000)

        cdf = self.Histo.CDF_y
        Tail = max([i for i in cdf if i <= Tail])
        TailIdx = cdf.index(Tail)
                           
        self.PlotIIDistribs(TailIdx)
        self.PlotGoodness(TailIdx)
        


    def PlotIIDistribs(self, TailIdx):
        
        p = self.DistribPlotter

        TailVal = self.Histo.CDF_x[TailIdx]
       
        Bounds = self.Histo.Bounds
        MaxBound = max([b for b in Bounds if b <=TailVal])
        MaxIdx = list(Bounds).index(MaxBound)
        Bounds = Bounds[:MaxIdx]
        Dens = self.Histo.Dens[:MaxIdx-1]

        maxy = max(Dens) *1.1
        # FIXME p.SetYRange(0, maxy) pyqtgraph.PlotItem setYRange 
        
        p.Histogram("Insertion Index Thresholds", Bounds,  Dens)
        
        LineOpt = {"width":2}
        p.LinePlot("Essen=%f"%self.EssenInsIdx, [self.EssenInsIdx, self.EssenInsIdx], [0, maxy], LineOpt)
        p.LinePlot("Ambig=%f"%self.AmbigInsIdx, [self.AmbigInsIdx, self.AmbigInsIdx], [0, maxy], LineOpt)

        MaxVal = max([x for x in self.HypothObs if x<TailVal])
        MaxIdx2 = self.HypothObs.index(MaxVal)
        p.LinePlot("Gamma", self.HypothObs[:MaxIdx2], self.GammaPDF[:MaxIdx2], LineOpt)#, width=10)
        p.LinePlot("Expon", self.HypothObs[:MaxIdx2], self.ExponPDF[:MaxIdx2], LineOpt)
        
       
    def PlotGoodness(self, TailIdx):

        p = self.ResiCDFPlotter

        x = self.UniqueIIs[:TailIdx]
        y = self.Resids[:TailIdx] #self.RelResids[:TailIdx]
        p.ScatterPlot("CDF Residuals", x, y)

        LineOpt = {"width":2}
        MinY = min(y)
        MaxY = max(y)
        p.LinePlot("Essen",   [self.EssenInsIdx, self.EssenInsIdx], [MinY,MaxY], LineOpt)
        p.LinePlot("Ambig",[self.AmbigInsIdx, self.AmbigInsIdx], [MinY,MaxY], LineOpt)
        
        p = self.ResiPDFPlotter
        Bounds = self.HistoResids.Bounds
        Dens = self.HistoResids.Dens
        p.Histogram("Distribution of CDF Residuals",Bounds, Dens)


    def ReportStr(self, ):

        nEssen = len([i for i in self.AllInsIdx if i <  self.EssenInsIdx])
        nAmbig = len([i for i in self.AllInsIdx if self.EssenInsIdx <= i < self.AmbigInsIdx])

        RItems = list( self.ResidsStats._asdict().items())
        RStrs = [" "]+[ i[0].ljust(8) + " : " + str(i[1]) for i in RItems]

        Lines = [
             ["Data        " , self.FName  ],
             ["Genes       " , self.nGenes ],
             ["Essential II" , self.EssenInsIdx ],
             ["n Essential " , nEssen      ],
             ["Ambig II    " , self.AmbigInsIdx ],
             ["n Ambig     " , nAmbig      ],
             ["Exp params  " , self.ExpParams ],
             ["Gam params  " , self.GamParams],
             ["SSq Resids  " , self.SSResids],
             ["Resids stats" , "\n\t".join(RStrs) ],
        ]
        Lines1 = [[line[0], str(line[1])] for line in Lines]
        Lines2 = [" : ".join(line) for line in Lines1]
        
        return "\n".join(Lines2)


    def Report(self, File=sys.stdout):

        if type (File) is str:
            File = open(File,"w")

        print(self.ReportStr())
            


    def GetAbsEssens(self):
        return self.AllData.Filter(lambda x: x==0, "ins_index")
        
    def GetEssens(self):
        return self.AllData.Filter(lambda x: x < self.EssensInsIdx, "ins_index")
    
    def GetAmbigs(self):
        return self.AllData.Filter(lambda x: self.EssensInsIdx <= x < self.AmbigInsIdx, "ins_index")

    def GetNonEssens(self):
        return self.AllData.Filter(lambda x:x >= self.AmbigInsIdx, "ins_index")

    def SaveFiles(self):

        name, extn = self.FName.rsplit(".",1)

        RFile = ".".join((name, "report", extn))
        self.Report(RFile)

        EFile =  ".".join((name, "essen", extn))
        self.GetEssens().WriteFile(EFile)

        AFile =  ".".join((name, "ambig", extn))
        self.GetAmbigs().WriteFile(AFile)
        
        
                            
if __name__ == "__main__":
    import sys
    te = Essens(FName = sys.argv[1])
    te.SaveFiles()
    te.Plot(KeepOpen=True)
  
  
    
    


      
    
      
    
