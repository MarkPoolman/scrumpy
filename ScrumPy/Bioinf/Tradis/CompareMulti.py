from . import Essens

from ScrumPy.UI.QtUIComponents import PlotClient

def IdxLCsfx(Strings):
    """  The index of the start of the longest common suffix in Strings """
    # TODO: move to ScrumPy.Util.String

    MaxLen = max( [len(s) for s in Strings] )
    S0 = Strings[0]
    idx = -1
    while idx > -MaxLen and all([ s[idx] == S0[idx] for s in Strings[1:]]):
        idx -= 1
    idx += 1
    return idx




#files = [ f for f in os.listdir() if f.endswith("insert_sites_all.txt")]
class MultiTradis:

    def __init__(self, FNames):

        FNames = sorted(FNames)

        self.PlotWin = PlotClient.MultiPlotWin("ScrumPy - MultiTradis", FNames)

        DiscardIdx = IdxLCsfx(FNames)

        self.Results = {}
        for FName in FNames:
            te = TradisEssen.Essens(FName)
            te.PlotWin = self.PlotWin
            Label = FName[:DiscardIdx]
            self.Results[Label] = te


    def Plot(self):

        for te in self.Results.values():
            te.Plot()
        self.PlotWin.Show()


    def IIPlot(self):

        w = self.IIWin = PlotClient.SquarePlotWin("InsIdxs", self.Results.keys())
        
        for row in w.Grid.keys():
            xdat = self.Results[row].AllInsIdx
            for col in w.Grid.keys():
                ydat =  self.Results[col].AllInsIdx
                w.Grid[row][col].ScatterPlot("II", xdat, ydat)
        w.Show()

        
