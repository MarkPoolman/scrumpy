#!/usr/bin/python3

import scipy
from scipy import stats

from ScrumPy.Data import Histogram
try:
    from . import Essens
except:
    import Essens
    
    
    


class Essens2(Essens.Essens):

    Info = "Essens2"


    def CalculateBP(self):
        self.BreakPointIdx = self.BreakPointVal = "N/A"


    def CalculateDistribs(self):

        MaxLoc = max(self.AllInsIdx) # Max location param of the both distribs
        MinLoc = -0.5 * MaxLoc

        MinParams =        [MinLoc,    0.0,    0.0,   MinLoc,     0.0,       0.0]
        MaxParams =        [0.0,       1.0,   10.0,   MaxLoc,  1.0,       1.0]
        def JointCDF(xdat, Exp_loc, Exp_scale, Gam_a, Gam_loc, Gam_scale, Propn):

            e = stats.expon.cdf(xdat, Exp_loc, Exp_scale)
            g = stats.gamma.cdf(xdat,  Gam_a, Gam_loc, Gam_scale)
            return Propn * e + (1-Propn)* g

        def JointPDF(xdat, Exp_loc, Exp_scale, Gam_a, Gam_loc, Gam_scale, Propn):

            e = stats.expon.pdf(xdat, Exp_loc, Exp_scale)
            g = stats.gamma.pdf(xdat,  Gam_a, Gam_loc, Gam_scale)
            return Propn * e + (1-Propn)* g
        
        self.Histo = Histogram.Histogram(self.AllInsIdx)
        Obs_x = self.UniqueIIs = self.Histo.CDF_x
        Obs_y = self.ObsCDF =  self.Histo.CDF_y
        PDF_x = self.Histo.Mids
        
        Bounds = (MinParams, MaxParams)
        self.FitRes = scipy.optimize.curve_fit(JointCDF, Obs_x, Obs_y, bounds=Bounds, full_output=True)
        
        FitParams = self.FitRes[0]
        self.CalcCDF = JointCDF(Obs_x, *FitParams)
        self.CalcPDF = JointPDF(PDF_x, *FitParams)

        ep = self.ExpParams = FitParams[:2]
        gp = self.GamParams = FitParams[2:5]
        rp = self.RatParam  = FitParams[5]

        self.ExponPDF = stats.expon.pdf(PDF_x, *ep)  * rp 
        self.GammaPDF = stats.gamma.pdf(PDF_x, *gp)  * (1-rp)

        self.ExponSF  = stats.expon.sf(Obs_x, *ep) * rp
        self.ExponCDF = stats.expon.cdf(Obs_x,*ep) * rp
        self.GammaCDF = stats.gamma.cdf(Obs_x,*gp) * (1-rp)


    def CalculateThresholds(self):
        
        Ratio  = self.RatParam

        self.LogRats = Essens.LogRatios(self.GammaCDF, self.ExponSF)

        try:    # protect against weird distributions
            EssenThresh = max([r for r in self.LogRats if r < -2])
            self.EssenInsIdx = self.AllInsIdx[self.LogRats.index(EssenThresh)]
        except:
            self.EssenInsIdx = self.AllInsIdx[0]
            
        try:
            AmbigThresh = min([r for r in self.LogRats if r > 2]) # 64/65
            self.AmbigInsIdx = self.AllInsIdx[self.LogRats.index(AmbigThresh)]
        except:
            self.AmbigInsIdx = self.AllInsIdx[-1]
    
    
    def PlotIIDistribs(self, TailIdx=1):

        p = self.DistribPlotter

        TailVal = self.Histo.CDF_x[TailIdx]
       
        Bounds = self.Histo.Bounds
        MaxBound = max([b for b in Bounds if b <=TailVal])
        MaxIdx = list(Bounds).index(MaxBound)
        Bounds = Bounds[:MaxIdx]
        Dens = self.Histo.Dens[:MaxIdx-1]
        Mids = self.Histo.Mids[:MaxIdx-1]
        maxy = max(Dens) *1.1
        #p.SetYRange(0, maxy)
        
        p.Histogram("Insertion Index Thresholds", Bounds,  Dens)

        LineOpt = {"width":2}
        
        maxy = max(Dens) *1.1
        essen_ii = self.EssenInsIdx
        ambig_ii = self.AmbigInsIdx
        p.LinePlot("Essen=%f"%essen_ii, [essen_ii, essen_ii], [0, maxy], LineOpt)
        p.LinePlot("Ambig=%f"%ambig_ii, [ambig_ii, ambig_ii], [0, maxy], LineOpt)

        p.LinePlot("Gamma", Mids, self.GammaPDF[:MaxIdx-1], LineOpt)#, width=10)
        p.LinePlot("Expon", Mids, self.ExponPDF[:MaxIdx-1], LineOpt)



if __name__ == "__main__":
    import sys
    te = Essens(FName = sys.argv[1])
    te.SaveFiles()
    te.Plot(KeepOpen=True)
  
  
    
    


      
    
      
    
