from scipy import stats

from ScrumPy.Data import DataSets, Histogram
from ScrumPy.Util import Sci


def MeanIIs(datasets):

    rv = datasets[0].ColAsNP("ins_index")
    for d in datasets[1:]:
        rv += d.ColAsNP("ins_index")
    return rv/len(datasets)


class Compare2:

    def __init__(self, Ctrls, Treats):

        PlotCaption = " vs ".join((Ctrls[0], Treats[0]))
        
        Ctrls  = self.Ctrls  = [DataSets.DataSet(c) for c in Ctrls]
        Treats = self.Treats = [DataSets.DataSet(t) for t in Treats]

        self.IICtrl = MeanIIs(Ctrls)
        self.IITreat = MeanIIs(Treats)
        
        self.Diffs = self.IITreat - self.IICtrl
        self.Histo = Histogram.Histogram(self.Diffs)
        self.Histo.SetupPlotter("ScrumPy - Compare Tradis", Caption=PlotCaption)
        self.Histo.Plot()
        self.Plotter = self.Histo.Plotter

    

        self.ObsCDF = self.Histo.CDF_y[:]
        self.ObsSF  = self.Histo.SF_y[:]

        self.LocusTags = Ctrls[0].GetCol(0)

        self.InitResults()
        self.Diffs.sort()
        self.FitDistrib()
        self.CalcTwoTail()

        
    def InitResults(self):

        self.Results = Results = DataSets.DataSet()
        
        Results.NewCol(self.LocusTags, "Locus Tag")
        Results.NewCol(self.Diffs, "Diffs" )
        Results.SortBy("Diffs")
        Results.SetPlotX("Diffs")
        
        Results.NewCol(self.ObsCDF,"ObsCDF")
        Results.NewCol(self.ObsSF, "ObsSF" )

        

    def FitDistrib(self, distrib=stats.laplace_asymmetric):

        self.Distrib=distrib
        p = self.FitParams = distrib.fit(self.Diffs)
        
        self.FittedPDF = distrib.pdf(self.Histo.Mids, *p)
        self.FittedCDF = distrib.cdf(self.Diffs, *p)
        self.FittedSF  = distrib.sf(self.Diffs, *p)
        
        self.Results.NewCol(self.FittedCDF,"FitCDF")
        self.Results.NewCol(self.FittedSF, "FitSF" )
        

    def CalcTwoTail(self):

        mid = len(self.Diffs)//2
        obs = self.ObsCDF[:mid] + self.ObsSF[mid:]
        exp = list(self.FittedCDF[:mid]) + list(self.FittedSF[mid:])
        rats = [e/o for e,o in zip(exp,obs)]

        self.Results.NewCol(obs, "ObsTwoTail")
        self.Results.NewCol(exp, "CalcTwoTail")
        self.Results.NewCol(rats,"TwoTailRats")
        

    def PlotPDF(self):

        self.Plotter.RemoveAll()

        self.Histo.Plot()
        self.Plotter.LinePointPlot("Fit - PDF", self.Histo.Mids, list(self.FittedPDF))


    def PlotCDF(self):

        self.Plotter.RemoveAll()

        self.Histo.PlotCDF()
        self.Plotter.LinePlot("Fit - CDF", self.Diffs, list(self.FittedCDF),{"width":2})


    def PlotSF(self):

        self.Plotter.RemoveAll()

        self.Histo.PlotSF()
        self.Plotter.LinePlot("Fit - SF", self.Diffs, list(self.FittedSF),{"width":2})


    def PlotSFCDF(self):

        self.Plotter.RemoveAll()

        mid = len(self.Diffs)//2
        obs = self.ObsCDF[:mid] + self.ObsSF[mid:]
        exp = list(self.FittedCDF[:mid]) + list(self.FittedSF[mid:])
        rats = [e/o for e,o in zip(exp,obs)]
        self.Plotter.ScatterPlot("Obs", self.Diffs, obs)
        self.Plotter.LinePlot("Exp", self.Diffs, exp,{"width":2})
        self.Plotter.ScatterPlot("Rats", self.Diffs, rats)
        
        

    


             

        

        
        
        
