"""
Implementation of the tradis_essentiality algorithm as (almost) a transliteration
of the origingal R source code usiing the rpy2 library.
"""


import sys, math
import scipy

try:
    from  rpy2.robjects import r, FloatVector, Formula
    r.library("MASS")
except:
    print("!!\n!! rpy2 missing or incomplete (install it separately) EssensR not available", file=sys.stderr)
    print("!! Other modules in the Tradis package not affected\n!!", file=sys.stderr)

from ScrumPy.UI.QtUIComponents import PlotClient
from ScrumPy.Data.DataSets import DataSet
from ScrumPy.Data import Histogram
          

def Rdollar(Obj, name):
    """ implents the r $ operator """
    if hasattr(Obj,"names"):#    return table[table.colnames.index(colname)]
        return Obj[Obj.names.index(name)]
    else:
        return Obj[Obj.colnames.index(name)]



def IdxOfMax(seq):
    try:
        return seq.index(max(seq))
    except:
        return seq.tolist().index(max(seq))

def IdxOfMin(seq):
    return seq.index(min(seq))


from scipy.signal import savgol_filter
def SmooFunc(data, winlen=None, order=1): # instead of loess, other aproaches are available

    if winlen is None:
        winlen = int(2+ len(data)/5)

    return savgol_filter(data, winlen, order)


from scipy.stats import iqr
def FreeDiac(data,nBins=True):  # Equivalent(ish) of r's FD directive for histograms
    width = 2*iqr(data)/len(data)**(1/3)
    if nBins:
        return int ( (max(data) - min(data))/width )
    return width


def FitDist(data, dist, ParamsOnly=True): # use r to fit distribution - scipy equiv saved as exercise for the student

    data = FloatVector(data)
    rv = r.fitdistr(data,dist)
    if ParamsOnly:
        rv =  Rdollar(rv,"estimate")
        
    return rv

    
def GammaDens(obs, shape,rate, **kwargs):
    return list(r.dgamma(FloatVector(obs), shape, rate, **kwargs))


def GammaCum(obs, shape,rate,**kwargs):
    return list(r.pgamma(FloatVector(obs), shape, rate, **kwargs))


def SafeLog(x, base):

    if x <=0.0:
        return -32
    return math.log(x,base)

    

def LogRatios(PGGam, PGExp):

    PGGam = list(PGGam)
    PGExp = list(PGExp)

    _PGGam = [1.0 - p for p in PGGam]
    _PGExp = [1.0 - p for p in PGExp]
    
    idxs = list(range(len(PGGam)))
     
    Numer = [PGGam[i] * _PGExp[i] for i in idxs]
    Denom = [PGExp[i] * _PGGam[i] for i in idxs]
    
    return [SafeLog(Numer[i]/Denom[i],2) for i in idxs]




#
##
###  python using scipy/numpy where possible rather than r2py
###  line num in comments refers to line numbers in original r code
##
#

class Essens:
    def __init__(self,FName = None):

        self.HaveCalc = False

        if FName is not None:
            self.Calculate(FName)


    def Calculate(self, FName):

        self.ReadData(FName)
        self.CalculateBP()
        self.CalculateDistribs()
        self.CalculateLoHi()
        self.HavCalc = True


    def ReadData(self, FName):
        
        self.FName = FName
        self.AllData = DataSet(FName)                           # line 20
        self.nGenes =  len(self.AllData)
        self.AllInsIdx = self.AllData.GetCol("ins_index" )               # line 21


    def CalculateBP(self):
        
        self.HistoAll = Histogram.Histogram(self.AllInsIdx, nbins=115)   # line 24
        self.maxindex = 8+IdxOfMax(self.HistoAll.Dens[8:])                 # 25 the valley will be before this
        self.maxval   = self.HistoAll.Mids[self.maxindex ]               # 26
        
        
        self.LoVals = [i for i in self.AllInsIdx if i < self.maxval ]    # 32 (actual values rather than bool)
        nbins  = math.floor(self.maxval *2000)                               # 31
        self.HistoLower = Histogram.Histogram(self.LoVals, nbins=nbins)
        
        self.LowDens     = self.HistoLower.Dens
        try:
            self.SmooLowDens = list(SmooFunc(self.LowDens)) # 34
        except:
            print(self.FName, "Smooth fails in CalculateBP")
            self.SmooLowDens = list(self.LowDens)
            
        self.LowMids     = self.HistoLower.Mids
        
        self.BreakPointIdx =  IdxOfMin(self.SmooLowDens)   # 37
        self.BreakPointVal =  self.LowMids[self.BreakPointIdx]
   
        self.InsIsLT_BP = [i for i in self.AllInsIdx if 0 <= i <self.BreakPointVal]   # 38 (actual values rather than bool)

    
    def CalculateDistribs(self):

        self.HistoAll2 = Histogram.Histogram(self.AllInsIdx, nbins=FreeDiac(self.AllInsIdx)) # 45
        Counts = self.HistoAll2.Counts
        
        CountGT5Idxs =  [i for i in range(len(Counts)) if Counts[i] >5]
        
        self.MaxMid = self.HistoAll2.Mids[CountGT5Idxs[-1]]
        self.InsIsGTE_BP = [i for i in self.AllInsIdx if i >= self.BreakPointVal and i<self.MaxMid] # 46(actual values rather than bool)

        self.ExpEst   = FitDist(self.InsIsLT_BP,  "exponential")                      # 51
        self.GammaEst = FitDist(self.InsIsGTE_BP, "gamma")                            # 52

        self.HypothObs = HypothObs = [n/500 for n in range(200)] #
        
        f1 = (len(self.InsIsLT_BP) + self.AllInsIdx.count(0)) / self.nGenes   # 48
        self.ExponProbs = [f1 * x for x in GammaDens(HypothObs, 1, *self.ExpEst)]

        f2 = len(self.InsIsGTE_BP)/self.nGenes                              # 49
        self.GammaProbs = [f2 * x for x in GammaDens(HypothObs, *self.GammaEst)]
        
        Indeps = [n/10000 for n in range(1000)]# 64/65
        self.PGGam = GammaCum(Indeps, *self.GammaEst)
        self.PGExp = GammaCum(Indeps, 1, *self.ExpEst, lower_tail=False)


    def CalculateLoHi(self):
        
        self.LogRats = LogRatios(self.PGGam, self.PGExp)
        self.lower = max([self.LogRats.index(r) for r in self.LogRats if r < -2])
        self.upper = min([self.LogRats.index(r) for r in self.LogRats if r > 2]) # 64/65

        self.essen = self.lower/10000
        self.ambig = self.upper/10000
        
   

    def Plot(self, KeepOpen=False):

        if not hasattr(self, "PlotWin"):
            title =  "Tradis Essentiality"
            self.PlotWin = PlotClient.PlotWin(Title = "Tradis Essentiality", KeepOpen=KeepOpen)
            self.PlotWin.NewPlotter(0,0, Name = self.FName )
            
        w = self.PlotWin
        p = w.Plotters[self.FName]
        

        LineOpt = {"width":2}

        p.Histogram("InsIdxs", self.HistoAll2.Bounds, self.HistoAll2.Dens)
        p.LinePlot("Gamma", self.HypothObs, self.GammaProbs,LineOpt)#, width=10)
        p.LinePlot("Expon", self.HypothObs, self.ExponProbs,LineOpt)
        
        maxy = max(self.HistoAll2.Dens) *1.1
        p.LinePlot("Essen=%f"%self.essen, [self.essen, self.essen], [0, maxy],LineOpt)
        p.LinePlot("Ambig=%f"%self.ambig, [self.ambig, self.ambig], [0, maxy],LineOpt)

        w.Show()


    def Report(self, File=sys.stdout):

        if type (File) is str:
            File = open(File,"w")
            
        nEssen = len([i for i in self.AllInsIdx if i <  self.essen])
        nAmbig = len([i for i in self.AllInsIdx if self.essen <= i < self.ambig])

        Lines = [
             ["Data        ", self.FName  ],
             ["Genes       ", self.nGenes ],
             ["Essential II" , self.essen ],
             ["n Essential ", nEssen      ],
             ["Ambig II    ", self.ambig  ],
             ["n Ambig     ", nAmbig      ]  
        ]

        for line in Lines:
            strl = [str(i) for i in line]
            print (" : ".join(strl), file=File)
             



    def GetEssens(self):
        return self.AllData.Filter(lambda x: x < self.essen, "ins_index")
    
    def GetAmbigs(self):
        return self.AllData.Filter(lambda x: self.essen <= x < self.ambig, "ins_index")

    def GetNonEssens(self):
        return self.AllData.Filter(lambda x:x >= self.ambig, "ins_index")

    def SaveFiles(self):

        name, extn = self.FName.rsplit(".",1)

        RFile = ".".join((name, "report", extn))
        self.Report(RFile)

        EFile =  ".".join((name, "essen", extn))
        self.GetEssens().WriteFile(EFile)

        AFile =  ".".join((name, "ambig", extn))
        self.GetAmbigs().WriteFile(AFile)
        
        
                               
        
    
if __name__ == "__main__":
    te = Essens(FName = sys.argv[1])
    te.SaveFiles()
    te.Plot(KeepOpen=True)
  
  
    
    


      
    
      
    
