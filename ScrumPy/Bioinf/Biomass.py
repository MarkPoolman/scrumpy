
from ScrumPy.Util import Set

from . import MolWts

#reload(MolWts)


class Composition(dict):

    def __init__(self, ammount, components={}):

        self.ammount = float(ammount)
        self.update(components)


    def __float__(self):

        return self.ammount


    def SetAmmount(self,  ammount, Comp=None):
        """pre:  float(ammount) """

        ammount = float(ammount)

        if Comp==None:
            self.ammount = ammount

        elif Comp in self:
            if self.IsLeaf(Comp):
                self[Comp] = ammount
            else:
                self[Comp].ammount = ammount
        else:
            print("! Can't find component",  Comp, " !")


    def Copy(self):

        components = {}
        for k in list(self.keys()):
            if hasattr(self[k], "Copy"):
                components[k] = self[k].Copy()
            else:
                components[k] = self[k]

        return Composition(self.ammount, components)



    def AmmountOf(self, i):

        rv = 0.0

        if i in self:
            rv =  float(self) * float(self[i])

        else:
            for k in self:
                rec = self[k]
                if hasattr(rec, "AmmountOf"):
                    rv += float(self) * rec.AmmountOf(i)

        return rv


    def MolsOf(self,i,StrFilt=lambda s:s):
        """ StrFilt function to remove compartmental suffixes etc from i """

        return self.AmmountOf(i) / MolWts.MolWt(StrFilt(i))


    def GetLeaves(self):

        rv = []

        for k in self:
            if hasattr(self[k], "GetLeaves"):
                rv.extend(self[k].GetLeaves())
            else:
                rv.append(k)
        return rv


    def IsLeaf(self, Comp):

        return Comp in self.GetLeaves()









def WhatExports(m, compound):

    Externs = m.md.GetExtMetNames()

    for reac in list(m.sm.InvolvedWith(compound).keys()):
        if "_tx" in reac and len(Set.Intersect(Externs, list(m.smx.InvolvedWith(reac).keys()))) != 0:
            return reac
