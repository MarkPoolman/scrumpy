#
##
### install_conf - Identify platform speicific featured for install.py
##
#


import os, sys

def GetDistID():    

    vers = sys.version.split()[0]
    
    if sys.version_info.minor > 7:
        import distro
        Dist,  Ver =  distro.id(), distro.version()
    else:
        import platform
        Dist,  Ver,  Name = platform.dist()
        
    Ver = Ver.split(".")[0]
    return Dist+Ver
    

DistName = GetDistID().title()

print("Distribution ", DistName)

try:
###   To install on other OSs update this dictionary
###   to map DistName (above) to the appropriate local module.
###   You must of course ensure the module exists!
    DistConfModule = {
        "Ubuntu20" : "Ubuntu20",
        "Ubuntu22" : "Ubuntu22",
        "Ubuntu24" : "Ubuntu24",
        "Debian GNU/Linux11": "debian11",
        "Debian11" : "debian11",
    	"Debian12" : "debian12",
    	"Fedora38" : "Fedora38",
        
    }[DistName]
except:
    "Cant't find a configuration for %s, giving up!" %  DistName
    sys.exit()
###
###   Do not modify below here !
###
    

print("importing", DistConfModule)

DistConf = None # dummy to prevent Eric reporting Undefined name warnings for references to DistConf - see following line
exec("import %s as DistConf" % DistConfModule)
        

# install managed packages
def InstallPackages():

    fails=[]
    for p in DistConf.Packages:
       if os.system(DistConf.InstallCommand+p) !=0:
           fails.append(p)

    return fails
        
        
# TODO
# how to install a bundled python library with distutils
PyInst = DistConf.PyCom+" setup.py install "

# install packages from pip
PipInst =  "pip3 install --upgrade " # move to do distribution specific modules?

def InstallPip():
    fails = []
    if "PipPkgs" in dir(DistConf):
        for p in DistConf.PipPkgs:
            if os.system(PipInst+p) !=0:
               fails.append(p)
               
    return fails
            
    



